#include <complex>
#include <limits>
#include <iostream>
#include <omp.h>

#include "init.h"
#include "kernels.h"

// computes max|A[i]-B[i]| / max|B[i]|
float get_error(
    const int size,
    const std::complex<float>* A,
    const std::complex<float>* B)
{
    float max_abs_error = 0.0f;
    float max_ref_val = 0.0f;
    float max_val = 0.0f;
    for (int i=0; i<size; i++) {
        float abs_error = abs(A[i] - B[i]);
        if ( abs_error > max_abs_error ) {
            max_abs_error = abs_error;
        }
        if (abs(B[i]) > max_ref_val) {
            max_ref_val = abs(B[i]);
        }
        if (abs(A[i]) > max_val) {
            max_val = abs(A[i]);
        }
    }
    if (max_ref_val == 0.0f) {
        if (max_val == 0.0f) {
            // both inputs are zero
            return 0.0f;
        } else {
            // refrence input is zero, but candidate value is not
            return std::numeric_limits<float>::infinity();
        }
    } else {
        return max_abs_error / max_ref_val;
    }
}


int main(int argc, char **argv) {
    unsigned int station_pairs[NR_BASELINES][2];
    int aterm_offsets[NR_ATERMS];
    std::complex<float> aterms[NR_ANTENNAS*NR_ATERMS*SUBGRID_SIZE*SUBGRID_SIZE][NR_CORRELATIONS];
    float uvw[NR_BASELINES][NR_TIMESTEPS][3];
    float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS];
    std::complex<float> average_beam[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];

    std::complex<float>* aterms_ptr = (std::complex<float> *) aterms;

    init_station_pairs(station_pairs);
    init_aterm_offsets(aterm_offsets);
    init_aterms(aterms_ptr);
    init_uvw(uvw);
    init_weights(weights);
    reset_average_beam(average_beam);

    double runtime;

    // Kernel reference
#if 1
    int size_average_beam = SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS;
    runtime = -omp_get_wtime();
    kernel_ref(station_pairs, aterm_offsets, aterms, uvw, weights, average_beam);
    runtime += omp_get_wtime();
    std::clog << "kernel_00: " << runtime << " s" << std::endl;
#endif

    // Kernel 01: move computation of sum_of_weights outside
#if 0
    runtime = -omp_get_wtime();
    std::complex<float> average_beam_01[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_01);
    kernel_01(station_pairs, aterm_offsets, aterms, uvw, weights, average_beam_01);
    runtime += omp_get_wtime();
    std::clog << "kernel_01: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_01);
    std::clog << std::endl;
#endif

    // Kernel 02: add dimensions to average_beam
#if 1
    std::complex<float> average_beam_02[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_02);
    runtime = -omp_get_wtime();
    kernel_02(station_pairs, aterm_offsets, aterms, uvw, weights, average_beam_02);
    runtime += omp_get_wtime();
    std::clog << "kernel_02: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_02);
    std::clog << std::endl;
#endif

    // Kernel 03: add dimensions to aterm
#if 1
    std::complex<float> average_beam_03[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_03);
    runtime = -omp_get_wtime();
    kernel_03(station_pairs, aterm_offsets, aterms_ptr, uvw, weights, average_beam_03);
    runtime += omp_get_wtime();
    std::clog << "kernel_03: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_03);
    std::clog << std::endl;
#endif

    // Kernel 04: transpose kp
#if 1
    std::complex<float> average_beam_04[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_04);
    runtime = -omp_get_wtime();
    kernel_04(station_pairs, aterm_offsets, aterms_ptr, uvw, weights, average_beam_04);
    runtime += omp_get_wtime();
    std::clog << "kernel_04: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_04);
    std::clog << std::endl;
#endif

    // Kernel 05: add loop in update
#if 1
    std::complex<float> average_beam_05[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_05);
    runtime = -omp_get_wtime();
    kernel_05(station_pairs, aterm_offsets, aterms_ptr, uvw, weights, average_beam_05);
    runtime += omp_get_wtime();
    std::clog << "kernel_05: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_05);
    std::clog << std::endl;
#endif

    // Kernel 06: split real/imag in update
#if 1
    std::complex<float> average_beam_06[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_06);
    runtime = -omp_get_wtime();
    kernel_06(station_pairs, aterm_offsets, aterms_ptr, uvw, weights, average_beam_06);
    runtime += omp_get_wtime();
    std::clog << "kernel_06: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_06);
    std::clog << std::endl;
#endif

    // Kernel 07: more explicit vectorization
#if 1
    std::complex<float> average_beam_07[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_07);
    runtime = -omp_get_wtime();
    kernel_07(station_pairs, aterm_offsets, aterms_ptr, uvw, weights, average_beam_07);
    runtime += omp_get_wtime();
    std::clog << "kernel_07: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_07);
    std::clog << std::endl;
#endif

#if 1
    // Kernel 08: based on the latest code in idg-api (2020-10)
    std::complex<float> average_beam_08[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS];
    reset_average_beam(average_beam_08);
    runtime = -omp_get_wtime();
    kernel_08((const unsigned int*) station_pairs, aterm_offsets, aterms_ptr, (const float *) uvw, (const float*) weights, average_beam_08);
    runtime += omp_get_wtime();
    std::clog << "kernel_08: " << runtime << " s, ";
    std::clog << "error: " << get_error(size_average_beam, average_beam, average_beam_08);
    std::clog << std::endl;
#endif

}
