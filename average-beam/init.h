#include <complex>
#include <cstring>

#include <omp.h>

#include "defines.h"

typedef std::complex<float> ATerms[NR_ATERMS][NR_ANTENNAS][SUBGRID_SIZE][SUBGRID_SIZE][NR_CORRELATIONS];

void init_station_pairs(
    unsigned int station_pairs[NR_BASELINES][2]);

void init_aterm_offsets(
    int aterm_offsets[NR_ATERMS]);

void init_aterms(
    std::complex<float> *aterms);

void init_uvw(
    float uvw[NR_BASELINES][NR_TIMESTEPS][3]);

void init_weights(
        float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS]);

void reset_average_beam(
    std::complex<float> average_beam[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS]);
