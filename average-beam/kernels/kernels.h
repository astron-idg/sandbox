#include <complex>

#include "defines.h"

/*
 * kernel_01
 */
void kernel_ref(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> aterms[NR_ANTENNAS*NR_ATERMS*SUBGRID_SIZE*SUBGRID_SIZE][NR_CORRELATIONS],
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> average_beam[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS]);

/*
 * kernel_01
 */
void kernel_01(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> aterms[NR_ANTENNAS*NR_ATERMS*SUBGRID_SIZE*SUBGRID_SIZE][NR_CORRELATIONS],
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> average_beam[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS]);

/*
 * kernel_02
 */
typedef std::complex<float> AverageBeam[SUBGRID_SIZE*SUBGRID_SIZE][NR_CORRELATIONS][NR_CORRELATIONS];

void kernel_02(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> aterms[NR_ANTENNAS*NR_ATERMS*SUBGRID_SIZE*SUBGRID_SIZE][NR_CORRELATIONS],
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> *average_beam);

/*
 * kernel_03
 */
typedef std::complex<float> ATerms[NR_ATERMS][NR_ANTENNAS][SUBGRID_SIZE][SUBGRID_SIZE][NR_CORRELATIONS];

void kernel_03(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> *average_beam);

/*
 * kernel_04
 */
void kernel_04(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> *average_beam);

/*
 * kernel_05
 */
typedef float Weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS];

void kernel_05(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float>* average_beam_ptr);

/*
 * kernel_06
 */
void kernel_06(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float>* average_beam_ptr);

/*
 * kernel_07
 */
void kernel_07(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float>* average_beam_ptr);

/*
 * kernel_08
 */
void kernel_08(
    const unsigned int* station_pairs_ptr,
    const int* aterm_offsets_ptr,
    const std::complex<float>* aterms_ptr,
    const float* uvw_ptr,
    const float* weights_ptr,
    std::complex<float>* average_beam_ptr);
