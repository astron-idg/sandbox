#include <cstring>

#include "kernels.h"

void kernel_06(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms_ptr,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> *average_beam_ptr)
{
    ATerms *aterms = (ATerms *) aterms_ptr;
    AverageBeam *average_beam = (AverageBeam *) average_beam_ptr;

    float sum_of_weights[NR_BASELINES][NR_ATERMS][NR_CORRELATIONS];
    memset(sum_of_weights, 0, NR_BASELINES * NR_ATERMS * NR_CORRELATIONS * sizeof(float));

    #pragma omp parallel for
    for (int n = 0; n < NR_ATERMS - 1; n++) {
        int time_start = aterm_offsets[n];
        int time_end = aterm_offsets[n+1];

        // loop over baselines
        for (int bl = 0; bl < NR_BASELINES; bl++)
        {
            unsigned int antenna1 = station_pairs[bl][0];
            unsigned int antenna2 = station_pairs[bl][1];

            for (int t = time_start; t < time_end; t++)
            {
                if (std::isinf(uvw[bl][t][0])) continue;

                for (int ch = 0; ch < NR_CHANNELS; ch++)
                {
                    for (int pol = 0; pol < NR_CORRELATIONS; pol++)
                    {
                        sum_of_weights[bl][n][pol] += weights[bl][t][ch][pol];
                    }
                }
            }
        }
    }

    #pragma omp parallel for
    for (int i = 0; i < (SUBGRID_SIZE * SUBGRID_SIZE); i++)
    {
        std::complex<double> sum[NR_CORRELATIONS][NR_CORRELATIONS];

        for (int n = 0; n < NR_ATERMS - 1; n++)
        {
            // loop over baselines
            for (int bl = 0; bl < NR_BASELINES; bl++)
            {
                unsigned int antenna1 = station_pairs[bl][0];
                unsigned int antenna2 = station_pairs[bl][1];

                std::complex<float> aXX1 = (*aterms)[n][antenna1][0][i][0];
                std::complex<float> aXY1 = (*aterms)[n][antenna1][0][i][1];
                std::complex<float> aYX1 = (*aterms)[n][antenna1][0][i][2];
                std::complex<float> aYY1 = (*aterms)[n][antenna1][0][i][3];

                std::complex<float> aXX2 = std::conj((*aterms)[n][antenna2][0][i][0]);
                std::complex<float> aXY2 = std::conj((*aterms)[n][antenna2][0][i][1]);
                std::complex<float> aYX2 = std::conj((*aterms)[n][antenna2][0][i][2]);
                std::complex<float> aYY2 = std::conj((*aterms)[n][antenna2][0][i][3]);

                std::complex<float> kp[16] = {};
                kp[0 +  0] = aXX2*aXX1;
                kp[0 +  4] = aXX2*aXY1;
                kp[0 +  8] = aXY2*aXX1;
                kp[0 + 12] = aXY2*aXY1;

                kp[1 +  0] = aXX2*aYX1;
                kp[1 +  4] = aXX2*aYY1;
                kp[1 +  8] = aXY2*aYX1;
                kp[1 + 12] = aXY2*aYY1;

                kp[2 +  0] = aYX2*aXX1;
                kp[2 +  4] = aYX2*aXY1;
                kp[2 +  8] = aYY2*aXX1;
                kp[2 + 12] = aYY2*aXY1;

                kp[3 +  0] = aYX2*aYX1;
                kp[3 +  4] = aYX2*aYY1;
                kp[3 +  8] = aYY2*aYX1;
                kp[3 + 12] = aYY2*aYY1;

                // add kronecker product to average beam
                for (int ii = 0; ii < NR_CORRELATIONS; ii++)
                {
                    for (int jj = 0; jj < NR_CORRELATIONS; jj++)
                    {
                        float *weights = &sum_of_weights[bl][n][0];
                        float update_real = 0;
                        float update_imag = 0;
                        for (int p = 0; p < NR_CORRELATIONS; p++) {
                            float kp1_real =  kp[4*ii+p].real();
                            float kp1_imag = -kp[4*ii+p].imag();
                            float kp2_real =  kp[4*jj+p].real();
                            float kp2_imag =  kp[4*jj+p].imag();
                            update_real += weights[p] * (kp1_real * kp2_real - kp1_imag * kp2_imag);
                            update_imag += weights[p] * (kp1_real * kp2_imag + kp1_imag * kp2_real);
                        }
                        sum[ii][jj] += std::complex<float>(update_real, update_imag);
                    }
                }
            }
        }

        for(size_t ii = 0; ii < 4; ii++)
        {
            for(size_t jj = 0; jj < 4; jj++)
            {
                (*average_beam)[i][ii][jj] = sum[ii][jj];
            }
        }
    }
}
