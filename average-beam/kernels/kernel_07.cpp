#include <cstring>
#include <immintrin.h>

#include "kernels.h"

void kernel_07(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms_ptr,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> *average_beam_ptr)
{
    ATerms *aterms = (ATerms *) aterms_ptr;
    AverageBeam *average_beam = (AverageBeam *) average_beam_ptr;

    float sum_of_weights[NR_BASELINES][NR_ATERMS][NR_CORRELATIONS];
    memset(sum_of_weights, 0, NR_BASELINES * NR_ATERMS * NR_CORRELATIONS * sizeof(float));

    #pragma omp parallel for
    for (int n = 0; n < NR_ATERMS - 1; n++) {
        int time_start = aterm_offsets[n];
        int time_end = aterm_offsets[n+1];

        // loop over baselines
        for (int bl = 0; bl < NR_BASELINES; bl++)
        {
            unsigned int antenna1 = station_pairs[bl][0];
            unsigned int antenna2 = station_pairs[bl][1];

            for (int t = time_start; t < time_end; t++)
            {
                if (std::isinf(uvw[bl][t][0])) continue;

                for (int ch = 0; ch < NR_CHANNELS; ch++)
                {
                    for (int pol = 0; pol < NR_CORRELATIONS; pol++)
                    {
                        sum_of_weights[bl][n][pol] += weights[bl][t][ch][pol];
                    }
                }
            }
        }
    }

    #pragma omp parallel for
    for (int i = 0; i < (SUBGRID_SIZE * SUBGRID_SIZE); i++)
    {
        std::complex<double> sum[NR_CORRELATIONS][NR_CORRELATIONS];

        for (int n = 0; n < NR_ATERMS - 1; n++)
        {
            // loop over baselines
            for (int bl = 0; bl < NR_BASELINES; bl++)
            {
                unsigned int antenna1 = station_pairs[bl][0];
                unsigned int antenna2 = station_pairs[bl][1];

                std::complex<float> aXX1 = (*aterms)[n][antenna1][0][i][0];
                std::complex<float> aXY1 = (*aterms)[n][antenna1][0][i][1];
                std::complex<float> aYX1 = (*aterms)[n][antenna1][0][i][2];
                std::complex<float> aYY1 = (*aterms)[n][antenna1][0][i][3];

                std::complex<float> aXX2 = std::conj((*aterms)[n][antenna2][0][i][0]);
                std::complex<float> aXY2 = std::conj((*aterms)[n][antenna2][0][i][1]);
                std::complex<float> aYX2 = std::conj((*aterms)[n][antenna2][0][i][2]);
                std::complex<float> aYY2 = std::conj((*aterms)[n][antenna2][0][i][3]);

                std::complex<float> kp[16] = {};
                kp[0 +  0] = aXX2*aXX1;
                kp[0 +  4] = aXX2*aXY1;
                kp[0 +  8] = aXY2*aXX1;
                kp[0 + 12] = aXY2*aXY1;

                kp[1 +  0] = aXX2*aYX1;
                kp[1 +  4] = aXX2*aYY1;
                kp[1 +  8] = aXY2*aYX1;
                kp[1 + 12] = aXY2*aYY1;

                kp[2 +  0] = aYX2*aXX1;
                kp[2 +  4] = aYX2*aXY1;
                kp[2 +  8] = aYY2*aXX1;
                kp[2 + 12] = aYY2*aXY1;

                kp[3 +  0] = aYX2*aYX1;
                kp[3 +  4] = aYX2*aYY1;
                kp[3 +  8] = aYY2*aYX1;
                kp[3 + 12] = aYY2*aYY1;

                // add kronecker product to average beam
                for (int ii = 0; ii < NR_CORRELATIONS; ii++)
                {
                    for (int jj = 0; jj < NR_CORRELATIONS; jj++)
                    {
                        // Load weights
                        float w[8];
                        for (int p = 0; p < NR_CORRELATIONS; p++) {
                            w[p]                 = sum_of_weights[bl][n][p];
                            w[p+NR_CORRELATIONS] = sum_of_weights[bl][n][p];
                        }

                        // Load operands of kp
                        float a_real_real[8];
                        a_real_real[0] = kp[4*ii+0].real();
                        a_real_real[1] = kp[4*ii+1].real();
                        a_real_real[2] = kp[4*ii+2].real();
                        a_real_real[3] = kp[4*ii+3].real();
                        a_real_real[4] = kp[4*ii+0].real();
                        a_real_real[5] = kp[4*ii+1].real();
                        a_real_real[6] = kp[4*ii+2].real();
                        a_real_real[7] = kp[4*ii+3].real();

                        float b_real_imag[8];
                        b_real_imag[0] = kp[4*jj+0].real();
                        b_real_imag[1] = kp[4*jj+1].real();
                        b_real_imag[2] = kp[4*jj+2].real();
                        b_real_imag[3] = kp[4*jj+3].real();
                        b_real_imag[4] = kp[4*jj+0].imag();
                        b_real_imag[5] = kp[4*jj+1].imag();
                        b_real_imag[6] = kp[4*jj+2].imag();
                        b_real_imag[7] = kp[4*jj+3].imag();

                        float a_imag_imag[8];
                        a_imag_imag[0] =  kp[4*ii+0].imag();
                        a_imag_imag[1] =  kp[4*ii+1].imag();
                        a_imag_imag[2] =  kp[4*ii+2].imag();
                        a_imag_imag[3] =  kp[4*ii+3].imag();
                        a_imag_imag[4] = -kp[4*ii+0].imag();
                        a_imag_imag[5] = -kp[4*ii+1].imag();
                        a_imag_imag[6] = -kp[4*ii+2].imag();
                        a_imag_imag[7] = -kp[4*ii+3].imag();

                        float b_imag_real[8];
                        b_imag_real[0] = kp[4*jj+0].imag();
                        b_imag_real[1] = kp[4*jj+1].imag();
                        b_imag_real[2] = kp[4*jj+2].imag();
                        b_imag_real[3] = kp[4*jj+3].imag();
                        b_imag_real[4] = kp[4*jj+0].real();
                        b_imag_real[5] = kp[4*jj+1].real();
                        b_imag_real[6] = kp[4*jj+2].real();
                        b_imag_real[7] = kp[4*jj+3].real();

                        // Initialize updates
                        float update_real = 0;
                        float update_imag = 0;

                        // Compute updates
                        #if defined(__AVX2__)
                        __m256 t0 = _mm256_load_ps(w);
                        __m256 t1 = _mm256_load_ps(a_real_real);
                        __m256 t2 = _mm256_load_ps(b_real_imag);
                        __m256 t3 = _mm256_load_ps(a_imag_imag);
                        __m256 t4 = _mm256_load_ps(b_imag_real);
                        __m256 t5 = _mm256_mul_ps(t1, t2);
                               t5 = _mm256_fmadd_ps(t3, t4, t5);
                               t5 = _mm256_mul_ps(t0, t5);
                       __m128  t6 = _mm256_castps256_ps128(t5);
                       __m128  t7 = _mm256_extractf128_ps(t5, 1);
                               t6 = _mm_hadd_ps(t6, t6);
                               t6 = _mm_hadd_ps(t6, t6);
                               t7 = _mm_hadd_ps(t7, t7);
                               t7 = _mm_hadd_ps(t7, t7);
                        update_real = _mm_cvtss_f32(t6);
                        update_imag = _mm_cvtss_f32(t7);
                        #elif defined(__AVX__)
                        __m128 t0 = _mm_load_ps(w);
                        __m128 t1, t2, t3, t4, t5;

                        // Compute update_real
                        t1 = _mm_load_ps(&a_real_real[0]);
                        t2 = _mm_load_ps(&b_real_imag[0]);
                        t3 = _mm_load_ps(&a_imag_imag[0]);
                        t4 = _mm_load_ps(&b_imag_real[0]);
                        t5 = _mm_mul_ps(t1, t2);
                        t5 = _mm_fmadd_ps(t3, t4, t5);
                        t5 = _mm_mul_ps(t0, t5);
                        t5 = _mm_hadd_ps(t5, t5);
                        t5 = _mm_hadd_ps(t5, t5);
                        update_real = _mm_cvtss_f32(t5);

                        // Compute update_imag
                        t1 = _mm_load_ps(&a_real_real[4]);
                        t2 = _mm_load_ps(&b_real_imag[4]);
                        t3 = _mm_load_ps(&a_imag_imag[4]);
                        t4 = _mm_load_ps(&b_imag_real[4]);
                        t5 = _mm_mul_ps(t1, t2);
                        t5 = _mm_fmadd_ps(t3, t4, t5);
                        t5 = _mm_mul_ps(t0, t5);
                        t5 = _mm_hadd_ps(t5, t5);
                        t5 = _mm_hadd_ps(t5, t5);
                        update_imag = _mm_cvtss_f32(t5);
                        #else
                        float update[8];
                        for (int k = 0; k < 8; k++) {
                            update[k] = w[k] * (a_real_real[k] * b_real_imag[k] + a_imag_imag[k] * b_imag_real[k]);
                        }
                        for (int k = 0; k < 4; k++) {
                            update_real += update[k];
                            update_imag += update[k+4];
                        }
                        #endif

                        // Update sum
                        sum[ii][jj] += std::complex<float>(update_real, update_imag);
                    }
                }
            }
        }

        for(size_t ii = 0; ii < 4; ii++)
        {
            for(size_t jj = 0; jj < 4; jj++)
            {
                (*average_beam)[i][ii][jj] = sum[ii][jj];
            }
        }
    }
}
