#include <cstring>
#include <complex>
#include <vector>

#include "kernels.h"

void kernel_08(
    const unsigned int* station_pairs_ptr,
    const int* aterm_offsets_ptr,
    const std::complex<float>* aterms_ptr,
    const float* uvw_ptr,
    const float* weights_ptr,
    std::complex<float>* average_beam_ptr)
{
  // Define multidimensional types
  typedef unsigned int ATermOffsets[NR_ATERMS + 1];
  typedef unsigned int StationPairs[NR_BASELINES][2];
  typedef float UVW[NR_BASELINES][NR_TIMESTEPS][3];
  typedef float Weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS]
                       [NR_CORRELATIONS];
  typedef float SumOfWeights[NR_BASELINES][NR_ATERMS][NR_CORRELATIONS];

  // Cast class members to multidimensional types used in this method
  const ATerms &aterms = *reinterpret_cast<const ATerms *>(aterms_ptr);
  AverageBeam &average_beam = *reinterpret_cast<AverageBeam *>(average_beam_ptr);
  const ATermOffsets &aterm_offsets =
      *reinterpret_cast<const ATermOffsets *>(aterm_offsets_ptr);
  const StationPairs &station_pairs =
      *reinterpret_cast<const StationPairs *>(station_pairs_ptr);
  const UVW &uvw = *reinterpret_cast<const UVW *>(uvw_ptr);
  const Weights &weights = *reinterpret_cast<const Weights *>(weights_ptr);

  // Initialize sum of weights
  std::vector<float> sum_of_weights_buffer(
      NR_BASELINES * NR_ATERMS * NR_CORRELATIONS, 0.0);
  SumOfWeights &sum_of_weights =
      *((SumOfWeights *)sum_of_weights_buffer.data());

  // Compute sum of weights
  #pragma omp parallel for
  for (int n = 0; n < NR_ATERMS; n++) {
    int time_start = aterm_offsets[n];
    int time_end = aterm_offsets[n + 1];

    // loop over baselines
    for (int bl = 0; bl < NR_BASELINES; bl++) {
      for (int t = time_start; t < time_end; t++) {
        if (std::isinf(uvw[bl][t][0])) continue;

        for (int ch = 0; ch < NR_CHANNELS; ch++) {
          for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
            sum_of_weights[bl][n][pol] += weights[bl][t][ch][pol];
          }
        }
      }
    }
  }

  // Compute average beam for all pixels
  #pragma omp parallel for
  for (int i = 0; i < (SUBGRID_SIZE * SUBGRID_SIZE); i++) {
    std::complex<double> sum[NR_CORRELATIONS][NR_CORRELATIONS];

    // Loop over aterms
    for (int n = 0; n < NR_ATERMS; n++) {
      // Loop over baselines
      for (int bl = 0; bl < NR_BASELINES; bl++) {
        unsigned int antenna1 = station_pairs[bl][0];
        unsigned int antenna2 = station_pairs[bl][1];

        // Check whether stationPair is initialized
        if (antenna1 >= NR_ANTENNAS || antenna2 >= NR_ANTENNAS) {
          continue;
        }

        std::complex<float> aXX1 = aterms[n][antenna1][0][i][0];
        std::complex<float> aXY1 = aterms[n][antenna1][0][i][1];
        std::complex<float> aYX1 = aterms[n][antenna1][0][i][2];
        std::complex<float> aYY1 = aterms[n][antenna1][0][i][3];

        std::complex<float> aXX2 = std::conj(aterms[n][antenna2][0][i][0]);
        std::complex<float> aXY2 = std::conj(aterms[n][antenna2][0][i][1]);
        std::complex<float> aYX2 = std::conj(aterms[n][antenna2][0][i][2]);
        std::complex<float> aYY2 = std::conj(aterms[n][antenna2][0][i][3]);

        std::complex<float> kp[16] = {};
        kp[0 + 0] = aXX2 * aXX1;
        kp[0 + 4] = aXX2 * aXY1;
        kp[0 + 8] = aXY2 * aXX1;
        kp[0 + 12] = aXY2 * aXY1;

        kp[1 + 0] = aXX2 * aYX1;
        kp[1 + 4] = aXX2 * aYY1;
        kp[1 + 8] = aXY2 * aYX1;
        kp[1 + 12] = aXY2 * aYY1;

        kp[2 + 0] = aYX2 * aXX1;
        kp[2 + 4] = aYX2 * aXY1;
        kp[2 + 8] = aYY2 * aXX1;
        kp[2 + 12] = aYY2 * aXY1;

        kp[3 + 0] = aYX2 * aYX1;
        kp[3 + 4] = aYX2 * aYY1;
        kp[3 + 8] = aYY2 * aYX1;
        kp[3 + 12] = aYY2 * aYY1;

        for (int ii = 0; ii < NR_CORRELATIONS; ii++) {
          for (int jj = 0; jj < NR_CORRELATIONS; jj++) {
            // Load weights for current baseline, aterm
            float *weights = &sum_of_weights[bl][n][0];

            // Compute real and imaginary part of update separately
            float update_real = 0;
            float update_imag = 0;
            for (int p = 0; p < NR_CORRELATIONS; p++) {
              float kp1_real = kp[4 * ii + p].real();
              float kp1_imag = -kp[4 * ii + p].imag();
              float kp2_real = kp[4 * jj + p].real();
              float kp2_imag = kp[4 * jj + p].imag();
              update_real +=
                  weights[p] * (kp1_real * kp2_real - kp1_imag * kp2_imag);
              update_imag +=
                  weights[p] * (kp1_real * kp2_imag + kp1_imag * kp2_real);
            }

            // Add kronecker product to sum
            sum[ii][jj] += std::complex<float>(update_real, update_imag);
          }
        }
      }  // end for baselines
    } // end for aterms

    // Set average beam from sum of kronecker products
    for (size_t ii = 0; ii < 4; ii++) {
      for (size_t jj = 0; jj < 4; jj++) {
        average_beam[i][ii][jj] += sum[ii][jj];
      }
    }
  } // end for pixels
}
