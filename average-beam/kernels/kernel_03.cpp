#include <cstring>

#include "kernels.h"

void kernel_03(
    const unsigned int station_pairs[NR_BASELINES][2],
    const int aterm_offsets[NR_ATERMS],
    const std::complex<float> *aterms_ptr,
    const float uvw[NR_BASELINES][NR_TIMESTEPS][3],
    const float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS],
    std::complex<float> *average_beam_ptr)
{
    AverageBeam *average_beam = (AverageBeam *) average_beam_ptr;
    ATerms *aterms = (ATerms *) aterms_ptr;

    float sum_of_weights[NR_BASELINES][NR_ATERMS][NR_CORRELATIONS];
    memset(sum_of_weights, 0, NR_BASELINES * NR_ATERMS * NR_CORRELATIONS * sizeof(float));

    #pragma omp parallel for
    for (int n = 0; n < NR_ATERMS - 1; n++)
    {
        int time_start = aterm_offsets[n];
        int time_end = aterm_offsets[n+1];

        // loop over baselines
        for (size_t bl = 0; bl < NR_BASELINES; bl++)
        {
            unsigned int antenna1 = station_pairs[bl][0];
            unsigned int antenna2 = station_pairs[bl][1];

            for(size_t t=time_start; t < time_end; t++)
            {
                if (std::isinf(uvw[bl][t][0])) continue;

                for(int ch=0; ch < NR_CHANNELS; ch++)
                {
                    for(int pol = 0; pol < 4; pol++)
                    {
                        sum_of_weights[bl][n][pol] += weights[bl][t][ch][pol];
                    }
                }
            }
        }
    }

    #pragma omp parallel for
    for (size_t i = 0; i < (SUBGRID_SIZE * SUBGRID_SIZE); i++)
    {
        std::complex<double> sum[NR_CORRELATIONS][NR_CORRELATIONS];

        for (int n = 0; n < NR_ATERMS - 1; n++)
        {
            int time_start = aterm_offsets[n];
            int time_end = aterm_offsets[n+1];

            // loop over baselines
            for (size_t bl = 0; bl < NR_BASELINES; bl++)
            {
                unsigned int antenna1 = station_pairs[bl][0];
                unsigned int antenna2 = station_pairs[bl][1];

                size_t offset = SUBGRID_SIZE * SUBGRID_SIZE * NR_ANTENNAS * n;
                size_t offset1 = offset + antenna1 * SUBGRID_SIZE * SUBGRID_SIZE;
                size_t offset2 = offset + antenna2 * SUBGRID_SIZE * SUBGRID_SIZE;

                std::complex<float> aXX1 = (*aterms)[n][antenna1][0][i][0];
                std::complex<float> aXY1 = (*aterms)[n][antenna1][0][i][1];
                std::complex<float> aYX1 = (*aterms)[n][antenna1][0][i][2];
                std::complex<float> aYY1 = (*aterms)[n][antenna1][0][i][3];

                std::complex<float> aXX2 = (*aterms)[n][antenna2][0][i][0];
                std::complex<float> aXY2 = (*aterms)[n][antenna2][0][i][1];
                std::complex<float> aYX2 = (*aterms)[n][antenna2][0][i][2];
                std::complex<float> aYY2 = (*aterms)[n][antenna2][0][i][3];

                std::complex<float> kp[16] = {};
                kp[0] = std::conj(aXX2)*aXX1;
                kp[1] = std::conj(aXX2)*aXY1;
                kp[2] = std::conj(aXY2)*aXX1;
                kp[3] = std::conj(aXY2)*aXY1;

                kp[4] = std::conj(aXX2)*aYX1;
                kp[5] = std::conj(aXX2)*aYY1;
                kp[6] = std::conj(aXY2)*aYX1;
                kp[7] = std::conj(aXY2)*aYY1;

                kp[ 8] = std::conj(aYX2)*aXX1;
                kp[ 9] = std::conj(aYX2)*aXY1;
                kp[10] = std::conj(aYY2)*aXX1;
                kp[11] = std::conj(aYY2)*aXY1;

                kp[12] = std::conj(aYX2)*aYX1;
                kp[13] = std::conj(aYX2)*aYY1;
                kp[14] = std::conj(aYY2)*aYX1;
                kp[15] = std::conj(aYY2)*aYY1;

                // add kronecker product to average beam
                for(size_t ii = 0; ii < 4; ii++)
                {
                    for(size_t jj = 0; jj < 4; jj++)
                    {
                        std::complex<float> update = 0;
                        update += sum_of_weights[bl][n][0] * std::conj(kp[ii  ])  * kp[jj  ];
                        update += sum_of_weights[bl][n][1] * std::conj(kp[ii+4])  * kp[jj+4];
                        update += sum_of_weights[bl][n][2] * std::conj(kp[ii+8])  * kp[jj+8];
                        update += sum_of_weights[bl][n][3] * std::conj(kp[ii+12]) * kp[jj+12];
                        sum[ii][jj] += update;
                    }
                }
            }
        }

        for(size_t ii = 0; ii < 4; ii++)
        {
            for(size_t jj = 0; jj < 4; jj++)
            {
                (*average_beam)[i][ii][jj] = sum[ii][jj];
            }
        }
    }
}
