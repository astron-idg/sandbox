#include "init.h"

void init_station_pairs(
    unsigned int station_pairs[NR_BASELINES][2])
{
    int bl = 0;

    for (int ant1 = 0; ant1 < NR_ANTENNAS; ant1++) {
        for (int ant2 = ant1 + 1; ant2 < NR_ANTENNAS; ant2++) {
            if (bl >= NR_BASELINES) {
                break;
            }
            station_pairs[bl][0] = ant1;
            station_pairs[bl][1] = ant2;
            bl++;
        }
    }
}

void init_aterm_offsets(
    int aterm_offsets[NR_ATERMS])
{
    for (int i = 0; i < NR_ATERMS; i++) {
        aterm_offsets[i] = i * (NR_TIMESTEPS / NR_ATERMS);
    }
}

void init_aterms(
    std::complex<float> *aterms_ptr)
{
    ATerms *aterms = (ATerms *) aterms_ptr;

    #pragma omp parallel for
    for (int ant = 0; ant < NR_ANTENNAS; ant++) {
        float rand_ant = random() / ((float) RAND_MAX);
        for (int aterm = 0; aterm < NR_ATERMS; aterm++) {
            float rand_aterm = random() / ((float) RAND_MAX);
            for (int y = 0; y < SUBGRID_SIZE; y++) {
                float rand_y = random() / ((float) RAND_MAX);
                for (int x = 0; x < SUBGRID_SIZE; x++) {
                    float rand_x = random() / ((float) RAND_MAX);
                    #pragma unroll
                    for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
                        float real = (rand_ant * rand_aterm * rand_y * rand_x * (pol+1)) + 0.01;
                        float imag = (rand_ant * rand_aterm * rand_y * rand_x * (pol+1)) - 0.01;
                        (*aterms)[aterm][ant][y][x][pol] = std::complex<float>(real, imag);
                    }
                }
            }
        }
    }
}


void init_uvw(
    float uvw[NR_BASELINES][NR_TIMESTEPS][3])
{
    #pragma omp parallel for
    for (int bl = 0; bl < NR_BASELINES; bl++) {
        float bl_u = random() / ((float) RAND_MAX) ;
        float bl_v = random() / ((float) RAND_MAX) ;
        float bl_w = random() / ((float) RAND_MAX) ;

        for (int time = 0; time < NR_TIMESTEPS; time++) {
            uvw[bl][time][0] = bl_u + time;
            uvw[bl][time][1] = bl_v + time;
            uvw[bl][time][2] = bl_w + time;
        }
    }
}


void init_weights(
    float weights[NR_BASELINES][NR_TIMESTEPS][NR_CHANNELS][NR_CORRELATIONS])
{
    #pragma omp parallel for
    for (int bl = 0; bl < NR_BASELINES; bl++) {
        float weight_bl = random() / ((float) RAND_MAX) ;
        for (int time = 0; time < NR_TIMESTEPS; time++) {
            float weight_time = random() / ((float) RAND_MAX) ;
            for (int chan = 0; chan < NR_CHANNELS; chan++) {
                float weight_chan = random() / ((float) RAND_MAX) ;
                for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
                    weights[bl][time][chan][pol] = (1.0f + pol/10) * weight_bl * weight_time * weight_chan;
                }
            }
        }

    }
}

void reset_average_beam(
    std::complex<float> average_beam[SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS])
{
    memset((void *) average_beam, 0, SUBGRID_SIZE*SUBGRID_SIZE*NR_CORRELATIONS*NR_CORRELATIONS*sizeof(std::complex<float>));
}
