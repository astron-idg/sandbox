# colors from: https://github.com/aschn/gnuplot-colorbrewer

# red line styles
set style line 11 lc rgb '#FFF5F0' # very light red
set style line 12 lc rgb '#FEE0D2' # 
set style line 13 lc rgb '#FCBBA1' # 
set style line 14 lc rgb '#FC9272' # light red
set style line 15 lc rgb '#FB6A4A' # 
set style line 16 lc rgb '#EF3B2C' # medium red
set style line 17 lc rgb '#CB181D' #
set style line 18 lc rgb '#99000D' # dark red

# green line styles
set style line 21 lc rgb '#F7FCF5' # very light green
set style line 22 lc rgb '#E5F5E0' # 
set style line 23 lc rgb '#C7E9C0' # 
set style line 24 lc rgb '#A1D99B' # light green
set style line 25 lc rgb '#74C476' # 
set style line 26 lc rgb '#41AB5D' # medium green
set style line 27 lc rgb '#238B45' #
set style line 28 lc rgb '#005A32' # dark green

# blue line styles
set style line 31 lc rgb '#F7FBFF' # very light blue
set style line 32 lc rgb '#DEEBF7' # 
set style line 33 lc rgb '#C6DBEF' # 
set style line 34 lc rgb '#9ECAE1' # light blue
set style line 35 lc rgb '#6BAED6' # 
set style line 36 lc rgb '#4292C6' # medium blue
set style line 37 lc rgb '#2171B5' #
set style line 38 lc rgb '#084594' # dark blue

# grey line styles
set style line 41 lc rgb '#FFFFFF' # white
set style line 42 lc rgb '#F0F0F0' # 
set style line 43 lc rgb '#D9D9D9' # 
set style line 44 lc rgb '#BDBDBD' # light grey
set style line 45 lc rgb '#969696' # 
set style line 46 lc rgb '#737373' # medium grey
set style line 47 lc rgb '#525252' #
set style line 48 lc rgb '#252525' # dark grey

# red-purple line styles
set style line 51 lc rgb '#FFF7F3' # very light red-purple
set style line 52 lc rgb '#FDE0DD' #
set style line 53 lc rgb '#FCC5C0' #
set style line 54 lc rgb '#FA9FB5' # light red-purple
set style line 55 lc rgb '#F768A1' #
set style line 56 lc rgb '#DD3497' # medium red-purple
set style line 57 lc rgb '#AE017E' #
set style line 58 lc rgb '#7A0177' # dark red-purple

# orange line styles
set style line 61 lc rgb '#FFF5EB' # very light orange
set style line 62 lc rgb '#FEE6CE' # 
set style line 63 lc rgb '#FDD0A2' # 
set style line 64 lc rgb '#FDAE6B' # light orange
set style line 65 lc rgb '#FD8D3C' # 
set style line 66 lc rgb '#F16913' # medium orange
set style line 67 lc rgb '#D94801' #
set style line 68 lc rgb '#8C2D04' # dark orange
