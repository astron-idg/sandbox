# Setup
set term pdf linewidth 3
set grid
set tics nomirror
set border 3 back
set xlabel "r [fma/sincos]"
set mxtic 10
set mytic 4
set logscale x 2
set logscale y
peak_fma = 2253
peak_sincos = 30.69
set yrange [peak_sincos:peak_fma]
set xrange [512:0.125] reverse
load "colors.plt"
set key top right reverse
set arrow from 16,peak_sincos to 16,peak_fma nohead ls 17
set output "plot.pdf"
set ylabel "Performance [Gops/s]"
plot           exp(-4.39018261974 / (1 + 0.184789345226 * exp(0.781123506876 * log(x))) + 7.79160118548) notitle with lines ls 37, \
    "data.dat" index 0 using 1:($2) title "Xeon Phi 7210 (SVML)"  with points ls 38 pt 7 ps 0.5, \
               exp(-4.23437393172 / (1 + 0.169978056708 * exp(0.920350372039 * log(x))) + 7.74602339738) notitle with lines ls 26, \
    "data.dat" index 1 using 1:($2) title "Xeon Phi 7210 (VML)" with points ls 27 pt 7 ps 0.5

unset logscale y
unset arrow
set arrow from 16,0 to 16,peak_fma nohead ls 17
set key top right
set yrange [0.1:peak_fma]
replot
