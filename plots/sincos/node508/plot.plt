# Setup
set term pdf linewidth 3
set grid
set tics nomirror
set border 3 back
set xlabel "r [fma/sincos]"
set mxtic 10
set mytic 4
set logscale x 2
set logscale y
peak_fma = 1433.6/2
peak_sincos = 8.30
set yrange [peak_sincos:peak_fma]
set xrange [512:0.125] reverse
load "colors.plt"
set key top right reverse
set arrow from 16,peak_sincos to 16,peak_fma nohead ls 17
set output "plot.pdf"
set ylabel "Performance [Gops/s]"
plot           exp(-4.55474121828 / (1 + 0.195762985676 * exp(0.786211492351 * log(x))) + 6.63142380771) notitle with lines ls 37, \
    "data.dat" index 0 using 1:($2) title "2x Xeon E5-2640v3 (SVML)"  with points ls 38 pt 7 ps 0.5, \
               exp(-4.17713112957 / (1 + 0.20944758353 * exp(0.825286634275 * log(x))) + 6.59754710973) notitle with lines ls 26, \
    "data.dat" index 1 using 1:($2) title "2x Xeon E5-2640v3 (VML)" with points ls 27 pt 7 ps 0.5

unset logscale y
unset arrow
set arrow from 16,0 to 16,peak_fma nohead ls 17
set key top right
set yrange [0.1:peak_fma]
replot
