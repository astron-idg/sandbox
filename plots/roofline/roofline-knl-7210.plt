set output "roofline-knl-7210.pdf"

# limits
x_min    = 0.125/4
x_max    = 1024
y_min    = 4
y_max    = 3000

load "roofline-common.conf"
set mytics 10


#########################
# 2x Intel Xeon E5-2640v3
#########################

# architecture specific data
a0_cpu_flop  = 16*2*2	# maximum floating point throughput (simd*fma*fpu)
a0_cpu_fma   = 16*2     # maximum floating point throughput (simd*fpu)
a0_cpu_freq  = 1.1		# clock cycle frequency (GHz)
a0_cpu_cores = 64		# number of cores
a0_mem_width = 8		# memory bus width (Bytes)
a0_mem_rate  = 2.133	# memory clock rate (GHz)
a0_mem_chan  = 6		# number of memory channels

# roofline
a0_cpu_peak = a0_cpu_fma * a0_cpu_freq * a0_cpu_cores	# theoretical peak GFMAS/s
a0_mem_peak = a0_mem_width * a0_mem_rate * a0_mem_chan	# peak memory bandwidth (GB/s)
a0_mem_teta = 45										# memory roof angle (degrees)
a0_mem_b	= tan( deg2rad( a0_mem_teta ) )				# slope

# ridge Point
a0_ridge_x = a0_cpu_peak / a0_mem_peak
a0_ridge_y = a0_cpu_peak

# memory roof
a0_mem_a(m)    = exp( log( a0_cpu_peak ) - log( m ) * a0_mem_b ) # y=ax^b when x=1
a0_mem_roof_a  = a0_mem_a( a0_ridge_x )							 # y=ax^b when x=1

# roofs
a0_cpu_roof(x) = a0_ridge_y
a0_mem_roof(x) = a0_mem_roof_a * x ** a0_mem_b
a0_roofline(x) = min( a0_cpu_roof(x) , a0_mem_roof(x) )
a0_cpu_frac(x,y) = frac( a0_cpu_roof(x), y )

# ceilings
a0_cpu_frac(x,y)   = frac(a0_cpu_roof(x), y)
a0_cpu_avx_ceil(x) = frac(a0_cpu_roof(x), 2)
a0_cpu_sse_ceil(x) = frac(a0_cpu_roof(x), 4)
a0_cpu_sca_ceil(x) = frac(a0_cpu_roof(x), 16)
a0_cpu_svml_sincos_ceil    = 30.69
a0_cpu_svml_sincos_ceil_16 = 437.01
a0_cpu_vml_sincos_ceil     = 32.14
a0_cpu_vml_sincos_ceil_16  = 569.90

######
# plot
######

# labels
set xlabel "Operational Intensity (Ops/Byte)"
set ylabel "Attainable Gops/s"

# labels
set label "Intel Xeon Phi 7210" at x_max-1,yabove(a0_cpu_roof(x_max))+10 right
oi_gridder = 38.76
gops_gridder = 247.92
set label "" at oi_gridder,gops_gridder point ls 5
set label "gridder (VML)" at oi_gridder-8,gops_gridder right
oi_gridder = 39.68
gops_gridder = 254.01
set label "" at oi_gridder,gops_gridder point ls 5
set label "gridder (SVML)" at oi_gridder-8,gops_gridder right

oi_gridder_synthetic = 0.8
# set label " FMA reduce" at oi_gridder_synthetic/2,592/2 point ls 5
oi_gridder_synthetic = 1500 
set label " FMA reduce (registers)" at oi_gridder_synthetic,2238.43-400 right
set label "" at oi_gridder_synthetic,2238.43 point ls 5

# no-fma ceiling
#set label "avx" at x_max-1,yabove(a0_cpu_avx_ceil(x_max)) right
#set label "sse" at x_max-1,yabove(a0_cpu_sse_ceil(x_max)) right
#set label "scalar" at x_max-1,yabove(a0_cpu_sca_ceil(x_max)) right
#plot \
#	min(a0_cpu_avx_ceil(x), a0_mem_roof(x)) ls 3, \
#	min(a0_cpu_sse_ceil(x), a0_mem_roof(x)) ls 3, \
#	min(a0_cpu_sca_ceil(x), a0_mem_roof(x)) ls 3

# sincos ceiling
sincos_label_x = x_max-1
#sincos_label_x = 64
# set label "SVML sincos (r=0)" at sincos_label_x,yabove(a0_cpu_svml_sincos_ceil) right
set label "SVML sincos (r=16)" at sincos_label_x,yabove(a0_cpu_svml_sincos_ceil_16) right
set label "VML sincos (r=0)" at sincos_label_x,yabove(a0_cpu_vml_sincos_ceil) right
set label "VML sincos (r=16)" at sincos_label_x,yabove(a0_cpu_vml_sincos_ceil_16) right
plot \
	min(a0_cpu_svml_sincos_ceil, a0_mem_roof(x)) ls 3, \
	min(a0_cpu_svml_sincos_ceil_16, a0_mem_roof(x)) ls 3, \
	min(a0_cpu_vml_sincos_ceil, a0_mem_roof(x)) ls 3, \
	min(a0_cpu_vml_sincos_ceil_16, a0_mem_roof(x)) ls 3

# global memory roof
plot \
	min(a0_cpu_roof(x), a0_mem_roof(x)) ls 2

# cache roof
# a0_mem_roof_temp(mem_peak, x) = (exp(log(a0_cpu_peak) - log(a0_cpu_peak/(mem_peak)) * tan(deg2rad(45)))) * x ** tan(deg2rad(45))

#plot \
#	min(a0_cpu_roof(x), a0_mem_roof_temp(2728, x)) ls 4, \
#	min(a0_cpu_roof(x), a0_mem_roof_temp(866, x)) ls 4, \
#	min(a0_cpu_roof(x), a0_mem_roof_temp(655, x)) ls 4
#plot \
#	min(a0_cpu_roof(x), a0_mem_roof_temp(1267, x)) ls 4, \
#	min(a0_cpu_roof(x), a0_mem_roof_temp(618, x)) ls 4, \
#	min(a0_cpu_roof(x), a0_mem_roof_temp(313, x)) ls 4
