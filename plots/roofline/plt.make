GNUPLOT = /usr/bin/gnuplot
PDFCROP = /usr/bin/pdfcrop

%.pdf: %.plt
	@$(GNUPLOT) "$(abspath $^)"
	@$(PDFCROP) "$(abspath $@)" > /dev/null
