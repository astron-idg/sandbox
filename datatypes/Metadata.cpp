#include <stdexcept>

#include "Metadata.h"

using namespace std;

namespace idg {


    Metadata::Metadata(unsigned int s) 
        : nr_subgrids(s),
          num_elements((size_type) s),
          buffer(new value_type[s]),
          have_allocated_data(true)  
    { }



    Metadata::Metadata(value_type* data, 
                       unsigned int s) 
        : nr_subgrids(s),
          num_elements((size_type) s),
          buffer(data),
          have_allocated_data(false)  
    { }



    Metadata::~Metadata() 
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    Metadata::Metadata(const Metadata& M)
        : nr_subgrids(M.nr_subgrids),
          num_elements((size_type) M.nr_subgrids),
          buffer(new value_type[M.nr_subgrids]),
          have_allocated_data(true)  
    {
        copy(M.begin(), M.end(), buffer);
    }


    // accesss

    Metadata::value_type& Metadata::get(unsigned int subgrid_index)
    {
        if (subgrid_index >= nr_subgrids)
            throw invalid_argument("Subscript index out of range");

        return (*this)(subgrid_index);
    }
    

    void Metadata::set(unsigned int subgrid_index, value_type metadatum)
    {
        if (subgrid_index >= nr_subgrids)
            throw invalid_argument("Subscript index out of range");

        (*this)(subgrid_index) = metadatum;
    }

    


    // helpers

    // helper functions
    ostream& operator<<(std::ostream& os, const Coordinate& C)
    {
        os << "(" << C.x << "," << C.y << ")";
        return os;
    }


    ostream& operator<<(std::ostream& os, const Metadatum& m)
    {
        os << "(" << m.get_time() << "," << m.get_baseline() << "," 
           << m.get_coordinate() << ")";
        return os;
    }
    

    void Metadata::print(ostream& os) const
    {
        os << "Dimensions: [" << nr_subgrids << "]" << endl;

        os << "Metadata = [";
        for (auto ptr = this->begin(); ptr != this->end(); ptr++) {
            os << *ptr;
            if (ptr != this->end()-1) os << ",";
        }
        os << "]";
    }


    void Metadata::print() const
    {
        print(cout);
    }


    ostream& operator<<(std::ostream& os, const Metadata& M)
    {
        M.print(os);
        return os;
    }


} // namespace idg
