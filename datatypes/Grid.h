// Grid data type

// Class Grid
// Functionality as complex<float>[NR_POLARIZATIONS][GRIDSIZE][GRIDSIZE]
// but access via array(polarization, y, x) notation
// instead of array[polarization][y][x]


#ifndef IDG_GRID_H_
#define IDG_GRID_H_

#include <iostream>
#include <complex>
#include <cstddef> // size_t
#include <array>

namespace idg {

    class Grid {

    public:
        typedef std::complex<float> value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        Grid(unsigned int nr_polarizations = 0,   
             unsigned int gridsize = 0);
        Grid(value_type *data, 
             unsigned int nr_polarizations, 
             unsigned int gridsize);
        virtual ~Grid();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Grid(const Grid& v);

        // assigment operator; not supported 
        Grid& operator=(const Grid& rhs) = delete;

        value_type* data() const { return buffer; }
        value_type* data(unsigned int polarization,
                         unsigned int y, 
                         unsigned int x) const { 
            return &buffer[x + lead_dim[0]*y + lead_dim[1]*polarization]; }

        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return nr_dimensions; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }
    
        // get data members
        unsigned int get_nr_polarizations() const { return nr_polarizations; };
        unsigned int get_grid_size() const { return grid_size; };


        // range checked access
        value_type& get(unsigned int polarization,
                        unsigned int y, 
                        unsigned int x);

        void set(unsigned int polarization,
                 unsigned int y, 
                 unsigned int x, 
                 value_type value);

        // non-range checked access
        const value_type& operator()(unsigned int polarization,
                                     unsigned int y,
                                     unsigned int x) const
        { return buffer[x + lead_dim[0]*y + lead_dim[1]*polarization]; }

        value_type& operator()(unsigned int polarization,
                               unsigned int y,
                               unsigned int x)
        { return buffer[x + lead_dim[0]*y + lead_dim[1]*polarization]; }


        // init to constant value (e.g. to zero)
        void init(const value_type a = value_type()); 

        // operators
        friend Grid operator+(const Grid& x, const Grid& y);
        friend Grid operator-(const Grid& x, const Grid& y);
        friend Grid operator*(const std::complex<float> a, Grid& X);
        friend Grid operator*(Grid& X, const std::complex<float> a);
        Grid& operator+=(const Grid& rhs);
        Grid& operator-=(const Grid& rhs);
        //    Grid& operator*=(const value_type scalar);
        //    Grid& operator/=(const value_type scalar);
        //    bool operator==(const Grid& rhs);
        //    bool operator!=(const Grid& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 
    
    private:
        const unsigned int nr_polarizations;
        const unsigned int grid_size;
   
        const size_type num_elements;
        static const unsigned int nr_dimensions = 3;
        const std::array<size_type, nr_dimensions> dim;
        const std::array<size_type, nr_dimensions> lead_dim;
        value_type* buffer;
        const bool have_allocated_data;
    };


    // helper functions
    std::ostream& operator<<(std::ostream& os, const Grid& g);
    
    // X = a*X + b*Y
    void axpy(std::complex<float> a, Grid& X, 
              std::complex<float> b, const Grid& Y);

    // X(begin:end) = a*X(begin:end)
    void scale(std::complex<float> a,
               Grid::iterator begin, 
               Grid::iterator end);

} // namespace idg

#endif
