#include <stdexcept>

#include "Visibilities.h"

using namespace std;

namespace idg {


    Visibilities::Visibilities(unsigned int baselines, 
                               unsigned int time, 
                               unsigned int channels, 
                               unsigned int polarizations) 
        : nr_baselines(baselines), 
          nr_time(time), 
          nr_channels(channels), 
          nr_polarizations(polarizations), 
          num_elements((size_type) baselines*time*channels*polarizations),
          dim({baselines,time,channels,polarizations}), 
          lead_dim({polarizations,channels*polarizations,
                    time*channels*polarizations,
                    baselines*time*channels*polarizations}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { }



    Visibilities::Visibilities(value_type *data, 
                               unsigned int baselines, 
                               unsigned int time, 
                               unsigned int channels, 
                               unsigned int polarizations) 
        : nr_baselines(baselines), 
          nr_time(time), 
          nr_channels(channels), 
          nr_polarizations(polarizations), 
          num_elements((size_type) baselines*time*channels*polarizations),
          dim({baselines,time,channels,polarizations}),
          lead_dim({polarizations,channels*polarizations,
                    time*channels*polarizations,
                    baselines*time*channels*polarizations}),
          buffer(data),
          have_allocated_data(false)  
    { }



    Visibilities::~Visibilities() 
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }



    Visibilities::Visibilities(const Visibilities& v) 
        : nr_baselines(v.nr_baselines), 
          nr_time(v.nr_time), 
          nr_channels(v.nr_channels), 
          nr_polarizations(v.nr_polarizations), 
          num_elements((size_type) v.nr_baselines*v.nr_time
                       *v.nr_channels*v.nr_polarizations),
          dim({v.nr_baselines,v.nr_time,v.nr_channels,v.nr_polarizations}), 
          lead_dim({v.nr_polarizations,v.nr_channels*v.nr_polarizations,
                    v.nr_time*v.nr_channels*v.nr_polarizations, 
                    v.nr_baselines*v.nr_time*v.nr_channels*v.nr_polarizations}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { 
        // copy all elements from v.buffer into buffer
        copy(v.begin(), v.end(), buffer);
    }



    Visibilities::value_type& 
    Visibilities::get(unsigned int baseline, 
                      unsigned int time, 
                      unsigned int channel, 
                      unsigned int polarization)
    {
        checkParametersInRange(baseline, time, channel, polarization);
        return (*this)(baseline, time, channel, polarization);
    }



    void Visibilities::set(unsigned int baseline, 
                           unsigned int time, 
                           unsigned int channel, 
                           unsigned int polarization,
                           value_type visibility)
    {
        checkParametersInRange(baseline, time, channel, polarization);
        (*this)(baseline, time, channel, polarization) = visibility;
    }



    // init to constant value (e.g. to zero)
    void Visibilities::init(const Visibilities::value_type a)
    {
        for (auto ptr = this->begin(); ptr != this->end(); ptr++) 
            *ptr = a;
    }



    // arithmetic

    void axpy(complex<float> a, Visibilities& x, 
              complex<float> b, const Visibilities& y)
    {
        if ( Visibilities::sameDimensions(x, y) == false )
            throw invalid_argument("Dimensions must be equal");

        auto xp = x.begin();
        for (auto elem_ptr = y.begin(); elem_ptr != y.end(); elem_ptr++) {
            *xp = a*(*xp) + b*(*elem_ptr); 
            xp++;
        }
    }



    // needs to be removed!
    void scale(std::complex<float> a, 
               Visibilities::iterator begin, 
               Visibilities::iterator end)
    {
        for (auto elem_ptr = begin; elem_ptr != end; elem_ptr++)
            *elem_ptr *= a;
    }



    Visibilities operator+(const Visibilities& x, const Visibilities& y) 
    {
        if ( Visibilities::sameDimensions(x, y) == false )
            throw invalid_argument("Dimensions must be equal");

        Visibilities v(x);
        axpy(1.0, v, 1.0, y);
        return v;
    }



    Visibilities operator-(const Visibilities& x, const Visibilities& y) 
    {
        if ( Visibilities::sameDimensions(x, y) == false )
            throw invalid_argument("Dimensions must be equal");

        Visibilities v(x);
        axpy(1.0, v, -1.0, y);
        return v;
    }



    Visibilities operator*(const complex<float> a, Visibilities& X)
    {
        Visibilities T(X);
        scale(a, T.begin(), T.end());
        return T;
    }


    Visibilities operator*(Visibilities& X, const complex<float> a)
    {
        return a*X;
    }


    Visibilities& Visibilities::operator+=(const Visibilities& rhs)
    {
        if ( sameDimensions(*this, rhs) == false )
            throw invalid_argument("Dimensions must be equal");

        axpy(1.0, *this, 1.0, rhs);
        return *this;
    }


    Visibilities& Visibilities::operator-=(const Visibilities& rhs)
    {
        if ( sameDimensions(*this, rhs) == false )
            throw invalid_argument("Dimensions must be equal");

        axpy(1.0, *this, -1.0, rhs);
        return *this;
    }



    Visibilities& Visibilities::operator*=(const Visibilities::value_type scalar)
    {
        for (auto ptr = this->begin(); ptr != this->end(); ptr++)
            *ptr *= scalar;

        return *this;
    }



    Visibilities& Visibilities::operator/=(const Visibilities::value_type scalar)
    {

        for (auto ptr = this->begin(); ptr != this->end(); ptr++)
            *ptr /= scalar;

        return *this;
    }



    bool Visibilities::operator==(const Visibilities& rhs) 
    {
        if ( sameDimensions(*this, rhs) == false )
            return false;
        
        auto p = rhs.buffer;
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            if (*elem_ptr != *p)
                return false;
            p++;
        }    
    
        return true;
    }


    bool Visibilities::operator!=(const Visibilities& rhs) 
    {
        return !(*this == rhs);
    }


    // auxiliary functions
    void Visibilities::print(ostream& os) const
    {
        os << "Dimensions: [" << dim[0] << "][" << dim[1] << "][" 
           << dim[2] << "][" << dim[3] << "]" << endl;

        os << "Visibilities = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }



    void Visibilities::print() const
    {
        print(cout);
    }



    ostream& operator<<(ostream& os, const Visibilities& v)
    {
        v.print(os);
        return os;
    }



    void Visibilities::checkParametersInRange(unsigned int baseline, 
                                              unsigned int time, 
                                              unsigned int channel, 
                                              unsigned int polarization) const
    {
        if (baseline >= nr_baselines)
            throw invalid_argument("Baseline index out of range");
        if (time >= nr_time)
            throw invalid_argument("Time index out of range");
        if (channel >= nr_channels)
            throw invalid_argument("Channel index out of range");
        if (polarization >= nr_polarizations)
            throw invalid_argument("Polarization index out of range");
    }



    bool Visibilities::sameDimensions(const Visibilities& x, 
                                      const Visibilities& y)
    {
        if (x.get_nr_baselines() != y.get_nr_baselines())
            return false;
        if (x.get_nr_time() != y.get_nr_time())
            return false;
        if (x.get_nr_channels() != y.get_nr_channels())
            return false;
        if (x.get_nr_polarizations() != y.get_nr_polarizations())
            return false;

        return true;
    }



// // assigment operator not supported, as want data members to be constant
// Visibilities& Visibilities::operator=(const Visibilities& rhs)
// {
//     // self-assignment
//     if (this == &rhs) return *this;
//
//     // free memory associated with buffer
//     delete[] buffer;
//
//     // allocate new memory, and copy data into it
//     auto tmp_buffer = (Visibilities::value_type*) new Visibilities::value_type[rhs.size()];
//     copy(rhs.begin(), rhs.end(), tmp_buffer);
//
//     nr_baselines = rhs.nr_baselines;
//     nr_time = rhs.nr_time;
//     nr_channels = rhs.nr_channels;
//     nr_polarizations = rhs.nr_polarizations;
//    
//     dim = {rhs.nr_baselines,rhs.nr_time,rhs.nr_channels,rhs.nr_polarizations};
//    
//     lead_dim = {rhs.nr_polarizations,rhs.nr_channels*rhs.nr_polarizations,
//                 rhs.nr_time*rhs.nr_channels*rhs.nr_polarizations, 
//                 rhs.nr_baselines*rhs.nr_time*rhs.nr_channels*rhs.nr_polarizations};
//  
//     have_allocated_data = true;
//
//     buffer = tmp_buffer;
//    
//     return *this;
// }


} // namespace idg
