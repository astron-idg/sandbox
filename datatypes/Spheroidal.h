

#ifndef IDG_SPHEROIDAL_H_
#define IDG_SPHEROIDAL_H_

#include <iostream>
#include <cstddef> // size_t
#include <array>

namespace idg {

    class Spheroidal {

    public:
        typedef float value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        explicit Spheroidal(unsigned int subgridsize = 0);
        Spheroidal(value_type *data, unsigned int subgridsize);
        virtual ~Spheroidal();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Spheroidal(const Spheroidal& S);

        // assigment operator; not supported 
        Spheroidal& operator=(const Spheroidal& rhs) = delete;

        value_type* data() const { return buffer; }
        value_type* data(unsigned int y, 
                         unsigned int x) const { 
            return &buffer[x + lead_dim[0]*y]; }

        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return nr_dimensions; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }

        // get data members
        unsigned int get_subgrid_size() const { return subgrid_size; };

        // range checked access
        value_type& get(unsigned int y, unsigned int x);

        void set(unsigned int y, unsigned int x, 
                 value_type value);

        // non-range checked access
        const value_type& operator()(unsigned int y, 
                                     unsigned int x) const
        {
            return buffer[x + lead_dim[0]*y];
        }

        value_type& operator()(unsigned int y, 
                               unsigned int x)
        {
            return buffer[x + lead_dim[0]*y];
        }
        
        // init to constant value (e.g. to zero)
        void init(const value_type a = value_type());

        // operators
        // bool operator==(const Spheroidal& rhs);
        // bool operator!=(const Spheriodal& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 

    private:
        const unsigned int subgrid_size;
        
        const size_type num_elements;
        static const unsigned int nr_dimensions = 2;
        const std::array<size_type, nr_dimensions> dim;
        const std::array<size_type, nr_dimensions> lead_dim;
        value_type* buffer;
        const bool have_allocated_data;
    };


    // helper functions
    std::ostream& operator<<(std::ostream& os, const Spheroidal& S);


} // namespace idg

#endif
