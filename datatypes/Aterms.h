


// #define TYPEDEF_ATERM_TYPE        typedef std::complex<float> ATermType[nr_stations][nr_time][nr_polarizations][subgridsize][subgridsize];


#ifndef IDG_ATERMS_H_
#define IDG_ATERMS_H_

#include <iostream>
#include <complex>
#include <cstddef> // size_t
#include <array>


namespace idg {

    class Aterms {

    public:
        typedef std::complex<float> value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        Aterms(unsigned int nr_stations = 0,   
               unsigned int nr_time = 0, 
               unsigned int nr_polarizations = 0, 
               unsigned int subgridsize = 0);
        Aterms(value_type *data,
               unsigned int nr_stations,
               unsigned int nr_time,
               unsigned int nr_polarizations,
               unsigned int subgridsize);
        virtual ~Aterms();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Aterms(const Aterms& A);

        // assigment operator; not supported
        Aterms& operator=(const Aterms& rhs) = delete;

        value_type* data() const { return buffer; }
        value_type* data(unsigned int station,   
                         unsigned int time, 
                         unsigned int polarization, 
                         unsigned int y,
                         unsigned int x) const { 
            return &buffer[x + lead_dim[0]*y + lead_dim[1]*polarization 
                          + lead_dim[2]*time + lead_dim[3]*station];
        }

        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return nr_dimensions; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }

        // get data members
        unsigned int get_nr_stations() const { return nr_stations; };
        unsigned int get_nr_time() const { return nr_time; };
        unsigned int get_nr_polarizations() const { return nr_polarizations; };
        unsigned int get_subgrid_size() const { return subgrid_size; };

        // range checked access
        value_type& get(unsigned int station,   
                        unsigned int time, 
                        unsigned int polarizations, 
                        unsigned int y,
                        unsigned int x);

        void set(unsigned int station,   
                 unsigned int time, 
                 unsigned int polarizations, 
                 unsigned int y,
                 unsigned int x,
                 value_type value);


        // non-range checked access
        const value_type& operator()(unsigned int station,   
                                     unsigned int time, 
                                     unsigned int polarization, 
                                     unsigned int y,
                                     unsigned int x) const
        {
            return buffer[x + lead_dim[0]*y + lead_dim[1]*polarization 
                          + lead_dim[2]*time + lead_dim[3]*station];
        }

        value_type& operator()(unsigned int station,   
                               unsigned int time, 
                               unsigned int polarization, 
                               unsigned int y,
                               unsigned int x)
        {
            return buffer[x + lead_dim[0]*y + lead_dim[1]*polarization 
                          + lead_dim[2]*time + lead_dim[3]*station];
        }


        // init to constant value (e.g. to zero)
        void init(const value_type a = value_type()); 


        // helpers
        void print(std::ostream& os) const; 
        void print() const; 

    private:
        void checkParametersInRange(unsigned int station, 
                                    unsigned int time, 
                                    unsigned int polarization, 
                                    unsigned int y, 
                                    unsigned int x);

        const unsigned int nr_stations;
        const unsigned int nr_time;
        const unsigned int nr_polarizations;
        const unsigned int subgrid_size;
        
        const size_type num_elements;
        static const unsigned int nr_dimensions = 5;
        const std::array<size_type, nr_dimensions> dim;
        const std::array<size_type, nr_dimensions> lead_dim;
        value_type* buffer;
        const bool have_allocated_data;
    };

    // helper functions
    std::ostream& operator<<(std::ostream& os, const Aterms& A);

} // namespace idg

#endif
