// Subgrids data type

// Class Subgrids

#ifndef IDG_SUBGRIDS_H_
#define IDG_SUBGRIDS_H_

#include <iostream>
#include <complex>
#include <cstddef> // size_t
#include <array>


namespace idg {

    
    class Subgrids {
        
        
    public:
        typedef std::complex<float> value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        Subgrids(unsigned int nr_baselines = 0,
                 unsigned int nr_chunks = 0,
                 unsigned int subgrid_size = 0,
                 unsigned int nr_polarizations = 0);
        Subgrids(value_type *data, 
                 unsigned int nr_baselines,
                 unsigned int nr_chunks,
                 unsigned int subgrid_size,
                 unsigned int nr_polarizations);
        virtual ~Subgrids();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Subgrids(const Subgrids& S);

        // assigment operator; not supported as want array sizes parameters 
        // to be constant; an implementation is in the cpp-file, which 
        // requires the data members to be non-constant
        Subgrids& operator=(const Subgrids& rhs) = delete;

        value_type* data() const { return buffer; }
        value_type* data(unsigned int baseline, 
                         unsigned int chunk, 
                         unsigned int y, 
                         unsigned int x, 
                         unsigned int polarization) const { 
            return &buffer[polarization + lead_dim[0]*x + 
                           lead_dim[1]*y + lead_dim[2]*chunk + 
                           lead_dim[3]*baseline]; }

        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return nr_dimensions; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }
        
        // get data members
        unsigned int get_nr_baselines() const { return nr_baselines; };
        unsigned int get_nr_chunks() const { return nr_chunks; };
        unsigned int get_subgrid_size() const { return subgrid_size; };
        unsigned int get_nr_polarizations() const { return nr_polarizations; };

        // range checked access
        value_type& get(unsigned int baseline, 
                        unsigned int chunk, 
                        unsigned int y, 
                        unsigned int x, 
                        unsigned int polarization);

        void set(unsigned int baseline, 
                 unsigned int chunk, 
                 unsigned int y, 
                 unsigned int x, 
                 unsigned int polarization,
                 value_type value);

        // non-range checked access
        const value_type& operator()(unsigned int baseline, 
                                     unsigned int chunk, 
                                     unsigned int y, 
                                     unsigned int x, 
                                     unsigned int polarization) const
        {
            return buffer[polarization + lead_dim[0]*x + 
                          lead_dim[1]*y + lead_dim[2]*chunk + 
                          lead_dim[3]*baseline];
        }

        value_type& operator()(unsigned int baseline, 
                               unsigned int chunk, 
                               unsigned int y, 
                               unsigned int x, 
                               unsigned int polarization)
        {
            return buffer[polarization + lead_dim[0]*x + 
                          lead_dim[1]*y + lead_dim[2]*chunk + 
                          lead_dim[3]*baseline];
        }


        // init to constant value (e.g. to zero)
        void init(const value_type a = value_type()); 

        // operators
        // bool operator==(const Visibilities& rhs);
        // bool operator!=(const Visibilities& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 


    private:

        void checkParametersInRange(unsigned int baseline, 
                                    unsigned int chunk, 
                                    unsigned int y, 
                                    unsigned int x, 
                                    unsigned int polarization) const;
        
        // data
        const unsigned int nr_baselines;
        const unsigned int nr_chunks;
        const unsigned int subgrid_size;
        const unsigned int nr_polarizations;
        
        const size_type num_elements;
        static const unsigned int nr_dimensions = 5;
        const std::array<size_type, nr_dimensions> dim;
        const std::array<size_type, nr_dimensions> lead_dim;
        value_type* buffer;
        const bool have_allocated_data;
    };


    // helper functions
    std::ostream& operator<<(std::ostream& os, const Subgrids& S);


} // namespace idg



#endif
