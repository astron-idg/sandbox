#include <iostream>
#include <stdexcept>

#include "Baselines.h"

using namespace std;

namespace idg {


    Baselines::Baselines(unsigned int b) 
        : nr_baselines(b),
          num_elements((size_type) b),
          buffer(new value_type[b]),
          have_allocated_data(true)  
    { }



    Baselines::Baselines(value_type* data, 
                         unsigned int b) 
        : nr_baselines(b),
          num_elements((size_type) b),
          buffer(data),
          have_allocated_data(false)  
    { }



    Baselines::~Baselines() 
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    Baselines::Baselines(const Baselines& B)
        : nr_baselines(B.nr_baselines),
          num_elements((size_type) B.nr_baselines),
          buffer(new value_type[B.nr_baselines]),
          have_allocated_data(true)  
    {
        copy(B.begin(), B.end(), buffer);
    }


    // access
    Baselines::value_type& Baselines::get(unsigned int baseline_index)
    {
        if (baseline_index >= nr_baselines)
            throw invalid_argument("Baseline index out of range");

        return (*this)(baseline_index);
    }

    
    void Baselines::set(unsigned int baseline_index, value_type baseline)
    {
        if (baseline_index >= nr_baselines)
            throw invalid_argument("Baseline index out of range");

        (*this)(baseline_index) = baseline;
    }
    

    // helpers    

    ostream& operator<<(ostream& os, const Baseline& b) 
    {
        os << "(" << b.station1 << "," << b.station2 << ")"; 
        return os;
    }



    void Baselines::print(ostream& os) const
    {
        os << "Dimensions: [" << nr_baselines << "]" << endl;

        os << "Baselines = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }


    void Baselines::print() const
    {
        print(cout);
    }


    ostream& operator<<(ostream& os, const Baselines& B)
    {
        B.print(os);
        return os;
    }


} // namespace idg
