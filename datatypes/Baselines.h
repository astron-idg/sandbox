

#ifndef IDG_BASELINES_H_
#define IDG_BASELINES_H_

#include <iostream>
#include <cstddef> // size_t


namespace idg {

    struct Baseline {
        int station1, station2;
        
    Baseline(int in_station1=0, int in_station2=0) 
    : station1(in_station1),
      station2(in_station2) { }
    
    };
    

    // Baselines: list of elements of type Baseline
    class Baselines {

    public:
        typedef Baseline value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        explicit Baselines(unsigned int nr_baselines = 0);
        Baselines(value_type *data, unsigned int nr_baselines);
        virtual ~Baselines();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Baselines(const Baselines& B);

        // assigment operator; not supported to enforce pass-by-reference 
        Baselines& operator=(const Baselines& rhs) = delete;

        value_type* data(unsigned int baseline_index=0) const { 
            return &buffer[baseline_index]; }
        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return 1; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }

        // get data members
        unsigned int get_nr_baselines() const { return nr_baselines; };

        // range checked access
        value_type& get(unsigned int baseline_index);
        void set(unsigned int baseline_index, value_type baseline);

        // non-range checked access
        const value_type& operator()(unsigned int baseline_index) const
        { return buffer[baseline_index]; }

        value_type& operator()(unsigned int baseline_index)
        { return buffer[baseline_index]; }

        // operators
        // bool operator==(const Wavenumbers& rhs);
        // bool operator!=(const Wavenumbers& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 

    private:
        const unsigned int nr_baselines;
        
        const size_type num_elements;
        value_type* buffer;
        const bool have_allocated_data;
    };


    // helper functions
    std::ostream& operator<<(std::ostream& os, const Baseline& b);
    std::ostream& operator<<(std::ostream& os, const Baselines& B);

} // namespace idg

#endif
