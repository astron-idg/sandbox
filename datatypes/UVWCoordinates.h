// UVW Coordinates data type

// Class UVWCoordinates 

#ifndef IDG_UVWCOORDINATES_H_
#define IDG_UVWCOORDINATES_H_

#include <iostream>
#include <cstddef> // size_t
#include <array>


namespace idg {

    // Basic structure to store UVW coordinates: {u,v,w}
    struct UVW {
        float u, v, w;

        UVW(float u_=0, float v_=0, float w_=0) { u=u_; v=v_; w=w_; }
        bool operator==(const UVW& r) {
            return (u==r.u && v==r.v && w==r.w);
        }
        bool operator!=(const UVW& r) { return !(*this==r); }
        UVW& operator*=(const float scalar);
    };

    
    // Class to store UVW coordinates for each baseline, time combination
    class UVWCoordinates {

    public:
        typedef UVW value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        UVWCoordinates(unsigned int nr_baselines = 0,   
                       unsigned int nr_time = 0);
        UVWCoordinates(value_type *data, 
                       unsigned int nr_baselines, 
                       unsigned int nr_time);
        virtual ~UVWCoordinates();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        UVWCoordinates(const UVWCoordinates& v);

        // assigment operator; not supported 
        UVWCoordinates& operator=(const UVWCoordinates& rhs) = delete;

        value_type* data() const { return buffer; }
        value_type* data(unsigned int baseline, 
                         unsigned int time) const { 
            return &buffer[time + lead_dim[0]*baseline]; }

        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return nr_dimensions; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }

        // get data members
        unsigned int get_nr_baselines() const { return nr_baselines; };
        unsigned int get_nr_time() const { return nr_time; };

        // range checked access
        value_type& get(unsigned int baseline, 
                        unsigned int time);

        void set(unsigned int baseline, 
                 unsigned int time, 
                 value_type uvw);

        // non-range checked access
        const value_type& operator()(unsigned int baseline, 
                                     unsigned int time) const
        {
            return buffer[time + lead_dim[0]*baseline];
        }

        value_type& operator()(unsigned int baseline, 
                               unsigned int time)
        {
            return buffer[time + lead_dim[0]*baseline];
        }

        // init to constant value (e.g. to zero)
        void init(const value_type a = value_type()); 

        // operators
        // friend UVWCoordinates operator*(const float a, UVWCoordinates& X);
        // friend UVWCoordinates operator*(UVWCoordinates& X, const float a);
        UVWCoordinates& operator*=(const float scalar);
        // bool operator==(const UVWCoordinates& rhs);
        // bool operator!=(const UVWCoordinates& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 

    private:
        const unsigned int nr_baselines;
        const unsigned int nr_time;
        
        const size_type num_elements;
        static const unsigned int nr_dimensions = 2;
        const std::array<size_type, nr_dimensions> dim;
        const std::array<size_type, nr_dimensions> lead_dim;
        value_type* buffer;
        const bool have_allocated_data;
    };

    // helpers
    std::ostream& operator<<(std::ostream& os, const UVW& uvw);
    std::ostream& operator<<(std::ostream& os, const UVWCoordinates& coord);


} // namespace idg

#endif


