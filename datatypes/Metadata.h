

#ifndef IDG_METADATA_H_
#define IDG_METADATA_H_

#include <iostream>
#include <cstddef> // size_t
#include <array>

#include "Baselines.h"


namespace idg {


    struct Coordinate { 
        int x, y; 
        Coordinate(int x_=0, int y_=0) : x(x_), y(y_) {}
    };


    class Metadatum {
    public:
        Metadatum() = default;
        Metadatum(const Metadatum& m) = default;
        ~Metadatum() = default;

        Metadatum(int time, Baseline b, Coordinate c) 
            : time_nr(time),
            baseline(b),
            coordinate(c)
        { }

        // get and set
        const int& get_time() const { return time_nr; }
        const Baseline& get_baseline() const { return baseline; }
        const Coordinate& get_coordinate() const { return coordinate; } 
        void set_time(int t) { time_nr = t; } 
        void set_baseline(Baseline& b) { baseline = b; } 
        void set_coordinate(Coordinate& c) { coordinate = c; } 

    private:
        int time_nr; 
        Baseline baseline; 
        Coordinate coordinate; 
    };


    class Metadata {

    public:
        typedef Metadatum value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        explicit Metadata(unsigned int nr_subgrids = 0);
        Metadata(value_type *data, unsigned int nr_subgrids);
        virtual ~Metadata();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Metadata(const Metadata& M);

        // assigment operator; not supported to enforce pass-by-reference 
        Metadata& operator=(const Metadata& rhs) = delete;

        value_type* data(unsigned int subgrid_index=0) const { 
            return &buffer[subgrid_index]; }
        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return 1; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }

        // get data members
        unsigned int get_subgrids() const { return nr_subgrids; };
        
        // range checked access
        value_type& get(unsigned int subgrid_index);
        void set(unsigned int subgrid_index, value_type metadatum);

        // non-range checked access
        const value_type& operator()(unsigned int subgrid_index) const
        { return buffer[subgrid_index]; }

        value_type& operator()(unsigned int subgrid_index)
        { return buffer[subgrid_index]; }

        // operators
        // bool operator==(const Wavenumbers& rhs);
        // bool operator!=(const Wavenumbers& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 
        
    private:
        const unsigned int nr_subgrids;

        const size_type num_elements;
        value_type* buffer;
        const bool have_allocated_data;
    };

    // helper functions
    std::ostream& operator<<(std::ostream& os, const Coordinate& C);
    std::ostream& operator<<(std::ostream& os, const Metadatum& m);
    std::ostream& operator<<(std::ostream& os, const Metadata& M);

} // namespace idg


#endif
