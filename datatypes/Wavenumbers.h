// Wavenumbers data type

// Class Wavenumbers
// Functionality as float[NR_CHANNELS], but access via array(channel) notation
// instead of array[channel]

// Works similar a std::array or std::vector, 
// but can be initialized with a pointer, 
// if data is already allocated (not freed in this case in destructor); 


#ifndef IDG_WAVENUMBERS_H_
#define IDG_WAVENUMBERS_H_

#include <iostream>
#include <cstddef> // size_t

namespace idg {

    class Wavenumbers {

    public:
        typedef float value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        explicit Wavenumbers(unsigned int nr_channels = 0);
        Wavenumbers(value_type *data, unsigned int nr_channels);
        virtual ~Wavenumbers();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Wavenumbers(const Wavenumbers& v);

        // assigment operator; not supported to enforce pass-by-reference 
        Wavenumbers& operator=(const Wavenumbers& rhs) = delete;

        value_type* data(unsigned int channel=0) const { return &buffer[channel]; }
        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return 1; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }

        // get data members
        unsigned int get_nr_channels() const { return nr_channels; };

        // range checked access
        value_type& get(unsigned int channel);
        void set(unsigned int channel, value_type wavenumber);

        // non-range checked access
        const value_type& operator()(unsigned int channel) const
        { return buffer[channel]; }

        value_type& operator()(unsigned int channel)
        { return buffer[channel]; }

        // operators
        bool operator==(const Wavenumbers& rhs);
        bool operator!=(const Wavenumbers& rhs);

        // helpers
        void print(std::ostream& os) const; 
        void print() const; 

    private:
        const unsigned int nr_channels;

        const size_type num_elements;
        value_type* buffer;
        const bool have_allocated_data;
    };


    // helper functions
    std::ostream& operator<<(std::ostream& os, const Wavenumbers& v);

} // namespece idg

#endif
