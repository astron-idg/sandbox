// Visibilities data type

// Class Visibilities
// Functionality as complex<float>[NR_BASELINES][NR_TIME][NR_CHANNELS][NR_POLARIZATIONS]
// but access via array(baseline, time, channel, polarization) notation
// instead of array[baseline][time][channel][polarization]

// Works similar a std::array or std::vector, 
// but can be initialized with a pointer, 
// if data is already allocated (not freed in this case in destructor); 


#ifndef IDG_VISIBILITIES_H_
#define IDG_VISIBILITIES_H_

#include <iostream>
#include <complex>
#include <cstddef> // size_t
#include <array>

namespace idg {

    class Visibilities {

    public:
        typedef std::complex<float> value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type* iterator;
        typedef const value_type* const_iterator;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;

        iterator begin() { return &buffer[0]; }
        const_iterator begin() const { return &buffer[0]; }
        iterator end() { return &buffer[num_elements]; }
        const_iterator end() const { return &buffer[num_elements]; }

        // constructors and destructor
        Visibilities(unsigned int nr_baselines = 0,   
                     unsigned int nr_time = 0, 
                     unsigned int nr_channels = 0, 
                     unsigned int nr_polarizations = 0);
        Visibilities(value_type *data, 
                     unsigned int nr_baselines, 
                     unsigned int nr_time, 
                     unsigned int nr_channels, 
                     unsigned int nr_polarizations);
        virtual ~Visibilities();

        // move contructor
        // move assignement

        // copy contructor; supported but not recommended (performance)
        Visibilities(const Visibilities& v);

        // assigment operator; not supported as want array sizes parameters 
        // to be constant; an implementation is in the cpp-file, which 
        // requires the data members to be non-constant
        Visibilities& operator=(const Visibilities& rhs) = delete;

        value_type* data() const { return buffer; }
        value_type* data(unsigned int baseline, 
                         unsigned int time, 
                         unsigned int channel, 
                         unsigned int polarization) const { 
            return &buffer[polarization + lead_dim[0]*channel + 
                          lead_dim[1]*time + lead_dim[2]*baseline]; }

        size_type size() const { return num_elements; }
        unsigned int dimensionality() const { return nr_dimensions; }
        bool empty() const { return begin() == end(); }
        size_type bytes() const { return sizeof(value_type)*size(); }
        
        // get data members
        unsigned int get_nr_baselines() const { return nr_baselines; };
        unsigned int get_nr_time() const { return nr_time; };
        unsigned int get_nr_channels() const { return nr_channels; };
        unsigned int get_nr_polarizations() const { return nr_polarizations; };

        // range checked access
        value_type& get(unsigned int baseline, 
                        unsigned int time, 
                        unsigned int channel, 
                        unsigned int polarization);

        void set(unsigned int baseline, 
                 unsigned int time, 
                 unsigned int channel, 
                 unsigned int polarization,
                 value_type visibility);

        // non-range checked access
        const value_type& operator()(unsigned int baseline, 
                                     unsigned int time, 
                                     unsigned int channel, 
                                     unsigned int polarization) const
        {
            return buffer[polarization + lead_dim[0]*channel + 
                          lead_dim[1]*time + lead_dim[2]*baseline];
        }

        value_type& operator()(unsigned int baseline, 
                               unsigned int time, 
                               unsigned int channel, 
                               unsigned int polarization)
        {
            return buffer[polarization + lead_dim[0]*channel + 
                          lead_dim[1]*time + lead_dim[2]*baseline];
        }

        // init to constant value (e.g. to zero)
        void init(const value_type a = value_type()); 

        // operators
        friend Visibilities operator+(const Visibilities& x, const Visibilities& y);
        friend Visibilities operator-(const Visibilities& x, const Visibilities& y);
        friend Visibilities operator*(const std::complex<float> a, Visibilities& X);
        friend Visibilities operator*(Visibilities& X, const std::complex<float> a);
        Visibilities& operator+=(const Visibilities& rhs);
        Visibilities& operator-=(const Visibilities& rhs);
        Visibilities& operator*=(const value_type scalar);
        Visibilities& operator/=(const value_type scalar);
        bool operator==(const Visibilities& rhs);
        bool operator!=(const Visibilities& rhs);
    
        // helpers
        void print(std::ostream& os) const; 
        void print() const; 

        static bool sameDimensions(const Visibilities& x, 
                                   const Visibilities& y);

    private:

        void checkParametersInRange(unsigned int baseline, 
                                    unsigned int time, 
                                    unsigned int channel, 
                                    unsigned int polarization) const;

        // data 
        const unsigned int nr_baselines;
        const unsigned int nr_time;
        const unsigned int nr_channels;
        const unsigned int nr_polarizations;
   
        const size_type num_elements;
        static const unsigned int nr_dimensions = 4;
        const std::array<size_type, nr_dimensions> dim;
        const std::array<size_type, nr_dimensions> lead_dim;
        value_type* buffer;
        const bool have_allocated_data;
    }; 


    // helper functions
    std::ostream& operator<<(std::ostream& os, const Visibilities& v);

    // x = a*x + b*y
    void axpy(std::complex<float> a, Visibilities& x, 
              std::complex<float> b, const Visibilities& y);

    // x(begin:end) = a*x(begin:end)
    void scale(std::complex<float> a, Visibilities::iterator begin, 
               Visibilities::iterator end);
 
} // namespace idg

#endif
