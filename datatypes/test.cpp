#include <iostream>
#include <algorithm> // sort
#include <chrono>

// these header should be included through "idg.h"
#include "Visibilities.h"
#include "Wavenumbers.h"
#include "Grid.h" 
#include "UVWCoordinates.h"
#include "Aterms.h"
#include "Baselines.h"
#include "Spheroidal.h"
#include "Metadata.h"
#include "Subgrids.h"


using namespace std;
using namespace idg;

#define PRINT false

#define NR_BASELINES 4
#define NR_TIME 3
#define NR_CHANNELS 2
#define NR_POLARIZATIONS 1
#define GRIDSIZE 2
#define SUBGRIDSIZE 2
#define NR_CHUNKS 1

// #define NR_BASELINES 2000
// #define NR_TIME 256
// #define NR_CHANNELS 200
// #define NR_POLARIZATIONS 4
// #define GRIDSIZE 2048
// #define SUBGRIDSIZE 32
// #define NR_CHUNKS 32


// this are the array we repace by data types
static complex<float> B[NR_BASELINES][NR_TIME][NR_CHANNELS][NR_POLARIZATIONS];
static float W[NR_CHANNELS];
static complex<float> G[NR_POLARIZATIONS][GRIDSIZE][GRIDSIZE];
// UVWType
// Aterms
// Baselines missing
// Spheroidal
// Metadata missing
static complex<float> SubGrid[NR_BASELINES][NR_CHUNKS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS];


void test_visibilities();
void test_wavenumbers();
void test_grid();
void test_uvw();
void test_aterms();
void test_baselines();
void test_spheroidal();
void test_metadata();
void test_subgrids();


// for timings
typedef chrono::high_resolution_clock Time;
typedef chrono::milliseconds ms;
typedef chrono::duration<float> fsec;



int main(int argc, char *argv[])
{
    test_visibilities();
    test_wavenumbers();
    test_grid();
    test_uvw();
    test_aterms();
    test_baselines();
    test_spheroidal();
    test_metadata();
    test_subgrids();

    return 0;
}




void test_visibilities()
{
    const unsigned int nr_baselines = 4;
    const unsigned int nr_time = 3;
    const unsigned int nr_channels = 2;
    const unsigned int nr_polarizations = 1;

    Visibilities v(nr_baselines, nr_time, nr_channels, nr_polarizations);

    // init values
    for (unsigned int bl = 0; bl < nr_baselines; bl++) 
		for (unsigned int time = 0; time < nr_time; time++) 
			for (unsigned int chan = 0; chan < nr_channels; chan++) 
                for (unsigned int pol = 0; pol < nr_polarizations; pol++) 
					v(bl, time, chan, pol) = complex<float>(chan, pol);

    if (PRINT) {
        cout << "Dimensionality = " << v.dimensionality() << endl;
        cout << "Size = " << v.size() << endl;
        cout << "Bytes = " << v.bytes() << endl;
        cout << "Data_pointer = " << v.data() << endl;
        cout << "Data_pointer = " << v.data(0,0,0,0) << endl;
        cout << v << endl;
    }

    // access
    unsigned int baseline = 0;
    unsigned int time = 0;
    unsigned int channel = 0;
    unsigned int polarization = 0;

    cout << "no range check access:" << endl;
    v(baseline, time, channel, polarization) = complex<float>(1.23, 3.45);

    cout << "v(" << baseline << "," << time << "," << channel << "," 
         << polarization<< ") = " 
         << v(baseline, time, channel, polarization) << endl;

    cout << "range checked access:" << endl;
    v.set(baseline, time, channel, polarization, complex<float>(6.756, 7.783));

    cout << "v(" << baseline << "," << time << "," << channel << "," 
         << polarization<< ") = " 
         << v.get(baseline, time, channel, polarization) << endl;


    // try copy constructor (but avoid passing Visibilities by value, 
    // allways pass-by-reference)
    Visibilities b(v);

    if (PRINT) {
        cout << "Dimensionality = " << b.dimensionality() << endl;
        cout << "Size = " << b.size() << endl;
        cout << "Number baselines = " << b.get_nr_baselines() << endl;
        cout << "Number time = " << b.get_nr_time() << endl;
        cout << "Number channels = " << b.get_nr_channels() << endl;
        cout << "Number polarizations = " << b.get_nr_polarizations() << endl;
        cout << b << endl;
    }
    

    // Call x <- a*x + b*y; here, v <- 0.1*v + 5.3*b 
    // a, b are complex<float> or anything that can be converted to it 
    // like float, double, ...
    axpy(0.1, v, 5.3, b);

    if (PRINT) {
        cout << v << endl;
    }

    // Test overloaded + operator for w = v + b
    // Copies v into w and calls axpy(1.0, w, 1.0, b) internally
    // not recommended to use for performance reasons
    Visibilities w = v + b;

    if (PRINT) {
        cout << w << endl;
    }


    ////// time access and compare to native arrays //////

    Visibilities u(NR_BASELINES, NR_TIME, NR_CHANNELS, NR_POLARIZATIONS);
    
    // Timing for native array
    auto s = Time::now();

    for (unsigned int bl = 0; bl < NR_BASELINES; bl++) 
		for (unsigned int time = 0; time < NR_TIME; time++) 
			for (unsigned int chan = 0; chan < NR_CHANNELS; chan++) 
                for (unsigned int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    B[bl][time][chan][pol] = complex<float>(bl + time, chan + pol);
                }

    auto e = Time::now();
    fsec fs = e - s;
    ms d = chrono::duration_cast<ms>(fs);

    cout << "time (init native array): " << fs.count() << " s, " 
         << d.count() << " ms" << endl;

    // Timing for initialization of visibilities
    s = Time::now();

    for (unsigned int bl = 0; bl < NR_BASELINES; bl++) 
		for (unsigned int time = 0; time < NR_TIME; time++) 
			for (unsigned int chan = 0; chan < NR_CHANNELS; chan++) 
                for (unsigned int pol = 0; pol < NR_POLARIZATIONS; pol++) 
					u(bl, time, chan, pol) = complex<float>(bl + time, chan + pol);

    e = Time::now();
    fs = e - s;
    d = chrono::duration_cast<ms>(fs);
 
    cout << "time (init Visibilities): " << fs.count() << " s, " 
         << d.count() << " ms" << endl;


    // init with pre-allocated array
    // (Note: B is not freed in destructor!)
    Visibilities vis((complex<float>*) B, 
                     NR_BASELINES, 
                     NR_TIME, 
                     NR_CHANNELS, 
                     NR_POLARIZATIONS);

    if (PRINT)
        cout << vis << endl;
    
    // test ==, !=
    cout << "vis == vis:\t" << (vis == vis) << endl;
    cout << "vis == v:\t" << (vis == v) << endl;
    cout << "vis != vis:\t" << (vis != vis) << endl;
    cout << "vis != v:\t" << (vis != v) << endl;

    if (PRINT) {
        cout << (vis += vis) << endl;
        cout << (vis *= 5) << endl;
        cout << (vis /= 5) << endl;
    }    
    
}






void test_wavenumbers()
{
    const unsigned int nr_channels = 5;

    Wavenumbers w(nr_channels);

    // init values
    for (unsigned int channel = 0; channel < nr_channels; channel++) 
        w(channel) = float(channel);

    cout << "Dimensionality = " << w.dimensionality() << endl;
    cout << "Size = " << w.size() << endl;
    cout << w << endl;


    // access
    unsigned int channel = 0;

    cout << "no range check access:" << endl;
    w(channel) = 5.0;
    
    cout << "w(" << channel << ") = " 
         << w(channel) << endl;

    cout << "range checked access:" << endl;
    w.set(channel, 6.0);
    
    cout << "w(" << channel << ") = " 
         << w.get(channel) << endl;


    Wavenumbers w2(w);
    
    cout << w2 << endl;
    cout << "Number channels = " << w2.get_nr_channels() << endl;


    // Timing for native array
    auto s = Time::now();

    for (unsigned int channel = 0; channel < NR_CHANNELS; channel++) 
        W[channel] = float(channel);

    auto e = Time::now();
    fsec fs = e - s;
    ms d = chrono::duration_cast<ms>(fs);

    cout << "time (init native array): " << fs.count() << " s, " 
         << d.count() << " ms" << endl;


    // Timing for initialization of visibilities
    Wavenumbers wave(NR_CHANNELS);

    s = Time::now();

    for (unsigned int channel = 0; channel < NR_CHANNELS; channel++) 
        wave(channel) = float(channel);

    e = Time::now();
    fs = e - s;
    d = chrono::duration_cast<ms>(fs);
 
    cout << "time (init Wavenumbers): " << fs.count() << " s, " 
         << d.count() << " ms" << endl;


    // init with pre-allocated array
    // (Note:  is not freed in destructor!)
    Wavenumbers ww((float*) W, NR_CHANNELS);

    if (PRINT)
        cout << ww << endl;

    cout << "ww == ww: " << (ww == ww) << endl;
    cout << "ww != ww: " << (ww != ww) << endl;    

}





void test_grid()
{
    const unsigned int nr_polarizations = 2;
    const unsigned int gridsize = 2;

    Grid g(nr_polarizations, gridsize);

    // init values
    for (unsigned int pol = 0; pol < nr_polarizations; pol++) 
        for (unsigned int y = 0; y < gridsize; y++) 
            for (unsigned int x = 0; x < gridsize; x++) 
                g(pol, y, x) = complex<float>(pol,x+y);
    
    cout << "Dimensionality = " << g.dimensionality() << endl;
    cout << "Size = " << g.size() << endl;
    cout << g << endl;


    // access
    unsigned int polarization = 0;
    unsigned int y = 0;
    unsigned int x = 0;

    cout << "no range check access:" << endl;
    g(polarization, y, x) = complex<float>(5.0, 6.7765);
    
    cout << "g(" << polarization << "," << y << "," << x << ") = " 
         << g(polarization, y, x) << endl;

    cout << "range checked access:" << endl;
    g.set(polarization, y, x, complex<float>(-225.0, -1.234));
    
    cout << "g(" << polarization << "," << y << "," << x << ") = " 
         << g.get(polarization, y, x) << endl;


    ////// time access and compare to native arrays //////

    Grid grid(NR_POLARIZATIONS, GRIDSIZE);
    
    // Timing for native array
    auto s = Time::now();

    for (unsigned int pol = 0; pol < NR_POLARIZATIONS; pol++) 
        for (unsigned int y = 0; y < GRIDSIZE; y++) 
            for (unsigned int x = 0; x < GRIDSIZE; x++) 
                G[pol][y][x] = complex<float>(pol + 1, y + x);
    

    auto e = Time::now();
    fsec fs = e - s;
    ms d = chrono::duration_cast<ms>(fs);

    cout << "time (init native array): " << fs.count() << " s, " 
         << d.count() << " ms" << endl;

    // Timing for initialization of visibilities
    s = Time::now();

    for (unsigned int pol = 0; pol < NR_POLARIZATIONS; pol++) 
        for (unsigned int y = 0; y < GRIDSIZE; y++) 
            for (unsigned int x = 0; x < GRIDSIZE; x++) 
                grid(pol, y, x) = complex<float>(pol + 1, y + x);

    e = Time::now();
    fs = e - s;
    d = chrono::duration_cast<ms>(fs);
 
    cout << "time (init Grid): " << fs.count() << " s, " 
         << d.count() << " ms" << endl;


    // init with pre-allocated array
    // (Note: G is not freed in destructor!)
    Grid gg((complex<float>*) G, NR_POLARIZATIONS, GRIDSIZE);

    // grid = 3.0*grid + 2.0*gg
    axpy(1.0, grid, 1.0, gg);

    cout << grid << endl;
    cout << gg << endl;

    grid += gg;

    cout << grid << endl;

    grid -= gg;

    cout << grid << endl;
    
}




void test_uvw() 
{
    const unsigned int nr_baselines = 4;
    const unsigned int nr_time = 3;

    UVWCoordinates uvw(nr_baselines, nr_time);

    UVW c(5.6,45.6,455.6);
    uvw.init(c);
    uvw *= 2; // scale by two

    cout << uvw << endl;

    uvw.init(); // set to zero
    // init values
    for (unsigned int bl = 0; bl < nr_baselines; bl++) 
		for (unsigned int time = 0; time < nr_time; time++) 
            uvw(bl, time) = UVW(1.23*bl, 312.21*time, bl+time);

    cout << uvw << endl;
}




void test_aterms()
{
    const unsigned int nr_stations = 2;
    const unsigned int nr_time = 2;
    const unsigned int nr_polarizations = 1;
    const unsigned int subgridsize = 4;

    Aterms A(nr_stations, nr_time, nr_polarizations, subgridsize);

    for (unsigned int station = 0; station < nr_stations; station++) 
		for (unsigned int time = 0; time < nr_time; time++) 
            for (unsigned int pol = 0; pol < nr_polarizations; pol++) 
                for (unsigned int y = 0; y < subgridsize; y++) 
                    for (unsigned int x = 0; x < subgridsize; x++) {
                        A(station, time, pol, y, x) = 
                            complex<float>(y, x);
                    }

    cout << A << endl;

    // more tests here
}



void test_baselines()
{
    const unsigned int nr_baselines = 4;

    Baselines BA(nr_baselines);

    // init data
    for (unsigned int bl = 0; bl < nr_baselines; bl++) 
        BA(bl) = Baseline(bl,bl*bl);

    cout << BA << endl;

    // more tests here
}



void test_spheroidal()
{
    const unsigned int subgrid_size = 2;

    Spheroidal S(subgrid_size);
    S.init(); // set to zero

    for (unsigned int y = 0; y < subgrid_size; y++) 
        for (unsigned int x = 0; x < subgrid_size; x++) 
            S(y,x) = y+x;
   
    cout << S << endl;

    // more test here
}




void test_metadata()
{
    const unsigned int nr_subgrids = 2;

    Metadata M(nr_subgrids);

    for (unsigned int i = 0; i < nr_subgrids; i++) 
    {
        int time = i;
        Baseline b(i,2*i);
        Coordinate c(0,1);
        Metadatum m(time, b, c);
        M(i) = m;
    }
    
    cout << M << endl;

    // more test here
}




void test_subgrids()
{
    const unsigned int nr_baselines = 1;
    const unsigned int nr_chunks = 2;
    const unsigned int subgrid_size = 3;
    const unsigned int nr_polarizations = 1;

    Subgrids S(nr_baselines, nr_chunks, subgrid_size, nr_polarizations);

    for (unsigned int bl = 0; bl < nr_baselines; bl++) 
		for (unsigned int ck = 0; ck < nr_chunks; ck++) 
            for (unsigned int y = 0; y < subgrid_size; y++) 
                for (unsigned int x = 0; x < subgrid_size; x++) 
                    for (unsigned int pol = 0; pol < nr_polarizations; pol++) 
                        S(bl, ck, y, x, pol) = complex<float>(y,x+pol);

    cout << S << endl;

    S.init(5.0);
    cout << S << endl;

    // access
    unsigned int baseline = 0;
    unsigned int chunk = 0;
    unsigned int y = 0;
    unsigned int x = 0;
    unsigned int polarization = 0;

    cout << "no range check access:" << endl;
    S(baseline, chunk, y, x, polarization) = complex<float>(1.23, 3.45);

    cout << "S(" << baseline << "," << chunk << "," << y << "," << x << "," 
         << polarization << ") = " 
         << S(baseline, chunk, y, x, polarization) << endl;

    cout << "range checked access:" << endl;
    S.set(baseline, chunk, y, x, polarization, complex<float>(6.756, 7.783));

    cout << "S(" << baseline << "," << chunk << "," << y << "," << x << "," 
         << polarization << ") = " 
         << S.get(baseline, chunk, y, x, polarization) << endl;

    // more test here
}
