// #define TYPEDEF_SUBGRID_TYPE      typedef std::complex<float> SubGridType[nr_baselines][nr_chunks][subgridsize][subgridsize][nr_polarizations];


#include <stdexcept>

#include "Subgrids.h"

using namespace std;

namespace idg {

    Subgrids::Subgrids(unsigned int bl,
                       unsigned int ck,
                       unsigned int sb,
                       unsigned int p)
        : nr_baselines(bl),
          nr_chunks(ck),
          subgrid_size(sb),
          nr_polarizations(p),
          num_elements((size_type) bl*ck*sb*sb*p),
          dim({bl,ck,sb,sb,p}), 
          lead_dim({p,p*sb,p*sb*sb,p*sb*sb*ck,p*sb*sb*ck*bl}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { }


    Subgrids::Subgrids(value_type *data,
                       unsigned int bl,
                       unsigned int ck,
                       unsigned int sb,
                       unsigned int p)
        : nr_baselines(bl),
          nr_chunks(ck),
          subgrid_size(sb),
          nr_polarizations(p),
          num_elements((size_type) bl*ck*sb*sb*p),
          dim({bl,ck,sb,sb,p}), 
          lead_dim({p,p*sb,p*sb*sb,p*sb*sb*ck,p*sb*sb*ck*bl}),
          buffer(data),
          have_allocated_data(false)  
    { }

    
    Subgrids::~Subgrids() 
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    Subgrids::Subgrids(const Subgrids& S)
        : nr_baselines(S.nr_baselines),
          nr_chunks(S.nr_chunks),
          subgrid_size(S.subgrid_size),
          nr_polarizations(S.nr_polarizations),
          num_elements((size_type) S.nr_baselines*S.nr_chunks*
                       S.subgrid_size*S.subgrid_size*S.nr_polarizations),
          dim({S.nr_baselines,S.nr_chunks,S.subgrid_size,S.subgrid_size,S.nr_polarizations}), 
          lead_dim({S.nr_polarizations,
                    S.nr_polarizations*S.subgrid_size,
                    S.nr_polarizations*S.subgrid_size*S.subgrid_size,
                    S.nr_polarizations*S.subgrid_size*S.subgrid_size*S.nr_chunks,
                    S.nr_polarizations*S.subgrid_size*S.subgrid_size*S.nr_chunks*S.nr_baselines}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { 
        copy(S.begin(), S.end(), buffer);
    }


    
    // access

    Subgrids::value_type& Subgrids::get(unsigned int baseline, 
                                        unsigned int chunk, 
                                        unsigned int y, 
                                        unsigned int x, 
                                        unsigned int polarization)
    {
        checkParametersInRange(baseline, chunk, y, x, polarization);
        return (*this)(baseline, chunk, y, x, polarization);
    }


    void Subgrids::set(unsigned int baseline, 
                       unsigned int chunk, 
                       unsigned int y, 
                       unsigned int x, 
                       unsigned int polarization,
                       value_type value)
    {
        checkParametersInRange(baseline, chunk, y, x, polarization);
        (*this)(baseline, chunk, y, x, polarization) = value;
    }


    // init to constant
    void Subgrids::init(const value_type a) 
    {
        for (auto p = this->begin(); p != this->end(); p++) 
            *p = a;
    } 



    // helpers

    void Subgrids::checkParametersInRange(unsigned int baseline, 
                                          unsigned int chunk, 
                                          unsigned int y, 
                                          unsigned int x, 
                                          unsigned int polarization) const
    {
        if (baseline >= nr_baselines)
            throw invalid_argument("Baseline index out of range");
        if (chunk >= nr_chunks)
            throw invalid_argument("Chunk index out of range");
        if (y >= subgrid_size || x >= subgrid_size)
            throw invalid_argument("Subgrid index out of range");
        if (polarization >= nr_polarizations)
            throw invalid_argument("Polarization index out of range");
    }


    void Subgrids::print(ostream& os) const
    {
        os << "Dimensions: [" << dim[0] << "][" << dim[1] << "][" 
           << dim[2] << "][" << dim[3] << "][" << dim[4] << "]" << endl;

        os << "Subgrids = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }



    void Subgrids::print() const
    {
        print(cout);
    }



    ostream& operator<<(ostream& os, const Subgrids& v)
    {
        v.print(os);
        return os;
    }




} // namespace idg
