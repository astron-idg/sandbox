// #define TYPEDEF_ATERM_TYPE        typedef std::complex<float> ATermType[nr_stations][nr_time][nr_polarizations][subgridsize][subgridsize];

#include <stdexcept>

#include "Aterms.h"

using namespace std;

namespace idg {

    Aterms::Aterms(unsigned int s,   
                   unsigned int t, 
                   unsigned int p, 
                   unsigned int sb)
        : nr_stations(s), 
          nr_time(t), 
          nr_polarizations(p),
          subgrid_size(sb),
          num_elements((size_type) s*t*p*sb*sb),
          dim({s,t,p,sb,sb}), 
          lead_dim({sb,sb*sb,sb*sb*p,sb*sb*p*t,sb*sb*p*t*s}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { }
    

    Aterms::Aterms(value_type *data, 
                   unsigned int s,   
                   unsigned int t, 
                   unsigned int p, 
                   unsigned int sb)
    : nr_stations(s), 
      nr_time(t), 
      nr_polarizations(p),
      subgrid_size(sb),
      num_elements((size_type) s*t*p*sb*sb),
      dim({s,t,p,sb,sb}), 
      lead_dim({sb,sb*sb,sb*sb*p,sb*sb*p*t,sb*sb*p*t*s}),
      buffer(data),
      have_allocated_data(false)  
    { }


    Aterms::~Aterms() 
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    Aterms::Aterms(const Aterms& A) 
        : nr_stations(A.nr_stations), 
          nr_time(A.nr_time), 
          nr_polarizations(A.nr_polarizations),
          subgrid_size(A.subgrid_size),
          num_elements((size_type) A.nr_stations*A.nr_time*A.nr_polarizations
                       *A.subgrid_size*A.subgrid_size),
          dim({A.nr_stations,A.nr_time,A.nr_polarizations,
               A.subgrid_size,A.subgrid_size}), 
      lead_dim({A.subgrid_size,A.subgrid_size*A.subgrid_size,
                A.subgrid_size*A.subgrid_size*A.nr_polarizations,
                A.subgrid_size*A.subgrid_size*A.nr_polarizations*A.nr_time,
                A.subgrid_size*A.subgrid_size*A.nr_polarizations*A.nr_time*
                A.nr_stations}),
      buffer(new value_type[num_elements]),
      have_allocated_data(true)  
    { 
        // copy all elements from v.buffer into buffer
        copy(A.begin(), A.end(), buffer);
    }



    // access
    Aterms::value_type& Aterms::get(unsigned int station,   
                                    unsigned int time, 
                                    unsigned int polarization, 
                                    unsigned int y,
                                    unsigned int x) 
    {
        checkParametersInRange(station, time, polarization, y, x);
        return (*this)(station, time, polarization, y, x);
    }


    void Aterms::set(unsigned int station,   
                     unsigned int time, 
                     unsigned int polarization, 
                     unsigned int y,
                     unsigned int x,
                     value_type value)
    {
        checkParametersInRange(station, time, polarization, y, x);
        (*this)(station, time, polarization, y, x) = value;
    }


    // init to constant value (e.g. to zero)
    void Aterms::init(const value_type a) 
    {
        for (auto ptr = this->begin(); ptr != this->end(); ptr++)
            *ptr = a;
    } 

    // operators
    // bool operator==(const Visibilities& rhs);
    // bool operator!=(const Visibilities& rhs);


    // helpers

    void Aterms::print(ostream& os) const
    {
        os << "Dimensions: [" << dim[0] << "][" << dim[1] << "][" 
           << dim[2] << "][" << dim[3] << "][" << dim[4] << "]" << endl;

        os << "Aterms = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }


    void Aterms::print() const
    {
        print(cout);
    }


    ostream& operator<<(ostream& os, const Aterms& v)
    {
        v.print(os);
        return os;
    }


    void Aterms::checkParametersInRange(unsigned int station, 
                                        unsigned int time, 
                                        unsigned int polarization, 
                                        unsigned int y, 
                                        unsigned int x)
    {
        if (station >= nr_stations)
            throw invalid_argument("Baseline index out of range");
        if (time >= nr_time)
            throw invalid_argument("Time index out of range");
        if (polarization >= nr_polarizations)
            throw invalid_argument("Polarization index out of range");
        if (x >= subgrid_size || y >= subgrid_size)
            throw invalid_argument("Subgrid index index out of range");
    }




} // namespace idg
