#include <stdexcept>

#include "Wavenumbers.h"

using namespace std;

namespace idg {


Wavenumbers::Wavenumbers(unsigned int channels) 
    : nr_channels(channels),
      num_elements((size_type) channels),
      buffer(new value_type[channels]),
      have_allocated_data(true)  
{ }



Wavenumbers::Wavenumbers(value_type* data, 
                         unsigned int channels) 
    : nr_channels(channels),
      num_elements((size_type) channels),
      buffer(data),
      have_allocated_data(false)  
{ }



Wavenumbers::~Wavenumbers() 
{
    if (have_allocated_data) {
        delete[] buffer;
    }
}



Wavenumbers::Wavenumbers(const Wavenumbers& v) 
    : nr_channels(v.nr_channels), 
      num_elements((size_type) v.nr_channels),
      buffer(new value_type[v.nr_channels]),
      have_allocated_data(true)  
{ 
    // copy all elements from v.buffer into buffer
    copy(v.begin(), v.end(), buffer);
}



Wavenumbers::value_type& Wavenumbers::get(unsigned int channel)
{
    if (channel >= nr_channels)
        throw invalid_argument("Channel index out of range");

    return (*this)(channel);
}



void Wavenumbers::set(unsigned int channel, 
                                          value_type wavenumber)
{
    if (channel >= nr_channels)
        throw invalid_argument("Channel index out of range");

    (*this)(channel) = wavenumber;
}



bool Wavenumbers::operator==(const Wavenumbers& rhs) 
{
    if (nr_channels != rhs.nr_channels)
        return false;
    
    auto p = rhs.buffer;
    for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
        if (*elem_ptr != *p)
            return false;
        p++;
    }    
    
    return true;
}



bool Wavenumbers::operator!=(const Wavenumbers& rhs) 
{
    return !(*this == rhs);
}



// helper functions

void Wavenumbers::print(ostream& os) const
{
    os << "Dimensions: [" << nr_channels << "]" << endl;

    os << "Wavenumbers = [";
    for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
        os << *elem_ptr;
        if (elem_ptr != this->end()-1) os << ",";
    }
    os << "]";
}


void Wavenumbers::print() const
{
    print(cout);
}


ostream& operator<<(ostream& os, const Wavenumbers& v)
{
    v.print(os);
    return os;
}


} // namespace idg
