#include <stdexcept>

#include "Grid.h"

using namespace std;

namespace idg {


    Grid::Grid(unsigned int polarizations,
               unsigned int gridsize) 
        : nr_polarizations(polarizations), grid_size(gridsize),
          num_elements((size_type) polarizations*gridsize*gridsize),
          dim({polarizations,gridsize,gridsize}), 
          lead_dim({gridsize,gridsize*gridsize,gridsize*gridsize*polarizations}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { }



    Grid::Grid(value_type *data, 
               unsigned int polarizations,
               unsigned int gridsize) 
        : nr_polarizations(polarizations), grid_size(gridsize),
          num_elements((size_type) polarizations*gridsize*gridsize),
          dim({polarizations,gridsize,gridsize}), 
          lead_dim({gridsize,gridsize*gridsize,gridsize*gridsize*polarizations}),
          buffer(data),
          have_allocated_data(false)  
    { }



    Grid::~Grid() 
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    Grid::Grid(const Grid& g) 
        : nr_polarizations(g.nr_polarizations), grid_size(g.grid_size),
          num_elements((size_type) g.nr_polarizations*g.grid_size*g.grid_size),
          dim({g.nr_polarizations,g.grid_size,g.grid_size}), 
          lead_dim({g.grid_size,g.grid_size*g.grid_size,
                      g.grid_size*g.grid_size*g.nr_polarizations}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { 
        // copy all elements from v.buffer into buffer
        copy(g.begin(), g.end(), buffer);
    }


    // access

    Grid::value_type& Grid::get(unsigned int polarization, 
                                unsigned int y, 
                                unsigned int x)
    {
        if (polarization >= nr_polarizations)
            throw invalid_argument("Polarization index out of range");
        if (x >= grid_size && y >= grid_size)
            throw invalid_argument("Grid index out of range");

        return (*this)(polarization, y, x);
    }



    void Grid::set(unsigned int polarization, 
                   unsigned int y, 
                   unsigned int x,
                   value_type value)
    {
        if (polarization >= nr_polarizations)
            throw invalid_argument("Polarization index out of range");
        if (x >= grid_size && y >= grid_size)
            throw invalid_argument("Grid index out of range");

        (*this)(polarization, y, x) = value;
    }


    void Grid::init(const value_type a) 
    {
        for (auto ptr = this->begin(); ptr != this->end(); ptr++)
            *ptr = a;
    }


    // arithmetic

    void axpy(complex<float> a, Grid& X, 
              complex<float> b, const Grid& Y)
    {
        if (X.get_nr_polarizations() != Y.get_nr_polarizations())
            throw invalid_argument("Number of polarizations must be the same");
        if (X.get_grid_size() != Y.get_grid_size())
            throw invalid_argument("Grid size must be the same");    

        auto y_ptr = Y.begin();
        for (auto x_ptr = X.begin(); x_ptr != X.end(); x_ptr++) {
            *x_ptr = a * (*x_ptr) + b * (*y_ptr);
            y_ptr++;
        } 
    }



    Grid operator+(const Grid& X, const Grid& Y) 
    {
        if (X.get_nr_polarizations() != Y.get_nr_polarizations())
            throw invalid_argument("Number of polarizations must be the same");
        if (X.get_grid_size() != Y.get_grid_size())
            throw invalid_argument("Grid size must be the same");    

        Grid T(X);
        axpy(1.0, T, 1.0, Y);
        return T;
    }



    Grid operator-(const Grid& X, const Grid& Y) 
    {
        if (X.get_nr_polarizations() != Y.get_nr_polarizations())
            throw invalid_argument("Number of polarizations must be the same");
        if (X.get_grid_size() != Y.get_grid_size())
            throw invalid_argument("Grid size must be the same");    

        Grid T(X);
        axpy(1.0, T, -1.0, Y);
        return T;
    }


    Grid operator*(const complex<float> a, Grid& X)
    {
        Grid T(X);
        for (auto x_ptr = T.begin(); x_ptr != T.end(); x_ptr++) 
            *x_ptr *= a;
    
        return T;
    }


    Grid operator*(Grid& X, const complex<float> a)
    {
        return a*X;
    }


    Grid& Grid::operator+=(const Grid& rhs)
    {
        if (get_nr_polarizations() != rhs.get_nr_polarizations())
            throw invalid_argument("Number of polarizations must be the same");
        if (get_grid_size() != rhs.get_grid_size())
            throw invalid_argument("Grid size must be the same");    

        axpy(1.0, *this, 1.0, rhs);
        return *this;
    }


    Grid& Grid::operator-=(const Grid& rhs)
    {
        if (get_nr_polarizations() != rhs.get_nr_polarizations())
            throw invalid_argument("Number of polarizations must be the same");
        if (get_grid_size() != rhs.get_grid_size())
            throw invalid_argument("Grid size must be the same");    

        axpy(1.0, *this, -1.0, rhs);
        return *this;
    }



    // helper functions

    void Grid::print(ostream& os) const
    {
        os << "Dimensions: [" << dim[0] << "][" << dim[1] << "][" 
           << dim[2] << "]" << endl;

        os << "Grid = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }


    void Grid::print() const
    {
        print(cout);
    }


    ostream& operator<<(ostream& os, const Grid& v)
    {
        v.print(os);
        return os;
    }


} // namespace idg
