#include <stdexcept>

#include "Spheroidal.h"

using namespace std;

namespace idg {


    Spheroidal::Spheroidal(unsigned int s)
        : subgrid_size(s), 
          num_elements((size_type) s*s),
          dim({s,s}), 
          lead_dim({s,s*s}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { }


    Spheroidal::Spheroidal(value_type *data, 
                           unsigned int s)
        : subgrid_size(s), 
          num_elements((size_type) s*s),
          dim({s,s}), 
          lead_dim({s,s*s}),
          buffer(data),
          have_allocated_data(false)  
    { }


    Spheroidal::~Spheroidal()
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    Spheroidal::Spheroidal(const Spheroidal& S)
        : subgrid_size(S.subgrid_size), 
          num_elements((size_type) S.subgrid_size*S.subgrid_size),
          dim({S.subgrid_size,S.subgrid_size}), 
          lead_dim({S.subgrid_size,S.subgrid_size*S.subgrid_size}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    {
        copy(S.begin(), S.end(), buffer);
    }

    

    // access
    Spheroidal::value_type& Spheroidal::get(unsigned int y, unsigned int x)
    {
        if (x >= subgrid_size || y >= subgrid_size)
            throw invalid_argument("Index out of range");

        return (*this)(y,x);
    }


    void Spheroidal::set(unsigned int y, unsigned int x, 
                         value_type value)
    {
        if (x >= subgrid_size || y >= subgrid_size)
            throw invalid_argument("Index out of range");

        (*this)(y,x) = value;
    }


    void Spheroidal::init(const value_type value)
    {
        for (auto p = this->begin(); p != this->end(); p++) 
            *p = value;
    }    


    // helpers

    void Spheroidal::print(ostream& os) const
    {
        os << "Dimensions: [" << subgrid_size << "][" 
           << subgrid_size << "]" << endl;

        os << "Spheroidal = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }


    void Spheroidal::print() const
    {
        print(cout);
    }


    ostream& operator<<(ostream& os, const Spheroidal& S)
    {
        S.print(os);
        return os;
    }



} // namespace idg
