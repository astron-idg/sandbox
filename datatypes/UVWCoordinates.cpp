#include <stdexcept>

#include "UVWCoordinates.h"

using namespace std;

namespace idg {


    UVWCoordinates::UVWCoordinates(unsigned int baselines,   
                                   unsigned int time)
        : nr_baselines(baselines), nr_time(time),
          num_elements((size_type) baselines*time),
          dim({baselines,time}), 
          lead_dim({time,time*baselines}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { }


    UVWCoordinates::UVWCoordinates(value_type *data, 
                                   unsigned int baselines, 
                                   unsigned int time)
        : nr_baselines(baselines), nr_time(time),
          num_elements((size_type) baselines*time),
          dim({baselines,time}), 
          lead_dim({time,time*baselines}),
          buffer(data),
          have_allocated_data(false)  
    { }


    UVWCoordinates::~UVWCoordinates()
    {
        if (have_allocated_data) {
            delete[] buffer;
        }
    }


    UVWCoordinates::UVWCoordinates(const UVWCoordinates& c) 
        : nr_baselines(c.nr_baselines), nr_time(c.nr_time),
          num_elements((size_type) c.nr_baselines*c.nr_time),
          dim({c.nr_baselines,c.nr_time}), 
          lead_dim({c.nr_time,c.nr_time*c.nr_baselines}),
          buffer(new value_type[num_elements]),
          have_allocated_data(true)  
    { 
        // copy all elements from v.buffer into buffer
        copy(c.begin(), c.end(), buffer);
    }


    // access

    UVWCoordinates::value_type& UVWCoordinates::get(unsigned int baseline, 
                                                    unsigned int time) 
    {
        if (baseline >= nr_baselines)
            throw invalid_argument("Baseline index out of range");
        if (time >= nr_time)
            throw invalid_argument("Time index out of range");

        return (*this)(baseline, time);
    }


    void UVWCoordinates::set(unsigned int baseline, 
                             unsigned int time, 
                             value_type uvw) 
    {
        if (baseline >= nr_baselines)
            throw invalid_argument("Baseline index out of range");
        if (time >= nr_time)
            throw invalid_argument("Time index out of range");

        (*this)(baseline, time) = uvw;
    }
   

    // init

    void UVWCoordinates::init(const value_type a) 
    {
        for (auto ptr = this->begin(); ptr != this->end(); ptr++) 
            *ptr = a;
    }    


    // arithmetic

    UVW& UVW::operator*=(const float scalar) 
    {
        this->u *= scalar;
        this->v *= scalar;
        this->w *= scalar;
        return *this;
    }
    
    UVWCoordinates& UVWCoordinates::operator*=(const float scalar)
    {
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++)
            *elem_ptr *= scalar;
        return *this;
    }


    // helpers

    ostream& operator<<(ostream& os, const UVW& uvw) 
    {
        os << "(" << uvw.u << "," << uvw.v << "," << uvw.w << ")";
        return os;
    }


    void UVWCoordinates::print(ostream& os) const
    {
        os << "Dimensions: [" << dim[0] << "][" << dim[1] << "]" << endl;

        os << "UVWCoordinates = [";
        for (auto elem_ptr = this->begin(); elem_ptr != this->end(); elem_ptr++) {
            os << *elem_ptr;
            if (elem_ptr != this->end()-1) os << ",";
        }
        os << "]";
    }


    void UVWCoordinates::print() const
    {
        print(cout);
    }


    ostream& operator<<(ostream& os, const UVWCoordinates& coord)
    {
        coord.print(os);
        return os;
    }


} // namespace idg
