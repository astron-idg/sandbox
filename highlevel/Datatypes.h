/*
 * Datatypes.h
 *
 */

#ifndef IDG_DATATYPES_H_
#define IDG_DATATYPES_H_

#include <iostream>
#include <vector>
#include <utility>

namespace idg {

	using TimeIndex = int;
	using AtermIndex = int;

	template<class T>
	struct UVWCoordinate {T u; T v; T w;};

	template<class T>
	using UVWCoordinates = std::vector<UVWCoordinate<T>>;

	template<class T>
	using Frequencies = std::vector<T>;

	template<class T>
	struct StationPair {T first; T second;};

	template<class T>
	using StationPairs = std::vector<StationPair<T>>;

	using float2 = struct {float real; float imag; };
	using double2 = struct {double real; double imag; };

	template<class T>
	struct Matrix2x2 {T xx; T xy; T yx; T yy;};

	template<class T>
	using Aterm = Matrix2x2<T>;

	template<class T>
	using Visibility = Matrix2x2<T>;

	template<class T>
	using Visibilities = std::vector<Visibility<T>>;

	template<class T>
	using VisibilityGroup = std::vector<Visibility<T>>;

	template<class T>
	using VisibilityGroups = std::vector<VisibilityGroup<T>>;


	template<class T>
	class Grid3D {
	public:
		Grid3D(unsigned int depth=1, unsigned int height=1,
			   unsigned int width=1)
			:  width(width),
			   height(height),
			   depth(depth),
			   delete_buffer(true),
			   buffer(new T[height*width*depth])
		{}

		Grid3D(T* data, unsigned int depth=1,
			   unsigned int height=1, unsigned int width=1)
			:  width(width),
			   height(height),
			   depth(depth),
			   delete_buffer(false),
			   buffer(data)
		{}

		Grid3D(const Grid3D& v) = delete;
		Grid3D& operator=(const Grid3D& rhs) = delete;
		virtual ~Grid3D() { if (delete_buffer) delete[] buffer; }

        T* data(int layer=0) const { return &buffer[layer*height*width]; }
        unsigned int get_width() const { return width; }
        unsigned int get_height() const { return height; }
        unsigned int get_depth() const { return depth; }

        const T& operator()(unsigned int layer,
        		            unsigned int y,
        		            unsigned int x) const
        {
        	return buffer[x + width*y + width*height*layer];
        }

        T& operator()(unsigned int layer,
                      unsigned int y,
                      unsigned int x)
        {
        	return buffer[x + width*y + width*height*layer];
        }

        void init(const T& a) {
    		for (unsigned int z = 0; z < get_depth(); ++z)
    			for (unsigned int y = 0; y < get_height(); ++y)
    				for (unsigned int x = 0; x < get_width(); ++x)
    					(*this)(z, y, x) = a;
        }

	private:
        const unsigned int width;
        const unsigned int height;
        const unsigned int depth;
        const bool delete_buffer;
		T* buffer;
	};


	// auxiliary methods

	template<class T>
	std::ostream& operator<<(std::ostream& os,
							 const UVWCoordinate<T>& c)
	{
		os << "{" << c.u << "," << c.v
		   << "," << c.w << "}";
		return os;
	}


	template<class T>
	std::ostream& operator<<(std::ostream& os,
							 const StationPair<T>& s)
	{
		os << "{" << s.first << "," << s.second << "}";
		return os;
	}


	std::ostream& operator<<(std::ostream& os,
							 const float2& x)
	{
		os << "(" << x.real << "," << x.imag << ")";
		return os;
	}


	std::ostream& operator<<(std::ostream& os,
							 const double2& x)
	{
		os << "(" << x.real << "," << x.imag << ")";
		return os;
	}


	template<class T>
	std::ostream& operator<<(std::ostream& os,
			                 const Matrix2x2<T>& A)
	{
		os << "[" << A.xx << "," << A.xy << ";" << std::endl
			<< " " << A.yx << "," << A.yy << "]";
		return os;
	}


	template<class T>
	std::ostream& operator<<(std::ostream& os,
			                 const Grid3D<T>& grid)
	{
		for (unsigned int z = 0; z < grid.get_depth(); ++z) {
			os << "layer==" << z << ":" << std::endl;
			for (unsigned int y = 0; y < grid.get_height(); ++y) {
				for (unsigned int x = 0; x < grid.get_width(); ++x) {
					os << grid(z,y,x);
					if (x != grid.get_width()-1) {
						os << ",";
					}
				}
				os << std::endl;
			}
		}
		return os;
	}


	float2 operator*(const float2& x,
			         const float2& y)
	{
		return {x.real*y.real - x.imag*y.imag,
		        x.real*y.imag + x.imag*y.real};
	}

	float2 operator+(const float2& x,
				     const float2& y)
	{
		return {x.real + y.real, x.imag + y.imag};
	}

	double2 operator*(const double2& x,
			          const double2& y)
	{
		return {x.real*y.real - x.imag*y.imag,
		        x.real*y.imag + x.imag*y.real};
	}

	double2 operator+(const double2& x,
				      const double2& y)
	{
		return {x.real + y.real, x.imag + y.imag};
	}


	template<class T>
	Matrix2x2<T> operator*(const Matrix2x2<T>& A,
			               const Matrix2x2<T>& B)
	{
		return {A.xx*B.xx + A.xy*B.yx,
				A.xx*B.xy + A.xy*B.yy,
				A.yx*B.xx + A.yy*B.yx,
				A.yx*B.xy + A.yy*B.yy};
	}

	template<class T>
	Matrix2x2<T> operator+(const Matrix2x2<T>& A,
				           const Matrix2x2<T>& B)
	{
		return {A.xx + B.xx, A.xy + B.xy,
				A.yx + B.yx, A.yy + B.yy};
	}

}

#endif /* DATATYPES_H_ */
