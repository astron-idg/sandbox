/*
 * main.cpp
 *
 */

#include <iostream>
#include "Datatypes.h"

using namespace std;
using namespace idg;


int main(int argc, char **argv) {

	int nr_channels = 3;
	int nr_groups = 2;


	cout << endl;
	cout << "Using Frequencies" << endl;
	Frequencies<float> f(nr_channels);
	f[0] = 123.4;

	for (auto& x : f) {
		cout << x << endl;
	}


	cout << endl;
	cout << "Using UVWCoordinates" << endl;
	UVWCoordinates<float> uvw(nr_groups);

	for (auto& x : uvw) {
		x = {12.3, 4.5, 9.9};
	}

	for (auto& x : uvw) {
		cout << x << endl;
	}


	cout << endl;
	cout << "Using StationPairs" << endl;
	StationPairs<int> stations(nr_groups);

	for (auto& x : stations) {
		x = {1,6};
	}

	for (auto& x : stations) {
		cout << x << endl;
	}


	cout << endl;
	cout << "Using Visibilities" << endl;
	Visibilities<float2> visibilities(nr_groups);

	for (auto& V : visibilities) {
		V = { {1,0} , {0,0}, {0,0}, {1,0} };
	}

	for (auto& V : visibilities) {
		cout << V << endl;
	}

	cout << visibilities[0]*visibilities[1] << endl;

	cout << endl;
	cout << "Using Grid3D" << endl;
	int nr_polarizations = 4;
	int height = 2;
	int width = 3;
	Grid3D<float2> grid(nr_polarizations, height, width);

	for (unsigned int z = 0; z < grid.get_depth(); ++z) {
		for (unsigned int y = 0; y < grid.get_height(); ++y) {
			for (unsigned int x = 0; x < grid.get_width(); ++x) {
                grid(z, y, x) = {float(z), float(x+y)};
			}
		}
	}

	cout << grid << endl;

	grid.init({0.0});

	cout << grid << endl;
}



