#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "idg.h"

int main(int argc, char **argv) {
    idg::Parameters parameters;
    parameters.set_from_env();

    std::clog << parameters;

    // Proxy example
    idg::proxy::cpu::Reference proxy(parameters);

    // Plan example
    int buffer_size = 256;
    idg::GridderPlan plan(idg::Type::CPU_REFERENCE, buffer_size);
    plan.set_stations(44);
    plan.set_frequencies(0, NULL);
    plan.set_grid(4, 1024, 1024, NULL);
    plan.set_cell_size(0.1 / 1024, 0.1 / 1024);
    plan.set_w_kernel(18);
    plan.bake();

    return EXIT_SUCCESS;
}
