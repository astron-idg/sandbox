Small example showing how to link an external program to the IDG library.

Usage:
```
mkdir build
cd build
cmake .. -DIDG_DIR=<path-to-idg>
make
./linking-example.x
```
