#ifndef UVECTOR_WRAPPER_H
#define UVECTOR_WRAPPER_H

#ifdef HAVE_UVECTOR

#include "uvector_11.h"

#else

#include "uvector_03.h"

#endif // HAVE_UVECTOR

#endif
