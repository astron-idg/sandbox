cmake_minimum_required(VERSION 2.8.7)

project(read-ms.x)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}")

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX ${CMAKE_BINARY_DIR} CACHE PATH "Install prefix" FORCE)
endif()

find_package(CasaCore REQUIRED)
find_package(Boost COMPONENTS filesystem system date_time REQUIRED)

# make sure C++11 is used (in newer CMake use CXX_STANDARD)
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
endif()



include_directories(
    ${CASACORE_INCLUDE_DIR}
)

set (${PROJECT_NAME}_headers
    msprovider.h
    contiguousms.h
    partitionedms.h
    polarizationenum.h
    logger.h
    msselection.h
    multibanddata.h
    banddata.h
    uvector.h
    uvector_03.h
    uvector_11.h
    progressbar.h
    msreader.h
)

set (${PROJECT_NAME}_sources
    main.cpp
    msprovider.cpp
    contiguousms.cpp
    partitionedms.cpp
    logger.cpp
    multibanddata.cpp
    progressbar.cpp
)

# Set build target
add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers} ${${PROJECT_NAME}_sources})

set(LINK_LIRBRARIES
    ${CASACORE_LIBRARIES}
    ${Boost_FILESYSTEM_LIBRARY_RELEASE}
    ${Boost_SYSTEM_LIBRARY_RELEASE}
    ${Boost_DATE_TIME_LIBRARY_RELEASE}
)

target_link_libraries (
    ${PROJECT_NAME}
    ${LINK_LIRBRARIES}
)

# install
install(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib/static
)
