#include <cstdlib>
#include <iostream>
#include <string>

#include "msreader.h"

using namespace std;

int main(int argc, char **argv) {
    // Measurement set parameters
	string msPath;
    string dataColumnName;
    MSSelection selection;
    PolarizationEnum polOut;
    size_t dataDescIndex;
    bool includeModel;

    // Set parameters
    const char *char_ms = getenv("MS");
    if (char_ms) {
        msPath = char_ms;
    } else {
        cerr << "No measurement set provided in MS environemental variable" << endl;
        return EXIT_FAILURE;
    }
    const char *char_datacolumn = getenv("DATACOLUMN");
    dataColumnName = char_datacolumn ? char_datacolumn : "DATA";
    selection = MSSelection::Everything();
    polOut = PolarizationEnum::Instrumental;
    dataDescIndex = 0;
    includeModel = false;

    // Print parameters
    cout << ">> Measurement set parameters:" << endl;
    cout << "/tmsPath:         " << msPath << endl;
    cout << "/tdataColumnName: " << dataColumnName << endl;
    cout << "/tpolOut:         " << "Instrumental" << endl;
    cout << "/tdataDescIndex:  " << dataDescIndex << endl;
    cout << "/tincludeModel:   " << includeModel << endl;
    cout << endl;

    // Create contiguous ms
    cout << ">> Initialize ContiguousMS" << endl;
    ContiguousMS msp(msPath, dataColumnName, selection, polOut, dataDescIndex, includeModel);

    return EXIT_SUCCESS;
}
