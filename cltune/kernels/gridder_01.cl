/*
    Defines
*/
#define NR_CHANNELS         8
#define NR_POLARIZATIONS    4
#define NR_TIMESLOTS        16
#define SUBGRIDSIZE         32
#define GRIDSIZE            512
#define IMAGESIZE           0.1


/*
	Structures
*/
typedef struct { float u, v, w; } UVW;
typedef struct { int x, y; } Coordinate;
typedef struct { int station1, station2; } Baseline;
typedef struct { int baseline_offset; int time_offset; int nr_timesteps;
                 int aterm_index; Baseline baseline; Coordinate coordinate; } Metadata;

/*
	Datatypes
*/
typedef UVW UVWType[1];
typedef float2 VisibilitiesType[1][NR_POLARIZATIONS];
typedef float WavenumberType[NR_CHANNELS];
typedef float2 ATermType[1][NR_TIMESLOTS][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE];
typedef float SpheroidalType[SUBGRIDSIZE][SUBGRIDSIZE];
typedef float2 GridType[NR_POLARIZATIONS][GRIDSIZE][GRIDSIZE];
typedef float2 SubGridType[1][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE];
typedef Metadata MetadataType[1];


/*
    Math
*/
inline float2 cadd(float2 a, float2 b) {
    return (float2) (a.x + b.x, a.y + b.y);
}

inline float2 cmul(float2 a, float2 b) {
    return (float2) (a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

inline float2 cexp(float ang) {
    return (float2) (native_cos(ang), native_sin(ang));
}


inline float2 conj(float2 z) {
    return (float2) (z.x, -z.y);
}

inline void atomic_add_float(volatile __global float *a, const float b) {
    union {
        unsigned int intVal;
        float floatVal;
    } newVal;
    union {
        unsigned int intVal;
        float floatVal;
    } prevVal;
    do {
        prevVal.floatVal = *a;
        newVal.floatVal = prevVal.floatVal + b;
    } while (atomic_cmpxchg((volatile __global unsigned int *)a, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}

inline void atomicAdd(__global float2 *a, float2 b) {
    __global float *a_ptr = (__global float *) a;
    atomic_add_float(a_ptr + 0, b.x);
    atomic_add_float(a_ptr + 1, b.y);
}

inline float2 clConjf(float2 a) {
    return (float2) (a.x, -a.y);
}

inline void apply_aterm(
    const float2 aXX1, const float2 aXY1, const float2 aYX1, const float2 aYY1,
    const float2 aXX2, const float2 aXY2, const float2 aYX2, const float2 aYY2,
          float2 *uvXX,      float2 *uvXY,      float2 *uvYX,      float2 *uvYY
) {
    // Apply aterm to subgrid: P*A1
    // [ uvXX, uvXY;    [ aXX1, aXY1;
    //   uvYX, uvYY ] *   aYX1, aYY1 ]
    float2 pixelsXX = *uvXX;
    float2 pixelsXY = *uvXY;
    float2 pixelsYX = *uvYX;
    float2 pixelsYY = *uvYY;
    *uvXX  = cmul(pixelsXX, aXX1);
    *uvXX += cmul(pixelsXY, aYX1);
    *uvXY  = cmul(pixelsXX, aXY1);
    *uvXY += cmul(pixelsXY, aYY1);
    *uvYX  = cmul(pixelsYX, aXX1);
    *uvYX += cmul(pixelsYY, aYX1);
    *uvYY  = cmul(pixelsYX, aXY1);
    *uvYY += cmul(pixelsYY, aYY1);

    // Apply aterm to subgrid: A2^H*P
    // [ aXX2, aYX1;      [ uvXX, uvXY;
    //   aXY1, aYY2 ]  *    uvYX, uvYY ]
    pixelsXX = *uvXX;
    pixelsXY = *uvXY;
    pixelsYX = *uvYX;
    pixelsYY = *uvYY;
    *uvXX  = cmul(pixelsXX, aXX2);
    *uvXX += cmul(pixelsYX, aYX2);
    *uvXY  = cmul(pixelsXY, aXX2);
    *uvXY += cmul(pixelsYY, aYX2);
    *uvYX  = cmul(pixelsXX, aXY2);
    *uvYX += cmul(pixelsYX, aYY2);
    *uvYY  = cmul(pixelsXY, aXY2);
    *uvYY += cmul(pixelsYY, aYY2);
}

/*
	Kernel
*/
__kernel void kernel_gridder(
    const float w_offset,
    const int nr_channels,
    __global const UVWType			uvw,
    __global const WavenumberType	wavenumbers,
    __global const VisibilitiesType	visibilities,
    __global const SpheroidalType	spheroidal,
    __global const ATermType		aterm,
    __global const MetadataType		metadata,
    __global       SubGridType      subgrid
	) {
	int tidx = get_local_id(0);
	int tidy = get_local_id(1);
	int tid = tidx + tidy * get_local_size(0);
    int blocksize = get_local_size(0) * get_local_size(1);
    int s = get_group_id(0);

    __local float4 _visibilities[MAX_NR_TIMESTEPS][NR_CHANNELS][NR_POLARIZATIONS/2];
    __local float4 _uvw[MAX_NR_TIMESTEPS];
    __local float  _wavenumbers[NR_CHANNELS];

    // Set subgrid to zero
    for (int i = tid; i < SUBGRIDSIZE * SUBGRIDSIZE; i += blocksize) {
        subgrid[s][0][0][i] = (float2) (0, 0);
        subgrid[s][1][0][i] = (float2) (0, 0);
        subgrid[s][2][0][i] = (float2) (0, 0);
        subgrid[s][3][0][i] = (float2) (0, 0);
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    // Load metadata for first subgrid
    const Metadata m_0 = metadata[0];

    // Load metadata for current subgrid
	const Metadata m = metadata[s];
    const int time_offset_global = (m.baseline_offset - m_0.baseline_offset) + (m.time_offset - m_0.time_offset);
    const int nr_timesteps = m.nr_timesteps;
	const int aterm_index = m.aterm_index;
	const int station1 = m.baseline.station1;
	const int station2 = m.baseline.station2;
	const int x_coordinate = m.coordinate.x;
	const int y_coordinate = m.coordinate.y;

    // Load wavenumbers
    for (int i = tid; i < NR_CHANNELS; i += blocksize) {
        _wavenumbers[i] = wavenumbers[i];
    }

    // Iterate all timesteps
    int current_nr_timesteps = MAX_NR_TIMESTEPS;
    for (int time_offset_local = 0; time_offset_local < nr_timesteps; time_offset_local += current_nr_timesteps) {
        current_nr_timesteps = nr_timesteps - time_offset_local < MAX_NR_TIMESTEPS ?
                               nr_timesteps - time_offset_local : MAX_NR_TIMESTEPS;

        barrier(CLK_LOCAL_MEM_FENCE);

        // Load UVW
        for (int time = tid; time < current_nr_timesteps; time += blocksize) {
            UVW a = uvw[time_offset_global + time_offset_local + time];
            _uvw[time] = (float4) (a.u, a.v, a.w, 0);
        }

        // Load visibilities
	    for (int i = tid; i < current_nr_timesteps * NR_CHANNELS; i += blocksize) {
            int time = i / NR_CHANNELS;
            int chan = i % NR_CHANNELS;
            int index = (time_offset_global + time_offset_local + time) * nr_channels + chan;
            float2 a = visibilities[index][0];
            float2 b = visibilities[index][1];
            float2 c = visibilities[index][2];
            float2 d = visibilities[index][3];
            _visibilities[0][i][0] = (float4) (a.x, a.y, b.x, b.y);
            _visibilities[0][i][1] = (float4) (c.x, c.y, d.x, d.y);
        }

        barrier(CLK_LOCAL_MEM_FENCE);

        // Compute u and v offset in wavelenghts
        float u_offset = (x_coordinate + SUBGRIDSIZE/2 - GRIDSIZE/2) / IMAGESIZE * 2 * M_PI;
        float v_offset = (y_coordinate + SUBGRIDSIZE/2 - GRIDSIZE/2) / IMAGESIZE * 2 * M_PI;

        // Iterate all pixels in subgrid
        for (int i = tid; i < SUBGRIDSIZE * SUBGRIDSIZE; i += blocksize) {
            int y = i / SUBGRIDSIZE;
            int x = i % SUBGRIDSIZE;

            // Private subgrid points
            float2 uvXX = (float2) (0, 0);
            float2 uvXY = (float2) (0, 0);
            float2 uvYX = (float2) (0, 0);
            float2 uvYY = (float2) (0, 0);

            // Compute l,m,n
            float l = (x-(SUBGRIDSIZE/2)) * IMAGESIZE/SUBGRIDSIZE;
            float m = (y-(SUBGRIDSIZE/2)) * IMAGESIZE/SUBGRIDSIZE;
            float n = 1.0f - (float) sqrt(1.0 - (double) (l * l) - (double) (m * m));

            // Iterate all timesteps
            for (int time = 0; time < current_nr_timesteps; time++) {
                 // Load UVW coordinates
                float u = _uvw[time].x;
                float v = _uvw[time].y;
                float w = _uvw[time].z;

                // Compute phase index
                float phase_index = u*l + v*m + w*n;

                // Compute phase offset
                float phase_offset = u_offset*l + v_offset*m + w_offset*n;

                // Compute phasor
                #pragma unroll
                for (int chan = 0; chan < NR_CHANNELS; chan++) {
                    float wavenumber = _wavenumbers[chan];
                    float phase = phase_offset - (phase_index * wavenumber);
                    float2 phasor = (float2) (native_cos(phase), native_sin(phase));

                    // Load visibilities from shared memory
                    float4 a = _visibilities[time][chan][0];
                    float4 b = _visibilities[time][chan][1];
                    float2 visXX = (float2) (a.x, a.y);
                    float2 visXY = (float2) (a.z, a.w);
                    float2 visYX = (float2) (b.x, b.y);
                    float2 visYY = (float2) (b.z, b.w);

                    // Multiply visibility by phasor
                    uvXX.x += phasor.x * visXX.x;
                    uvXX.y += phasor.x * visXX.y;
                    uvXX.x -= phasor.y * visXX.y;
                    uvXX.y += phasor.y * visXX.x;

                    uvXY.x += phasor.x * visXY.x;
                    uvXY.y += phasor.x * visXY.y;
                    uvXY.x -= phasor.y * visXY.y;
                    uvXY.y += phasor.y * visXY.x;

                    uvYX.x += phasor.x * visYX.x;
                    uvYX.y += phasor.x * visYX.y;
                    uvYX.x -= phasor.y * visYX.y;
                    uvYX.y += phasor.y * visYX.x;

                    uvYY.x += phasor.x * visYY.x;
                    uvYY.y += phasor.x * visYY.y;
                    uvYY.x -= phasor.y * visYY.y;
                    uvYY.y += phasor.y * visYY.x;
                }
            }

            // Get a term for station1
            float2 aXX1 = aterm[aterm_index][station1][y][x][0];
            float2 aXY1 = aterm[aterm_index][station1][y][x][1];
            float2 aYX1 = aterm[aterm_index][station1][y][x][2];
            float2 aYY1 = aterm[aterm_index][station1][y][x][3];

            // Get aterm for station2
            float2 aXX2 = conj(aterm[aterm_index][station2][y][x][0]);
            float2 aXY2 = conj(aterm[aterm_index][station2][y][x][1]);
            float2 aYX2 = conj(aterm[aterm_index][station2][y][x][2]);
            float2 aYY2 = conj(aterm[aterm_index][station2][y][x][3]);

            // Apply aterm
            apply_aterm(
                aXX1,   aXY1,  aYX1,  aYY1,
                aXX2,   aXY2,  aYX2,  aYY2,
                &uvXX, &uvXY, &uvYX, &uvYY);

            // Load spheroidal
            float sph = spheroidal[y][x];

            // Compute shifted position in subgrid
            int x_dst = (x + (SUBGRIDSIZE/2)) % SUBGRIDSIZE;
            int y_dst = (y + (SUBGRIDSIZE/2)) % SUBGRIDSIZE;

            // Apply spheroidal and update uv grid
            subgrid[s][0][y_dst][x_dst] += uvXX * sph;
            subgrid[s][1][y_dst][x_dst] += uvXY * sph;
            subgrid[s][2][y_dst][x_dst] += uvYX * sph;
            subgrid[s][3][y_dst][x_dst] += uvYY * sph;
        }
    }
}
