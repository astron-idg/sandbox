#include <vector>
#include <chrono>
#include <random>
#include <complex>

#include "cltune.h"
#include "initialize/Init.h"
#include "initialize/Plan.h"

#define MAX_LOCAL_SIZE 512

// Contstraint helper functions
bool IsValidLocalSize(size_t a) {
    return (a <= MAX_LOCAL_SIZE) ? true : false;
};

bool IsMultiple(size_t a, size_t b) {
    return ((a/b)*b == a) ? true : false;
};

// Functions to compute  flops and bytes
uint64_t flops(idg::Parameters &parameters, int nr_subgrids) {
    int subgridsize = parameters.get_subgrid_size();
    int nr_time = parameters.get_nr_time();
    int nr_channels = parameters.get_nr_channels();
    int nr_polarizations = parameters.get_nr_polarizations();
    uint64_t flops = 0;
    flops += 1ULL * nr_time * 5; // phase index
    flops += 1ULL * nr_time * 5; // phase offset
    flops += 1ULL * nr_time * nr_channels * 2; // phase
    flops += 1ULL * nr_time * nr_channels * (nr_polarizations * 8); // update
    flops += 1ULL * nr_polarizations * 30; // aterm
    flops += 1ULL * nr_polarizations * 2; // spheroidal
    flops += 1ULL * nr_polarizations * 6; // shift
    return flops * nr_subgrids * subgridsize * subgridsize;
}

int main() {
   // Parameters
    std::clog << ">>> Configuration"  << std::endl;
    idg::Parameters parameters;
    parameters.set_from_env();

    // Retrieve parameters
    int nr_stations      = parameters.get_nr_stations();
    int nr_baselines     = parameters.get_nr_baselines();
    int nr_time          = parameters.get_nr_time();
    int nr_timeslots     = parameters.get_nr_timeslots();
    int nr_channels      = parameters.get_nr_channels();
    int gridsize         = parameters.get_grid_size();
    int subgridsize      = parameters.get_subgrid_size();
    float imagesize      = parameters.get_imagesize();
    int nr_polarizations = 4;
    float w_offset       = 0;
    int kernel_size      = (subgridsize / 4) + 1;

    // Print configuration
    std::clog << parameters << std::endl;

    // Size of data structures
    auto size_visibilities = 1ULL*nr_baselines*nr_time*nr_channels*nr_polarizations*2;
    auto size_uvw = 1ULL*nr_baselines*nr_time*3;
    auto size_wavenumbers = 1ULL*nr_channels;
    auto size_aterm = 1ULL*nr_stations*nr_timeslots*nr_polarizations*subgridsize*subgridsize*2;
    auto size_spheroidal = 1ULL*subgridsize*subgridsize;
    auto size_grid = 1ULL*nr_polarizations*gridsize*gridsize*2;
    auto size_baselines = 1ULL*nr_baselines*2;

    // Allocate data structures
    std::vector<float> visibilities(size_visibilities);
    std::vector<float> uvw(size_uvw);
    std::vector<float> wavenumbers(size_wavenumbers);
    std::vector<float> aterm(size_aterm);
    std::vector<int>   aterm_offsets(nr_timeslots+1);
    std::vector<float> spheroidal(size_spheroidal);
    std::vector<float> grid(size_grid);
    std::vector<int>   baselines(size_baselines);

    // Populates input data structures
    idg::init_visibilities(visibilities.data(), nr_baselines, nr_time, nr_channels, nr_polarizations);
    idg::init_uvw(uvw.data(), nr_stations, nr_baselines, nr_time);
    idg::init_wavenumbers(wavenumbers.data(), nr_channels);
    idg::init_aterm(aterm.data(), nr_stations, nr_timeslots, nr_polarizations, subgridsize);
    idg::init_aterm_offsets(aterm_offsets.data(), nr_timeslots, nr_time);
    idg::init_spheroidal(spheroidal.data(), subgridsize);
    idg::init_grid(grid.data(), gridsize, nr_polarizations);
    idg::init_baselines(baselines.data(), nr_stations, nr_baselines);

    // Initialize metadata
    idg::Plan plan(parameters, uvw.data(), wavenumbers.data(), baselines.data(), aterm_offsets.data(), kernel_size, nr_time);
    long unsigned int nr_subgrids = plan.get_nr_subgrids();
    auto size_subgrids = 1ULL*nr_subgrids*subgridsize*subgridsize*nr_polarizations*2;
    std::vector<float> subgrid(size_subgrids);
    std::vector<idg::Metadata> metadata_ = plan.copy_metadata(); auto size_metadata = 1ULL*nr_subgrids*sizeof(idg::Metadata)/sizeof(int);
    std::vector<int> metadata(size_metadata);
    memcpy(metadata.data(), metadata_.data(), size_metadata*sizeof(int));

    // Initializes the tuner (platform 0, device 0)
    cltune::Tuner tuner(size_t{0}, size_t{0});

    // Set-up the constraints functions to use
    auto ValidLocalSize = [] (std::vector<size_t> v) { return IsValidLocalSize(v[0] * v[1]); };
    auto MultipleOfX = [] (std::vector<size_t> v) { return IsMultiple(v[0], v[1]); };

    // Add gridder kernel
    auto gridder_01 = std::vector<std::string>{"./kernels/gridder_01.cl"};
    auto id = tuner.AddKernel(gridder_01, "kernel_gridder", {nr_subgrids, 1}, {1, 1});

    // Sets the function's arguments
    tuner.AddArgumentScalar(static_cast<float>(w_offset));
    tuner.AddArgumentScalar(static_cast<int>(nr_channels));
    tuner.AddArgumentInput(uvw);
    tuner.AddArgumentInput(wavenumbers);
    tuner.AddArgumentInput(visibilities);
    tuner.AddArgumentInput(spheroidal);
    tuner.AddArgumentInput(aterm);
    tuner.AddArgumentInput(metadata);
    tuner.AddArgumentOutput(subgrid);

    // Add tuner parameters
    tuner.AddParameter(id, "LS_X", {8, 16, 32, 64, 128, 256});
    tuner.AddParameter(id, "LS_Y", {1, 2, 4, 8, 16});
    tuner.MulLocalSize(id, {"LS_X", "LS_Y"});
    tuner.MulGlobalSize(id, {"LS_X", "LS_Y"});
    tuner.AddParameter(id, "MAX_NR_TIMESTEPS", {16, 32, 64});

    // Add constraint: number of threads in work group can not exceed MAX_LOCAL_SIZE
    tuner.AddConstraint(id, ValidLocalSize, {"LS_X", "LS_Y"});

    // Sets one of the following search methods:
    // 0) Random search
    // 1) Simulated annealing
    // 2) Particle swarm optimisation (PSO)
    // 3) Full search
    auto method = -1;
    auto fraction = 0.5;
    auto search_param = size_t{4};
    if      (method == 0) { tuner.UseRandomSearch(fraction); }
    else if (method == 1) { tuner.UseAnnealing(fraction, static_cast<double>(search_param)); }
    else if (method == 2) { tuner.UsePSO(fraction, static_cast<size_t>(search_param), 0.4, 0.0, 0.4); }
    else                  { tuner.UseFullSearch(); }

    // Starts the tuner
    tuner.Tune();

    // Prints the results to screen
    auto runtime = tuner.PrintToScreen();

    // Also print the performance of the best-case terms of GB/s and GFLOPS
    auto gflops = (flops(parameters, nr_subgrids)/runtime) * 1e-7;
    if (gflops != 0.0) {
        printf("[ -------> ] %.1lf ms or %1.lf GFLOPS\n",
            runtime, gflops);
    }

    return EXIT_SUCCESS;
}
