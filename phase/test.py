#!/usr/bin/env python

import idg
import idg.util as util
from idg.data import Data
import numpy as np
import matplotlib.pyplot as plt
import math
import progressbar

def compute_wavenumbers(
    frequencies):
    speed_of_light = 299792458.0
    return (frequencies * 2 * np.pi) / speed_of_light

######################################################################
# Set parameters
######################################################################
nr_stations      = 2
nr_baselines     = int(nr_stations*(nr_stations-1)/2)
nr_channels      = 16
nr_timesteps     = 8
nr_timeslots     = 1
subgrid_size     = 64
grid_size        = 2048
image_size       = 0.4
cell_size        = image_size / grid_size
integration_time = 1
kernel_size      = int((subgrid_size/2)+1)
nr_correlations  = 4
layout_file      = "LOFAR_lba.txt"
max_nr_timesteps = np.iinfo(np.int32).max
w_offset_in_lambda = 10

######################################################################
# initialize data generator
######################################################################

# Initialize full dataset
data = Data(layout_file)
print(">> Dataset full: ")
data.print_info()

# Determine the maximum suggested grid_size using this dataset
grid_size_max = data.compute_grid_size()
print("maximum grid size: %d" % (grid_size_max))

# Determine the max baseline length for given grid_size
max_uv = data.compute_max_uv(grid_size) # m
print("longest baseline required: %.2f km" % (max_uv * 1e-3))

# Select only baselines up to max_uv meters long
data.limit_max_baseline_length(max_uv)
print(">> Dataset limited to baseline up to %.2f km: " % (max_uv * 1e-3))
data.print_info()

# Restrict the number of baselines to nr_baselines
data.limit_nr_baselines(nr_baselines)
print(">> Dataset limited to %d baselines: " % (nr_baselines))
data.print_info()

# Get remaining parameters
image_size = round(data.compute_image_size(grid_size), 4)
cell_size  = image_size / grid_size


######################################################################
# initialize data
######################################################################
channel_offset  = 0
baseline_offset = 0
time_offset     = 0

uvw            = np.zeros((nr_baselines, nr_timesteps), dtype=idg.uvwtype)
frequencies    = np.zeros((nr_channels), dtype=idg.frequenciestype)
data.get_frequencies(frequencies, nr_channels, image_size, channel_offset)
data.get_uvw(uvw, nr_baselines, nr_timesteps, baseline_offset, time_offset, integration_time)
wavenumbers    = compute_wavenumbers(frequencies)

baselines      = util.get_example_baselines(nr_baselines)

aterms_offsets = util.get_example_aterms_offset(
                    nr_timeslots, nr_timesteps)
shift          = np.zeros(3, dtype=float)


######################################################################
# initialize plan
######################################################################
plan = idg.Plan(
        kernel_size, subgrid_size, grid_size, cell_size,
        frequencies, uvw, baselines, aterms_offsets)
nr_subgrids = plan.get_nr_subgrids()
metadata = np.zeros(nr_subgrids, dtype = idg.metadatatype)
plan.copy_metadata(metadata)


######################################################################
# plot data
######################################################################
# util.plot_uvw_pixels(uvw, frequencies, image_size)
# plt.show()


######################################################################
# compute phase values
######################################################################
nr_values = nr_timesteps*nr_channels*subgrid_size*subgrid_size

# flatten uvw
u_ = uvw['u'].flatten()
v_ = uvw['v'].flatten()
w_ = uvw['w'].flatten()

def reference():
    phase_all = list()
    phasor_real_all = list()
    phasor_imag_all = list()

    bar = progressbar.ProgressBar(maxval=len(metadata))
    bar.start()
    for i, m in enumerate(metadata):
        bar.update(i)

        # subgrid coordinates
        x_coordinate = m['coordinate']['x']
        y_coordinate = m['coordinate']['y']

        # compute u,v,w offset in wavelenghts
        u_offset = (x_coordinate + subgrid_size/2 - grid_size/2) * (2*math.pi / image_size)
        v_offset = (y_coordinate + subgrid_size/2 - grid_size/2) * (2*math.pi / image_size)
        w_offset = 2*math.pi * w_offset_in_lambda

        # compute time offset
        offset = m['time_index']

        # number of timesteps in subgrid
        current_nr_timesteps = m['nr_timesteps']

        # iterate all pixels in subgrid
        for y in range(subgrid_size):
            for x in range(subgrid_size):

                # compute l,m,n
                l = (x+0.5-(subgrid_size/2)) * image_size/subgrid_size
                m = (y+0.5-(subgrid_size/2)) * image_size/subgrid_size
                tmp = (l*l) + (m*m)
                n = tmp / (1.0 + math.sqrt(1.0 - tmp))

                # iterate all timesteps for the current subgrid
                for t in range(current_nr_timesteps):
                    # load uvw coordinate
                    u = u_[offset + t]
                    v = v_[offset + t]
                    w = w_[offset + t]

                    # compute phase index and offset
                    phase_index = u*l + v*m + w*n
                    phase_offset = u_offset*l + v_offset*m + w_offset*n

                    for c in range(nr_channels):
                        # compute phase
                        phase = phase_offset - (phase_index * wavenumbers[c])

                        # compute phasor
                        phasor_real = math.cos(phase)
                        phasor_imag = math.sin(phase)

                        if (x == 0 and y == 0):
                            phase_all.append(phase)
                            phasor_real_all.append(phasor_real)
                            phasor_imag_all.append(phasor_imag)
    bar.update(len(metadata))

    return phase_all, phasor_real_all, phasor_imag_all

def extrapolate():
    phase_all = list()
    phasor_real_all = list()
    phasor_imag_all = list()

    bar = progressbar.ProgressBar(maxval=len(metadata))
    bar.start()
    for i, m in enumerate(metadata):
        bar.update(i)

        # subgrid coordinates
        x_coordinate = m['coordinate']['x']
        y_coordinate = m['coordinate']['y']

        # compute u,v,w offset in wavelenghts
        u_offset = (x_coordinate + subgrid_size/2 - grid_size/2) * (2*math.pi / image_size)
        v_offset = (y_coordinate + subgrid_size/2 - grid_size/2) * (2*math.pi / image_size)
        w_offset = 2*math.pi * w_offset_in_lambda

        # compute time offset
        offset = m['time_index']

        # number of timesteps in subgrid
        current_nr_timesteps = m['nr_timesteps']

        # iterate all pixels in subgrid
        for y in range(subgrid_size):
            for x in range(subgrid_size):

                # compute l,m,n
                l = (x+0.5-(subgrid_size/2)) * image_size/subgrid_size
                m = (y+0.5-(subgrid_size/2)) * image_size/subgrid_size
                tmp = (l*l) + (m*m)
                n = tmp / (1.0 + math.sqrt(1.0 - tmp))

                # iterate all timesteps for the current subgrid
                for t in range(current_nr_timesteps):
                    # load uvw coordinate
                    u = u_[offset + t]
                    v = v_[offset + t]
                    w = w_[offset + t]

                    # compute phase index and offset
                    phase_index = u*l + v*m + w*n
                    phase_offset = u_offset*l + v_offset*m + w_offset*n

                    phase_0 = phase_offset - (phase_index * wavenumbers[0])
                    phase_1 = phase_offset - (phase_index * wavenumbers[1])
                    phase_d = phase_1 - phase_0

                    phase_1 = phase_offset - (phase_index * wavenumbers[nr_channels-1])
                    phase_d = (phase_1 - phase_0) / (nr_channels-1)

                    phasor_0 = complex(math.cos(phase_0), math.sin(phase_0))
                    phasor_d = complex(math.cos(phase_d), math.sin(phase_d))

                    phase_c = phase_0
                    phasor_c = phasor_0

                    for c in range(nr_channels):
                        phase = phase_c
                        phasor_real = phasor_c.real
                        phasor_imag = phasor_c.imag
                        phase_c += phase_d
                        phasor_c *= phasor_d
                        #phasor_real = math.cos(phase)
                        #phasor_imag = math.sin(phase)

                        if (x == 0 and y == 0):
                            phase_all.append(phase)
                            phasor_real_all.append(phasor_real)
                            phasor_imag_all.append(phasor_imag)
    bar.update(len(metadata))

    return phase_all, phasor_real_all, phasor_imag_all


phase_all_ref, phasor_real_all_ref, phasor_imag_all_ref = reference()
phase_all_ext, phasor_real_all_ext, phasor_imag_all_ext = extrapolate()

######################################################################
# plot results
######################################################################
fig, axs = plt.subplots(4)
axs[0].plot(phase_all_ref, ".", label="phase reference")
axs[0].plot(phase_all_ext, ".", label="phase extrapolate")
axs[1].plot(phasor_real_all_ref, ".", label="phasor real reference")
axs[1].plot(phasor_imag_all_ref, ".", label="phasor imag reference")
axs[1].plot(phasor_real_all_ext, ".", label="phasor real extrapolate")
axs[1].plot(phasor_imag_all_ext, ".", label="phasor imag extrapolate")
axs[0].legend()
axs[1].legend()

phase_diff = np.asarray(phase_all_ref) - np.asarray(phase_all_ext)
phasor_real_diff = np.asarray(phasor_real_all_ref) - np.asarray(phasor_real_all_ext)
phasor_imag_diff = np.asarray(phasor_imag_all_ref) - np.asarray(phasor_imag_all_ext)
axs[2].plot(phase_diff, ".", label="phase diff")
axs[3].plot(phasor_real_diff, ".", label="phasor real diff")
axs[3].plot(phasor_imag_diff, ".", label="phasor imag diff")
axs[2].legend()
axs[3].legend()

phase_error = np.sum(np.abs(phase_diff)) / len(phase_diff)
phasor_real_error = np.sum(np.abs(phasor_real_diff)) / len(phasor_real_diff)
phasor_imag_error = np.sum(np.abs(phasor_imag_diff)) / len(phasor_imag_diff)
print("Phase error: ", phase_error)
print("Phasor real error: ", phasor_real_error)
print("Phasor imag error: ", phasor_imag_error)

plt.legend()
plt.show()
