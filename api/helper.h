#include <iostream>

template<typename T>
void get_accuracy(
    const int n,
    const std::complex<T>* A,
    const std::complex<T>* B)
{
    double r_error = 0.0;
    double i_error = 0.0;
    int nnz = 0;

    for (int i = 0; i < n; i++) {
        T r_cmp = A[i].real();
        T i_cmp = A[i].imag();
        T r_ref = B[i].real();
        T i_ref = B[i].imag();
        double r_diff = r_ref - r_cmp;
        double i_diff = i_ref - i_cmp;
        if (abs(B[i]) > 0.0f) {
            nnz++;
            r_error += r_diff * r_diff;
            i_error += i_diff * i_diff;
        }
    }

    r_error /= max(1, nnz);
    i_error /= max(1, nnz);

    auto error = sqrt(r_error + i_error);

    std::cout << "nnz = " << nnz << ", error = " << error << std::endl;
}

template<typename T>
void get_accuracy(
    const int n,
    const T* A,
    const T* B)
{
    double error = 0.0;
    int nnz = 0;

    for (int i = 0; i < n; i++) {
        T cmp = A[i];
        T ref = B[i];
        double diff = ref - cmp;
        if (ref > 0.0f) {
            nnz++;
            error += diff * diff;
        }
    }

    error /= max(1, nnz);
    error = sqrt(error);

    std::cout << "nnz = " << nnz << ", error = " << error << std::endl;
}


void get_accuracy(
    const idg::Grid& A,
    const idg::Grid& B)
{
    get_accuracy<float>(A.size(), A.data(), B.data());
}

template<typename T>
void get_accuracy(
    const idg::Array3D<T>& A,
    const idg::Array3D<T>& B)
{
    get_accuracy<T>(A.size(), A.data(), B.data());
}


template<typename T>
void init_1d(
    const int n,
    std::vector<T>& data)
{
    for (unsigned i = 0; i < n; i++) {
        data[i] = (i - n) / n;
    }
}


template<typename T>
void init_2d(
    const int ydim,
    const int xdim,
    std::vector<T>& data)
{
    for (unsigned y = 0; y < ydim; y++) {
        for (unsigned x = 0; x < xdim; x++) {
            auto value = (y+1.0f) / ydim +
                         (x+1.0f) / xdim;
            size_t idx = y * xdim + x;
            data[idx] = value;
        }
    }
}

template<typename T>
void init_3d(
    const int zdim,
    const int ydim,
    const int xdim,
    std::vector<T>& data)
{
    for (unsigned z = 0; z < zdim; z++) {
        for (unsigned y = 0; y < ydim; y++) {
            auto value = (z+1.0f) / zdim +
                         (y+1.0f) / ydim;

            for (unsigned x = 0; x < xdim; x++) {
                size_t idx = z * ydim * xdim +
                                    y * xdim +
                                             x;
                data[idx] = value / (x + 1);
            }
        }
    }
}

template<typename T>
void init_3d(
    const int zdim,
    const int ydim,
    const int xdim,
    std::complex<T>* data)
{
    for (unsigned z = 0; z < zdim; z++) {
        for (unsigned y = 0; y < ydim; y++) {
            auto real = (z+1.0f) / zdim +
                        (y+1.0f) / ydim;
            auto imag = -real;
            auto value = std::complex<T>(real, imag);

            for (unsigned x = 0; x < xdim; x++) {
                size_t idx = z * ydim * xdim +
                                    y * xdim +
                                             x;
                data[idx] = value / (x + 1);
            }
        }
    }
}

template<typename T>
void init_3d(
    const int zdim,
    const int ydim,
    const int xdim,
    T* data)
{
    for (unsigned z = 0; z < zdim; z++) {
        for (unsigned y = 0; y < ydim; y++) {
            auto value = (z+1.0f) / zdim +
                         (y+1.0f) / ydim;

            for (unsigned x = 0; x < xdim; x++) {
                size_t idx = z * ydim * xdim +
                                    y * xdim +
                                             x;
                data[idx] = value / (x + 1);
            }
        }
    }
}