#include <iostream>
#include <iomanip>
#include <vector>

#include "idg-common.h"
#include "idg-fft.h"

#include <omp.h>

#define ENABLE_VERBOSE_TIMING 1

inline float compute_n(
    float l,
    float m,
    const float* __restrict__ shift)
{
    const float lc = l + shift[0];
    const float mc = m + shift[1];
    const float tmp = (lc * lc) + (mc * mc);
    return tmp > 1.0 ? 1.0 : tmp / (1.0f + sqrtf(1.0f - tmp)) + shift[2];

    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
    // accurately for small values of l and m
    //return tmp > 1.0 ? 1.0 : tmp / (1.0f + sqrtf(1.0f - tmp));
}

#define SET_IMAGE_ARGS const double*, bool, size_t, size_t, float, float, float*, idg::Grid*, std::vector<float>&, std::vector<float>*

void set_image_01(SET_IMAGE_ARGS);
void set_image_02(SET_IMAGE_ARGS);
void set_image_03(SET_IMAGE_ARGS);
void set_image_04(
    SET_IMAGE_ARGS,
    bool apply_wstack_correction);

#define GET_IMAGE_ARGS double*, bool, size_t, size_t, float, float, float*, idg::Grid*, std::vector<float>&

void get_image_01(GET_IMAGE_ARGS);
void get_image_02(GET_IMAGE_ARGS);
void get_image_03(GET_IMAGE_ARGS);
void get_image_04(GET_IMAGE_ARGS);