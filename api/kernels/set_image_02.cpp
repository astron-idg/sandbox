#include "kernels.h"

void set_image_02(
    const double *image,
    bool do_scale,
    size_t m_size,
    size_t m_padded_size,
    float m_w_step,
    float m_cell_size,
    float* m_shift,
    idg::Grid* m_grid,
    std::vector<float>& m_inv_taper,
    std::vector<float>* m_scalar_beam)
{
    double runtime = -omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << std::setprecision(3);
#endif

    const int nr_w_layers = m_grid->get_w_dim();
    const size_t y0 = (m_padded_size - m_size) / 2;
    const size_t x0 = (m_padded_size - m_size) / 2;

    typedef std::complex<float> Grid[nr_w_layers][4][m_padded_size][m_padded_size];
    Grid *grid_ptr = (Grid *) m_grid->data();

    // Convert from stokes to linear into w plane 0
#if ENABLE_VERBOSE_TIMING
    std::cout << "set grid from image" << std::endl;
#endif
    double runtime_copy = -omp_get_wtime();
    m_grid->zero();
#if ENABLE_VERBOSE_TIMING
    std::cout << "copy: " << (*m_scalar_beam)[0] << std::endl;
#endif
#pragma omp parallel for
    for (int y = 0; y < m_size; y++)
    {
        for (int x = 0; x < m_size; x++)
        {
            double scale = do_scale ? 1.0 / (*m_scalar_beam)[m_size * y + x] : 1.0;
            // Stokes I
            (*grid_ptr)[0][0][y + y0][x + x0] = image[m_size * y + x] * scale;
            (*grid_ptr)[0][3][y + y0][x + x0] = image[m_size * y + x] * scale;
            // Stokes Q
            (*grid_ptr)[0][0][y + y0][x + x0] += image[m_size * m_size + m_size * y + x] * scale;
            (*grid_ptr)[0][3][y + y0][x + x0] -= image[m_size * m_size + m_size * y + x] * scale;
            // Stokes U
            (*grid_ptr)[0][1][y + y0][x + x0] = image[2 * m_size * m_size + m_size * y + x] * scale;
            (*grid_ptr)[0][2][y + y0][x + x0] = image[2 * m_size * m_size + m_size * y + x] * scale;
            // Stokes V
            (*grid_ptr)[0][1][y + y0][x + x0].imag(-image[3 * m_size * m_size + m_size * y + x] * scale);
            (*grid_ptr)[0][2][y + y0][x + x0].imag(image[3 * m_size * m_size + m_size * y + x] * scale);
        } // end for x
    }     // end for y
    runtime_copy += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << "runtime:" << runtime_copy << std::endl;
#endif

    // Copy to other w planes and multiply by w term
    double runtime_stacking = -omp_get_wtime();
    for (int w = nr_w_layers - 1; w >= 0; w--)
    {
#if ENABLE_VERBOSE_TIMING
        std::cout << "unstacking w_layer: " << w + 1 << "/" << nr_w_layers << std::endl;
#endif

#pragma omp parallel for
        for (int y = 0; y < m_size; y++)
        {
            for (int x = 0; x < m_size; x++)
            {
                // Compute phase
                const float w_offset = (w + 0.5) * m_w_step;
                const float l = (x - ((int)m_size / 2)) * m_cell_size;
                const float m = (y - ((int)m_size / 2)) * m_cell_size;
                // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
                // accurately for small values of l and m
                const float n = compute_n(l, -m, m_shift);
                float phase = 2 * M_PI * n * w_offset;

                // Compute phasor
                std::complex<float> phasor(std::cos(phase), std::sin(phase));

                // Compute inverse spheroidal
                float inv_taper = m_inv_taper[y] * m_inv_taper[x];

                // Set to current w-plane
#pragma unroll
                for (int pol = 0; pol < 4; pol++)
                {
                    (*grid_ptr)[w][pol][y + y0][x + x0] = (*grid_ptr)[0][pol][y + y0][x + x0] * inv_taper * phasor;
                }
            } // end for x
        }     // end for y
    }         // end for w
    runtime_stacking += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << "w-stacking runtime: " << runtime_stacking << std::endl;
#endif

    // Fourier transform w layers
#if ENABLE_VERBOSE_TIMING
    std::cout << "fft w_layers";
#endif
    int batch = nr_w_layers * 4;
    double runtime_fft = -omp_get_wtime();
    idg::fft2f(batch, m_padded_size, m_padded_size, m_grid->data(0, 0, 0, 0));
    runtime_fft += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << ", runtime: " << runtime_fft << std::endl;
#endif

    // Report overall runtime
    runtime += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << "runtime " << __func__ << ": " << runtime << std::endl;
#endif
};
