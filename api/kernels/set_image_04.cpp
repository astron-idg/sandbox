#include "kernels.h"

using namespace idg;

void set_image_04(
    const double *image,
    bool do_scale,
    size_t m_size,
    size_t m_padded_size,
    float m_w_step,
    float m_cell_size,
    float* m_shift,
    idg::Grid* m_grid,
    std::vector<float>& m_inv_taper,
    std::vector<float>* m_scalar_beam,
    bool apply_wstack_correction)
{
    double runtime = -omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << std::setprecision(3);
#endif

  const int nr_w_layers = m_grid->get_w_dim();
  const size_t y0 = (m_padded_size - m_size) / 2;
  const size_t x0 = (m_padded_size - m_size) / 2;

  // Convert from stokes to linear into w plane 0
#if ENABLE_VERBOSE_TIMING
  std::cout << "set grid from image" << std::endl;
#endif
  double runtime_stacking = -omp_get_wtime();
  m_grid->zero();
#pragma omp parallel
  {
    typedef float arr_float_1D_t[m_size];
    typedef float arr_float_2D_t[NR_CORRELATIONS][m_size];

    arr_float_2D_t& w0_row_real __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_2D_t& w0_row_imag __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_2D_t& w_row_real __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_2D_t& w_row_imag __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_1D_t& inv_tapers __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));
    arr_float_1D_t& phases __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));
    arr_float_1D_t& phasor_real __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));
    arr_float_1D_t& phasor_imag __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));

#pragma omp for
    for (int y = 0; y < m_size; y++) {
      memset(w0_row_real, 0, NR_CORRELATIONS * m_size * sizeof(float));
      memset(w0_row_imag, 0, NR_CORRELATIONS * m_size * sizeof(float));

      const Array3D<double> image_array(const_cast<double*>(image),
                                        NR_CORRELATIONS, m_size, m_size);

      // Copy row of image and convert stokes to polarizations
      for (int x = 0; x < m_size; x++) {
        float scale = do_scale ? (*m_scalar_beam)[m_size * y + x] : 1.0f;
        // Stokes I
        w0_row_real[0][x] = image_array(0, y, x) / scale;
        w0_row_real[3][x] = image_array(0, y, x) / scale;
        // Stokes Q
        w0_row_real[0][x] += image_array(1, y, x) / scale;
        w0_row_real[3][x] -= image_array(1, y, x) / scale;
        // Stokes U
        w0_row_real[1][x] = image_array(2, y, x) / scale;
        w0_row_real[2][x] = image_array(2, y, x) / scale;
        // Stokes V
        w0_row_imag[1][x] = -image_array(3, y, x) / scale;
        w0_row_imag[2][x] = image_array(3, y, x) / scale;

        // Check whether the beam response was so small (or zero) that the
        // result was non-finite. This test is done after having divided the
        // image by the beam, instead of testing the beam itself for zero,
        // because the beam can be unequal to zero and still cause an overflow.
        for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
          if (!std::isfinite(w0_row_real[pol][x]) ||
              !std::isfinite(w0_row_imag[pol][x])) {
            w0_row_real[pol][x] = 0.0;
            w0_row_imag[pol][x] = 0.0;
          }
        }
      }  // end for x

      // Compute inverse spheroidal
      for (int x = 0; x < m_size; x++) {
        inv_tapers[x] = m_inv_taper[y] * m_inv_taper[x];
      }  // end for x

      // Copy to other w planes and multiply by w term
      for (int w = nr_w_layers - 1; w >= 0; w--) {
        // Compute current row of w-plane

        if (!apply_wstack_correction) {
          for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
            for (int x = 0; x < m_size; x++) {
              w_row_real[pol][x] = w0_row_real[pol][x] * inv_tapers[x];
              w_row_imag[pol][x] = w0_row_imag[pol][x] * inv_tapers[x];
            }  // end for x
          }    // end for pol
        } else {
          // Compute phase
          for (int x = 0; x < m_size; x++) {
            const float w_offset = (w + 0.5) * m_w_step;
            const float l = (x - ((int)m_size / 2)) * m_cell_size;
            const float m = (y - ((int)m_size / 2)) * m_cell_size;
            // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
            // accurately for small values of l and m
            const float n = compute_n(l, -m, m_shift);
            phases[x] = 2 * M_PI * n * w_offset;
          }  // end for x

          // Compute phasor
          for (int x = 0; x < m_size; x++) {
            float phase = phases[x];
            phasor_real[x] = cosf(phase);
            phasor_imag[x] = sinf(phase);
          }  // end for x

          // Compute current row of w-plane
          for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
            for (int x = 0; x < m_size; x++) {
              float value_real = w0_row_real[pol][x] * inv_tapers[x];
              float value_imag = w0_row_imag[pol][x] * inv_tapers[x];
              float phasor_real_ = phasor_real[x];
              float phasor_imag_ = phasor_imag[x];
              w_row_real[pol][x] = value_real * phasor_real_;
              w_row_imag[pol][x] = value_real * phasor_imag_;
              w_row_real[pol][x] -= value_imag * phasor_imag_;
              w_row_imag[pol][x] += value_imag * phasor_real_;
            }  // end for x
          }    // end for pol
        }

        // Set m_grid
        for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
          for (int x = 0; x < m_size; x++) {
            float value_real = w_row_real[pol][x];
            float value_imag = w_row_imag[pol][x];
            (*m_grid)(w, pol, y + y0, x + x0) = {value_real, value_imag};
          }  // end for x
        }    // end for pol
      }      // end for w
    }        // end for y
    free(w0_row_real);
    free(w0_row_imag);
    free(w_row_real);
    free(w_row_imag);
    free(inv_tapers);
    free(phases);
    free(phasor_real);
    free(phasor_imag);
  }

  runtime_stacking += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << "w-stacking runtime: " << runtime_stacking << std::endl;
#endif

// Fourier transform w layers
#if ENABLE_VERBOSE_TIMING
  std::cout << "fft w_layers";
#endif
  int batch = nr_w_layers * 4;
  double runtime_fft = -omp_get_wtime();
  fft2f(batch, m_padded_size, m_padded_size, m_grid->data(0, 0, 0, 0));
  runtime_fft += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << ", runtime: " << runtime_fft << std::endl;
#endif

  // Report overall runtime
  runtime += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << "runtime " << __func__ << ": " << runtime << std::endl;
#endif

};
