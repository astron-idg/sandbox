#include "kernels.h"

using namespace idg;

void get_image_04(
    double *image,
    bool do_scale,
    size_t m_size,
    size_t m_padded_size,
    float m_w_step,
    float m_cell_size,
    float* m_shift,
    idg::Grid* m_grid,
    std::vector<float>& m_inv_taper)
{
    double runtime = -omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << std::setprecision(3);
#endif

  const int nr_w_layers = m_grid->get_w_dim();
  const size_t y0 = (m_padded_size - m_size) / 2;
  const size_t x0 = (m_padded_size - m_size) / 2;

  // Fourier transform w layers
#if ENABLE_VERBOSE_TIMING
  std::cout << "ifft w_layers";
#endif
  int batch = nr_w_layers * 4;
  double runtime_fft = -omp_get_wtime();
  idg::ifft2f(batch, m_padded_size, m_padded_size, m_grid->data(0, 0, 0, 0));
  runtime_fft += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << ", runtime: " << runtime_fft << std::endl;
#endif

  // Stack w layers
  double runtime_stacking = -omp_get_wtime();

#pragma omp parallel
  {
    typedef float arr_float_1D_t[m_size];
    typedef float arr_float_2D_t[NR_CORRELATIONS][m_size];

    arr_float_2D_t& w0_row_real __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_2D_t& w0_row_imag __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_2D_t& w_row_real __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_2D_t& w_row_imag __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_2D_t*>(
            aligned_alloc(64, (sizeof(arr_float_2D_t) + 63) & ~64));
    arr_float_1D_t& inv_tapers __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));
    arr_float_1D_t& phases __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));
    arr_float_1D_t& phasor_real __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));
    arr_float_1D_t& phasor_imag __attribute__((aligned(64))) =
        *reinterpret_cast<arr_float_1D_t*>(
            aligned_alloc(64, (sizeof(arr_float_1D_t) + 63) & ~64));

#pragma omp for
    for (int y = 0; y < m_size; y++) {
      Array3D<double> image_array((double*)image, NR_CORRELATIONS, m_size,
                                  m_size);

      // Compute inverse spheroidal
      for (int x = 0; x < m_size; x++) {
        inv_tapers[x] = m_inv_taper[y] * m_inv_taper[x];
      }

      if (m_w_step == 0.0) {
        // Compute current row of w-plane
        for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
          for (int x = 0; x < m_size; x++) {
            w0_row_real[pol][x] = w_row_real[pol][x] * inv_tapers[x];
            w0_row_imag[pol][x] = w_row_imag[pol][x] * inv_tapers[x];
          }  // end for x
        }    // end for pol
      } else {
        memset(w0_row_real, 0, sizeof(w0_row_real));
        memset(w0_row_imag, 0, sizeof(w0_row_imag));

        for (int w = 0; w < nr_w_layers; w++) {
          // Copy current row of w-plane
          for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
            for (int x = 0; x < m_size; x++) {
              auto value = (*m_grid)(w, pol, y + y0, x + x0);
              w_row_real[pol][x] = value.real();
              w_row_imag[pol][x] = value.imag();
            }  // end for pol
          }    // end for x

          // Compute phase
          for (int x = 0; x < m_size; x++) {
            const float w_offset = (w + 0.5) * m_w_step;
            const float l = (x - ((int)m_size / 2)) * m_cell_size;
            const float m = (y - ((int)m_size / 2)) * m_cell_size;
            const float n = compute_n(l, -m, m_shift);
            phases[x] = -2 * M_PI * n * w_offset;
          }

          // Compute phasor
          for (int x = 0; x < m_size; x++) {
            float phase = phases[x];
            phasor_real[x] = cosf(phase);
            phasor_imag[x] = sinf(phase);
          }  // end for x

          // Compute current row of w-plane
          for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
            for (int x = 0; x < m_size; x++) {
              float value_real = w_row_real[pol][x] * inv_tapers[x];
              float value_imag = w_row_imag[pol][x] * inv_tapers[x];
              float phasor_real_ = phasor_real[x];
              float phasor_imag_ = phasor_imag[x];
              w_row_real[pol][x] = value_real * phasor_real_;
              w_row_imag[pol][x] = value_real * phasor_imag_;
              w_row_real[pol][x] -= value_imag * phasor_imag_;
              w_row_imag[pol][x] += value_imag * phasor_real_;
            }  // end for x
          }    // end for pol

          // Add to first w-plane
          for (int pol = 0; pol < NR_CORRELATIONS; pol++) {
            for (int x = 0; x < m_size; x++) {
              w0_row_real[pol][x] += w_row_real[pol][x];
              w0_row_imag[pol][x] += w_row_imag[pol][x];
            }  // end for x
          }    // end for pol
        }
      }  // end for w

      // Copy grid to image
      for (int x = 0; x < m_size; x++) {
        float polXX_real = w0_row_real[0][x];
        float polXY_real = w0_row_real[1][x];
        float polYX_real = w0_row_real[2][x];
        float polYY_real = w0_row_real[3][x];
        float polXY_imag = w0_row_imag[1][x];
        float polYX_imag = w0_row_imag[2][x];
        double stokesI = 0.5 * (polXX_real + polYY_real);
        double stokesQ = 0.5 * (polXX_real - polYY_real);
        double stokesU = 0.5 * (polXY_real + polYX_real);
        double stokesV = 0.5 * (-polXY_imag + polYX_imag);
        image_array(0, y, x) = stokesI;
        image_array(1, y, x) = stokesQ;
        image_array(2, y, x) = stokesU;
        image_array(3, y, x) = stokesV;
      }  // end for x
    }    // end for y
    free(w0_row_real);
    free(w0_row_imag);
    free(w_row_real);
    free(w_row_imag);
    free(inv_tapers);
    free(phases);
    free(phasor_real);
    free(phasor_imag);
  }
  runtime_stacking += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << "w-stacking runtime: " << runtime_stacking << std::endl;
#endif

  // Report overall runtime
  runtime += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
  std::cout << "runtime " << __func__ << ": " << runtime << std::endl;
#endif
}
