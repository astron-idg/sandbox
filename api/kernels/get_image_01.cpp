#include "kernels.h"

void get_image_01(
    double *image,
    bool do_scale,
    size_t m_size,
    size_t m_padded_size,
    float m_w_step,
    float m_cell_size,
    float* m_shift,
    idg::Grid* m_grid,
    std::vector<float>& m_inv_taper)
{
    double runtime = -omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << std::setprecision(3);
#endif

    const int nr_w_layers = m_grid->get_w_dim();
    const size_t y0 = (m_padded_size - m_size) / 2;
    const size_t x0 = (m_padded_size - m_size) / 2;

    // Fourier transform w layers
#if ENABLE_VERBOSE_TIMING
    std::cout << "ifft w_layers";
#endif
    int batch = nr_w_layers * 4;
    double runtime_fft = -omp_get_wtime();
    idg::ifft2f(batch, m_padded_size, m_padded_size, m_grid->data(0, 0, 0, 0));
    runtime_fft += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << ", runtime: " << runtime_fft << std::endl;
#endif

    // Stack w layers
    double runtime_stacking = -omp_get_wtime();
    for (int w = 0; w < nr_w_layers; w++)
    {
#if ENABLE_VERBOSE_TIMING
        std::cout << "stacking w_layer: " << w + 1 << "/" << nr_w_layers << std::endl;
#endif
#pragma omp parallel for
        for (int y = 0; y < m_size; y++)
        {
            for (int x = 0; x < m_size; x++)
            {
                // Compute phase
                const float w_offset = (w + 0.5) * m_w_step;
                const float l = (x - ((int)m_size / 2)) * m_cell_size;
                const float m = (y - ((int)m_size / 2)) * m_cell_size;
                const float n = compute_n(l, -m, m_shift);
                const float phase = -2 * M_PI * n * w_offset;

                // Compute phasor
                std::complex<float> phasor(std::cos(phase), std::sin(phase));

                // Compute inverse spheroidal
                float inv_taper = m_inv_taper[y] * m_inv_taper[x];

                // Apply correction
                (*m_grid)(w, 0, y + y0, x + x0) = (*m_grid)(w, 0, y + y0, x + x0) * inv_taper * phasor;
                (*m_grid)(w, 1, y + y0, x + x0) = (*m_grid)(w, 1, y + y0, x + x0) * inv_taper * phasor;
                (*m_grid)(w, 2, y + y0, x + x0) = (*m_grid)(w, 2, y + y0, x + x0) * inv_taper * phasor;
                (*m_grid)(w, 3, y + y0, x + x0) = (*m_grid)(w, 3, y + y0, x + x0) * inv_taper * phasor;

                // Add to first w-plane
                if (w > 0)
                {
#pragma unroll
                    for (int pol = 0; pol < 4; pol++)
                    {
                        (*m_grid)(0, pol, y + y0, x + x0) += (*m_grid)(w, pol, y + y0, x + x0);
                    }
                }
            } // end for x
        }     // end for y
    }         // end for w
    runtime_stacking += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << "w-stacking runtime: " << runtime_stacking << std::endl;
#endif

    // Copy grid to image
#if ENABLE_VERBOSE_TIMING
    std::cout << "set image from grid";
#endif
    double runtime_copy = -omp_get_wtime();
#pragma omp parallel for
    for (int y = 0; y < m_size; y++)
    {
        for (int x = 0; x < m_size; x++)
        {
            // Stokes I
            image[0 * m_size * m_size + m_size * y + x] = 0.5 * ((*m_grid)(0, 0, y + y0, x + x0).real() + (*m_grid)(0, 3, y + y0, x + x0).real());
            // Stokes Q
            image[1 * m_size * m_size + m_size * y + x] = 0.5 * ((*m_grid)(0, 0, y + y0, x + x0).real() - (*m_grid)(0, 3, y + y0, x + x0).real());
            // Stokes U
            image[2 * m_size * m_size + m_size * y + x] = 0.5 * ((*m_grid)(0, 1, y + y0, x + x0).real() + (*m_grid)(0, 2, y + y0, x + x0).real());
            // Stokes V
            image[3 * m_size * m_size + m_size * y + x] = 0.5 * (-(*m_grid)(0, 1, y + y0, x + x0).imag() + (*m_grid)(0, 2, y + y0, x + x0).imag());

        } // end for x
    }     // end for y
    runtime_copy += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << ", runtime: " << runtime_copy << std::endl;
#endif

    // Report overall runtime
    runtime += omp_get_wtime();
#if ENABLE_VERBOSE_TIMING
    std::cout << "runtime " << __func__ << ": " << runtime << std::endl;
#endif
}
