#include "kernels/kernels.h"
#include "helper.h"

int main(int argc, char** argv)
{
    bool do_scale = true;
    size_t grid_size = 3000;
    size_t padded_grid_size = 4000;
    float w_step = 9;
    float cell_size = 0.1;
    float shift[] = {1.0f, 2.0f, 3.0f};
    int nr_correlations = 4;
    int nr_w_layers = 3;

    std::vector<float> inv_taper(grid_size);
    init_1d<float>(grid_size, inv_taper);

    std::vector<float> scalar_beam(grid_size * grid_size);
    init_2d<float>(grid_size, grid_size, scalar_beam);

    idg::Array3D<double> image_ref(grid_size, grid_size, nr_correlations);
    init_3d<double>(grid_size, grid_size, nr_correlations, image_ref.data());

    idg::Grid grid_ref(nr_w_layers, nr_correlations, padded_grid_size, padded_grid_size);
    idg::Grid grid_opt(nr_w_layers, nr_correlations, padded_grid_size, padded_grid_size);

    std::cout << ">>> set_image_01" << std::endl;
    grid_ref.zero();
    set_image_01(image_ref.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_ref, inv_taper, &scalar_beam);
    std::cout << std::endl;

    std::cout << ">>> set_image_02" << std::endl;
    grid_opt.zero();
    set_image_02(image_ref.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper, &scalar_beam);
    get_accuracy(grid_ref, grid_opt);
    std::cout << std::endl;

    std::cout << ">>> set_image_03" << std::endl;
    grid_opt.zero();
    set_image_03(image_ref.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper, &scalar_beam);
    get_accuracy(grid_ref, grid_opt);
    std::cout << std::endl;

    std::cout << ">>> set_image_04" << std::endl;
    grid_opt.zero();
    set_image_04(image_ref.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper, &scalar_beam, true);
    get_accuracy(grid_ref, grid_opt);
    std::cout << std::endl;

    idg::Array3D<double> image_opt(grid_size, grid_size, nr_correlations);

    std::cout << ">>> get_image_01" << std::endl;
    memcpy(grid_opt.data(), grid_ref.data(), grid_ref.bytes());
    image_ref.zero();
    get_image_01(image_ref.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper);
    std::cout << std::endl;

    std::cout << ">>> get_image_02" << std::endl;
    memcpy(grid_opt.data(), grid_ref.data(), grid_ref.bytes());
    image_opt.zero();
    get_image_02(image_opt.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper);
    get_accuracy<double>(image_ref, image_opt);
    std::cout << std::endl;

    std::cout << ">>> get_image_03" << std::endl;
    memcpy(grid_opt.data(), grid_ref.data(), grid_ref.bytes());
    image_opt.zero();
    get_image_03(image_opt.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper);
    get_accuracy<double>(image_ref, image_opt);
    std::cout << std::endl;

    std::cout << ">>> get_image_04" << std::endl;
    memcpy(grid_opt.data(), grid_ref.data(), grid_ref.bytes());
    image_opt.zero();
    get_image_04(image_opt.data(), do_scale, grid_size, padded_grid_size, w_step, cell_size, shift, &grid_opt, inv_taper);
    get_accuracy<double>(image_ref, image_opt);
    std::cout << std::endl;

    return EXIT_SUCCESS;
}