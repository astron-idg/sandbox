#include <complex>

#define float2 std::complex<float>

void cmul_01(float2 &a, float2 b, float2 c);

void test_01() {
    float2 a = float2(1,2);
    float2 b = float2(3,4);
    float2 c = float2(5,6);

    cmul_01(a, b, c);
    printf("cmul_01: %.0f, %.0f\n", a.real(), a.imag());
}

void cmul_02(float2 &a, float2 b, float2 c);

void test_02() {
    float2 a = float2(1,2);
    float2 b = float2(3,4);
    float2 c = float2(5,6);

    cmul_02(a, b, c);
    printf("cmul_02: %.0f, %.0f\n", a.real(), a.imag());
}

void cmul_03(float2 &a, float2 b, float2 c);

void test_03() {
    float2 a = float2(1,2);
    float2 b = float2(3,4);
    float2 c = float2(5,6);

    cmul_03(a, b, c);
    printf("cmul_03: %.0f, %.0f\n", a.real(), a.imag());
}

void cmul_04(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2);

void test_04() {
    float2 a1 = float2(1,2);
    float2 b1 = float2(3,4);
    float2 c1 = float2(5,6);
    float2 a2 = float2(2,1);
    float2 b2 = float2(4,3);
    float2 c2 = float2(6,5);

    cmul_04(a1, b1, c1, a2, b2, c2);
    printf("cmul_04: %.0f, %.0f, %.0f, %.0f\n", a1.real(), a1.imag(), a2.real(), a2.imag());
}

void cmul_05(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2);

void test_05() {
    float2 a1 = float2(1,2);
    float2 b1 = float2(3,4);
    float2 c1 = float2(5,6);
    float2 a2 = float2(2,1);
    float2 b2 = float2(4,3);
    float2 c2 = float2(6,5);

    cmul_05(a1, b1, c1, a2, b2, c2);
    printf("cmul_05: %.0f, %.0f, %.0f, %.0f\n", a1.real(), a1.imag(), a2.real(), a2.imag());
}

void cmul_06(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2);

void test_06() {
    float2 a1 = float2(1,2);
    float2 b1 = float2(3,4);
    float2 c1 = float2(5,6);
    float2 a2 = float2(2,1);
    float2 b2 = float2(4,3);
    float2 c2 = float2(6,5);

    cmul_06(a1, b1, c1, a2, b2, c2);
    printf("cmul_06: %.0f, %.0f, %.0f, %.0f\n", a1.real(), a1.imag(), a2.real(), a2.imag());
}

void cmul_07(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2);

void test_07() {
    float2 a1 = float2(1,2);
    float2 b1 = float2(3,4);
    float2 c1 = float2(5,6);
    float2 a2 = float2(2,1);
    float2 b2 = float2(4,3);
    float2 c2 = float2(6,5);

    cmul_07(a1, b1, c1, a2, b2, c2);
    printf("cmul_07: %.0f, %.0f, %.0f, %.0f\n", a1.real(), a1.imag(), a2.real(), a2.imag());
}

void cmul_08(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2);

void test_08() {
    float2 a1 = float2(1,2);
    float2 b1 = float2(3,4);
    float2 c1 = float2(5,6);
    float2 a2 = float2(2,1);
    float2 b2 = float2(4,3);
    float2 c2 = float2(6,5);

    cmul_08(a1, b1, c1, a2, b2, c2);
    printf("cmul_08: %.0f, %.0f, %.0f, %.0f\n", a1.real(), a1.imag(), a2.real(), a2.imag());
}

void cmul_09(
    float* a1, float *b1,
    float* a2, float *b2);

void test_09() {
    float a1[2] = {1, 2};
    float a2[2] = {2, 1};
    float b1[4] = {3, 4, 5, 6};
    float b2[4] = {4, 3, 6, 5};

    cmul_09(a1, b1, a2, b2);
    printf("cmul_09: %.0f, %.0f, %.0f, %.0f\n", a1[0], a1[1], a2[0], a2[1]);
}

void cmul_10(
    float* a1, float *b1,
    float* a2, float *b2);

void test_10() {
    float a1[2] = {1, 2};
    float a2[2] = {2, 1};
    float b1[4] = {3, 4, 5, 6};
    float b2[4] = {4, 3, 6, 5};

    cmul_10(a1, b1, a2, b2);
    printf("cmul_10: %.0f, %.0f, %.0f, %.0f\n", a1[0], a1[1], a2[0], a2[1]);
}

int main(int argc, char **argv) {
    test_01();
    test_02();
    test_03();
    test_04();
    test_05();
    test_06();
    test_07();
    test_08();

    return 0;
}
