import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as sc
import pyrap.tables
import signal
import argparse
import time
import idg
import idg.util as util


msin = "/var/scratch/bvdtol/RX42_SB100-109.2ch10s.ms"
datacolumn = "DATA"

######################################################################
# Open measurementset
######################################################################
table = pyrap.tables.table(msin)

# Read parameters from measurementset
t_ant = pyrap.tables.table(table.getkeyword("ANTENNA"))
t_spw = pyrap.tables.table(table.getkeyword("SPECTRAL_WINDOW"))
frequencies = np.asarray(t_spw[0]['CHAN_FREQ'], dtype=np.float32)


######################################################################
# Parameters
######################################################################
nr_stations      = len(t_ant)
nr_baselines     = (nr_stations * (nr_stations - 1)) / 2
nr_channels      = table[0][datacolumn].shape[0]

nr_timesteps     = 10

nr_timeslots     = 1
nr_correlations  = 4
grid_size        = 1024
image_size       = 0.01
subgrid_size     = 32
kernel_size      = 16
cell_size        = image_size / grid_size

nr_parameters = 4



# TODO open model image
# divide out taper
# zero pad
# fft


######################################################################
# Initialize data
######################################################################
aterms         = util.get_identity_aterms(
                    nr_timeslots, nr_stations, subgrid_size, nr_correlations)
aterms_offsets = util.get_example_aterms_offset(
                    nr_timeslots, nr_timesteps)

# Initialize spheroidal
spheroidal = util.get_example_spheroidal(subgrid_size)

proxy = idg.CPU.Optimized(nr_correlations, subgrid_size)
#proxy = idg.HybridCUDA.GenericOptimized(nr_correlations, subgrid_size)



######################################################################
# Process entire measurementset
######################################################################
nr_rows = table.nrows()
nr_rows_read = 0
nr_rows_per_batch = (nr_baselines + nr_stations) * nr_timesteps

# Initialize empty buffers
uvw          = np.zeros(shape=(nr_baselines, nr_timesteps),
                        dtype=idg.uvwtype)
visibilities = np.zeros(shape=(nr_baselines, nr_timesteps, nr_channels,
                               nr_correlations),
                        dtype=idg.visibilitiestype)
baselines    = np.zeros(shape=(nr_baselines),
                        dtype=idg.baselinetype)
img          = np.zeros(shape=(nr_correlations, grid_size, grid_size),
                        dtype=idg.gridtype)

for i in range(2,6) :
    for j in range(2,6) :
        img[0, i*128, j*128] = 1.0
        img[3, i*128, j*128] = 1.0

grid = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(img, axes = (1,2))), axes=(1,2)).astype(np.complex64)

    # Reset buffers
uvw.fill(0)
visibilities.fill(0)
baselines.fill(0)

# Read nr_timesteps samples for all baselines including auto correlations
timestamp_block = table.getcol('TIME',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
antenna1_block  = table.getcol('ANTENNA1',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
antenna2_block  = table.getcol('ANTENNA2',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
uvw_block       = table.getcol('UVW',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
vis_block       = table.getcol(datacolumn,
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
flags_block     = table.getcol('FLAG',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
vis_block = vis_block * -flags_block
vis_block[np.isnan(vis_block)] = 0

nr_rows_read += nr_rows_per_batch


# Change precision
uvw_block = uvw_block.astype(np.float32)
vis_block = vis_block.astype(np.complex64)

# Remove autocorrelations
flags = antenna1_block != antenna2_block
antenna1_block = antenna1_block[flags]
antenna2_block = antenna2_block[flags]
uvw_block      = uvw_block[flags]
vis_block      = vis_block[flags]

# Reshape data
antenna1_block = np.reshape(antenna1_block,
                            newshape=(nr_timesteps, nr_baselines))
antenna2_block = np.reshape(antenna2_block,
                            newshape=(nr_timesteps, nr_baselines))
uvw_block = np.reshape(uvw_block,
                        newshape=(nr_timesteps, nr_baselines, 3))
vis_block = np.reshape(vis_block,
                        newshape=(nr_timesteps, nr_baselines,
                                    nr_channels, nr_correlations))

# Transpose data
for t in range(nr_timesteps):
    for bl in range(nr_baselines):
        # Set baselines
        antenna1 = antenna1_block[t][bl]
        antenna2 = antenna2_block[t][bl]

        baselines[bl] = (antenna1, antenna2)

        # Set uvw
        uvw_ = uvw_block[t][bl]
        uvw[bl][t] = uvw_

        # Set visibilities
        visibilities[bl][t] = vis_block[t][bl]

# Grid visibilities
w_step = 0.0

shift = np.array((0.0, 0.0), dtype=np.float32)

B0 = np.ones((1, subgrid_size, subgrid_size,1))
#B0 /= norm(B0)

x = np.linspace(-0.5, 0.5, subgrid_size)

B1,B2 = np.meshgrid(x,x)

B1 = B1[np.newaxis, :, :, np.newaxis]
#B1 /= norm(B1)

B2 = B2[np.newaxis, :, :, np.newaxis]
#B2 /= norm(B2)

B3 = x[np.newaxis, :, np.newaxis, np.newaxis] * x[np.newaxis, np.newaxis, :, np.newaxis]
#B3 /= norm(B3)


BB = np.concatenate((B0,B1,B2,B3))
B = np.kron(BB, np.array([1.0, 1.0, 1.0, 1.0]))

X0 = -0.2*np.ones((nr_stations, 1)) + 0.4*np.random.random((nr_stations, 1))
X1 = -0.2*np.ones((nr_stations, 1)) + 0.4*np.random.random((nr_stations, 1))
X2 = -0.2*np.ones((nr_stations, 1)) + 0.4*np.random.random((nr_stations, 1))
X3 = -0.2*np.ones((nr_stations, 1)) + 0.4*np.random.random((nr_stations, 1))

parameters0 = np.concatenate((X0,X1,X2,X3), axis=1)
parameters0 -= mean(parameters0, axis=0)

aterms[0,:,:,:,:] = np.exp(1j*np.tensordot(parameters0, B, axes = ((1,), (0,)))) * [1.0, 0.0, 0.0, 1.0]

aterms0 = aterms.copy()


proxy.degridding(
    w_step,
    shift,
    cell_size, kernel_size,
    subgrid_size,
    frequencies, visibilities,
    uvw, baselines, grid, aterms, aterms_offsets, spheroidal)

print visibilities.shape


# Using fft from library
#np.copyto(img, grid)
#proxy.transform(idg.FourierDomainToImageDomain, img)
#img_real = np.real(img[0,:,:])

# Remove spheroidal from grid
#img_real = img_real/spheroidal_grid


#TODO
# init basis functions

# generate random screens

# predict visibilities

# initialize solutions

# iterate over stations

X0 =  np.zeros((nr_stations, 1))
X1 =  np.zeros((nr_stations, 1))
X2 =  np.zeros((nr_stations, 1))
X3 =  np.zeros((nr_stations, 1))

parameters = np.concatenate((X0,X1,X2,X3), axis=1)

aterms[0,:,:,:,:] = np.exp(1j*np.tensordot(parameters, B, axes = ((1,), (0,))))

uvw1          = np.zeros(shape=(nr_stations, nr_stations-1, nr_timesteps),
                        dtype=idg.uvwtype)
visibilities1 = np.zeros(shape=(nr_stations, nr_stations-1, nr_timesteps, nr_channels,
                               nr_correlations),
                        dtype=idg.visibilitiestype)
baselines1    = np.zeros(shape=(nr_stations, nr_stations-1),
                        dtype=idg.baselinetype)

for bl in range(nr_baselines):
    # Set baselines
    antenna1 = antenna1_block[0][bl]
    antenna2 = antenna2_block[0][bl]

    bl1 = antenna2 - (antenna2>antenna1)

    print bl, antenna1, antenna2, bl1
    baselines1[antenna1][bl1] = (antenna1, antenna2)

    # Set uvw
    uvw1[antenna1][bl1] = uvw[bl]

    # Set visibilities
    visibilities1[antenna1][bl1] = visibilities[bl]

    antenna1, antenna2 = antenna2, antenna1

    bl1 = antenna2 - (antenna2>antenna1)

    baselines1[antenna1][bl1] = (antenna1, antenna2)

    # Set uvw
    uvw1[antenna1][bl1]['u'] = -uvw[bl]['u']
    uvw1[antenna1][bl1]['v'] = -uvw[bl]['v']
    uvw1[antenna1][bl1]['w'] = -uvw[bl]['w']

    # Set visibilities
    visibilities1[antenna1][bl1] = np.conj(visibilities[bl,:,:,(0,2,1,3)].transpose((1,2,0)))

nr_iterations = 0
converged = False

nr_display_stations = 55

fig, axs = subplots(7, 16, figsize=(24, 12))

axs[-1,-1].set_visible(False)
axs[-1,-2].set_visible(False)

fig.canvas.manager.window.set_keep_above(1)

figtitle = fig.suptitle("", fontsize=16)

max_dx = 0.0

ims = []

for i in range(nr_display_stations):
    ii = i % 7
    jj = i / 7
    plt.sca(axs[ii,jj*2])
    plt.cla()
    imshow(np.angle(aterms0[0,i,:,:,0]), clim=(-0.5,0.5))
    plt.sca(axs[ii, jj*2+1])
    plt.cla()
    ims.append(imshow(np.angle(aterms[0,i,:,:,0]), clim=(-0.5,0.5)))

P = []

while True:

    figtitle.set_text('Iteration {} - {}'.format(nr_iterations, max_dx))
    pause(0.001)

    nr_iterations += 1
    max_dx = 0.0
    for i in range(nr_stations):

        # predict visibilities for current solution

        predicted_visibilities = np.zeros(shape=(nr_parameters+1, nr_stations-1, nr_timesteps, nr_channels,
                                    nr_correlations),
                                dtype=idg.visibilitiestype)

        # iterate over degrees of freedom
        for j in range(nr_parameters+1):
            # fill a-term with derivative
            if j>0:
                aterms[0][i] = 1j * B[j-1] * np.exp(1j*np.tensordot(parameters[i], B, axes = ((0,), (0,)))) * [1.0, 0.0, 0.0, 1.0]

            # degridding
            proxy.calibrate(
                w_step,
                shift,
                cell_size, kernel_size,
                subgrid_size,
                frequencies, predicted_visibilities[j],
                uvw1[i], baselines1[i], grid, aterms, aterms_offsets, spheroidal)


        # compute residual visibilities
        residual_visibilities = visibilities1[i] - predicted_visibilities[0]


        # compute vector and  matrix
        v = np.zeros((nr_parameters, 1), dtype = np.complex128)
        M = np.zeros((nr_parameters, nr_parameters), dtype = np.complex128)

        for j in range(nr_parameters):
            v[j] = np.sum(residual_visibilities * np.conj(predicted_visibilities[j+1]))
            for k in range(j+1): # [j]: #
                M[j,k] = np.sum(predicted_visibilities[j+1] * np.conj(predicted_visibilities[k+1]))
                M[k,j] = np.conj(M[j,k])

        print np.amax(abs(residual_visibilities)), M, v

        # compute update step

        dx = np.dot(np.linalg.pinv(np.conj(M)), v)

        print parameters0[i], parameters[i]
        print dx

        dx = dx.real

        # store norm of update step
        max_dx = max(max_dx, np.linalg.norm(dx))

        parameters[i] += 1.0*dx[:,0]

        # compute new aterms
        aterms[0][i] = np.exp(1j*np.tensordot(parameters[i], B, axes = ((0,), (0,)))) * [1.0, 0.0, 0.0, 1.0]

        ims[i].set_array(np.angle(aterms[0,i,:,:,0]))




    parameters -= mean(parameters, axis=0)

    P.append(parameters.flatten())

    converged = (max_dx < 1.0e-3)
    if converged:
        msg = "Converged after {nr_iterations} iterations - {max_dx}".format(nr_iterations=nr_iterations, max_dx=max_dx)
        print msg
        figtitle.set_text(msg)
        break

    if nr_iterations == 200:
        msg = "Did not converge after {nr_iterations} iterations - {max_dx}".format(nr_iterations=nr_iterations, max_dx=max_dx)
        print msg
        figtitle.set_text(msg)
        break

p = parameters0.flatten()
figure()
semilogy(abs(P-p))


