import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as sc
import pyrap.tables
import signal
import argparse
import time
import idg
import idg.util as util


######################################################################
# Open measurementset
######################################################################
msin = "/var/scratch/bvdtol/RX42_SB100-109.2ch10s.ms"
datacolumn = "DATA"

table = pyrap.tables.table(msin)

# Read parameters from measurementset
t_ant = pyrap.tables.table(table.getkeyword("ANTENNA"))
t_spw = pyrap.tables.table(table.getkeyword("SPECTRAL_WINDOW"))
frequencies = np.asarray(t_spw[0]['CHAN_FREQ'], dtype=np.float32)


######################################################################
# Parameters
######################################################################
nr_stations      = len(t_ant)
nr_baselines     = (nr_stations * (nr_stations - 1)) / 2
nr_channels      = table[0][datacolumn].shape[0]

nr_timesteps     = 10

nr_timeslots     = 1
nr_correlations  = 4
grid_size        = 1024
image_size       = 0.01
subgrid_size     = 32
kernel_size      = 16
cell_size        = image_size / grid_size

max_nr_parameters = 10


######################################################################
# Initialize data
######################################################################
aterms         = util.get_identity_aterms(
                    nr_timeslots, nr_stations, subgrid_size, nr_correlations)
aterms_offsets = util.get_example_aterms_offset(
                    nr_timeslots, nr_timesteps)

# Initialize spheroidal
spheroidal = util.get_example_spheroidal(subgrid_size)

proxy = idg.CPU.Optimized(nr_correlations, subgrid_size)
#proxy = idg.HybridCUDA.GenericOptimized(nr_correlations, subgrid_size)


######################################################################
# Initialization
######################################################################
np.random.seed(1234)

nr_rows = table.nrows()
nr_rows_read = 0
nr_rows_per_batch = (nr_baselines + nr_stations) * nr_timesteps

# Initialize empty buffers
uvw          = np.zeros(shape=(nr_baselines, nr_timesteps),
                        dtype=idg.uvwtype)
visibilities = np.zeros(shape=(nr_baselines, nr_timesteps, nr_channels,
                               nr_correlations),
                        dtype=idg.visibilitiestype)
baselines    = np.zeros(shape=(nr_baselines),
                        dtype=idg.baselinetype)
img          = np.zeros(shape=(nr_correlations, grid_size, grid_size),
                        dtype=idg.gridtype)

grid = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(img, axes = (1,2))), axes=(1,2)).astype(np.complex64)

# Reset buffers
uvw.fill(0)
visibilities.fill(0)
baselines.fill(0)

# Read nr_timesteps samples for all baselines including auto correlations
timestamp_block = table.getcol('TIME',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
antenna1_block  = table.getcol('ANTENNA1',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
antenna2_block  = table.getcol('ANTENNA2',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
uvw_block       = table.getcol('UVW',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
vis_block       = table.getcol(datacolumn,
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
flags_block     = table.getcol('FLAG',
                                startrow = nr_rows_read,
                                nrow = nr_rows_per_batch)
vis_block = vis_block * -flags_block
vis_block[np.isnan(vis_block)] = 0

nr_rows_read += nr_rows_per_batch


# Change precision
uvw_block = uvw_block.astype(np.float32)
vis_block = vis_block.astype(np.complex64)

# Remove autocorrelations
flags = antenna1_block != antenna2_block
antenna1_block = antenna1_block[flags]
antenna2_block = antenna2_block[flags]
uvw_block      = uvw_block[flags]
vis_block      = vis_block[flags]

# Reshape data
antenna1_block = np.reshape(antenna1_block,
                            newshape=(nr_timesteps, nr_baselines))
antenna2_block = np.reshape(antenna2_block,
                            newshape=(nr_timesteps, nr_baselines))
uvw_block = np.reshape(uvw_block,
                        newshape=(nr_timesteps, nr_baselines, 3))
vis_block = np.reshape(vis_block,
                        newshape=(nr_timesteps, nr_baselines,
                                    nr_channels, nr_correlations))

# Transpose data
for t in range(nr_timesteps):
    for bl in range(nr_baselines):
        # Set baselines
        antenna1 = antenna1_block[t][bl]
        antenna2 = antenna2_block[t][bl]

        baselines[bl] = (antenna1, antenna2)

        # Set uvw
        uvw_ = uvw_block[t][bl]
        uvw[bl][t] = uvw_

        # Set visibilities
        visibilities[bl][t] = vis_block[t][bl]

# Initialization
w_step = 0.0
shift = np.array((0.0, 0.0, 0.0), dtype=np.float32)

print ">> Predict visibilities"
proxy.degridding(
    w_step,
    shift,
    cell_size, kernel_size,
    subgrid_size,
    frequencies, visibilities,
    uvw, baselines, grid, aterms, aterms_offsets, spheroidal)
print ""

print ">> Initialize calibration"
proxy.calibrate_init(
    w_step,
    shift,
    cell_size,
    kernel_size,
    subgrid_size,
    frequencies,
    visibilities,
    uvw,
    baselines,
    grid,
    spheroidal)
print ""

uvw1          = np.zeros(shape=(nr_stations, nr_stations-1, nr_timesteps),
                        dtype=idg.uvwtype)
visibilities1 = np.zeros(shape=(nr_stations, nr_stations-1, nr_timesteps, nr_channels,
                               nr_correlations),
                        dtype=idg.visibilitiestype)
baselines1    = np.zeros(shape=(nr_stations, nr_stations-1),
                        dtype=idg.baselinetype)

for bl in range(nr_baselines):
    # Get antennas
    antenna1 = antenna1_block[0][bl]
    antenna2 = antenna2_block[0][bl]

    # Set baseline
    bl1 = antenna2 - (antenna2>antenna1)
    baselines1[antenna1][bl1] = (antenna1, antenna2)

    # Set uvw
    uvw1[antenna1][bl1] = uvw[bl]

    # Set visibilities
    visibilities1[antenna1][bl1] = visibilities[bl]

    # Swap antennas
    antenna1, antenna2 = antenna2, antenna1

    # Set baseline
    bl1 = antenna2 - (antenna2>antenna1)
    baselines1[antenna1][bl1] = (antenna1, antenna2)

    # Set uvw
    uvw1[antenna1][bl1]['u'] = -uvw[bl]['u']
    uvw1[antenna1][bl1]['v'] = -uvw[bl]['v']
    uvw1[antenna1][bl1]['w'] = -uvw[bl]['w']

    # Set visibilities
    visibilities1[antenna1][bl1] = np.conj(visibilities[bl,:,:,(0,2,1,3)].transpose((1,2,0)))


######################################################################
# Calibration
######################################################################
nr_iterations = 0
converged = False
max_dx = 0.0

hessian  = np.zeros((max_nr_parameters, max_nr_parameters), dtype = np.complex64)
gradient = np.zeros((max_nr_parameters, ), dtype = np.complex64)
aterm_derivatives = np.zeros(shape=(max_nr_parameters, subgrid_size, subgrid_size, nr_correlations), dtype=np.complex64)

predicted_visibilities = np.zeros(shape=(nr_stations-1, nr_timesteps, nr_channels,
                            nr_correlations),
                        dtype=idg.visibilitiestype)

print ">> Run calibration"

timer = np.zeros(max_nr_parameters+1)
timer[:] = np.inf

while True:

    for i in range(max_nr_parameters+1):

        h = hessian[:i,:i].copy()
        g = gradient[:i].copy()

        # Warmup
        for j in range(nr_stations):
            proxy.calibrate_update(j, aterms[0], aterm_derivatives[:i], h, g)

        # Benchmark
        timer0 = -time.time()
        for j in range(nr_stations):
            proxy.calibrate_update(j, aterms[0], aterm_derivatives[:i], h, g)
        timer0 += time.time()

        timer[i] = min(timer[i], timer0)

    # Plot
    plt.clf()
    plt.xlabel("# parameters")
    plt.ylabel("normalized runtime [s]")
    x = np.arange(1, max_nr_parameters+2)
    y = timer/np.amin(timer)
    plt.plot(x, y)
    plt.pause(0.001)
