#include <complex>

#define float2 std::complex<float>

void cmul_01(float2& a, float2 b, float2 c)
{
    a += b * c;
}

void cmul_02(float2& a, float2 b, float2 c)
{
    float a_real = a.real();
    float a_imag = a.imag();
    float b_real = b.real();
    float b_imag = b.imag();
    float c_real = c.real();
    float c_imag = c.imag();

    a_real += b_real * c_real - b_imag * c_imag;
    a_imag += b_real * c_imag + b_imag * c_real;

    a.real(a_real);
    a.imag(a_imag);
}

void cmul_03(float2& a, float2 b, float2 c)
{
    float a_real = a.real();
    float a_imag = a.imag();
    float b_real = b.real();
    float b_imag = b.imag();
    float c_real = c.real();
    float c_imag = c.imag();

    a_real += b_real * c_real;
    a_real -= b_imag * c_imag;
    a_imag += b_real * c_imag;
    a_imag += b_imag * c_real;

    a.real(a_real);
    a.imag(a_imag);
}

void cmul_04(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2)
{
    float a1_real = a1.real();
    float a1_imag = a1.imag();
    float b1_real = b1.real();
    float b1_imag = b1.imag();
    float c1_real = c1.real();
    float c1_imag = c1.imag();

    a1_real += b1_real * c1_real;
    a1_real -= b1_imag * c1_imag;
    a1_imag += b1_real * c1_imag;
    a1_imag += b1_imag * c1_real;

    a1.real(a1_real);
    a1.imag(a1_imag);

    float a2_real = a2.real();
    float a2_imag = a2.imag();
    float b2_real = b2.real();
    float b2_imag = b2.imag();
    float c2_real = c2.real();
    float c2_imag = c2.imag();

    a2_real += b2_real * c2_real;
    a2_real -= b2_imag * c2_imag;
    a2_imag += b2_real * c2_imag;
    a2_imag += b2_imag * c2_real;

    a2.real(a2_real);
    a2.imag(a2_imag);
}

void cmul_05(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2)
{
    float x[8];
    float a1_real = a1.real();
    float a1_imag = a1.imag();
    x[0] = b1.real();
    x[1] = b1.imag();
    x[2] = c1.real();
    x[3] = c1.imag();

    a1_real += x[0] * x[2];
    a1_real -= x[1] * x[3];
    a1_imag += x[0] * x[3];
    a1_imag += x[1] * x[2];

    a1.real(a1_real);
    a1.imag(a1_imag);

    float a2_real = a2.real();
    float a2_imag = a2.imag();
    x[4] = b2.real();
    x[5] = b2.imag();
    x[6] = c2.real();
    x[7] = c2.imag();

    a2_real += x[4] * x[6];
    a2_real -= x[5] * x[7];
    a2_imag += x[4] * x[7];
    a2_imag += x[5] * x[6];

    a2.real(a2_real);
    a2.imag(a2_imag);
}

void cmul_06(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2)
{
    float x[8];
    float a1_real = a1.real();
    float a1_imag = a1.imag();
    x[0] = b1.real();
    x[1] = b1.imag();
    x[2] = c1.real();
    x[3] = c1.imag();

    float y[8];
    y[0] = a1_real;
    y[1] = 0;
    y[2] = a1_imag;
    y[3] = 0;

    y[0] += x[0] * x[2];
    y[1] -= x[1] * x[3];
    y[2] += x[0] * x[3];
    y[3] += x[1] * x[2];

    a1.real(y[0] + y[1]);
    a1.imag(y[2] + y[3]);

    float a2_real = a2.real();
    float a2_imag = a2.imag();
    x[4] = b2.real();
    x[5] = b2.imag();
    x[6] = c2.real();
    x[7] = c2.imag();

    y[4] = a2_real;
    y[5] = 0;
    y[6] = a2_imag;
    y[7] = 0;

    y[4] += x[4] * x[6];
    y[5] -= x[5] * x[7];
    y[6] += x[4] * x[7];
    y[7] += x[5] * x[6];

    a2.real(y[4] + y[5]);
    a2.imag(y[6] + y[7]);
}

void cmul_07(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2)
{
    float x[8];
    float y[8];

    float a1_real = a1.real();
    float a1_imag = a1.imag();
    float a2_real = a2.real();
    float a2_imag = a2.imag();

    x[0] = b1.real();
    x[1] = -b1.imag();
    x[2] = c1.real();
    x[3] = c1.imag();
    x[4] = b2.real();
    x[5] = -b2.imag();
    x[6] = c2.real();
    x[7] = c2.imag();

    y[0] = a1_real;
    y[1] = 0;
    y[2] = a1_imag;
    y[3] = 0;
    y[4] = a2_real;
    y[5] = 0;
    y[6] = a2_imag;
    y[7] = 0;

    y[0] += x[0] * x[2];
    y[1] += x[1] * x[3];
    y[2] += x[0] * x[3];
    y[3] += x[1] * x[2];
    y[4] += x[4] * x[6];
    y[5] += x[5] * x[7];
    y[6] += x[4] * x[7];
    y[7] += x[5] * x[6];

    a1.real(y[0] + y[1]);
    a1.imag(y[2] - y[3]);
    a2.real(y[4] + y[5]);
    a2.imag(y[6] - y[7]);
}

void cmul_08(
    float2& a1, float2 b1, float2 c1,
    float2& a2, float2 b2, float2 c2)
{
    float t1[8];
    float t2[8];
    float t3[8];

    t1[0] = a1.real();
    t1[1] = 0;
    t1[2] = a1.imag();
    t1[3] = 0;
    t1[4] = a2.real();
    t1[5] = 0;
    t1[6] = a2.imag();
    t1[7] = 0;

    t2[0] =  b1.real();
    t2[1] = -b1.imag();
    t2[2] =  b1.real();
    t2[3] = -b1.imag();
    t2[4] =  b2.real();
    t2[5] = -b2.imag();
    t2[6] =  b2.real();
    t2[7] = -b2.imag();

    t3[0] = c1.real();
    t3[1] = c1.imag();
    t3[2] = c1.imag();
    t3[3] = c1.real();
    t3[4] = c2.real();
    t3[5] = c2.imag();
    t3[6] = c2.imag();
    t3[7] = c2.real();

    t1[0] += t2[0] * t3[0];
    t1[1] += t2[1] * t3[1];
    t1[2] += t2[2] * t3[2];
    t1[3] += t2[3] * t3[3];
    t1[4] += t2[4] * t3[4];
    t1[5] += t2[5] * t3[5];
    t1[6] += t2[6] * t3[6];
    t1[7] += t2[7] * t3[7];

    a1.real(t1[0] + t1[1]);
    a1.imag(t1[2] - t1[3]);
    a2.real(t1[4] + t1[5]);
    a2.imag(t1[6] - t1[7]);
}
