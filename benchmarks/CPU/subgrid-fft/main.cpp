#include <iostream>
#include <complex>
#include <omp.h> // omp_get_wtime
#include <fftw3.h> // FFTW_BACKWARD, FFTW_FORWARD

using namespace std;

void kernel_fft_subgrid(
    int size,
    int batch,
    fftwf_complex *data,
    int sign);

int main(int argc, char *argv[])
{
    // Constants
    const int default_batch = 4096;
    const int default_subgridsize = 24;
    const int nr_polarizations = 4;

    // Set parameters
    char *batch_str = getenv("BATCH");
    char *subgridsize_str = getenv("SUBGRIDSIZE");
    char *repetitions_str = getenv("NR_REPETITIONS");
    const int batch = batch_str ? atoi(batch_str) : default_batch;
    const int subgridsize = subgridsize_str ? atoi(subgridsize_str) : default_subgridsize;
    const int nr_repetitions = repetitions_str ? atoi(repetitions_str) : 1;
    cout << "batch: " << batch;
    cout << ", subgrid_size: " << subgridsize;
    cout << ", nr_repetitions: " << nr_repetitions << endl;

    // Allocate subgrids
    auto size_subgrids = 1ULL * batch * nr_polarizations *
        subgridsize * subgridsize;
    auto subgrids = new complex<float>[size_subgrids];

    // Initialize subgrids
    for (auto j = 0; j < size_subgrids; j++) {
        subgrids[j] = 0.839878;
    }

    // Run fft kernel
    fftwf_complex *subgrids_ptr = (fftwf_complex *) subgrids;

    double runtime = 0;

    for (int i = 0; i < nr_repetitions; i++) {
        runtime -= omp_get_wtime();
        kernel_fft_subgrid(subgridsize, batch, subgrids_ptr, FFTW_FORWARD);
        runtime += omp_get_wtime();
    }

    cout << "runtime: " << runtime << " seconds" << endl;

    return 0;
}
