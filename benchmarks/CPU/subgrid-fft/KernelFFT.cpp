#include <iostream>
#include <complex>

#include <math.h>
#include <fftw3.h>
#include <stdint.h>

#include "Types.h"

#define NR_POLARIZATIONS 4

void kernel_fft_subgrid(
	int size,
	int batch,
    fftwf_complex *_data,
	int sign
	) {

    fftwf_complex *data = (fftwf_complex *) _data;

    // 2D FFT
    int rank = 2;

    // For grids of size*size elements
    int n[] = {(int) size, (int) size};

    // Set stride
    int istride = 1;
    int ostride = istride;

    // Set dist
    int idist = n[0] * n[1];
    int odist = idist;

    // Planner flags
    int flags = FFTW_ESTIMATE;


    // Create plan
    fftwf_plan plan;
    plan = fftwf_plan_many_dft(
        rank, n, NR_POLARIZATIONS, _data, n,
        istride, idist, _data, n,
        ostride, odist, sign, flags);

    #pragma omp parallel for private(data)
    for (int i = 0; i < batch; i++) {
        data = (fftwf_complex *) _data + i * (NR_POLARIZATIONS * size * size);

        // Execute FFTs
        fftwf_execute_dft(plan, data, data);

        // Scaling in case of an inverse FFT, so that FFT(iFFT())=identity()
        if (sign == FFTW_BACKWARD) {
            float scale = 1 / (double(size)*double(size));
            for (int i = 0; i < NR_POLARIZATIONS*size*size; i++) {
                data[i][0] *= scale;
                data[i][1] *= scale;
            }
        }

    } // end for batch

    // Cleanup
    fftwf_destroy_plan(plan);
}
