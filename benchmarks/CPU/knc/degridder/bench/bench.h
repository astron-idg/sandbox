#pragma once

#include <iostream>
#include <iomanip>
#include <complex>
#include <cstdint>
#include <limits>
#include <omp.h>
#include <cstring>

#include "Globals.h"
#include "auxiliary.h"
#include "degridder.h"

using FunctionPtr = void (*)(int, float, void*, void*,
                             void*, void*, void*, void*,
                             void*);

void run( const std::string& name, int jobsize, FunctionPtr kernel_gridder );
void check( int jobsize, FunctionPtr kernel_gridder );
void check( const std::string& name, int jobsize, FunctionPtr kernel_gridder );
void run_and_check( const std::string& name, int jobsize, FunctionPtr kernel_gridder );
