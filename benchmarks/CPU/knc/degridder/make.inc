# C++ compiler
CXX=icpc

# Default C++ compiler flags
CXXFLAGS=-std=c++11 -mmic -O3 -openmp -mkl -fp-model fast=1 -static-intel

# OPTREPORT=-guide-vec=3
OPTREPORT=-opt-report=3

# Additional compiler flags for debug mode
DEBUGFLAGS=-g
