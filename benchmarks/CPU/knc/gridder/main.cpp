#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include "Globals.h"
#include "bench.h"

using namespace std;


int main(int argc, char **argv)
{
    // Sizes (= number of subgrids)
    auto jobsize = 1792; // 8*224 (224=threads)

    cout << endl;
    cout << "Setting:" << endl;
    cout << "NR_REPETITIONS = " << NR_REPETITIONS << endl;
    cout << "JOBSIZE = " << jobsize << endl;
    cout << "NR_TIMESTEPS = " << NR_TIMESTEPS << endl;
    cout << "NR_CHANNELS = " << NR_CHANNELS << endl;
    cout << "NR_POLARIZATIONS = " << NR_POLARIZATIONS << endl;
    cout << "SUBGRIDSIZE = " << SUBGRIDSIZE << endl;
    cout << "NR_STATIONS = " << NR_STATIONS << endl;
    cout << "NR_TIMESLOTS = " << NR_TIMESLOTS << endl;
    cout << "GRIDSIZE = " << GRIDSIZE << endl;
    cout << "W_OFFSET = " << W_OFFSET << endl;
    cout << "IMAGESIZE = " << IMAGESIZE << endl;
    cout << "INTEGRATION_TIME = " << INTEGRATION_TIME << endl;

    // Use run, check, and run_and_check below

    // run( "Previous optimized", jobsize,
    //      (FunctionPtr) kernel_gridder_opt);

    cout << endl;
    cout << "--------- Start optimizing ---------" << endl;

    run( "Reference (base)", jobsize,
         (FunctionPtr) kernel_gridder_01);

    run( "Preload visibilities", jobsize,
         (FunctionPtr) kernel_gridder_02);

    run( "Vectorize over channels", jobsize,
         (FunctionPtr) kernel_gridder_03);

    run( "Precompute phase", jobsize,
         (FunctionPtr) kernel_gridder_04);

    run( "Precompute phasor", jobsize,
         (FunctionPtr) kernel_gridder_05);

    run( "Introduce VML", jobsize,
          (FunctionPtr) kernel_gridder_06);

    run( "Make nr_timesteps non-const", jobsize,
          (FunctionPtr) kernel_gridder_07);

    run( "Remove preload from phasor and VML", jobsize,
         (FunctionPtr) kernel_gridder_08);

    run( "Precompute phasor for channels", jobsize,
         (FunctionPtr) kernel_gridder_09);

    run( "Using intrinsics", jobsize,
         (FunctionPtr) kernel_gridder_10);

    return EXIT_SUCCESS;
}
