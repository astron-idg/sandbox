#include "bench.h"

using namespace std;

void run( const string& name, int jobsize, FunctionPtr kernel_gridder )
{
    auto size_uvw = 1ULL * jobsize * NR_TIMESTEPS * sizeof(UVW) / sizeof(float);
    auto size_wavenumbers = 1ULL * NR_CHANNELS;
    auto size_visibilities = 1ULL * jobsize * NR_TIMESTEPS * NR_CHANNELS * NR_POLARIZATIONS;
    auto size_spheroidal = 1ULL * SUBGRIDSIZE * SUBGRIDSIZE;
    auto size_aterm = 1ULL * NR_STATIONS * NR_TIMESLOTS * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;
    auto size_metadata = 1ULL * jobsize * sizeof(Metadata) / sizeof(int);
    auto size_subgrid = 1ULL * jobsize * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;

    // Data
    auto uvw = new float[NR_REPETITIONS * size_uvw];
    auto wavenumbers = new float[NR_REPETITIONS * size_wavenumbers];
    auto visibilities = new float_complex[NR_REPETITIONS * size_visibilities];
    auto spheroidal = new float[NR_REPETITIONS * size_spheroidal];
    auto aterm = new float_complex[NR_REPETITIONS * size_aterm];
    auto metadata = new int[NR_REPETITIONS * size_metadata];
    auto subgrid = new float_complex[NR_REPETITIONS * size_subgrid];

    // Initialize data (NR_REPETITIONS sets of same input)
    auto uvw_ptr = uvw;
    auto wavenumbers_ptr = wavenumbers;
    auto visibilities_ptr = visibilities;
    auto spheroidal_ptr = spheroidal;
    auto aterm_ptr = aterm;
    auto metadata_ptr = metadata;
    auto subgrid_ptr = subgrid;
    for (int i = 0; i < NR_REPETITIONS; i++) {
        init_uvw(uvw_ptr, 2, 1, jobsize * NR_TIMESTEPS, INTEGRATION_TIME);
        init_wavenumbers(wavenumbers_ptr, NR_CHANNELS);
        init_spheroidal(spheroidal_ptr, size_spheroidal);
        init_aterm(aterm_ptr, size_aterm);
        init_metadata(metadata_ptr, uvw_ptr, wavenumbers_ptr, 2, 1,
                      NR_TIMESTEPS, jobsize,
                      NR_CHANNELS, GRIDSIZE, SUBGRIDSIZE, IMAGESIZE);
        init_subgrid(subgrid_ptr, size_subgrid);
        uvw_ptr += size_uvw;
        wavenumbers_ptr += size_wavenumbers;
        visibilities_ptr += size_visibilities;
        spheroidal_ptr += size_spheroidal;
        aterm_ptr += size_aterm;
        metadata_ptr += size_metadata;
        subgrid_ptr += size_subgrid;
    }

    // clear cache
    size_t num_bytes = 100*1024*1024;
    auto clear = new char[num_bytes];
    memset_array(clear, num_bytes);

    // Run reference kernel
    uvw_ptr = uvw;
    wavenumbers_ptr = wavenumbers;
    visibilities_ptr = visibilities;
    spheroidal_ptr = spheroidal;
    aterm_ptr = aterm;
    metadata_ptr = metadata;
    subgrid_ptr = subgrid;

    auto total_runtime = -omp_get_wtime();
    auto min_runtime = numeric_limits<double>::infinity();

    for (int i = 0; i < NR_REPETITIONS; i++) {

        auto runtime = -omp_get_wtime();

        kernel_gridder(
            jobsize, W_OFFSET,
            (UVWType *) uvw_ptr,
            (WavenumberType *) wavenumbers_ptr,
            (VisibilitiesType *) visibilities_ptr,
            (SpheroidalType *) spheroidal_ptr,
            (ATermType *) aterm_ptr,
            (MetadataType *) metadata_ptr,
            (SubGridType *) subgrid_ptr);

        uvw_ptr += size_uvw;
        wavenumbers_ptr += size_wavenumbers;
        visibilities_ptr += size_visibilities;
        spheroidal_ptr += size_spheroidal;
        aterm_ptr += size_aterm;
        metadata_ptr += size_metadata;
        subgrid_ptr += size_subgrid;

        runtime += omp_get_wtime();
        min_runtime = fmin(min_runtime, runtime);
    }

    total_runtime += omp_get_wtime();
    auto avg_runtime = total_runtime/NR_REPETITIONS;
    auto flops = gridder_flops(jobsize);
    auto sincos = gridder_sincos(jobsize);
    auto bytes = gridder_bytes(jobsize);
    auto nr_visibilities = gridder_visibilities(jobsize);
    auto gflops = flops * 1e-9 / min_runtime;
    auto gsincos = sincos * 1e-9 / min_runtime;
    auto mvis =  nr_visibilities * 1e-6 / min_runtime;
    auto gbytes = bytes * 1e-9 / min_runtime;
    printf("\n%s\n", name.c_str());
    printf("Avg. runtime:     %f\n", avg_runtime);
    printf("Min. runtime:     %f\n", min_runtime);
    printf("Gsincos:          %f\n", gsincos);
    printf("Mvisibilities:    %f\n", mvis);
    printf("Gflops (approx.): %f\n", gflops);
    printf("Bandwidth [GB/s]: %f\n", gbytes);

    // Free memory
    delete[] clear;
    delete[] uvw;
    delete[] wavenumbers;
    delete[] visibilities;
    delete[] spheroidal;
    delete[] aterm;
    delete[] metadata;
    delete[] subgrid;
}




void check( const string& name, int jobsize, FunctionPtr kernel_gridder )
{
    auto size_uvw = 1ULL * jobsize * NR_TIMESTEPS * sizeof(UVW) / sizeof(float);
    auto size_wavenumbers = 1ULL * NR_CHANNELS;
    auto size_visibilities = 1ULL * jobsize * NR_TIMESTEPS * NR_CHANNELS * NR_POLARIZATIONS;
    auto size_spheroidal = 1ULL * SUBGRIDSIZE * SUBGRIDSIZE;
    auto size_aterm = 1ULL * NR_STATIONS * NR_TIMESLOTS * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;
    auto size_metadata = 1ULL * jobsize * sizeof(Metadata) / sizeof(int);
    auto size_subgrid = 1ULL * jobsize * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;

    // Data
    auto uvw = new float[size_uvw];
    auto wavenumbers = new float[size_wavenumbers];
    auto visibilities = new float_complex[size_visibilities];
    auto spheroidal = new float[size_spheroidal];
    auto aterm = new float_complex[size_aterm];
    auto metadata = new int[size_metadata];
    auto subgrid = new float_complex[size_subgrid];
    auto subgrid_ref = new float_complex[size_subgrid];

    // Initialize data (NR_REPETITIONS sets of same input)
    init_uvw(uvw, 2, 1, jobsize * NR_TIMESTEPS, INTEGRATION_TIME);
    init_wavenumbers(wavenumbers, NR_CHANNELS);
    init_spheroidal(spheroidal, size_spheroidal);
    init_aterm(aterm, size_aterm);
    init_metadata(metadata, uvw, wavenumbers, 2, 1,
                  NR_TIMESTEPS, jobsize,
                  NR_CHANNELS, GRIDSIZE, SUBGRIDSIZE, IMAGESIZE);
    init_subgrid(subgrid, size_subgrid);

    kernel_gridder(
        jobsize, W_OFFSET,
        (UVWType *) uvw,
        (WavenumberType *) wavenumbers,
        (VisibilitiesType *) visibilities,
        (SpheroidalType *) spheroidal,
        (ATermType *) aterm,
        (MetadataType *) metadata,
        (SubGridType *) subgrid);

    // Run reference kernel
    kernel_gridder_reference(
        jobsize, W_OFFSET,
        (UVWType *) uvw,
        (WavenumberType *) wavenumbers,
        (VisibilitiesType *) visibilities,
        (SpheroidalType *) spheroidal,
        (ATermType *) aterm,
        (MetadataType *) metadata,
        (SubGridType *) subgrid_ref);

    if (name != "") cout << name << endl;
    float tol = 1000*std::numeric_limits<float>::epsilon();
    float subgrid_error = get_accucary(size_subgrid,
                                       (complex<float>*) subgrid,
                                       (complex<float>*) subgrid_ref);

    // Report results
    if (subgrid_error < tol) {
        cout << "PASSED! Error: ";
    } else {
        cout << "FAILED! Error: ";
    }
    cout << scientific << subgrid_error << endl;

    // Free memory
    delete[] uvw;
    delete[] wavenumbers;
    delete[] visibilities;
    delete[] spheroidal;
    delete[] aterm;
    delete[] metadata;
    delete[] subgrid;
    delete[] subgrid_ref;
}


void check( int jobsize, FunctionPtr kernel_gridder )
{
    check("", jobsize, kernel_gridder);
}


void run_and_check( const string& name, int jobsize, FunctionPtr kernel_gridder )
{
    run( name, jobsize, kernel_gridder );
    check( "", jobsize, kernel_gridder );
}
