#pragma once

#include <complex>

// namespace {
const int NR_REPETITIONS = 20;
const int NR_TIMESTEPS = 16;
const int NR_CHANNELS = 32;
const int NR_POLARIZATIONS = 4;
const int SUBGRIDSIZE = 32;
const int NR_STATIONS = 20;
const int NR_TIMESLOTS = 128;
const int GRIDSIZE = 4096;
const float W_OFFSET = 0.0f;
const float IMAGESIZE = 0.01f;
const int INTEGRATION_TIME = 1;
// };

/* Structures */
typedef struct { float u, v, w; } UVW;
typedef struct { int x, y; } Coordinate;
typedef struct { int station1, station2; } Baseline;
typedef struct { int offset; int nr_timesteps; int aterm_index;
                 Baseline baseline; Coordinate coordinate; } Metadata;
typedef struct { float real, imag; } float_complex;

/* Complex numbers */
using FLOAT_COMPLEX = std::complex<float>;

/* Datatype */
typedef UVW UVWType[1];
typedef FLOAT_COMPLEX VisibilitiesType[1][NR_CHANNELS][NR_POLARIZATIONS];
typedef float WavenumberType[NR_CHANNELS];
typedef FLOAT_COMPLEX ATermType[NR_STATIONS][NR_TIMESLOTS][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE];
typedef float SpheroidalType[SUBGRIDSIZE][SUBGRIDSIZE];
typedef FLOAT_COMPLEX SubGridType[1][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE];
typedef Metadata MetadataType[1];
// Not needed:
// typedef FLOAT_COMPLEX GridType[NR_POLARIZATIONS][GRIDSIZE][GRIDSIZE];

/* Datatypes (for HaswellEPConstant) */
typedef UVW UVWConstantType[1][NR_TIMESTEPS];
typedef FLOAT_COMPLEX VisibilitiesConstantType[1][NR_TIMESTEPS][NR_CHANNELS][NR_POLARIZATIONS];
