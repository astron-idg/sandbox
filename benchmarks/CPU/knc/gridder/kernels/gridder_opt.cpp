#include <complex>
#include <cmath>
#include <cstdio>
#include <immintrin.h>
#include <omp.h>
#include <cstring> // memset
#include <cstdint>

#include "gridder.h"

void update_4(
    const int subgridsize,
    const int nr_channels,
    const float wavenumbers[NR_CHANNELS],
    const FLOAT_COMPLEX vis[NR_POLARIZATIONS][NR_CHANNELS],
    const float phase_index[SUBGRIDSIZE][SUBGRIDSIZE],
    const float phase_offset[SUBGRIDSIZE][SUBGRIDSIZE],
    FLOAT_COMPLEX pixels[SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS]
    ) {
    #pragma simd
    for (int chan = 0; chan < 4; chan++) {
        for (int y = 0; y < subgridsize; y++) {
            for (int x = 0; x < subgridsize; x++) {
                float phase = (phase_index[y][x] * wavenumbers[chan]) - phase_offset[y][x];
                FLOAT_COMPLEX phasor = FLOAT_COMPLEX(cosf(phase), sinf(phase));

                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    pixels[y][x][pol] += vis[pol][chan] * phasor;
                }
            }
        }
    }
}

void update_8(
    const int subgridsize,
    const int nr_channels,
    const float wavenumbers[NR_CHANNELS],
    const FLOAT_COMPLEX vis[NR_POLARIZATIONS][NR_CHANNELS],
    const float phase_index[SUBGRIDSIZE][SUBGRIDSIZE],
    const float phase_offset[SUBGRIDSIZE][SUBGRIDSIZE],
    FLOAT_COMPLEX pixels[SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS]
    ) {
    #pragma simd
    for (int chan = 0; chan < 8; chan++) {
        for (int y = 0; y < subgridsize; y++) {
            for (int x = 0; x < subgridsize; x++) {
                float phase = (phase_index[y][x] * wavenumbers[chan]) - phase_offset[y][x];
                FLOAT_COMPLEX phasor = FLOAT_COMPLEX(cosf(phase), sinf(phase));

                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    pixels[y][x][pol] += vis[pol][chan] * phasor;
                }
            }
        }
    }
}

void update_24(
    const int subgridsize,
    const int nr_channels,
    const float wavenumbers[NR_CHANNELS],
    const FLOAT_COMPLEX vis[NR_POLARIZATIONS][NR_CHANNELS],
    const float phase_index[SUBGRIDSIZE][SUBGRIDSIZE],
    const float phase_offset[SUBGRIDSIZE][SUBGRIDSIZE],
    FLOAT_COMPLEX pixels[SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS]
    ) {
    #pragma simd
    for (int chan = 0; chan < 24; chan++) {
        for (int y = 0; y < SUBGRIDSIZE; y++) {
            for (int x = 0; x < SUBGRIDSIZE; x++) {
                float phase = (phase_index[y][x] * wavenumbers[chan]) - phase_offset[y][x];
                FLOAT_COMPLEX phasor = FLOAT_COMPLEX(cosf(phase), sinf(phase));

                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    pixels[y][x][pol] += vis[pol][chan] * phasor;
                }
            }
        }
    }
}

void update_n(
    const int subgridsize,
    const int nr_channels,
    const float wavenumbers[NR_CHANNELS],
    const FLOAT_COMPLEX vis[NR_POLARIZATIONS][NR_CHANNELS],
    const float phase_index[SUBGRIDSIZE][SUBGRIDSIZE],
    const float phase_offset[SUBGRIDSIZE][SUBGRIDSIZE],
    FLOAT_COMPLEX pixels[SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS]
    ) {
    #pragma simd
    for (int chan = 0; chan < nr_channels; chan++) {
        for (int y = 0; y < SUBGRIDSIZE; y++) {
            for (int x = 0; x < SUBGRIDSIZE; x++) {
                float phase = (phase_index[y][x] * wavenumbers[chan]) - phase_offset[y][x];
                FLOAT_COMPLEX phasor = FLOAT_COMPLEX(cosf(phase), sinf(phase));

                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    pixels[y][x][pol] += vis[pol][chan] * phasor;
                }
            }
        }
    }
}

void kernel_gridder_opt (
    const int jobsize, const float w_offset,
    const UVWConstantType	       __restrict__ *uvw,
    const WavenumberType           __restrict__ *wavenumbers,
    const VisibilitiesConstantType __restrict__ *visibilities,
    const SpheroidalType           __restrict__ *spheroidal,
    const ATermType		           __restrict__ *aterm,
    const MetadataType	           __restrict__ *metadata,
    SubGridType			           __restrict__ *subgrid)
{
    // Find offset of first subgrid
    const Metadata m = (*metadata)[0];
    // const int baseline_offset_1 = m.baseline_offset;
    const int time_offset_1 = m.offset; // should be 0

    #pragma omp parallel shared(uvw, wavenumbers, visibilities, spheroidal, aterm, metadata)
    {
    // Iterate all subgrids
    #pragma omp for
	for (int s = 0; s < jobsize; s++) {
        // Load metadata
        const Metadata m = (*metadata)[s];
        const int station1 = m.baseline.station1;
        const int station2 = m.baseline.station2;
        const int aterm_index = m.aterm_index;
        const int x_coordinate = m.coordinate.x;
        const int y_coordinate = m.coordinate.y;

        // Compute u and v offset in wavelenghts
        float u_offset = (x_coordinate + SUBGRIDSIZE/2 - GRIDSIZE/2) /
            IMAGESIZE * 2*M_PI;
        float v_offset = (y_coordinate + SUBGRIDSIZE/2 - GRIDSIZE/2) /
            IMAGESIZE * 2*M_PI;

        // Initialize private subgrid
        FLOAT_COMPLEX pixels[SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS] __attribute__((aligned(64)));
        memset(pixels, 0, SUBGRIDSIZE * SUBGRIDSIZE * NR_POLARIZATIONS * sizeof(FLOAT_COMPLEX));

        // Storage for precomputed values
        float phase_index[SUBGRIDSIZE][SUBGRIDSIZE]  __attribute__((aligned(64)));
        float phase_offset[SUBGRIDSIZE][SUBGRIDSIZE] __attribute__((aligned(64)));
        FLOAT_COMPLEX vis[NR_POLARIZATIONS][NR_CHANNELS] __attribute__((aligned(64)));

        // Iterate all timesteps
        for (int time = 0; time < NR_TIMESTEPS; time++) {
            // Load UVW coordinates
            float u = (*uvw)[s][time].u;
            float v = (*uvw)[s][time].v;
            float w = (*uvw)[s][time].w;

            // Compute phase indices and phase offsets
            for (int y = 0; y < SUBGRIDSIZE; y++) {
                for (int x = 0; x < SUBGRIDSIZE; x++) {
                    // Compute l,m,n
                    float l = (x-(SUBGRIDSIZE/2)) * IMAGESIZE/SUBGRIDSIZE;
                    float m = (y-(SUBGRIDSIZE/2)) * IMAGESIZE/SUBGRIDSIZE;
                    float n = 1.0f - (float) sqrt(1.0 - (double) (l * l) - (double) (m * m));

                    // Compute phase index
                    phase_index[y][x] = u*l + v*m + w*n;

                    // Compute phase offset
                    phase_offset[y][x] = u_offset*l + v_offset*m + w_offset*n;
                }
            }

            // Preload visibilities
            for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                for (int chan = 0; chan < NR_CHANNELS; chan++) {
                    vis[pol][chan] = (*visibilities)[s][time][chan][pol];
                }
            }

            // Compute phasor and update current subgrid
            if (NR_CHANNELS == 4) {
                update_4(SUBGRIDSIZE, NR_CHANNELS,  *wavenumbers, vis, phase_index, phase_offset, pixels);
                continue;
            }
            if (NR_CHANNELS == 8) {
                update_8(SUBGRIDSIZE, NR_CHANNELS, *wavenumbers, vis, phase_index, phase_offset, pixels);
                continue;
            }
            if (NR_CHANNELS == 24) {
                update_24(SUBGRIDSIZE, NR_CHANNELS, *wavenumbers, vis, phase_index, phase_offset, pixels);
                continue;
            }

            update_n(SUBGRIDSIZE, NR_CHANNELS, *wavenumbers, vis, phase_index, phase_offset, pixels);
        }

        // Apply aterm and spheroidal and store result
        for (int y = 0; y < SUBGRIDSIZE; y++) {
            for (int x = 0; x < SUBGRIDSIZE; x++) {
                // Load a term for station1
                FLOAT_COMPLEX aXX1 = (*aterm)[station1][aterm_index][0][y][x];
                FLOAT_COMPLEX aXY1 = (*aterm)[station1][aterm_index][1][y][x];
                FLOAT_COMPLEX aYX1 = (*aterm)[station1][aterm_index][2][y][x];
                FLOAT_COMPLEX aYY1 = (*aterm)[station1][aterm_index][3][y][x];

                // Load aterm for station2
                FLOAT_COMPLEX aXX2 = conj((*aterm)[station2][aterm_index][0][y][x]);
                FLOAT_COMPLEX aXY2 = conj((*aterm)[station2][aterm_index][1][y][x]);
                FLOAT_COMPLEX aYX2 = conj((*aterm)[station2][aterm_index][2][y][x]);
                FLOAT_COMPLEX aYY2 = conj((*aterm)[station2][aterm_index][3][y][x]);

                // Load uv values
                FLOAT_COMPLEX pixelsXX = pixels[y][x][0];
                FLOAT_COMPLEX pixelsXY = pixels[y][x][1];
                FLOAT_COMPLEX pixelsYX = pixels[y][x][2];
                FLOAT_COMPLEX pixelsYY = pixels[y][x][3];

                // Apply aterm to subgrid
                pixels[y][x][0]  = (pixelsXX * aXX1);
                pixels[y][x][0] += (pixelsXY * aYX1);
                pixels[y][x][0] += (pixelsXX * aXX2);
                pixels[y][x][0] += (pixelsYX * aYX2);
                pixels[y][x][1]  = (pixelsXX * aXY1);
                pixels[y][x][1] += (pixelsXY * aYY1);
                pixels[y][x][1] += (pixelsXY * aXX2);
                pixels[y][x][1] += (pixelsYY * aYX2);
                pixels[y][x][2]  = (pixelsYX * aXX1);
                pixels[y][x][2] += (pixelsYY * aYX1);
                pixels[y][x][2] += (pixelsXX * aXY2);
                pixels[y][x][2] += (pixelsYX * aYY2);
                pixels[y][x][3]  = (pixelsYX * aXY1);
                pixels[y][x][3] += (pixelsYY * aYY1);
                pixels[y][x][3] += (pixelsXY * aXY2);
                pixels[y][x][3] += (pixelsYY * aYY2);

                // Load spheroidal
                float sph = (*spheroidal)[y][x];

                // Compute shifted position in subgrid
                int x_dst = (x + (SUBGRIDSIZE/2)) % SUBGRIDSIZE;
                int y_dst = (y + (SUBGRIDSIZE/2)) % SUBGRIDSIZE;

                // Set subgrid value
                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    (*subgrid)[s][pol][y_dst][x_dst] = pixels[y][x][pol] * sph;
                }
            }
        }
    }
    }
}
