#pragma once

#include <cmath>
#include <cstring>
#include <complex>

#include <omp.h>

#ifdef USE_LIKWID
#include <likwid.h>
#endif

#include "Globals.h"
#include "Types.h"

#define PARAMETERS \
    int                       nr_subgrids, \
    int                       grid_size, \
    int                       subgrid_size, \
    float                     image_size, \
    float                     w_offset_in_lambda, \
    int                       nr_channels, \
    int                       nr_stations, \
    idg::UVWCoordinate<float> uvw[], \
    float                     wavenumbers[], \
    idg::float2               visibilities[][NR_POLARIZATIONS], \
    float                     spheroidal[subgrid_size][subgrid_size], \
    idg::float2               aterms[][subgrid_size][subgrid_size][NR_POLARIZATIONS], \
    idg::Metadata             metadata[], \
    idg::float2               subgrid[][NR_POLARIZATIONS][subgrid_size][subgrid_size]

inline void apply_aterm(
    const idg::float2 aXX1, const idg::float2 aXY1,
    const idg::float2 aYX1, const idg::float2 aYY1,
    const idg::float2 aXX2, const idg::float2 aXY2,
    const idg::float2 aYX2, const idg::float2 aYY2,
    idg::float2 pixels[NR_POLARIZATIONS]
) {
    // Apply aterm to subgrid: P*A1
    // [ pixels[0], pixels[1];    [ aXX1, aXY1;
    //   pixels[2], pixels[3] ] *   aYX1, aYY1 ]
    idg::float2 pixelsXX = pixels[0];
    idg::float2 pixelsXY = pixels[1];
    idg::float2 pixelsYX = pixels[2];
    idg::float2 pixelsYY = pixels[3];
    pixels[0]  = (pixelsXX * aXX1);
    pixels[0] += (pixelsXY * aYX1);
    pixels[1]  = (pixelsXX * aXY1);
    pixels[1] += (pixelsXY * aYY1);
    pixels[2]  = (pixelsYX * aXX1);
    pixels[2] += (pixelsYY * aYX1);
    pixels[3]  = (pixelsYX * aXY1);
    pixels[3] += (pixelsYY * aYY1);

    // Apply aterm to subgrid: A2^H*P
    // [ aXX2, aYX1;      [ pixels[0], pixels[1];
    //   aXY1, aYY2 ]  *    pixels[2], pixels[3] ]
    pixelsXX = pixels[0];
    pixelsXY = pixels[1];
    pixelsYX = pixels[2];
    pixelsYY = pixels[3];
    pixels[0]  = (pixelsXX * aXX2);
    pixels[0] += (pixelsYX * aYX2);
    pixels[1]  = (pixelsXY * aXX2);
    pixels[1] += (pixelsYY * aYX2);
    pixels[2]  = (pixelsXX * aXY2);
    pixels[2] += (pixelsYX * aYY2);
    pixels[3]  = (pixelsXY * aXY2);
    pixels[3] += (pixelsYY * aYY2);
}

// Reference implementation
void kernel_gridder_ref(PARAMETERS);
void kernel_degridder_ref(PARAMETERS);

// Optimized implementation
void kernel_gridder_opt_01(PARAMETERS);
void kernel_gridder_opt_02(PARAMETERS);
void kernel_gridder_opt_03(PARAMETERS);
void kernel_gridder_opt_04(PARAMETERS);
void kernel_gridder_opt_05(PARAMETERS);
void kernel_gridder_opt_06(PARAMETERS);
void kernel_gridder_opt_07(PARAMETERS);
void kernel_degridder_opt_01(PARAMETERS);
void kernel_degridder_opt_02(PARAMETERS);
void kernel_degridder_opt_03(PARAMETERS);
void kernel_degridder_opt_04(PARAMETERS);
