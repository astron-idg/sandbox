#include "gridder.h"

void kernel_degridder_ref_(
    int                        nr_subgrids,
    int                        grid_size,
    int                        subgrid_size,
    float                      image_size,
    float                      w_offset_in_lambda,
    int                        nr_channels,
    int                        nr_stations,
    idg::UVWCoordinate<float>* uvw,
    float*                     wavenumbers,
    idg::float2*               visibilities,
    float*                     spheroidal,
    idg::float2*               aterms,
    idg::Metadata*             metadata,
    idg::float2*               subgrid)
{
    // Find offset of first subgrid
    const idg::Metadata m       = metadata[0];
    const int baseline_offset_1 = m.baseline_offset;
    const int time_offset_1     = m.time_offset;

    // Iterate all subgrids
    #pragma omp parallel for
    for (int s = 0; s < nr_subgrids; s++) {

        // Load metadata
        const idg::Metadata m  = metadata[s];
        const int local_offset = (m.baseline_offset - baseline_offset_1) +
                                 (m.time_offset - time_offset_1);
        const int nr_timesteps = m.nr_timesteps;
        const int aterm_index  = m.aterm_index;
        const int station1     = m.baseline.station1;
        const int station2     = m.baseline.station2;
        const int x_coordinate = m.coordinate.x;
        const int y_coordinate = m.coordinate.y;

        // Storage
        idg::float2 pixels[subgrid_size][subgrid_size][NR_POLARIZATIONS];

        // Apply aterm to subgrid
        for (int y = 0; y < subgrid_size; y++) {
            for (int x = 0; x < subgrid_size; x++) {
                // Load aterm for station1
                int station1_index =
                    (aterm_index * nr_stations + station1) *
                    subgrid_size * subgrid_size * NR_POLARIZATIONS +
                    y * subgrid_size * NR_POLARIZATIONS + x * NR_POLARIZATIONS;
                idg::float2 aXX1 = conj(aterms[station1_index + 0]);
                idg::float2 aXY1 = conj(aterms[station1_index + 1]);
                idg::float2 aYX1 = conj(aterms[station1_index + 2]);
                idg::float2 aYY1 = conj(aterms[station1_index + 3]);

                // Load aterm for station2
                int station2_index =
                    (aterm_index * nr_stations + station2) *
                    subgrid_size * subgrid_size * NR_POLARIZATIONS +
                    y * subgrid_size * NR_POLARIZATIONS + x * NR_POLARIZATIONS;
                idg::float2 aXX2 = aterms[station2_index + 0];
                idg::float2 aXY2 = aterms[station2_index + 1];
                idg::float2 aYX2 = aterms[station2_index + 2];
                idg::float2 aYY2 = aterms[station2_index + 3];

                // Load spheroidal
                float sph = spheroidal[y * subgrid_size + x];

                // Compute shifted position in subgrid
                int x_src = (x + (subgrid_size/2)) % subgrid_size;
                int y_src = (y + (subgrid_size/2)) % subgrid_size;

                // Load uv values
                idg::float2 pixelsXX = sph * subgrid[
                    s * NR_POLARIZATIONS * subgrid_size * subgrid_size +
                    0 * subgrid_size * subgrid_size + y_src * subgrid_size + x_src];
                idg::float2 pixelsXY = sph * subgrid[
                    s * NR_POLARIZATIONS * subgrid_size * subgrid_size +
                    1 * subgrid_size * subgrid_size + y_src * subgrid_size + x_src];
                idg::float2 pixelsYX = sph * subgrid[
                    s * NR_POLARIZATIONS * subgrid_size * subgrid_size +
                    2 * subgrid_size * subgrid_size + y_src * subgrid_size + x_src];
                idg::float2 pixelsYY = sph * subgrid[
                    s * NR_POLARIZATIONS * subgrid_size * subgrid_size +
                    3 * subgrid_size * subgrid_size + y_src * subgrid_size + x_src];

                // Apply aterm to subgrid: P*A1^H
                // [ pixels[0], pixels[1];    [ conj(aXX1), conj(aYX1);
                //   pixels[2], pixels[3] ] *   conj(aXY1), conj(aYY1) ]
                pixels[y][x][0]  = pixelsXX * aXX1;
                pixels[y][x][0] += pixelsXY * aXY1;
                pixels[y][x][1]  = pixelsXX * aYX1;
                pixels[y][x][1] += pixelsXY * aYY1;
                pixels[y][x][2]  = pixelsYX * aXX1;
                pixels[y][x][2] += pixelsYY * aXY1;
                pixels[y][x][3]  = pixelsYX * aYX1;
                pixels[y][x][3] += pixelsYY * aYY1;

                // Apply aterm to subgrid: A2*P
                // [ aXX2, aXY1;      [ pixels[0], pixels[1];
                //   aYX1, aYY2 ]  *    pixels[2], pixels[3] ]
                pixelsXX = pixels[y][x][0];
                pixelsXY = pixels[y][x][1];
                pixelsYX = pixels[y][x][2];
                pixelsYY = pixels[y][x][3];
                pixels[y][x][0]  = pixelsXX * aXX2;
                pixels[y][x][0] += pixelsYX * aXY2;
                pixels[y][x][1]  = pixelsXY * aXX2;
                pixels[y][x][1] += pixelsYY * aXY2;
                pixels[y][x][2]  = pixelsXX * aYX2;
                pixels[y][x][2] += pixelsYX * aYY2;
                pixels[y][x][3]  = pixelsXY * aYX2;
                pixels[y][x][3] += pixelsYY * aYY2;
            } // end x
        } // end y

        // Compute u and v offset in wavelenghts
        const float u_offset = (x_coordinate + subgrid_size/2 - grid_size/2)
                               * (2*M_PI / image_size);
        const float v_offset = (y_coordinate + subgrid_size/2 - grid_size/2)
                               * (2*M_PI / image_size);
        const float w_offset = 2*M_PI * w_offset_in_lambda;

        // Iterate all timesteps
        for (int time = 0; time < nr_timesteps; time++) {
            // Load UVW coordinates
            float u = uvw[local_offset + time].u;
            float v = uvw[local_offset + time].v;
            float w = uvw[local_offset + time].w;

            // Iterate all channels
            for (int chan = 0; chan < nr_channels; chan++) {

                // Update all polarizations
                idg::float2 sum[NR_POLARIZATIONS];
                memset(sum, 0, NR_POLARIZATIONS * sizeof(idg::float2));

                // Iterate all pixels in subgrid
                for (int y = 0; y < subgrid_size; y++) {
                    for (int x = 0; x < subgrid_size; x++) {

                        // Compute l,m,n
                        const float l = (x+0.5-(subgrid_size/2)) * image_size/subgrid_size;
                        const float m = (y+0.5-(subgrid_size/2)) * image_size/subgrid_size;
                        // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
                        // accurately for small values of l and m
                        const float tmp = (l * l) + (m * m);
                        const float n = tmp / (1.0f + sqrtf(1.0f - tmp));

                        // Compute phase index
                        float phase_index = u*l + v*m + w*n;

                        // Compute phase offset
                        float phase_offset = u_offset*l + v_offset*m + w_offset*n;

                        // Compute phase
                        float phase = (phase_index * wavenumbers[chan]) - phase_offset;

                        // Compute phasor
                        idg::float2 phasor = {cosf(phase), sinf(phase)};

                        for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                            sum[pol] += pixels[y][x][pol] * phasor;
                        }
                    } // end for x
                } // end for y

                const float scale = 1.0f / (subgrid_size*subgrid_size);
                size_t index = (local_offset + time)*nr_channels + chan;
                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    visibilities[index * NR_POLARIZATIONS + pol] = sum[pol] * scale;
                }
            } // end for channel
        } // end for time
    } // end for s
} // end kernel_degridder

void kernel_degridder_ref(
    int                       nr_subgrids,
    int                       grid_size,
    int                       subgrid_size,
    float                     image_size,
    float                     w_offset_in_lambda,
    int                       nr_channels,
    int                       nr_stations,
    idg::UVWCoordinate<float> uvw[],
    float                     wavenumbers[],
    idg::float2               visibilities[][NR_POLARIZATIONS],
    float                     spheroidal[subgrid_size][subgrid_size],
    idg::float2               aterms[][subgrid_size][subgrid_size][NR_POLARIZATIONS],
    idg::Metadata             metadata[],
    idg::float2               subgrid[][NR_POLARIZATIONS][subgrid_size][subgrid_size])
{
    kernel_degridder_ref_(
        nr_subgrids,
        grid_size,
        subgrid_size,
        image_size,
        w_offset_in_lambda,
        nr_channels,
        nr_stations,
        uvw,
        wavenumbers,
        (idg::float2*) visibilities,
        (float*) spheroidal,
        (idg::float2*) aterms,
        metadata,
        (idg::float2*) subgrid
    );
}
