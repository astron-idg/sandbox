#if defined(USE_VML)
#include <mkl_vml.h>
#endif

#include "gridder.h"

void kernel_gridder_opt_03(
    int                       nr_subgrids,
    int                       grid_size,
    int                       subgrid_size,
    float                     image_size,
    float                     w_offset_in_lambda,
    int                       nr_channels,
    int                       nr_stations,
    idg::UVWCoordinate<float> uvw[],
    float                     wavenumbers[],
    idg::float2               visibilities[][NR_POLARIZATIONS],
    float                     spheroidal[subgrid_size][subgrid_size],
    idg::float2               aterms[][subgrid_size][subgrid_size][NR_POLARIZATIONS],
    idg::Metadata             metadata[],
    idg::float2               subgrid[][NR_POLARIZATIONS][subgrid_size][subgrid_size]
    )
{
    // Find offset of first subgrid
    const idg::Metadata m       = metadata[0];
    const int baseline_offset_1 = m.baseline_offset;
    const int time_offset_1     = m.time_offset; // should be 0

    // Iterate all subgrids
    #pragma omp parallel
    {
        #ifdef USE_LIKWID
        likwid_markerThreadInit();
        likwid_markerStartRegion("gridder_opt_03");
        #endif

        #pragma omp for
        for (int s = 0; s < nr_subgrids; s++) {
            // Load metadata
            const idg::Metadata m  = metadata[s];
            const int offset       = (m.baseline_offset - baseline_offset_1) +
                                     (m.time_offset - time_offset_1);
            const int nr_timesteps = m.nr_timesteps;
            const int aterm_index  = m.aterm_index;
            const int station1     = m.baseline.station1;
            const int station2     = m.baseline.station2;
            const int x_coordinate = m.coordinate.x;
            const int y_coordinate = m.coordinate.y;

            // Compute u and v offset in wavelenghts
            const float u_offset = (x_coordinate + subgrid_size/2 - grid_size/2) * (2*M_PI / image_size);
            const float v_offset = (y_coordinate + subgrid_size/2 - grid_size/2) * (2*M_PI / image_size);
            const float w_offset = 2*M_PI * w_offset_in_lambda;

            // Preload visibilities
            const int nr_visibilities = nr_timesteps * nr_channels;
            float vis_real[NR_POLARIZATIONS][nr_visibilities];
            float vis_imag[NR_POLARIZATIONS][nr_visibilities];

            for (int time = 0; time < nr_timesteps; time++) {
                for (int chan = 0; chan < nr_channels; chan++) {
                    size_t index_src = (offset + time)*nr_channels + chan;
                    size_t index_dst = time * nr_channels;

                    for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        vis_real[pol][index_dst] = visibilities[index_src][pol].real;
                        vis_imag[pol][index_dst] = visibilities[index_src][pol].imag;
                    }
                }
            }

            // Preload uvw
            float uvw_u[nr_timesteps];
            float uvw_v[nr_timesteps];
            float uvw_w[nr_timesteps];

            for (int time = 0; time < nr_timesteps; time++) {
                uvw_u[time] = uvw[offset + time].u;
                uvw_v[time] = uvw[offset + time].v;
                uvw_w[time] = uvw[offset + time].w;
            }

            // Iterate all pixels in subgrid
            for (int y = 0; y < subgrid_size; y++) {
                for (int x = 0; x < subgrid_size; x++) {

                    // Compute phase
                    float phase[nr_visibilities];

                    // Compute l,m,n
                    const float l = (x+0.5-(subgrid_size/2)) * image_size/subgrid_size;
                    const float m = (y+0.5-(subgrid_size/2)) * image_size/subgrid_size;
                    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
                    // accurately for small values of l and m
                    const float tmp = (l * l) + (m * m);
                    const float n = tmp / (1.0f + sqrtf(1.0f - tmp));

                    #pragma vector aligned
                    for (int time = 0; time < nr_timesteps; time++) {
                        // Load UVW coordinates
                        float u = uvw_u[time];
                        float v = uvw_v[time];
                        float w = uvw_w[time];

                        // Compute phase index
                        float phase_index = u*l + v*m + w*n;

                        // Compute phase offset
                        float phase_offset = u_offset*l + v_offset*m + w_offset*n;

                        #pragma vector aligned
                        for (int chan = 0; chan < nr_channels; chan++) {
                            // Compute phase
                            float wavenumber = wavenumbers[chan];
                            phase[time * nr_channels + chan] = phase_offset - (phase_index * wavenumber);
                        }
                    } // end time

                    #if defined(USE_VML)
                    // Compute phasor
                    float phasor_real[nr_visibilities];
                    float phasor_imag[nr_visibilities];

                    vmsSinCos(
                        nr_timesteps * nr_channels,
                        (float *) phase,
                        (float *) phasor_imag,
                        (float *) phasor_real,
                        VML_LA);
                    #endif

                    // Initialize pixel for every polarization
                    float pixels_xx_real = 0.0f;
                    float pixels_xy_real = 0.0f;
                    float pixels_yx_real = 0.0f;
                    float pixels_yy_real = 0.0f;
                    float pixels_xx_imag = 0.0f;
                    float pixels_xy_imag = 0.0f;
                    float pixels_yx_imag = 0.0f;
                    float pixels_yy_imag = 0.0f;

                    #pragma vector aligned
                    #pragma omp simd reduction(+:pixels_xx_real,pixels_xx_imag, \
                                                 pixels_xy_real,pixels_xy_imag, \
                                                 pixels_yx_real,pixels_yx_imag, \
                                                 pixels_yy_real,pixels_yy_imag)
                    for (int i = 0; i < nr_visibilities; i++) {
                        int time = i / nr_channels;
                        int chan = i % nr_channels;

                        #if defined(USE_VML)
                        float phasor_real_ = phasor_real[i];
                        float phasor_imag_ = phasor_imag[i];
                        #else
                        float phasor_real_ = cosf(phase[i]);
                        float phasor_imag_ = sinf(phase[i]);
                        #endif

                        pixels_xx_real += vis_real[0][i] * phasor_real_;
                        pixels_xx_imag += vis_real[0][i] * phasor_imag_;
                        pixels_xx_real -= vis_imag[0][i] * phasor_imag_;
                        pixels_xx_imag += vis_imag[0][i] * phasor_real_;

                        pixels_xy_real += vis_real[1][i] * phasor_real_;
                        pixels_xy_imag += vis_real[1][i] * phasor_imag_;
                        pixels_xy_real -= vis_imag[1][i] * phasor_imag_;
                        pixels_xy_imag += vis_imag[1][i] * phasor_real_;

                        pixels_yx_real += vis_real[2][i] * phasor_real_;
                        pixels_yx_imag += vis_real[2][i] * phasor_imag_;
                        pixels_yx_real -= vis_imag[2][i] * phasor_imag_;
                        pixels_yx_imag += vis_imag[2][i] * phasor_real_;

                        pixels_yy_real += vis_real[3][i] * phasor_real_;
                        pixels_yy_imag += vis_real[3][i] * phasor_imag_;
                        pixels_yy_real -= vis_imag[3][i] * phasor_imag_;
                        pixels_yy_imag += vis_imag[3][i] * phasor_real_;
                    }

                    // Create the pixels
                    idg::float2 pixels[NR_POLARIZATIONS];
                    pixels[0] = {pixels_xx_real, pixels_xx_imag};
                    pixels[1] = {pixels_xy_real, pixels_xy_imag};
                    pixels[2] = {pixels_yx_real, pixels_yx_imag};
                    pixels[3] = {pixels_yy_real, pixels_yy_imag};

                    // Load a term for station1
                    idg::float2 aXX1 = aterms[aterm_index * nr_stations + station1][y][x][0];
                    idg::float2 aXY1 = aterms[aterm_index * nr_stations + station1][y][x][1];
                    idg::float2 aYX1 = aterms[aterm_index * nr_stations + station1][y][x][2];
                    idg::float2 aYY1 = aterms[aterm_index * nr_stations + station1][y][x][3];

                    // Load aterm for station2
                    idg::float2 aXX2 = conj(aterms[aterm_index * nr_stations + station2][y][x][0]);
                    idg::float2 aXY2 = conj(aterms[aterm_index * nr_stations + station2][y][x][1]);
                    idg::float2 aYX2 = conj(aterms[aterm_index * nr_stations + station2][y][x][2]);
                    idg::float2 aYY2 = conj(aterms[aterm_index * nr_stations + station2][y][x][3]);

                    apply_aterm(
                        aXX1, aXY1, aYX1, aYY1,
                        aXX2, aXY2, aYX2, aYY2,
                        pixels);

                    // Load spheroidal
                    float sph = spheroidal[y][x];

                    // Compute shifted position in subgrid
                    int x_dst = (x + (subgrid_size/2)) % subgrid_size;
                    int y_dst = (y + (subgrid_size/2)) % subgrid_size;

                    // Set subgrid value
                    for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        subgrid[s][pol][y_dst][x_dst] = pixels[pol] * sph;
                    }
                } // end for x
            } // end for y
        } // end for s

        #ifdef USE_LIKWID
        likwid_markerStopRegion("gridder_opt_03");
        #endif
    } // end parallel
} // end kernel_gridder_opt_03
