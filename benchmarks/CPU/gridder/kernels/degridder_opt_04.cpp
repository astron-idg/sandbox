#if defined(USE_VML)
#define VML_PRECISION VML_LA
#include <mkl_vml.h>
#endif

#include <immintrin.h>

#include "gridder.h"

#define ALIGNMENT 64

inline size_t index_visibility(
    int nr_channels,
    int nr_polarizations,
    int time,
    int chan,
    int pol)
{
    // visibilities: [nr_time][nr_channels][nr_polarizations]
    return static_cast<size_t>(time) * nr_channels * nr_polarizations +
           static_cast<size_t>(chan) * nr_polarizations +
           static_cast<size_t>(pol);
}

inline size_t index_aterm(
    int subgrid_size,
    int nr_polarizations,
    int nr_stations,
    int aterm_index,
    int station,
    int y,
    int x)
{
    // aterm: [nr_aterms][subgrid_size][subgrid_size][nr_polarizations]
    size_t aterm_nr = (aterm_index * nr_stations + station);
    return static_cast<size_t>(aterm_nr) * subgrid_size * subgrid_size * nr_polarizations +
           static_cast<size_t>(y) * subgrid_size * nr_polarizations +
           static_cast<size_t>(x) * nr_polarizations;
}

inline size_t index_subgrid(
    int nr_polarizations,
    int subgrid_size,
    int s,
    int pol,
    int y,
    int x)
{
    // subgrid: [nr_subgrids][nr_polarizations][subgrid_size][subgrid_size]
    return static_cast<size_t>(s) * nr_polarizations * subgrid_size * subgrid_size +
           static_cast<size_t>(pol) * subgrid_size * subgrid_size +
           static_cast<size_t>(y) * subgrid_size +
           static_cast<size_t>(x);
}

inline float compute_l(
    int x,
    int subgrid_size,
    float image_size)
{
    return (x+0.5-(subgrid_size/2)) * image_size/subgrid_size;
}

inline float compute_m(
    int y,
    int subgrid_size,
    float image_size)
{
    return compute_l(y, subgrid_size, image_size);
}

inline float compute_n(
    float l,
    float m)
{
    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
    // accurately for small values of l and m
    const float tmp = (l * l) + (m * m);
    return tmp > 1.0 ? 1.0 : tmp / (1.0f + sqrtf(1.0f - tmp));
}

inline void compute_sincos(
    const int n,
    const float *x,
    float *sin,
    float *cos
) {
    #if defined(USE_VML)
    vmsSinCos(n, x, sin, cos, VML_PRECISION);
    #else
    for (int i = 0; i < n; i++) {
            sin[i] = sinf(x[i]);
    }
    for (int i = 0; i < n; i++) {
            cos[i] = cosf(x[i]);
    }
    #endif
}

// http://bit.ly/2shIfmP
#if defined(__AVX__)
inline float _mm256_horizontal_add(__m256 x) {
    /* ( x3+x7, x2+x6, x1+x5, x0+x4 ) */
    const __m128 x128 = _mm_add_ps(_mm256_extractf128_ps(x, 1),
                                   _mm256_castps256_ps128(x));
    /* ( -, -, x1+x3+x5+x7, x0+x2+x4+x6 ) */
    const __m128 x64 = _mm_add_ps(x128,
                                   _mm_movehl_ps(x128, x128));
    /* ( -, -, -, x0+x1+x2+x3+x4+x5+x6+x7 ) */
    const __m128 x32 = _mm_add_ss(x64,
                                   _mm_shuffle_ps(x64, x64, 0x55));
    /* Conversion to float is a no-op on x86-64 */
    return _mm_cvtss_f32(x32);
}
#endif

#if defined(__AVX512F__)
inline float _mm512_horizontal_add(__m512 x) {
    /* x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15 */
    __m512 x1 = x;

    /* x8, x9, x10, x11, x12, x13, x14, x15, x0, x1, x2, x3, x4, x5, x6, x7 */
    __m512 x2 = _mm512_shuffle_f32x4(x1,x1,_MM_SHUFFLE(0,0,3,2));

    /* x0+x8, x1+x9, x2+x10, x3+x11, x4+x12, x5+x13, x6+x14, x7+x15, -, -, -, -, -, -, -, - */
    __m512 x3 = _mm512_add_ps(x1, x2);

    /* x4+x12, x5+x13, x6+x14, x7+x15, x0+x8, x1+x9, x2+x10, x3+x11, -, -, -, -, -, -, -, - */
    __m512 x4 = _mm512_shuffle_f32x4(x3,x3,_MM_SHUFFLE(0,0,0,1));

    /* x0+x8+x4+x12, x1+x9+x5+x13, x2+x10+x6+x14, x3+x11+x7+x15, -, -, -, -, -, -, -, -, -, -, -, - */
    __m512 x5 = _mm512_add_ps(x3, x4);

    /* x0+x8+x4+x12, x1+x9+x5+x13, x2+x10+x6+x14, x3+x11+x7+x15 */
    __m128 x6 = _mm512_castps512_ps128(x5);

    /* x0+x8+x4+x12+x1+x9+x5+x13, x2+x10+x6+x14+x3+x11+x7+x15, -, - */
    __m128 x7 = _mm_hadd_ps(x6, x6);

    /* x0+x8+x4+x12+x1+x9+x5+x13+x2+x10+x6+x14+x3+x11+x7+x15, -, - */
    __m128 x8 = _mm_hadd_ps(x7, x7);

    return  _mm_cvtss_f32(x8);
}
#endif

inline void compute_reduction_scalar(
    int *offset,
    const int n,
    const float input[NR_POLARIZATIONS][2][n],
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
    float sums[NR_POLARIZATIONS][2];

    #if defined(__INTEL_COMPILER)
    #pragma vector aligned
    #pragma omp simd reduction(+:sums)
    #endif
    for (int i = *offset; i < n; i++) {
        float phasor_real_ = phasor_real[i];
        float phasor_imag_ = phasor_imag[i];

        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            sums[pol][0] += input[pol][0][i] * phasor_real_;
            sums[pol][1] += input[pol][0][i] * phasor_imag_;
            sums[pol][0] -= input[pol][1][i] * phasor_imag_;
            sums[pol][1] += input[pol][1][i] * phasor_real_;
        }
    }

    *offset = n;

    // Update output
    for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
        output[pol] += {sums[pol][0], sums[pol][1]};
    }
} // end compute_reduction_scalar

inline void compute_reduction_avx2(
    int *offset,
    const int n,
    const float input[NR_POLARIZATIONS][2][n],
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
#if defined(__AVX2__)
    const int vector_length = 8;

    __m256 sums[NR_POLARIZATIONS][2];

    // Compute sums
    for (int i = *offset; i < (n / vector_length) * vector_length; i += vector_length) {
        __m256 phasor_r = _mm256_load_ps(&phasor_real[i]);
        __m256 phasor_i = _mm256_load_ps(&phasor_imag[i]);

        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            __m256 input_r = _mm256_load_ps(&input[pol][0][i]);
            __m256 input_i = _mm256_load_ps(&input[pol][1][i]);
            sums[pol][0] =  _mm256_fmadd_ps(input_r, phasor_r, sums[pol][0]);
            sums[pol][1] =  _mm256_fmadd_ps(input_r, phasor_i, sums[pol][1]);
            sums[pol][0] = _mm256_fnmadd_ps(input_i, phasor_i, sums[pol][0]);
            sums[pol][1] =  _mm256_fmadd_ps(input_i, phasor_r, sums[pol][1]);
        }
    }

    // Reduce all vectors
    if (n - *offset > 0) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            output[pol].real += _mm256_horizontal_add(sums[pol][0]);
            output[pol].imag += _mm256_horizontal_add(sums[pol][1]);
        }
    }

    *offset += vector_length * ((n - *offset) / vector_length);
#endif
} // end compute_reduction_avx2

inline void compute_reduction_avx(
    int *offset,
    const int n,
    const float input[NR_POLARIZATIONS][2][n],
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
#if defined(__AVX__)
    const int vector_length = 8;

    __m256 sums[NR_POLARIZATIONS][2];

    // Compute sums
    for (int i = *offset; i < (n / vector_length) * vector_length; i += vector_length) {
        __m256 phasor_r = _mm256_load_ps(&phasor_real[i]);
        __m256 phasor_i = _mm256_load_ps(&phasor_imag[i]);

        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            __m256 input_r = _mm256_load_ps(&input[pol][0][i]);
            __m256 input_i = _mm256_load_ps(&input[pol][1][i]);
            sums[pol][0] = _mm256_add_ps(sums[pol][0], _mm256_mul_ps(input_r, phasor_r));
            sums[pol][1] = _mm256_add_ps(sums[pol][1], _mm256_mul_ps(input_r, phasor_i));
            sums[pol][0] = _mm256_sub_ps(sums[pol][0], _mm256_mul_ps(input_i, phasor_i));
            sums[pol][1] = _mm256_add_ps(sums[pol][1], _mm256_mul_ps(input_i, phasor_r));
        }
    }

    // Reduce all vectors
    if (n - *offset > 0) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            output[pol].real += _mm256_horizontal_add(sums[pol][0]);
            output[pol].imag += _mm256_horizontal_add(sums[pol][1]);
        }
    }

    *offset += vector_length * ((n - *offset) / vector_length);
#endif
} // end compute_reduction_avx

inline void compute_reduction_avx512(
    int *offset,
    const int n,
    const float input[NR_POLARIZATIONS][2][n],
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
#if defined(__AVX512F__)
    const int vector_length = 16;

    __m512 sums[NR_POLARIZATIONS][2];

    // Compute sums
    for (int i = *offset; i < (n / vector_length) * vector_length; i += vector_length) {
        __m512 phasor_r = _mm512_load_ps(&phasor_real[i]);
        __m512 phasor_i = _mm512_load_ps(&phasor_imag[i]);

        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            __m512 input_r = _mm512_load_ps(&input[pol][0][i]);
            __m512 input_i = _mm512_load_ps(&input[pol][1][i]);
            sums[pol][0] =  _mm512_fmadd_ps(input_r, phasor_r, sums[pol][0]);
            sums[pol][1] =  _mm512_fmadd_ps(input_r, phasor_i, sums[pol][1]);
            sums[pol][0] = _mm512_fnmadd_ps(input_i, phasor_i, sums[pol][0]);
            sums[pol][1] =  _mm512_fmadd_ps(input_i, phasor_r, sums[pol][1]);
        }
    }

    // Reduce all vectors
    if (n - *offset > 0) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            output[pol].real += _mm512_horizontal_add(sums[pol][0]);
            output[pol].imag += _mm512_horizontal_add(sums[pol][1]);
        }
    }

    *offset += vector_length * ((n - *offset) / vector_length);
#endif
} // end compute_reduction_avx512

inline void compute_reduction(
    const int n,
    const float input[NR_POLARIZATIONS][2][n],
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
    int offset = 0;

    // Initialize output to zero
    memset(output, 0, NR_POLARIZATIONS * sizeof(idg::float2));

    // Vectorized loop, 16-elements, AVX512
    compute_reduction_avx512(
            &offset, n,
            input,
            phasor_real, phasor_imag,
            output);

    // Vectorized loop, 8-elements, AVX2
    compute_reduction_avx2(
            &offset, n,
            input,
            phasor_real, phasor_imag,
            output);

    // Vectorized loop, 8-elements, AVX
    compute_reduction_avx(
            &offset, n,
            input,
            phasor_real, phasor_imag,
            output);

    // Remainder loop, scalar
    compute_reduction_scalar(
            &offset, n,
            input,
            phasor_real, phasor_imag,
            output);
}

inline void matmul(
    const idg::float2 *a,
    const idg::float2 *b,
          idg::float2 *c)
{
    c[0]  = a[0] * b[0];
    c[1]  = a[1] * b[0];
    c[2]  = a[0] * b[2];
    c[3]  = a[1] * b[2];
    c[0] += a[2] * b[1];
    c[1] += a[3] * b[1];
    c[2] += a[2] * b[3];
    c[3] += a[3] * b[3];
}

inline void conjugate(
    const idg::float2 *a,
          idg::float2 *b)
{
    float s[8] = {1, -1, 1, -1, 1, -1, 1};
    float *a_ptr = (float *) a;
    float *b_ptr = (float *) b;

    for (unsigned i = 0; i < 8; i++) {
        b_ptr[i] = s[i] * a_ptr[i];
    }
}

inline void transpose(
    const idg::float2 *a,
          idg::float2 *b)
{
    b[0] = a[0];
    b[1] = a[2];
    b[2] = a[1];
    b[3] = a[3];
}

inline void hermitian(
    const idg::float2 *a,
          idg::float2 *b)
{
    idg::float2 temp[4];
    conjugate(a, temp);
    transpose(temp, b);
}

inline void apply_aterm_generic(
    idg::float2 *pixels,
    const idg::float2 *aterm1,
    const idg::float2 *aterm2)
{
    // Apply aterm: P = A1 * P
    idg::float2 temp1[4];
    matmul(pixels, aterm1, temp1);

    // Apply aterm: P = P * A2^H
    idg::float2 temp2[4];
    hermitian(aterm2, temp2);
    matmul(temp2, temp1, pixels);
}

void kernel_degridder_opt_04_(
    int                        nr_subgrids,
    int                        grid_size,
    int                        subgrid_size,
    float                      image_size,
    float                      w_step_in_lambda,
    int                        nr_channels,
    int                        nr_stations,
    idg::UVWCoordinate<float>* uvw,
    float*                     wavenumbers,
    idg::float2*               visibilities,
    float*                     spheroidal,
    idg::float2*               aterms,
    idg::Metadata*             metadata,
    idg::float2*               subgrid
    )
{
    // Find offset of first subgrid
    const idg::Metadata m       = metadata[0];
    const int baseline_offset_1 = m.baseline_offset;

    // Compute l,m,n
    const unsigned nr_pixels = subgrid_size*subgrid_size;
    float l_[nr_pixels];
    float m_[nr_pixels];
    float n_[nr_pixels];

    for (unsigned i = 0; i < nr_pixels; i++) {
        int y = i / subgrid_size;
        int x = i % subgrid_size;

        l_[i] = compute_l(x, subgrid_size, image_size);
        m_[i] = compute_m(y, subgrid_size, image_size);
        n_[i] = compute_n(l_[i], m_[i]);
    }

    // Iterate all subgrids
    //#pragma omp parallel for schedule(guided)
    for (int s = 0; s < nr_subgrids; s++) {

        // Load metadata
        const idg::Metadata m  = metadata[s];
        const int offset       = (m.baseline_offset - baseline_offset_1) + m.time_offset;
        const int nr_timesteps = m.nr_timesteps;
        const int aterm_index  = m.aterm_index;
        const int station1     = m.baseline.station1;
        const int station2     = m.baseline.station2;
        const int x_coordinate = m.coordinate.x;
        const int y_coordinate = m.coordinate.y;
        const float w_offset_in_lambda = w_step_in_lambda;// * (m.coordinate.z + 0.5);

        // Initialize aterm indices to first timestep
        size_t aterm1_idx_previous = 0;
        size_t aterm2_idx_previous = 0;

        // Storage
        float pixels_local[NR_POLARIZATIONS][2][nr_pixels] __attribute__((aligned((ALIGNMENT))));

        // Compute u and v offset in wavelenghts
        const float u_offset = (x_coordinate + subgrid_size/2 - grid_size/2)
                               * (2*M_PI / image_size);
        const float v_offset = (y_coordinate + subgrid_size/2 - grid_size/2)
                               * (2*M_PI / image_size);
        const float w_offset = 2*M_PI * w_offset_in_lambda;

        float phase_offset[nr_pixels];

        // Iterate all timesteps
        for (int time = 0; time < nr_timesteps; time++) {
            // Load UVW coordinates
            float u = uvw[offset + time].u;
            float v = uvw[offset + time].v;
            float w = uvw[offset + time].w;

            // Get aterm indices for current timestep
            size_t aterm1_idx_current = 0;
            size_t aterm2_idx_current = 0;

            // Determine whether aterm has changed
            bool aterm_changed = aterm1_idx_previous != aterm1_idx_current ||
                                 aterm2_idx_previous != aterm2_idx_current;

            float phase_index[nr_pixels];

            for (unsigned i = 0; i < nr_pixels; i++) {
                // Compute phase index
                phase_index[i] = u*l_[i] + v*m_[i] + w*n_[i];

                // Compute phase offset
                if (time == 0) {
                    phase_offset[i] = u_offset*l_[i] + v_offset*m_[i] + w_offset*n_[i];
                }
            }

            // Apply aterm to subgrid
            if (time == 0 || aterm_changed) {
                for (unsigned i = 0; i < nr_pixels; i++) {
                    int y = i / subgrid_size;
                    int x = i % subgrid_size;

                    // Load spheroidal
                    float _spheroidal = spheroidal[y * subgrid_size + x];

                    // Compute shifted position in subgrid
                    int x_src = (x + (subgrid_size/2)) % subgrid_size;
                    int y_src = (y + (subgrid_size/2)) % subgrid_size;

                    // Load pixel values and apply spheroidal
                    idg::float2 pixels[NR_POLARIZATIONS] __attribute__((aligned(ALIGNMENT)));
                    for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        size_t src_idx = index_subgrid(NR_POLARIZATIONS, subgrid_size, s, pol, y_src, x_src);
                        pixels[pol] = _spheroidal * subgrid[src_idx];
                    }

                    // Apply aterm
                    size_t station1_idx = index_aterm(subgrid_size, NR_POLARIZATIONS, nr_stations, aterm_index, station1, y, x);
                    size_t station2_idx = index_aterm(subgrid_size, NR_POLARIZATIONS, nr_stations, aterm_index, station2, y, x);
                    idg::float2 *aterm1_ptr = (idg::float2 *) &aterms[station1_idx];
                    idg::float2 *aterm2_ptr = (idg::float2 *) &aterms[station2_idx];
                    apply_aterm_generic(pixels, aterm1_ptr, aterm2_ptr);

                    // Store pixels
                    for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        pixels_local[pol][0][i] = pixels[pol].real;
                        pixels_local[pol][1][i] = pixels[pol].imag;
                    }
                }
            }

            // Iterate all channels
            for (int chan = 0; chan < nr_channels; chan++) {
                // Compute phase
                float phase[nr_pixels];

                for (unsigned i = 0; i < nr_pixels; i++) {
                    // Compute phase
                    float wavenumber = wavenumbers[chan];
                    phase[i] = (phase_index[i] * wavenumber) - phase_offset[i];
                }

                // Compute phasor
                float phasor_real[nr_pixels] __attribute__((aligned((ALIGNMENT))));
                float phasor_imag[nr_pixels] __attribute__((aligned((ALIGNMENT))));
                compute_sincos(nr_pixels, phase, phasor_imag, phasor_real);

                // Compute visibilities
                idg::float2 sums[NR_POLARIZATIONS];

                compute_reduction(
                    nr_pixels,
                    pixels_local,
                    phasor_real, phasor_imag, sums);

                // Store visibilities
                const float scale = 1.0f / nr_pixels;
                int time_idx = offset + time;
                int chan_idx = chan;
                size_t dst_idx = index_visibility( nr_channels, NR_POLARIZATIONS, time_idx, chan_idx, 0);
                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    visibilities[dst_idx+pol] = {scale*sums[pol].real, scale*sums[pol].imag};
                }
            } // end for channel
        } // end for time
    } // end #pragma parallel
} // end kernel_degridder


void kernel_degridder_opt_04(
    int                       nr_subgrids,
    int                       grid_size,
    int                       subgrid_size,
    float                     image_size,
    float                     w_offset_in_lambda,
    int                       nr_channels,
    int                       nr_stations,
    idg::UVWCoordinate<float> uvw[],
    float                     wavenumbers[],
    idg::float2               visibilities[][NR_POLARIZATIONS],
    float                     spheroidal[subgrid_size][subgrid_size],
    idg::float2               aterms[][subgrid_size][subgrid_size][NR_POLARIZATIONS],
    idg::Metadata             metadata[],
    idg::float2               subgrid[][NR_POLARIZATIONS][subgrid_size][subgrid_size]
    )
{
    kernel_degridder_opt_04_(
        nr_subgrids,
        grid_size,
        subgrid_size,
        image_size,
        w_offset_in_lambda,
        nr_channels,
        nr_stations,
        (idg::UVWCoordinate<float>*) uvw,
        (float*)                     wavenumbers,
        (idg::float2*)               visibilities,
        (float*)                     spheroidal,
        (idg::float2*)               aterms,
        (idg::Metadata*)             metadata,
        (idg::float2*)               subgrid);
}
