#if defined(USE_VML)
#define VML_PRECISION VML_LA
#include <mkl_vml.h>
#endif

#include <immintrin.h>

#include "gridder.h"

#define ALIGNMENT 64

inline size_t index_visibility(
    int nr_channels,
    int nr_polarizations,
    int time,
    int chan,
    int pol)
{
    // visibilities: [nr_time][nr_channels][nr_polarizations]
    return static_cast<size_t>(time) * nr_channels * nr_polarizations +
           static_cast<size_t>(chan) * nr_polarizations +
           static_cast<size_t>(pol);
}

inline size_t index_aterm(
    int subgrid_size,
    int nr_polarizations,
    int nr_stations,
    int aterm_index,
    int station,
    int y,
    int x)
{
    // aterm: [nr_aterms][subgrid_size][subgrid_size][nr_polarizations]
    size_t aterm_nr = (aterm_index * nr_stations + station);
    return static_cast<size_t>(aterm_nr) * subgrid_size * subgrid_size * nr_polarizations +
           static_cast<size_t>(y) * subgrid_size * nr_polarizations +
           static_cast<size_t>(x) * nr_polarizations;
}

inline size_t index_subgrid(
    int nr_polarizations,
    int subgrid_size,
    int s,
    int pol,
    int y,
    int x)
{
    // subgrid: [nr_subgrids][nr_polarizations][subgrid_size][subgrid_size]
    return static_cast<size_t>(s) * nr_polarizations * subgrid_size * subgrid_size +
           static_cast<size_t>(pol) * subgrid_size * subgrid_size +
           static_cast<size_t>(y) * subgrid_size +
           static_cast<size_t>(x);
}

inline float compute_l(
    int x,
    int subgrid_size,
    float image_size)
{
    return (x+0.5-(subgrid_size/2)) * image_size/subgrid_size;
}

inline float compute_m(
    int y,
    int subgrid_size,
    float image_size)
{
    return compute_l(y, subgrid_size, image_size);
}

inline float compute_n(
    float l,
    float m)
{
    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
    // accurately for small values of l and m
    const float tmp = (l * l) + (m * m);
    return tmp > 1.0 ? 1.0 : tmp / (1.0f + sqrtf(1.0f - tmp));
}

inline void compute_sincos(
    const int n,
    const float *x,
    float *sin,
    float *cos
) {
    #if defined(USE_VML)
    vmsSinCos(n, x, sin, cos, VML_PRECISION);
    #else
    for (int i = 0; i < n; i++) {
            sin[i] = sinf(x[i]);
    }
    for (int i = 0; i < n; i++) {
            cos[i] = cosf(x[i]);
    }
    #endif
}

// http://bit.ly/2shIfmP
#if defined(__AVX__)
inline float _mm256_horizontal_add(__m256 x) {
    /* ( x3+x7, x2+x6, x1+x5, x0+x4 ) */
    const __m128 x128 = _mm_add_ps(_mm256_extractf128_ps(x, 1),
                                   _mm256_castps256_ps128(x));
    /* ( -, -, x1+x3+x5+x7, x0+x2+x4+x6 ) */
    const __m128 x64 = _mm_add_ps(x128,
                                   _mm_movehl_ps(x128, x128));
    /* ( -, -, -, x0+x1+x2+x3+x4+x5+x6+x7 ) */
    const __m128 x32 = _mm_add_ss(x64,
                                   _mm_shuffle_ps(x64, x64, 0x55));
    /* Conversion to float is a no-op on x86-64 */
    return _mm_cvtss_f32(x32);
}
#endif

inline void compute_reduction_scalar(
    int *offset,
    const int n,
    const float *input_xx_real,
    const float *input_xy_real,
    const float *input_yx_real,
    const float *input_yy_real,
    const float *input_xx_imag,
    const float *input_xy_imag,
    const float *input_yx_imag,
    const float *input_yy_imag,
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
    float output_xx_real = 0.0f;
    float output_xy_real = 0.0f;
    float output_yx_real = 0.0f;
    float output_yy_real = 0.0f;
    float output_xx_imag = 0.0f;
    float output_xy_imag = 0.0f;
    float output_yx_imag = 0.0f;
    float output_yy_imag = 0.0f;

    #if defined(__INTEL_COMPILER)
    #pragma vector aligned
    #pragma omp simd reduction(+:output_xx_real,output_xx_imag, \
                                 output_xy_real,output_xy_imag, \
                                 output_yx_real,output_yx_imag, \
                                 output_yy_real,output_yy_imag)
    #endif
    for (int i = *offset; i < n; i++) {
        float phasor_real_ = phasor_real[i];
        float phasor_imag_ = phasor_imag[i];

        output_xx_real += input_xx_real[i] * phasor_real_;
        output_xx_imag += input_xx_real[i] * phasor_imag_;
        output_xx_real -= input_xx_imag[i] * phasor_imag_;
        output_xx_imag += input_xx_imag[i] * phasor_real_;

        output_xy_real += input_xy_real[i] * phasor_real_;
        output_xy_imag += input_xy_real[i] * phasor_imag_;
        output_xy_real -= input_xy_imag[i] * phasor_imag_;
        output_xy_imag += input_xy_imag[i] * phasor_real_;

        output_yx_real += input_yx_real[i] * phasor_real_;
        output_yx_imag += input_yx_real[i] * phasor_imag_;
        output_yx_real -= input_yx_imag[i] * phasor_imag_;
        output_yx_imag += input_yx_imag[i] * phasor_real_;

        output_yy_real += input_yy_real[i] * phasor_real_;
        output_yy_imag += input_yy_real[i] * phasor_imag_;
        output_yy_real -= input_yy_imag[i] * phasor_imag_;
        output_yy_imag += input_yy_imag[i] * phasor_real_;
    }

    *offset = n;

    // Update output
    output[0] += {output_xx_real, output_xx_imag};
    output[1] += {output_xy_real, output_xy_imag};
    output[2] += {output_yx_real, output_yx_imag};
    output[3] += {output_yy_real, output_yy_imag};
} // end compute_reduction_scalar

inline void compute_reduction_avx2(
    int *offset,
    const int n,
    const float *input_xx_real,
    const float *input_xy_real,
    const float *input_yx_real,
    const float *input_yy_real,
    const float *input_xx_imag,
    const float *input_xy_imag,
    const float *input_yx_imag,
    const float *input_yy_imag,
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
#if defined(__AVX2__)
    const int vector_length = 8;

    __m256 output_xx_r = _mm256_setzero_ps();
    __m256 output_xy_r = _mm256_setzero_ps();
    __m256 output_yx_r = _mm256_setzero_ps();
    __m256 output_yy_r = _mm256_setzero_ps();
    __m256 output_xx_i = _mm256_setzero_ps();
    __m256 output_xy_i = _mm256_setzero_ps();
    __m256 output_yx_i = _mm256_setzero_ps();
    __m256 output_yy_i = _mm256_setzero_ps();

    for (int i = *offset; i < (n / vector_length) * vector_length; i += vector_length) {
        __m256 input_xx, input_xy, input_yx, input_yy;
        __m256 phasor_r, phasor_i;

        phasor_r  = _mm256_load_ps(&phasor_real[i]);
        phasor_i  = _mm256_load_ps(&phasor_imag[i]);

        // Load real part of input
        input_xx = _mm256_load_ps(&input_xx_real[i]);
        input_xy = _mm256_load_ps(&input_xy_real[i]);
        input_yx = _mm256_load_ps(&input_yx_real[i]);
        input_yy = _mm256_load_ps(&input_yy_real[i]);

        // Update output
        output_xx_r = _mm256_fmadd_ps(input_xx, phasor_r, output_xx_r);
        output_xx_i = _mm256_fmadd_ps(input_xx, phasor_i, output_xx_i);
        output_xy_r = _mm256_fmadd_ps(input_xy, phasor_r, output_xy_r);
        output_xy_i = _mm256_fmadd_ps(input_xy, phasor_i, output_xy_i);
        output_yx_r = _mm256_fmadd_ps(input_yx, phasor_r, output_yx_r);
        output_yx_i = _mm256_fmadd_ps(input_yx, phasor_i, output_yx_i);
        output_yy_r = _mm256_fmadd_ps(input_yy, phasor_r, output_yy_r);
        output_yy_i = _mm256_fmadd_ps(input_yy, phasor_i, output_yy_i);

        // Load imag part of input
        input_xx = _mm256_load_ps(&input_xx_imag[i]);
        input_xy = _mm256_load_ps(&input_xy_imag[i]);
        input_yx = _mm256_load_ps(&input_yx_imag[i]);
        input_yy = _mm256_load_ps(&input_yy_imag[i]);

        // Update output
        output_xx_r = _mm256_fnmadd_ps(input_xx, phasor_i, output_xx_r);
        output_xx_i =  _mm256_fmadd_ps(input_xx, phasor_r, output_xx_i);
        output_xy_r = _mm256_fnmadd_ps(input_xy, phasor_i, output_xy_r);
        output_xy_i =  _mm256_fmadd_ps(input_xy, phasor_r, output_xy_i);
        output_yx_r = _mm256_fnmadd_ps(input_yx, phasor_i, output_yx_r);
        output_yx_i =  _mm256_fmadd_ps(input_yx, phasor_r, output_yx_i);
        output_yy_r = _mm256_fnmadd_ps(input_yy, phasor_i, output_yy_r);
        output_yy_i =  _mm256_fmadd_ps(input_yy, phasor_r, output_yy_i);
    }

    // Reduce all vectors
    if (n - *offset > 0) {
        output[0].real += _mm256_horizontal_add(output_xx_r);
        output[1].real += _mm256_horizontal_add(output_xy_r);
        output[2].real += _mm256_horizontal_add(output_yx_r);
        output[3].real += _mm256_horizontal_add(output_yy_r);
        output[0].imag += _mm256_horizontal_add(output_xx_i);
        output[1].imag += _mm256_horizontal_add(output_xy_i);
        output[2].imag += _mm256_horizontal_add(output_yx_i);
        output[3].imag += _mm256_horizontal_add(output_yy_i);
    }


    *offset += vector_length * ((n - *offset) / vector_length);
#endif
} // end compute_reduction_avx2

inline void compute_reduction_avx(
    int *offset,
    const int n,
    const float *input_xx_real,
    const float *input_xy_real,
    const float *input_yx_real,
    const float *input_yy_real,
    const float *input_xx_imag,
    const float *input_xy_imag,
    const float *input_yx_imag,
    const float *input_yy_imag,
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
#if defined(__AVX__)
    const int vector_length = 8;

    __m256 output_xx_r = _mm256_setzero_ps();
    __m256 output_xy_r = _mm256_setzero_ps();
    __m256 output_yx_r = _mm256_setzero_ps();
    __m256 output_yy_r = _mm256_setzero_ps();
    __m256 output_xx_i = _mm256_setzero_ps();
    __m256 output_xy_i = _mm256_setzero_ps();
    __m256 output_yx_i = _mm256_setzero_ps();
    __m256 output_yy_i = _mm256_setzero_ps();

    for (int i = *offset; i < (n / vector_length) * vector_length; i += vector_length) {
        __m256 input_xx, input_xy, input_yx, input_yy;
        __m256 phasor_r, phasor_i;

        phasor_r  = _mm256_load_ps(&phasor_real[i]);
        phasor_i  = _mm256_load_ps(&phasor_imag[i]);

        // Load real part of input
        input_xx = _mm256_load_ps(&input_xx_real[i]);
        input_xy = _mm256_load_ps(&input_xy_real[i]);
        input_yx = _mm256_load_ps(&input_yx_real[i]);
        input_yy = _mm256_load_ps(&input_yy_real[i]);

        // Update output
        output_xx_r = _mm256_add_ps(output_xx_r, _mm256_mul_ps(input_xx, phasor_r));
        output_xx_i = _mm256_add_ps(output_xx_i, _mm256_mul_ps(input_xx, phasor_i));
        output_xy_r = _mm256_add_ps(output_xy_r, _mm256_mul_ps(input_xy, phasor_r));
        output_xy_i = _mm256_add_ps(output_xy_i, _mm256_mul_ps(input_xy, phasor_i));
        output_yx_r = _mm256_add_ps(output_yx_r, _mm256_mul_ps(input_yx, phasor_r));
        output_yx_i = _mm256_add_ps(output_yx_i, _mm256_mul_ps(input_yx, phasor_i));
        output_yy_r = _mm256_add_ps(output_yy_r, _mm256_mul_ps(input_yy, phasor_r));
        output_yy_i = _mm256_add_ps(output_yy_i, _mm256_mul_ps(input_yy, phasor_i));

        // Load imag part of input
        input_xx = _mm256_load_ps(&input_xx_imag[i]);
        input_xy = _mm256_load_ps(&input_xy_imag[i]);
        input_yx = _mm256_load_ps(&input_yx_imag[i]);
        input_yy = _mm256_load_ps(&input_yy_imag[i]);

        // Update output
        output_xx_r = _mm256_sub_ps(output_xx_r, _mm256_mul_ps(input_xx, phasor_i));
        output_xx_i = _mm256_add_ps(output_xx_i, _mm256_mul_ps(input_xx, phasor_r));
        output_xy_r = _mm256_sub_ps(output_xy_r, _mm256_mul_ps(input_xy, phasor_i));
        output_xy_i = _mm256_add_ps(output_xy_i, _mm256_mul_ps(input_xy, phasor_r));
        output_yx_r = _mm256_sub_ps(output_yx_r, _mm256_mul_ps(input_yx, phasor_i));
        output_yx_i = _mm256_add_ps(output_yx_i, _mm256_mul_ps(input_yx, phasor_r));
        output_yy_r = _mm256_sub_ps(output_yy_r, _mm256_mul_ps(input_yy, phasor_i));
        output_yy_i = _mm256_add_ps(output_yy_i, _mm256_mul_ps(input_yy, phasor_r));
    }

    // Reduce all vectors
    if (n - *offset > 0) {
        output[0].real += _mm256_horizontal_add(output_xx_r);
        output[1].real += _mm256_horizontal_add(output_xy_r);
        output[2].real += _mm256_horizontal_add(output_yx_r);
        output[3].real += _mm256_horizontal_add(output_yy_r);
        output[0].imag += _mm256_horizontal_add(output_xx_i);
        output[1].imag += _mm256_horizontal_add(output_xy_i);
        output[2].imag += _mm256_horizontal_add(output_yx_i);
        output[3].imag += _mm256_horizontal_add(output_yy_i);
    }


    *offset += vector_length * ((n - *offset) / vector_length);
#endif
} // end compute_reduction_avx

inline void compute_reduction_avx512(
    int *offset,
    const int n,
    const float *input_xx_real,
    const float *input_xy_real,
    const float *input_yx_real,
    const float *input_yy_real,
    const float *input_xx_imag,
    const float *input_xy_imag,
    const float *input_yx_imag,
    const float *input_yy_imag,
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
#if defined(__AVX512F__)
    const int vector_length = 16;

    __m512 output_xx_r = _mm512_setzero_ps();
    __m512 output_xy_r = _mm512_setzero_ps();
    __m512 output_yx_r = _mm512_setzero_ps();
    __m512 output_yy_r = _mm512_setzero_ps();
    __m512 output_xx_i = _mm512_setzero_ps();
    __m512 output_xy_i = _mm512_setzero_ps();
    __m512 output_yx_i = _mm512_setzero_ps();
    __m512 output_yy_i = _mm512_setzero_ps();

    for (int i = *offset; i < (n / vector_length) * vector_length; i += vector_length) {
        __m512 input_xx, input_xy, input_yx, input_yy;
        __m512 phasor_r, phasor_i;

        phasor_r  = _mm512_load_ps(&phasor_real[i]);
        phasor_i  = _mm512_load_ps(&phasor_imag[i]);

        // Load real part of input
        input_xx = _mm512_load_ps(&input_xx_real[i]);
        input_xy = _mm512_load_ps(&input_xy_real[i]);
        input_yx = _mm512_load_ps(&input_yx_real[i]);
        input_yy = _mm512_load_ps(&input_yy_real[i]);

        // Update output
        output_xx_r = _mm512_fmadd_ps(input_xx, phasor_r, output_xx_r);
        output_xx_i = _mm512_fmadd_ps(input_xx, phasor_i, output_xx_i);
        output_xy_r = _mm512_fmadd_ps(input_xy, phasor_r, output_xy_r);
        output_xy_i = _mm512_fmadd_ps(input_xy, phasor_i, output_xy_i);
        output_yx_r = _mm512_fmadd_ps(input_yx, phasor_r, output_yx_r);
        output_yx_i = _mm512_fmadd_ps(input_yx, phasor_i, output_yx_i);
        output_yy_r = _mm512_fmadd_ps(input_yy, phasor_r, output_yy_r);
        output_yy_i = _mm512_fmadd_ps(input_yy, phasor_i, output_yy_i);

        // Load imag part of input
        input_xx = _mm512_load_ps(&input_xx_imag[i]);
        input_xy = _mm512_load_ps(&input_xy_imag[i]);
        input_yx = _mm512_load_ps(&input_yx_imag[i]);
        input_yy = _mm512_load_ps(&input_yy_imag[i]);

        // Update output
        output_xx_r = _mm512_fnmadd_ps(input_xx, phasor_i, output_xx_r);
        output_xx_i =  _mm512_fmadd_ps(input_xx, phasor_r, output_xx_i);
        output_xy_r = _mm512_fnmadd_ps(input_xy, phasor_i, output_xy_r);
        output_xy_i =  _mm512_fmadd_ps(input_xy, phasor_r, output_xy_i);
        output_yx_r = _mm512_fnmadd_ps(input_yx, phasor_i, output_yx_r);
        output_yx_i =  _mm512_fmadd_ps(input_yx, phasor_r, output_yx_i);
        output_yy_r = _mm512_fnmadd_ps(input_yy, phasor_i, output_yy_r);
        output_yy_i =  _mm512_fmadd_ps(input_yy, phasor_r, output_yy_i);
    }

    // Reduce all vectors
    if (n - *offset > 0) {
        output[0].real += _mm512_reduce_add_ps(output_xx_r);
        output[1].real += _mm512_reduce_add_ps(output_xy_r);
        output[2].real += _mm512_reduce_add_ps(output_yx_r);
        output[3].real += _mm512_reduce_add_ps(output_yy_r);
        output[0].imag += _mm512_reduce_add_ps(output_xx_i);
        output[1].imag += _mm512_reduce_add_ps(output_xy_i);
        output[2].imag += _mm512_reduce_add_ps(output_yx_i);
        output[3].imag += _mm512_reduce_add_ps(output_yy_i);
    }

    *offset += vector_length * ((n - *offset) / vector_length);
#endif
} // end compute_reduction_avx512

inline void compute_reduction(
    const int n,
    const float *input_xx_real,
    const float *input_xy_real,
    const float *input_yx_real,
    const float *input_yy_real,
    const float *input_xx_imag,
    const float *input_xy_imag,
    const float *input_yx_imag,
    const float *input_yy_imag,
    const float *phasor_real,
    const float *phasor_imag,
    idg::float2 output[NR_POLARIZATIONS])
{
    int offset = 0;

    // Initialize output to zero
    memset(output, 0, NR_POLARIZATIONS * sizeof(idg::float2));

    // Vectorized loop, 16-elements, AVX512
    compute_reduction_avx512(
            &offset, n,
            input_xx_real, input_xy_real, input_yx_real, input_yy_real,
            input_xx_imag, input_xy_imag, input_yx_imag, input_yy_imag,
            phasor_real, phasor_imag,
            output);

    // Vectorized loop, 8-elements, AVX2
    compute_reduction_avx2(
            &offset, n,
            input_xx_real, input_xy_real, input_yx_real, input_yy_real,
            input_xx_imag, input_xy_imag, input_yx_imag, input_yy_imag,
            phasor_real, phasor_imag,
            output);

    // Vectorized loop, 8-elements, AVX
    compute_reduction_avx(
            &offset, n,
            input_xx_real, input_xy_real, input_yx_real, input_yy_real,
            input_xx_imag, input_xy_imag, input_yx_imag, input_yy_imag,
            phasor_real, phasor_imag,
            output);

    // Remainder loop, scalar
    compute_reduction_scalar(
            &offset, n,
            input_xx_real, input_xy_real, input_yx_real, input_yy_real,
            input_xx_imag, input_xy_imag, input_yx_imag, input_yy_imag,
            phasor_real, phasor_imag,
            output);
}

inline void matmul(
    const idg::float2 *a,
    const idg::float2 *b,
          idg::float2 *c)
{
    c[0]  = a[0] * b[0];
    c[1]  = a[1] * b[0];
    c[2]  = a[0] * b[2];
    c[3]  = a[1] * b[2];
    c[0] += a[2] * b[1];
    c[1] += a[3] * b[1];
    c[2] += a[2] * b[3];
    c[3] += a[3] * b[3];
}

inline void conjugate(
    const idg::float2 *a,
          idg::float2 *b)
{
    float s[8] = {1, -1, 1, -1, 1, -1, 1};
    float *a_ptr = (float *) a;
    float *b_ptr = (float *) b;

    for (unsigned i = 0; i < 8; i++) {
        b_ptr[i] = s[i] * a_ptr[i];
    }
}

inline void transpose(
    const idg::float2 *a,
          idg::float2 *b)
{
    b[0] = a[0];
    b[1] = a[2];
    b[2] = a[1];
    b[3] = a[3];
}

inline void hermitian(
    const idg::float2 *a,
          idg::float2 *b)
{
    idg::float2 temp[4];
    conjugate(a, temp);
    transpose(temp, b);
}

inline void apply_aterm_generic(
    idg::float2 *pixels,
    const idg::float2 *aterm1,
    const idg::float2 *aterm2)
{
    // Apply aterm: P = A1 * P
    idg::float2 temp1[4];
    matmul(pixels, aterm1, temp1);

    // Apply aterm: P = P * A2^H
    idg::float2 temp2[4];
    hermitian(aterm2, temp2);
    matmul(temp2, temp1, pixels);
}

void kernel_degridder_opt_03_(
    int                        nr_subgrids,
    int                        grid_size,
    int                        subgrid_size,
    float                      image_size,
    float                      w_step_in_lambda,
    int                        nr_channels,
    int                        nr_stations,
    idg::UVWCoordinate<float>* uvw,
    float*                     wavenumbers,
    idg::float2*               visibilities,
    float*                     spheroidal,
    idg::float2*               aterms,
    idg::Metadata*             metadata,
    idg::float2*               subgrid
    )
{
    // Find offset of first subgrid
    const idg::Metadata m       = metadata[0];
    const int baseline_offset_1 = m.baseline_offset;

    // Compute l,m,n
    const unsigned nr_pixels = subgrid_size*subgrid_size;
    float l_[nr_pixels];
    float m_[nr_pixels];
    float n_[nr_pixels];

    for (unsigned i = 0; i < nr_pixels; i++) {
        int y = i / subgrid_size;
        int x = i % subgrid_size;

        l_[i] = compute_l(x, subgrid_size, image_size);
        m_[i] = compute_m(y, subgrid_size, image_size);
        n_[i] = compute_n(l_[i], m_[i]);
    }

    // Iterate all subgrids
    #pragma omp parallel for schedule(guided)
    for (int s = 0; s < nr_subgrids; s++) {

        // Load metadata
        const idg::Metadata m  = metadata[s];
        const int offset       = (m.baseline_offset - baseline_offset_1) + m.time_offset;
        const int nr_timesteps = m.nr_timesteps;
        const int aterm_index  = m.aterm_index;
        const int station1     = m.baseline.station1;
        const int station2     = m.baseline.station2;
        const int x_coordinate = m.coordinate.x;
        const int y_coordinate = m.coordinate.y;
        const float w_offset_in_lambda = w_step_in_lambda;// * (m.coordinate.z + 0.5);

        // Initialize aterm indices to first timestep
        size_t aterm1_idx_previous = 0;
        size_t aterm2_idx_previous = 0;

        // Storage
        float pixels_xx_real[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_xy_real[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_yx_real[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_yy_real[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_xx_imag[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_xy_imag[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_yx_imag[nr_pixels] __attribute__((aligned((ALIGNMENT))));
        float pixels_yy_imag[nr_pixels] __attribute__((aligned((ALIGNMENT))));

        // Compute u and v offset in wavelenghts
        const float u_offset = (x_coordinate + subgrid_size/2 - grid_size/2)
                               * (2*M_PI / image_size);
        const float v_offset = (y_coordinate + subgrid_size/2 - grid_size/2)
                               * (2*M_PI / image_size);
        const float w_offset = 2*M_PI * w_offset_in_lambda;

        float phase_offset[nr_pixels];

        // Iterate all timesteps
        for (int time = 0; time < nr_timesteps; time++) {
            // Load UVW coordinates
            float u = uvw[offset + time].u;
            float v = uvw[offset + time].v;
            float w = uvw[offset + time].w;

            // Get aterm indices for current timestep
            size_t aterm1_idx_current = 0;
            size_t aterm2_idx_current = 0;

            // Determine whether aterm has changed
            bool aterm_changed = aterm1_idx_previous != aterm1_idx_current ||
                                 aterm2_idx_previous != aterm2_idx_current;

            float phase_index[nr_pixels];

            for (unsigned i = 0; i < nr_pixels; i++) {
                // Compute phase index
                phase_index[i] = u*l_[i] + v*m_[i] + w*n_[i];

                // Compute phase offset
                if (time == 0) {
                    phase_offset[i] = u_offset*l_[i] + v_offset*m_[i] + w_offset*n_[i];
                }
            }

            // Apply aterm to subgrid
            if (time == 0 || aterm_changed) {
                for (unsigned i = 0; i < nr_pixels; i++) {
                    int y = i / subgrid_size;
                    int x = i % subgrid_size;

                    // Load spheroidal
                    float _spheroidal = spheroidal[y * subgrid_size + x];

                    // Compute shifted position in subgrid
                    int x_src = (x + (subgrid_size/2)) % subgrid_size;
                    int y_src = (y + (subgrid_size/2)) % subgrid_size;

                    // Load pixel values and apply spheroidal
                    idg::float2 pixels[NR_POLARIZATIONS] __attribute__((aligned(ALIGNMENT)));
                    for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        size_t src_idx = index_subgrid(NR_POLARIZATIONS, subgrid_size, s, pol, y_src, x_src);
                        pixels[pol] = _spheroidal * subgrid[src_idx];
                    }

                    // Apply aterm
                    size_t station1_idx = index_aterm(subgrid_size, NR_POLARIZATIONS, nr_stations, aterm_index, station1, y, x);
                    size_t station2_idx = index_aterm(subgrid_size, NR_POLARIZATIONS, nr_stations, aterm_index, station2, y, x);
                    idg::float2 *aterm1_ptr = (idg::float2 *) &aterms[station1_idx];
                    idg::float2 *aterm2_ptr = (idg::float2 *) &aterms[station2_idx];
                    apply_aterm_generic(pixels, aterm1_ptr, aterm2_ptr);

                    // Store pixels
                    pixels_xx_real[i] = pixels[0].real;
                    pixels_xy_real[i] = pixels[1].real;
                    pixels_yx_real[i] = pixels[2].real;
                    pixels_yy_real[i] = pixels[3].real;
                    pixels_xx_imag[i] = pixels[0].imag;
                    pixels_xy_imag[i] = pixels[1].imag;
                    pixels_yx_imag[i] = pixels[2].imag;
                    pixels_yy_imag[i] = pixels[3].imag;
                }
            }

            // Iterate all channels
            for (int chan = 0; chan < nr_channels; chan++) {
                // Compute phase
                float phase[nr_pixels];

                for (unsigned i = 0; i < nr_pixels; i++) {
                    // Compute phase
                    float wavenumber = wavenumbers[chan];
                    phase[i] = (phase_index[i] * wavenumber) - phase_offset[i];
                }

                // Compute phasor
                float phasor_real[nr_pixels] __attribute__((aligned((ALIGNMENT))));
                float phasor_imag[nr_pixels] __attribute__((aligned((ALIGNMENT))));
                compute_sincos(nr_pixels, phase, phasor_imag, phasor_real);

                // Compute visibilities
                idg::float2 sums[NR_POLARIZATIONS];

                compute_reduction(
                    nr_pixels,
                    pixels_xx_real, pixels_xy_real, pixels_yx_real, pixels_yy_real,
                    pixels_xx_imag, pixels_xy_imag, pixels_yx_imag, pixels_yy_imag,
                    phasor_real, phasor_imag, sums);

                // Store visibilities
                const float scale = 1.0f / nr_pixels;
                int time_idx = offset + time;
                int chan_idx = chan;
                size_t dst_idx = index_visibility( nr_channels, NR_POLARIZATIONS, time_idx, chan_idx, 0);
                for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                    visibilities[dst_idx+pol] = {scale*sums[pol].real, scale*sums[pol].imag};
                }
            } // end for channel
        } // end for time
    } // end #pragma parallel
} // end kernel_degridder


void kernel_degridder_opt_03(
    int                       nr_subgrids,
    int                       grid_size,
    int                       subgrid_size,
    float                     image_size,
    float                     w_offset_in_lambda,
    int                       nr_channels,
    int                       nr_stations,
    idg::UVWCoordinate<float> uvw[],
    float                     wavenumbers[],
    idg::float2               visibilities[][NR_POLARIZATIONS],
    float                     spheroidal[subgrid_size][subgrid_size],
    idg::float2               aterms[][subgrid_size][subgrid_size][NR_POLARIZATIONS],
    idg::Metadata             metadata[],
    idg::float2               subgrid[][NR_POLARIZATIONS][subgrid_size][subgrid_size]
    )
{
    kernel_degridder_opt_03_(
        nr_subgrids,
        grid_size,
        subgrid_size,
        image_size,
        w_offset_in_lambda,
        nr_channels,
        nr_stations,
        (idg::UVWCoordinate<float>*) uvw,
        (float*)                     wavenumbers,
        (idg::float2*)               visibilities,
        (float*)                     spheroidal,
        (idg::float2*)               aterms,
        (idg::Metadata*)             metadata,
        (idg::float2*)               subgrid);
}
