#ifndef IDG_GLOBALS_H_
#define IDG_GLOBALS_H_

// Default parameters
// (can be overwritten by environmental variables)
const unsigned int DEFAULT_NR_STATIONS    = 15;
const unsigned int DEFAULT_NR_CHANNELS    = 16;
const unsigned int DEFAULT_NR_TIME        = 1024;
const unsigned int DEFAULT_NR_TIMESLOTS   = 8;
const float DEFAULT_IMAGESIZE             = 0.1f;
const unsigned int DEFAULT_GRIDSIZE       = 1024;
const unsigned int DEFAULT_SUBGRIDSIZE    = 24;
const unsigned int DEFAULT_NR_REPETITIONS = 1;

// Fixed parameters
const int NR_POLARIZATIONS = 4;
const int INTEGRATION_TIME = 1;

#endif
