#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>
#include <cmath>
#include <limits>

#include "auxiliary.h"


uint64_t gridder_flops(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size)
{
    uint64_t nr_polarizations = NR_POLARIZATIONS;

    // Number of flops per visibility
    uint64_t flops_per_visibility = 0;
    flops_per_visibility += 5; // phase index
    flops_per_visibility += 5; // phase offset
    flops_per_visibility += nr_channels * 2; // phase
    flops_per_visibility += nr_channels * nr_polarizations * 8; // update

    // Number of flops per subgrid
    uint64_t flops_per_subgrid = 0;
    flops_per_subgrid += nr_polarizations * 30; // aterm
    flops_per_subgrid += nr_polarizations * 2; // spheroidal
    flops_per_subgrid += 6; // shift

    // Total number of flops
    uint64_t flops_total = 0;
    flops_total += nr_timesteps * subgrid_size * subgrid_size * flops_per_visibility;
    flops_total += nr_subgrids  * subgrid_size * subgrid_size * flops_per_subgrid;
    return flops_total;
}


uint64_t gridder_sincos(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size)
{
    uint64_t nr_polarizations = NR_POLARIZATIONS;

    // Number of sincos per visibility
    uint64_t sincos_per_visibility = 0;
    sincos_per_visibility += nr_channels; // phasor

    // Total number of sincos
    uint64_t sincos_total = 0;
    sincos_total += nr_timesteps * subgrid_size * subgrid_size * sincos_per_visibility;
    return sincos_total;
}


uint64_t gridder_bytes(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size)
{
    uint64_t nr_polarizations = NR_POLARIZATIONS;

    // Number of bytes per uvw coordinate
    uint64_t bytes_per_uvw = 0;
    bytes_per_uvw += 1ULL * 3 * sizeof(float); // uvw

    // Number of bytes per visibility
    uint64_t bytes_per_vis = 0;
    bytes_per_vis += 1ULL * nr_channels * nr_polarizations * 2 * sizeof(float); // visibilities

    // Number of bytes per pixel
    uint64_t bytes_per_pix = 0;
    bytes_per_pix += 1ULL * nr_polarizations * 2 * sizeof(float); // pixel

    // Total number of bytes
    uint64_t bytes_total = 0;
    bytes_total += 1ULL * nr_timesteps * bytes_per_uvw;
    bytes_total += 1ULL * nr_timesteps * bytes_per_vis;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_pix;
    return bytes_total;
}


// computes max|A[i]-B[i]| / max|B[i]|
float get_accucary(
    const int size,
    const std::complex<float>* A,
    const std::complex<float>* B)
{
    float max_abs_error = 0.0f;
    float max_ref_val = 0.0f;
    float max_val = 0.0f;
    for (int i=0; i<size; i++) {
        float abs_error = abs(A[i] - B[i]);
        if ( abs_error > max_abs_error ) {
            max_abs_error = abs_error;
        }
        if (abs(B[i]) > max_ref_val) {
            max_ref_val = abs(B[i]);
        }
        if (abs(A[i]) > max_val) {
            max_val = abs(A[i]);
        }
    }
    if (max_ref_val == 0.0f) {
        if (max_val == 0.0f)
            // both grid are zero
            return 0.0f;
        else
            // refrence grid is zero, but computed grid not
            return std::numeric_limits<float>::infinity();
    } else {
        return max_abs_error / max_ref_val;
    }
}

std::tuple<int, int, int, int, float, int, int, int, int, int>read_parameters() {
    char *cstr_nr_stations = getenv("NR_STATIONS");
    auto nr_stations = cstr_nr_stations ? atoi(cstr_nr_stations): DEFAULT_NR_STATIONS;

    char *cstr_nr_channels = getenv("NR_CHANNELS");
    auto nr_channels = cstr_nr_channels ? atoi(cstr_nr_channels) : DEFAULT_NR_CHANNELS;

    char *cstr_nr_time = getenv("NR_TIME");
    auto nr_time = cstr_nr_time ? atoi(cstr_nr_time) : DEFAULT_NR_TIME;

    char *cstr_nr_timeslots = getenv("NR_TIMESLOTS");
    auto nr_timeslots = cstr_nr_timeslots ? atoi(cstr_nr_timeslots) : DEFAULT_NR_TIMESLOTS;

    char *cstr_image_size = getenv("IMAGESIZE");
    auto image_size = cstr_image_size ? atof(cstr_image_size) : DEFAULT_IMAGESIZE;

    char *cstr_grid_size = getenv("GRIDSIZE");
    auto grid_size = cstr_grid_size ? atoi(cstr_grid_size) : DEFAULT_GRIDSIZE;

    char *cstr_subgrid_size = getenv("SUBGRIDSIZE");
    auto subgrid_size = cstr_subgrid_size ? atoi(cstr_subgrid_size) : DEFAULT_SUBGRIDSIZE;

    char *cstr_kernel_size = getenv("KERNELSIZE");
    auto kernel_size = cstr_kernel_size ? atoi(cstr_kernel_size) : (subgrid_size / 4) + 1;

    char *cstr_nr_repetitions = getenv("NR_REPETITIONS");
    auto nr_repetitions = cstr_nr_repetitions ? atoi(cstr_nr_repetitions) : DEFAULT_NR_REPETITIONS;

    char *cstr_max_nr_time = getenv("MAX_NR_TIME");
    auto max_nr_time = cstr_max_nr_time ? atoi(cstr_max_nr_time) : nr_time;

    return std::make_tuple(
        nr_stations, nr_channels, nr_time, nr_timeslots,
        image_size, grid_size, subgrid_size, kernel_size,
        nr_repetitions, max_nr_time);
}

void print_parameters(
    unsigned int nr_stations,
    unsigned int nr_channels,
    unsigned int nr_timesteps,
    unsigned int nr_timeslots,
    float image_size,
    unsigned int grid_size,
    unsigned int subgrid_size,
    unsigned int kernel_size,
    unsigned int max_nr_time
) {
    const int fw1 = 30;
    const int fw2 = 10;
    std::ostream &os = std::clog;

    os << "-----------" << std::endl;
    os << "PARAMETERS:" << std::endl;

    os << std::setw(fw1) << std::left << "Number of stations" << "== "
       << std::setw(fw2) << std::right << nr_stations << std::endl;

    os << std::setw(fw1) << std::left << "Number of channels" << "== "
       << std::setw(fw2) << std::right << nr_channels << std::endl;

    os << std::setw(fw1) << std::left << "Number of timesteps" << "== "
       << std::setw(fw2) << std::right << nr_timesteps << std::endl;

    os << std::setw(fw1) << std::left << "Number of timeslots" << "== "
       << std::setw(fw2) << std::right << nr_timeslots << std::endl;

    os << std::setw(fw1) << std::left << "Imagesize" << "== "
       << std::setw(fw2) << std::right << image_size  << std::endl;

    os << std::setw(fw1) << std::left << "Grid size" << "== "
       << std::setw(fw2) << std::right << grid_size << std::endl;

    os << std::setw(fw1) << std::left << "Subgrid size" << "== "
       << std::setw(fw2) << std::right << subgrid_size << std::endl;

    os << std::setw(fw1) << std::left << "Kernel size" << "== "
       << std::setw(fw2) << std::right << kernel_size << std::endl;

    os << std::setw(fw1) << std::left << "Max nr time" << "== "
       << std::setw(fw2) << std::right << max_nr_time << std::endl;

    os << "-----------" << std::endl;
}
