#pragma once

#include <complex>
#include <cstring>
#include <tuple>

#include "Globals.h"
#include "Init.h"
#include "Globals.h"
#include "Plan.h"

uint64_t gridder_visibilities(uint64_t nr_timesteps);
uint64_t gridder_sincos(uint64_t nr_channels, uint64_t nr_timesteps, uint64_t nr_subgrids, uint64_t subgrid_size);
uint64_t gridder_flops(uint64_t nr_channels, uint64_t nr_timesteps, uint64_t nr_subgrids, uint64_t subgrid_size);
uint64_t gridder_bytes(uint64_t nr_channels, uint64_t nr_timesteps, uint64_t nr_subgrids, uint64_t subgrid_size); 
void memset_array(void *clear, size_t num_bytes);

float get_accucary(
    const int size,
    const std::complex<float>* A,
    const std::complex<float>* B);


std::tuple<int, int, int, int, float, int, int, int, int, int>read_parameters();

void print_parameters(
    unsigned int nr_stations,
    unsigned int nr_channels,
    unsigned int nr_timesteps,
    unsigned int nr_timeslots,
    float image_size,
    unsigned int grid_size,
    unsigned int subgrid_size,
    unsigned int kernel_size,
    unsigned int max_nr_timesteps
);
