#include <iostream>
#include <iomanip>
#include <complex>
#include <cstdint>
#include <limits>
#include <cstring>

#include "auxiliary.h"
#include "gridder.h"
#include "Globals.h"

using FunctionPtr = void (*)(
    int,    // nr_subgrids
    int,    // gridsize
    int,    // subgridsize
    float,  // imagesize
    float,  // w_offset_in_lambda
    int,    // nr_channels
    int,    // nr_stations
    void*,  // uvw
    void*,  // wavenumbers
    void*,  // visibilities
    void*,  // spheroidal
    void*,  // aterm
    int*,   // metadata
    void*); // subgrid

void run(const std::string& name, FunctionPtr kernel_gridder);
