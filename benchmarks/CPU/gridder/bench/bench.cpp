#include <tuple>

#include "bench.h"

using namespace std;

int invocations = 0;

void run(const string& name, FunctionPtr kernel_gridder)
{
    // Constants
    unsigned int nr_correlations = NR_POLARIZATIONS;
    float w_offset = 0;
    unsigned int nr_stations;
    unsigned int nr_channels;
    unsigned int nr_timesteps;
    unsigned int nr_timeslots;
    float image_size;
    unsigned int grid_size;
    unsigned int subgrid_size;
    unsigned int kernel_size;
    unsigned int nr_repetitions;
    unsigned int max_nr_timesteps;

    // Read parameters from environment
    std::tie(
        nr_stations, nr_channels, nr_timesteps, nr_timeslots,
        image_size, grid_size, subgrid_size, kernel_size,
        nr_repetitions, max_nr_timesteps) = read_parameters();

    // Print parameters
    if (invocations++ == 0)
    print_parameters(
        nr_stations, nr_channels, nr_timesteps, nr_timeslots,
        image_size, grid_size, subgrid_size, kernel_size, max_nr_timesteps);

    // Compute nr_baselines
    unsigned int nr_baselines = (nr_stations * (nr_stations - 1)) / 2;

    // Compute cell_size
    float cell_size = image_size / grid_size;

    // Allocate and initialize data structures
    idg::Array1D<float> frequencies =
        idg::get_example_frequencies(nr_channels);
    idg::Array1D<float> wavenumbers =
        idg::get_example_wavenumbers(nr_channels);
    idg::Array3D<idg::Visibility<std::complex<float>>> visibilities =
        idg::get_example_visibilities(nr_baselines, nr_timesteps, nr_channels);
    idg::Array1D<std::pair<unsigned int,unsigned int>> baselines =
        idg::get_example_baselines(nr_stations, nr_baselines);
    idg::Array2D<idg::UVWCoordinate<float>> uvw =
        idg::get_example_uvw(nr_stations, nr_baselines, nr_timesteps);
    idg::Array3D<std::complex<float>> grid =
        idg::get_zero_grid(nr_correlations, grid_size, grid_size);
    idg::Array4D<idg::Matrix2x2<std::complex<float>>> aterms =
        idg::get_example_aterms(nr_timeslots, nr_stations, subgrid_size, subgrid_size);
    idg::Array1D<unsigned int> aterms_offsets =
        idg::get_example_aterms_offsets(nr_timeslots, nr_timesteps);
    idg::Array2D<float> spheroidal =
        idg::get_example_spheroidal(subgrid_size, subgrid_size);

    // Create plan
    idg::Plan plan(
        kernel_size, subgrid_size, grid_size, cell_size,
        frequencies, uvw, baselines, aterms_offsets, max_nr_timesteps);
    auto total_nr_subgrids  = plan.get_nr_subgrids();
    auto total_nr_timesteps = plan.get_nr_timesteps();
    auto metadata           = plan.get_metadata_ptr();

    // Allocate memory for subgrids
    idg::Array4D<std::complex<float>> subgrids(
        total_nr_subgrids, nr_correlations, subgrid_size, subgrid_size);

    // Run kernel
    auto total_runtime = -omp_get_wtime();
    auto min_runtime = numeric_limits<double>::infinity();

    for (int i = 0; i < nr_repetitions; i++) {
        auto runtime = -omp_get_wtime();

        kernel_gridder(
            total_nr_subgrids,
            grid_size,
            subgrid_size,
            image_size,
            w_offset,
            nr_channels,
            nr_stations,
            uvw.data(),
            wavenumbers.data(),
            (std::complex<float> *) visibilities.data(),
            spheroidal.data(),
            aterms.data(),
            (int *) metadata,
            (std::complex<float> *) subgrids.data());

        runtime += omp_get_wtime();
        min_runtime = fmin(min_runtime, runtime);
    }

    // Report performance
    total_runtime += omp_get_wtime();
    auto avg_runtime = total_runtime/nr_repetitions;
    auto flops = gridder_flops(nr_channels, total_nr_timesteps, total_nr_subgrids, subgrid_size);
    auto sincos = gridder_sincos(nr_channels, total_nr_timesteps, total_nr_subgrids, subgrid_size);
    auto bytes = gridder_bytes(nr_channels, total_nr_timesteps, total_nr_subgrids, subgrid_size);
    auto nr_visibilities = total_nr_timesteps * nr_channels;
    auto gflops = flops * 1e-9 / min_runtime;
    auto gsincos = sincos * 1e-9 / min_runtime;
    auto gops = (flops + sincos) * 1e-9 / min_runtime;
    auto mvis =  nr_visibilities * 1e-6 / min_runtime;
    auto gbytes = bytes * 1e-9 / min_runtime;
    printf("\n%s\n", name.c_str());
    printf("Avg. runtime:     %f\n", avg_runtime);
    printf("Min. runtime:     %f\n", min_runtime);
    printf("Gsincos:          %f\n", gsincos);
    printf("Mvisibilities:    %f\n", mvis);
    printf("Gflops:           %f\n", gflops);
    printf("Gops:             %f\n", gops);
    printf("Bandwidth [GB/s]: %f\n", gbytes);
}
