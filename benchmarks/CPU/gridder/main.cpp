#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#include "Parameters.h"
#include "Globals.h"
#include "bench.h"

using namespace std;

int main(int argc, char **argv)
{
    //run( "Gridder reference", (FunctionPtr) kernel_gridder_ref);

    //run( "Degridder reference", (FunctionPtr) kernel_degridder_ref);

    #if USE_LIKWID
	likwid_markerInit();
    #endif

    run( "Gridder optimized 01", (FunctionPtr) kernel_gridder_opt_01);
    run( "Gridder optimized 02", (FunctionPtr) kernel_gridder_opt_02);
    run( "Gridder optimized 03", (FunctionPtr) kernel_gridder_opt_03);
    run( "Gridder optimized 04", (FunctionPtr) kernel_gridder_opt_04);
    run( "Gridder optimized 05", (FunctionPtr) kernel_gridder_opt_05);

    run( "Degridder optimized 01", (FunctionPtr) kernel_degridder_opt_01);
    run( "Degridder optimized 02", (FunctionPtr) kernel_degridder_opt_02);

    #if USE_LIKWID
	likwid_markerClose();
    #endif

    return EXIT_SUCCESS;
}
