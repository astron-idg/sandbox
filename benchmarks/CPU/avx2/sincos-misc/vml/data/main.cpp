#include <iostream>
#include <random>

#include "common_vml.h"
#include "kernels.h"

using namespace std;

#define MAX_THREADS 512
#define LENGTH 8192
float input[MAX_THREADS*LENGTH] __attribute__((aligned(64)));
float output_sin[MAX_THREADS*LENGTH] __attribute__((aligned(64)));
float output_cos[MAX_THREADS*LENGTH] __attribute__((aligned(64)));

void init(double value) {
    for (int i = 0; i < MAX_THREADS*LENGTH; i++) {
        input[i] = value;
    }
}

void init_geometrical(double sigma) {
    random_device rd;
    mt19937 gen(rd());
    normal_distribution<double> d(0,1);

    for (int i = 0; i < MAX_THREADS*LENGTH; i++) {
        input[i] = d(gen);
    }
}


int main() {

    unsigned width = 16;
    double gsincos_per_second = 0.0;

    for (int j = 1; j < 5; j++) {

        double value = 9998 + j;

        init(value);

        run("  0:1", 2048,     0 * VECTOR_LENGTH, &kernel_b2, false);

        // Benchmark
        for (int i = 0; i < NR_REPETITIONS; i++) {
            gsincos_per_second = run("  0:1", 2048,     0 * VECTOR_LENGTH, &kernel_b2, false);
        }

        cout << setw(width) << value << setw(width) << gsincos_per_second << endl;
    }
}
