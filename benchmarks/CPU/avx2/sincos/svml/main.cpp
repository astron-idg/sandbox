#include <iostream>
#include "common.h"
#include "kernels.h"

int main() {

    std::cout << "Definition operation ('op'): *,/,+,-,sin,cos" << std::endl;

    // Benchmark
    for (int i = 0; i < NR_REPETITIONS; i++) {
        bool print = i > 0;
        run("   1:0",   0 * VECTOR_LENGTH, 256 * VECTOR_LENGTH, &kernel_b0, print);
        run("   0:1", 256 * VECTOR_LENGTH,   0 * VECTOR_LENGTH, &kernel_b1, print);
        run("   1:8", 256 * VECTOR_LENGTH,  32 * VECTOR_LENGTH, &kernel_v00, print);
        run("   1:4", 128 * VECTOR_LENGTH,  32 * VECTOR_LENGTH, &kernel_v01, print);
        run("   1:2",  64 * VECTOR_LENGTH,  32 * VECTOR_LENGTH, &kernel_v02, print);
        run("   1:1",  32 * VECTOR_LENGTH,  32 * VECTOR_LENGTH, &kernel_v03, print);
        run("   2:1",  16 * VECTOR_LENGTH,  32 * VECTOR_LENGTH, &kernel_v04, print);
        run("   4:1",   8 * VECTOR_LENGTH,  32 * VECTOR_LENGTH, &kernel_v05, print);
        run("   8:1",   8 * VECTOR_LENGTH,  64 * VECTOR_LENGTH, &kernel_v06, print);
        run("  16:1",   8 * VECTOR_LENGTH, 128 * VECTOR_LENGTH, &kernel_v07, print);
        run("  32:1",   8 * VECTOR_LENGTH, 256 * VECTOR_LENGTH, &kernel_v08, print);
        run("  64:1",   4 * VECTOR_LENGTH, 256 * VECTOR_LENGTH, &kernel_v09, print);
        run(" 128:1",   2 * VECTOR_LENGTH, 256 * VECTOR_LENGTH, &kernel_v10, print);
        run(" 256:1",   1 * VECTOR_LENGTH, 256 * VECTOR_LENGTH, &kernel_v11, print);
        run(" 512:1",   1 * VECTOR_LENGTH, 512 * VECTOR_LENGTH, &kernel_v12, print);
    }
}
