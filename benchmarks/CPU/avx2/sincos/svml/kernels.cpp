#include "kernels.h"

__m256 a1, b1, c1, d1, e1, f1, g1, h1;

#define FMA_16() \
    a1 = _mm256_fmadd_ps(a1, a1, a1);  \
    b1 = _mm256_fmadd_ps(b1, b1, b1);  \
    c1 = _mm256_fmadd_ps(c1, c1, c1);  \
    d1 = _mm256_fmadd_ps(d1, d1, d1);  \
    e1 = _mm256_fmadd_ps(e1, e1, e1);  \
    f1 = _mm256_fmadd_ps(f1, f1, f1);  \
    g1 = _mm256_fmadd_ps(g1, g1, g1);  \
    h1 = _mm256_fmadd_ps(h1, h1, h1);  \
    a1 = _mm256_fmadd_ps(a1, a1, a1);  \
    b1 = _mm256_fmadd_ps(b1, b1, b1);  \
    c1 = _mm256_fmadd_ps(c1, c1, c1);  \
    d1 = _mm256_fmadd_ps(d1, d1, d1);  \
    e1 = _mm256_fmadd_ps(e1, e1, e1);  \
    f1 = _mm256_fmadd_ps(f1, f1, f1);  \
    g1 = _mm256_fmadd_ps(g1, g1, g1);  \
    h1 = _mm256_fmadd_ps(h1, h1, h1);

#define FMA_32()   FMA_16();   FMA_16();
#define FMA_64()   FMA_32();   FMA_32();
#define FMA_128()  FMA_64();   FMA_64();
#define FMA_256()  FMA_128();  FMA_128();
#define FMA_512()  FMA_256();  FMA_256();

inline void SINCOS_1() {
    a1 =_mm256_sincos_ps(&a1, a1);
}

inline void SINCOS_2() {
    a1 =_mm256_sincos_ps(&a1, a1);
    b1 =_mm256_sincos_ps(&b1, b1);
}

inline void SINCOS_4() {
    a1 =_mm256_sincos_ps(&a1, a1);
    b1 =_mm256_sincos_ps(&b1, b1);
    c1 =_mm256_sincos_ps(&c1, c1);
    d1 =_mm256_sincos_ps(&d1, d1);
}

inline void SINCOS_8() {
    a1 =_mm256_sincos_ps(&a1, a1);
    b1 =_mm256_sincos_ps(&b1, b1);
    c1 =_mm256_sincos_ps(&c1, c1);
    d1 =_mm256_sincos_ps(&d1, d1);
    e1 =_mm256_sincos_ps(&e1, e1);
    f1 =_mm256_sincos_ps(&f1, f1);
    g1 =_mm256_sincos_ps(&g1, g1);
    h1 =_mm256_sincos_ps(&h1, h1);
}

inline void SINCOS_16() {
    a1 =_mm256_sincos_ps(&a1, a1);
    b1 =_mm256_sincos_ps(&b1, b1);
    c1 =_mm256_sincos_ps(&c1, c1);
    d1 =_mm256_sincos_ps(&d1, d1);
    e1 =_mm256_sincos_ps(&e1, e1);
    f1 =_mm256_sincos_ps(&f1, f1);
    g1 =_mm256_sincos_ps(&g1, g1);
    h1 =_mm256_sincos_ps(&h1, h1);

    a1 =_mm256_sincos_ps(&a1, a1);
    b1 =_mm256_sincos_ps(&b1, b1);
    c1 =_mm256_sincos_ps(&c1, c1);
    d1 =_mm256_sincos_ps(&d1, d1);
    e1 =_mm256_sincos_ps(&e1, e1);
    f1 =_mm256_sincos_ps(&f1, f1);
    g1 =_mm256_sincos_ps(&g1, g1);
    h1 =_mm256_sincos_ps(&h1, h1);
}

#define SINCOS_32()   SINCOS_16();  SINCOS_16();
#define SINCOS_64()   SINCOS_32();  SINCOS_32();
#define SINCOS_128()  SINCOS_64();  SINCOS_64();
#define SINCOS_256()  SINCOS_128(); SINCOS_128();

void kernel_b0() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        FMA_256();
    }
}

void kernel_b1() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_256();
    }
}

void kernel_v00() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_256(); FMA_32();
    }
}

void kernel_v01() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_128(); FMA_32();
    }
}

void kernel_v02() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_64(); FMA_32();
    }
}

void kernel_v03() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_32(); FMA_32();
    }
}

void kernel_v04() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_16(); FMA_32();
    }
}

void kernel_v05() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_8(); FMA_32();
    }
}

void kernel_v06() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_8(); FMA_64();
    }
}

void kernel_v07() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_8(); FMA_128();
    }
}

void kernel_v08() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_8(); FMA_256();
    }
}

void kernel_v09() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_4(); FMA_256();
    }
}

void kernel_v10() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_2(); FMA_256();
    }
}

void kernel_v11() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_1(); FMA_256();
    }
}

void kernel_v12() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        SINCOS_1(); FMA_512();
    }
}
