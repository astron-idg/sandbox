#ifndef COMMON_H_
#define COMMON_H_

#include <iostream>
#include <iomanip>

#include <math.h>
#include <immintrin.h>
#include <omp.h>

#define NR_REPETITIONS      3
#define COUNT               128000000ULL
#define VECTOR_LENGTH       8
#define FMA                 2

using namespace std;

void report(string name, double runtime, double gflops, double gsincos);
void run(const char *name, const double sincos, const double fma, void (*kernel)(void), bool print=true);

#endif
