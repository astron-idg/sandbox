#include <mkl_vml.h>
#include "common_vml.h"

#define VML_PRECISION VML_LA

void kernel_b0();
void kernel_b1();
void kernel_b2();
void kernel_b3();
void kernel_b4();
void kernel_v00();
void kernel_v01();
void kernel_v02();
void kernel_v03();
void kernel_v04();
void kernel_v05();
void kernel_v06();
void kernel_v07();
void kernel_v08();
void kernel_v09();
void kernel_v10();
void kernel_v11();
void kernel_v12();
