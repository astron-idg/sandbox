#include "common_vml.h"

using namespace std;

void report(string name, double runtime, double gflops, double gsincos) {
    unsigned width = 8;
    cout << setw(width) << string(name) << ": ";
    cout << setprecision(2) << fixed;
    cout << setw(width) << runtime * 1e3 << " ms";
    double gfmas = gflops / 2;
    double gops = gflops + 2*gsincos;

    cout << ", " << setw(width) << gflops / runtime << " GFLOPS/s";
    cout << ", " << setw(width) << gfmas / runtime << " GFMAS/s";
    cout << ", " << setw(width) << gsincos / runtime << " GSINCOS/s";
    cout << ", " << setw(width) << gops / runtime << " GOPS/s";
    cout << endl;
}

void run(const char *name, const double sincos, const double fma, void (*kernel)(void), bool print) {
    double runtime = -omp_get_wtime();
    kernel();
    runtime += omp_get_wtime();
    double gflops = COUNT * fma * FMA * 1e-9;
    double gsincos = COUNT * sincos * 1e-9;
    if (print) {
        report(name, runtime, gflops, gsincos);
    }
}
