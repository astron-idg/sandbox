#include <iostream>
#include "common_vml.h"
#include "kernels.h"

#define MAX_THREADS 512
#define LENGTH 8192
float input[MAX_THREADS*LENGTH] __attribute__((aligned(64)));
float output_sin[MAX_THREADS*LENGTH] __attribute__((aligned(64)));
float output_cos[MAX_THREADS*LENGTH] __attribute__((aligned(64)));

// init to some value in interval [-1e4,1e4]
// ouside of this interval, perfomarnce drops by a factor 10
void init() {
    for (int i = 0; i < MAX_THREADS*LENGTH; i++) {
        input[i] = 0.8738;
    }
}

// parallel init
// void init() {
//    #pragma omp parallel
//    {
//        int tid = omp_get_thread_num();
//        for (int i = 0; i < LENGTH; i++) {
//            input[tid*LENGTH + i] = 0.8738;
//        }
//    }
// }


int main() {
    init();

    std::cout << "Definition operation ('op'): *,/,+,-,sin,cos" << std::endl;

    for (int i = 0; i < 5; i++) {
        run("  1:0", 0,   512*VECTOR_LENGTH,   &kernel_b0, false);
    }

    // Benchmark
    for (int i = 0; i < NR_REPETITIONS; i++) {
        run("  1:0", 0,      512 * VECTOR_LENGTH, &kernel_b0);
        // run("  0:1", 1024,     0 * VECTOR_LENGTH, &kernel_b1);
        run("  0:1", 2048,     0 * VECTOR_LENGTH, &kernel_b2);
        // run("  0:1", 4096,     0 * VECTOR_LENGTH, &kernel_b3);
        // run("  0:1", 8192,     0 * VECTOR_LENGTH, &kernel_b4);
        run("  1:8", 2048,    32 * VECTOR_LENGTH, &kernel_v12);
        run("  1:4", 2048,    64 * VECTOR_LENGTH, &kernel_v11);
        run("  1:2", 2048,   128 * VECTOR_LENGTH, &kernel_v10);

        run("  1:1", 2048,   256 * VECTOR_LENGTH, &kernel_v00);

        run("  2:1", 2048,   512 * VECTOR_LENGTH, &kernel_v01);
        run("  4:1", 2048,  1024 * VECTOR_LENGTH, &kernel_v02);
        run("  8:1", 2048,  2048 * VECTOR_LENGTH, &kernel_v03);
        run(" 16:1", 2048,  4096 * VECTOR_LENGTH, &kernel_v04);
        run(" 32:1", 2048,  8192 * VECTOR_LENGTH, &kernel_v05);
        run(" 64:1", 2048, 16384 * VECTOR_LENGTH, &kernel_v06);
        run("128:1", 2048, 32768 * VECTOR_LENGTH, &kernel_v07);
        run("256:1", 2048, 65536 * VECTOR_LENGTH, &kernel_v08);
        run("512:1", 2048,131072 * VECTOR_LENGTH, &kernel_v09);
    }
}
