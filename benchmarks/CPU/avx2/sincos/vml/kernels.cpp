#include "kernels.h"

#define LENGTH 8192
extern float input[LENGTH];
extern float output_sin[LENGTH];
extern float output_cos[LENGTH];

__m256 a1, b1, c1, d1, e1, f1, g1, h1;

#define FMA_kernel_16() \
    a1 = _mm256_fmadd_ps(a1, a1, a1);  \
    b1 = _mm256_fmadd_ps(b1, b1, b1);  \
    c1 = _mm256_fmadd_ps(c1, c1, c1);  \
    d1 = _mm256_fmadd_ps(d1, d1, d1);  \
    e1 = _mm256_fmadd_ps(e1, e1, e1);  \
    f1 = _mm256_fmadd_ps(f1, f1, f1);  \
    g1 = _mm256_fmadd_ps(g1, g1, g1);  \
    h1 = _mm256_fmadd_ps(h1, h1, h1);  \
    a1 = _mm256_fmadd_ps(a1, a1, a1);  \
    b1 = _mm256_fmadd_ps(b1, b1, b1);  \
    c1 = _mm256_fmadd_ps(c1, c1, c1);  \
    d1 = _mm256_fmadd_ps(d1, d1, d1);  \
    e1 = _mm256_fmadd_ps(e1, e1, e1);  \
    f1 = _mm256_fmadd_ps(f1, f1, f1);  \
    g1 = _mm256_fmadd_ps(g1, g1, g1);  \
    h1 = _mm256_fmadd_ps(h1, h1, h1);

inline void FMA_16() {
    for (unsigned j = 0;  j < 16/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_32() {
    for (unsigned j = 0;  j < 32/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_64() {
    for (unsigned j = 0;  j < 64/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_128() {
    for (unsigned j = 0;  j < 128/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_256() {
    for (unsigned j = 0;  j < 256/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_512() {
    for (unsigned j = 0;  j < 512/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_1024() {
    for (unsigned j = 0;  j < 1024/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_2048() {
    for (unsigned j = 0;  j < 2048/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_4096() {
    for (unsigned j = 0;  j < 4096/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_8192() {
    for (unsigned j = 0;  j < 8192/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_16384() {
    for (unsigned j = 0;  j < 16384/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_32768() {
    for (unsigned j = 0;  j < 32768/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_65536() {
    for (unsigned j = 0;  j < 65536/16; j++) {
        FMA_kernel_16();
    }
}

inline void FMA_131072() {
    for (unsigned j = 0;  j < 131072/16; j++) {
        FMA_kernel_16();
    }
}

inline void SINCOS_512() {
    vmsSinCos(512, input, output_sin, output_cos, VML_PRECISION);
}

inline void SINCOS_1024(int tid) {
    vmsSinCos(1024, &input[tid*LENGTH], &output_sin[tid*LENGTH],
              &output_cos[tid*LENGTH], VML_PRECISION);
}

inline void SINCOS_2048(int tid) {
    vmsSinCos(2048, &input[tid*LENGTH], &output_sin[tid*LENGTH],
              &output_cos[tid*LENGTH], VML_PRECISION);
}

inline void SINCOS_4096(int tid) {
    vmsSinCos(4096, &input[tid*LENGTH], &output_sin[tid*LENGTH],
              &output_cos[tid*LENGTH], VML_PRECISION);
}

inline void SINCOS_8192(int tid) {
    vmsSinCos(8192, &input[tid*LENGTH], &output_sin[tid*LENGTH],
              &output_cos[tid*LENGTH], VML_PRECISION);
}

void kernel_b0() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT; i++) {
        FMA_512();
    }
}

void kernel_b1() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_1024(tid);
        }
    }
}

void kernel_b2() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid);
        }
    }
}

void kernel_b3() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_4096(tid);
        }
    }
}

void kernel_b4() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_8192(tid);
        }
    }
}

void kernel_v00() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_256();
        }
    }
}

void kernel_v01() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_512();
        }
    }
}

void kernel_v02() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_1024();
        }
    }
}

void kernel_v03() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_2048();
        }
    }
}

void kernel_v04() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_4096();
        }
    }
}

void kernel_v05() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_8192();
        }
    }
}

void kernel_v06() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_16384();
        }
    }
}

void kernel_v07() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_32768();
        }
    }
}

void kernel_v08() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_65536();
        }
    }
}

void kernel_v09() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_131072();
        }
    }
}

void kernel_v10() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_128();
        }
    }
}

void kernel_v11() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_64();
        }
    }
}

void kernel_v12() {
    #pragma omp parallel
    {
        int tid = omp_get_thread_num();
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            SINCOS_2048(tid); FMA_32();
        }
    }
}
