#pragma once

#include "Globals.h"

// Reference implementation
void kernel_gridder_reference(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	const VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	SubGridType			   __restrict__ *subgrid);


// Original optimized code for HaswellEP with const
// number of timesteps per subgrid
void kernel_gridder_haswellep_constant(
    const int jobsize, const float w_offset,
    const UVWConstantType		   __restrict__ *uvw,
    const WavenumberType           __restrict__ *wavenumbers,
    const VisibilitiesConstantType __restrict__ *visibilities,
    const SpheroidalType           __restrict__ *spheroidal,
    const ATermType		           __restrict__ *aterm,
    const MetadataType	           __restrict__ *metadata,
    SubGridType			           __restrict__ *subgrid);


// Original optimized code for HaswellEP with variable
// number of timesteps per subgrid
void kernel_gridder_haswellep(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	const VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	SubGridType			   __restrict__ *subgrid);
#pragma once



// Start of optimizing:
void kernel_gridder_01(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	const VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	SubGridType			   __restrict__ *subgrid);

// Preload visibilities
void kernel_gridder_02(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

// Vectorize of channels
void kernel_gridder_03(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

// Precompute phase
void kernel_gridder_04(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

// Precompute phasor
void kernel_gridder_05(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);


// introduced VML
void kernel_gridder_06(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

// flexible nr_timesteps
void kernel_gridder_07a(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

// split inner loop
void kernel_gridder_07b(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

// Replace inner loop by intrinsics
void kernel_gridder_08(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);

//
void kernel_gridder_09(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
    const VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    SubGridType			   __restrict__ *subgrid);
