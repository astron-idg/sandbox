#pragma once

#include <complex>
#include <cstring>
#include "Globals.h"
#include "uvwsim.h"

uint64_t gridder_flops(int jobsize);
uint64_t gridder_sincos(int jobsize);
uint64_t gridder_bytes(int jobsize);
uint64_t gridder_visibilities(int jobsize);
void memset_array(void *clear, size_t num_bytes);

void init_wavenumbers(float wavenumbers[], int size_wavenumbers);
void init_visibilities(float_complex visibilities[], int size_visibilities);
void init_spheroidal(float spheroidal[], int size_spheroidal);
void init_aterm(float_complex aterm[], int size_aterm);
void init_subgrid(float_complex subgrid[], int size_subgrid);

// from code init routines:
void init_uvw(void *ptr, int nr_stations, int nr_baselines,
              int nr_time, int integration_time);
void init_baselines(void *ptr, int nr_stations, int nr_baselines);
void* init_baselines(int nr_stations, int nr_baselines);
void init_metadata(void *ptr, void *_uvw, void *_wavenumbers,
                   int nr_stations, int nr_baselines, int nr_timesteps,
                   int nr_timeslots, int nr_channels, int gridsize,
                   int subgridsize, float imagesize);

float get_accucary(
    const int size,
    const std::complex<float>* A,
    const std::complex<float>* B);
