#include <iostream>
#include <cstdint>
#include <cstring>
#include <cmath>
#include <limits>
#include "auxiliary.h"

// #define COUNT_SINCOS_AS_FLOPS
#if defined(COUNT_SINCOS_AS_FLOPS)
#define FLOPS_PER_SINCOS 8
#endif

static const char* LAYOUT_DIR = "./auxiliary";
static const char* LAYOUT_FILE = "SKA1_low_ecef.txt";
static const int RANDOM_SEED = 1236;
static const double RIGHT_ASCENSION = (10.0 * (M_PI/180.));
static const double DECLINATION = (70.0 * (M_PI/180.));
static const int YEAR = 2014;
static const int MONTH = 03;
static const int DAY = 20;
static const int HOUR = 01;
static const int MINUTE = 57;
static const double SECONDS = 1.3;
static const int DEFAULT_INTEGRATION_TIME = 1;



uint64_t gridder_flops(int jobsize)
{
    uint64_t flops = 0;
    flops += 1ULL * NR_TIMESTEPS * 5; // phase index
    flops += 1ULL * NR_TIMESTEPS * 5; // phase offset
    flops += 1ULL * NR_TIMESTEPS * NR_CHANNELS * 2; // phase
    #if defined(COUNT_SINCOS_AS_FLOPS)
    flops += 1ULL * NR_TIMESTEPS * NR_CHANNELS * FLOPS_PER_SINCOS; // phasor
    #endif
    flops += 1ULL * NR_TIMESTEPS * NR_CHANNELS * (NR_POLARIZATIONS * 8); // update
    flops += 1ULL * NR_POLARIZATIONS * 30; // aterm
    flops += 1ULL * NR_POLARIZATIONS * 2; // spheroidal
    flops += 1ULL * NR_POLARIZATIONS * 6; // shift
    return jobsize * SUBGRIDSIZE * SUBGRIDSIZE * flops;
}


uint64_t gridder_sincos(int jobsize)
{
    uint64_t sincos = 0;
    sincos += 1ULL * NR_TIMESTEPS * NR_CHANNELS; // phasor
    return jobsize * SUBGRIDSIZE * SUBGRIDSIZE * sincos;
}


uint64_t gridder_visibilities(int jobsize)
{
    // This benchmark online processes one baseline
    uint64_t nr_baselines = 1; // 1ULL*(NR_STATIONS*(NR_STATIONS-1))/2;
    uint64_t nr_time = 1ULL * jobsize * NR_TIMESTEPS; // NR_TIMESTEPS * NR_TIMESLOTS;
    return 1ULL * nr_baselines * nr_time * NR_CHANNELS;
}


uint64_t gridder_bytes(int jobsize) {
    uint64_t bytes = 0;
    bytes += 1ULL * NR_TIMESTEPS * 3 * sizeof(float); // uvw
    bytes += 1ULL * NR_TIMESTEPS * NR_CHANNELS * NR_POLARIZATIONS * 2 * sizeof(float); // visibilities
    bytes += 1ULL * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE  * 2 * sizeof(float); // subgrids
    return jobsize*bytes;
}


void memset_array(void *array, size_t num_bytes) {
    memset(array, 0, num_bytes);
}


void init_wavenumbers(float wavenumbers[], int nr_channels)
{
    const double speed_of_light = 299792458.0;
    const double start_frequency = 150.0e6;
    const double frequency_increment = 0.7e6;

    float frequencies[nr_channels];
    for (int chan = 0; chan < nr_channels; chan++) {
        frequencies[chan] = start_frequency + frequency_increment * chan;
    }

    for (int i = 0; i < nr_channels; i++) {
        wavenumbers[i] =  2 * M_PI * frequencies[i] / speed_of_light;
    }
}

void init_visibilities(float_complex visibilities[], int size_visibilities) {
    for (int i = 0; i < size_visibilities; i++) {
        visibilities[i] = {1.f, 0.f};
    }
}

void init_spheroidal(float spheroidal[], int size_spheroidal) {
    for (int i = 0; i < size_spheroidal; i++) {
        spheroidal[i] = 1.f;
    }
}

void init_aterm(float_complex aterm[], int size_aterm) {
    for (int i = 0; i < size_aterm; i+=4) {
        aterm[i+0] = {1.f, 0.f};
        aterm[i+0] = {0.f, 0.f};
        aterm[i+0] = {0.f, 0.f};
        aterm[i+0] = {1.f, 0.f};
    }
}


void init_subgrid(float_complex subgrid[], int size_subgrid) {
    memset(subgrid, 0, size_subgrid * sizeof(float_complex));
}

/* Methods where pointed to allocated memory is provided */
void init_uvw(void *ptr, int nr_stations, int nr_baselines,
              int nr_time, int integration_time = DEFAULT_INTEGRATION_TIME)
{
    typedef struct { float u, v, w; } UVW;
    typedef UVW UVWType[nr_baselines][nr_time];

    UVWType *uvw = (UVWType *) ptr;

    // Check whether layout file exists
    bool found = false;
    char filename[512];
    sprintf(filename, "./%s/%s", LAYOUT_DIR, LAYOUT_FILE);

    // Read the number of stations in the layout file.
    int nr_stations_file = uvwsim_get_num_stations(filename);

    // Allocate memory for antenna coordinates
    double *x = (double*) malloc(nr_stations_file * sizeof(double));
    double *y = (double*) malloc(nr_stations_file * sizeof(double));
    double *z = (double*) malloc(nr_stations_file * sizeof(double));

    // Load the antenna coordinates
    #if defined(DEBUG)
    printf("looking for stations file in: %s\n", filename);
    #endif

    if (uvwsim_load_station_coords(filename, nr_stations_file, x, y, z) != nr_stations_file) {
        std::cerr << "Failed to read antenna coordinates." << std::endl;
        exit(EXIT_FAILURE);
    }

    // Select some antennas randomly when not all antennas are requested
    if (nr_stations < nr_stations_file) {
        // Allocate memory for selection of antenna coordinates
        double *_x = (double*) malloc(nr_stations * sizeof(double));
        double *_y = (double*) malloc(nr_stations * sizeof(double));
        double *_z = (double*) malloc(nr_stations * sizeof(double));

        // Generate nr_stations random numbers
        int station_number[nr_stations];
        int i = 0;
        srandom(RANDOM_SEED);
        while (i < nr_stations) {
            int index = nr_stations_file * ((double) random() / RAND_MAX);
            bool found = true;
            for (int j = 0; j < i; j++) {
                if (station_number[j] == index) {
                    found = false;
                    break;
                }
            }
            if (found) {
                station_number[i++] = index;
            }
        }

        // Set stations
        for (int i = 0; i < nr_stations; i++) {
            _x[i] = x[station_number[i]];
            _y[i] = y[station_number[i]];
            _z[i] = z[station_number[i]];
        }

        // Swap pointers and free memory
        double *__x = x;
        double *__y = y;
        double *__z = z;
        x = _x;
        y = _y;
        z = _z;
        free(__x);
        free(__y);
        free(__z);
    }

    // Define observation parameters
    double ra0  = RIGHT_ASCENSION;
    double dec0 = DECLINATION;
    double start_time_mjd = uvwsim_datetime_to_mjd(YEAR, MONTH, DAY, HOUR, MINUTE, SECONDS);
    double obs_length_hours = (nr_time * integration_time) / (3600.0);
    double obs_length_days = obs_length_hours / 24.0;

    // Allocate memory for baseline coordinates
    int nr_coordinates = nr_time * nr_baselines;
    double *uu = (double*) malloc(nr_coordinates * sizeof(double));
    double *vv = (double*) malloc(nr_coordinates * sizeof(double));
    double *ww = (double*) malloc(nr_coordinates * sizeof(double));

    // Evaluate baseline uvw coordinates.
    for (int t = 0; t < nr_time; t++) {
        double time_mjd = start_time_mjd + t
            * (obs_length_days/(double)nr_time);
        size_t offset = t * nr_baselines;
        uvwsim_evaluate_baseline_uvw(
            &uu[offset], &vv[offset], &ww[offset],
            nr_stations, x, y, z, ra0, dec0, time_mjd);
    }

    // Fill UVW datastructure
    for (int bl = 0; bl < nr_baselines; bl++) {
        for (int t = 0; t < nr_time; t++) {
            int i = t * nr_baselines + bl;
            UVW value = {(float) uu[i], (float) vv[i], (float) ww[i]};
            (*uvw)[bl][t] = value;
        }
    }

    // Free memory
    free(x); free(y); free(z);
    free(uu); free(vv); free(ww);
}

void init_baselines(void *ptr, int nr_stations, int nr_baselines)
{
    typedef Baseline BaselineType[nr_baselines];
    BaselineType *baselines = (BaselineType *) ptr;

    int bl = 0;

    for (int station1 = 1 ; station1 < nr_stations; station1++) {
        for (int station2 = 0; station2 < station1; station2++) {
            if (bl >= nr_baselines) {
                break;
            }
            (*baselines)[bl].station1 = station1;
            (*baselines)[bl].station2 = station2;
            bl++;
        }
    }
}

void* init_baselines(int nr_stations, int nr_baselines)
{
    typedef Baseline BaselineType[nr_baselines];
    void *ptr = malloc(sizeof(BaselineType));
    init_baselines(ptr, nr_stations, nr_baselines);
    return ptr;

}

void init_metadata(void *ptr, void *_uvw, void *_wavenumbers,
            int nr_stations, int nr_baselines, int nr_timesteps,
            int nr_timeslots, int nr_channels, int gridsize,
            int subgridsize, float imagesize)
{
    int nr_subgrids = nr_baselines * nr_timeslots;
    int nr_time = nr_timesteps * nr_timeslots;

    // Define datatypes
    typedef UVW UVWType[nr_baselines][nr_time];
    typedef float WavenumberType[nr_channels];
    typedef Baseline BaselineType[nr_baselines];
    typedef Metadata MetadataType[nr_subgrids];

    // Pointers to datastructures
    UVWType *uvw = (UVWType *) _uvw;
    WavenumberType *wavenumbers = (WavenumberType *) _wavenumbers;
    BaselineType *baselines = (BaselineType *) init_baselines(nr_stations, nr_baselines);
    MetadataType *metadata = (MetadataType *) ptr;

    // Get wavenumber for first and last frequency
    float wavenumber_first = (*wavenumbers)[0];
    float wavenumber_last  = (*wavenumbers)[nr_channels-1];

    // Iterate all baselines
    for (int bl = 0; bl < nr_baselines; bl++) {
        // Load baseline
        Baseline baseline = (*baselines)[bl];

        // Iterate all timeslots
        for (int timeslot = 0; timeslot < nr_timeslots; timeslot++) {
            int time_offset = timeslot * nr_timesteps;

            // Find mininmum and maximum u and v for current timeslot in pixels
            float u_min =  std::numeric_limits<float>::infinity();
            float u_max = -std::numeric_limits<float>::infinity();
            float v_min =  std::numeric_limits<float>::infinity();
            float v_max = -std::numeric_limits<float>::infinity();

            // Iterate all timesteps
            for (int timestep = 0; timestep < nr_timesteps; timestep++) {
                UVW current = (*uvw)[bl][time_offset + timestep];

                // U,V in meters
                float u_meters = current.u;
                float v_meters = current.v;

                // Iterate all channels
                for (int chan = 0; chan < nr_channels; chan++) {
                    float wavenumber = (*wavenumbers)[chan];
                    float scaling = imagesize * wavenumber / (2 * M_PI);

                    // U,V in pixels
                    float u_pixels = u_meters * scaling;
                    float v_pixels = v_meters * scaling;

                    if (u_pixels < u_min) u_min = u_pixels;
                    if (u_pixels > u_max) u_max = u_pixels;
                    if (v_pixels < v_min) v_min = v_pixels;
                    if (v_pixels > v_max) v_max = v_pixels;
                }
            }

            // Compute middle point in pixels
            int u_pixels = roundf((u_max + u_min) / 2);
            int v_pixels = roundf((v_max + v_min) / 2);

            // Shift center from middle of grid to top left
            u_pixels += (gridsize/2);
            v_pixels += (gridsize/2);

            // Shift from middle of subgrid to top left
            u_pixels -= (subgridsize/2);
            v_pixels -= (subgridsize/2);

            // Construct coordinate
            Coordinate coordinate = { u_pixels, v_pixels };

            // Compute subgrid number
            int subgrid_nr = bl * nr_timeslots + timeslot;

            // Set metadata
            // NOTE: the aterm is not used in our benchmark!
            Metadata m = { time_offset, nr_timesteps,
                           0, baseline, coordinate };
            (*metadata)[subgrid_nr] = m;
        }
    }

    // Free memory
    free(baselines);
}



// computes max|A[i]-B[i]| / max|B[i]|
float get_accucary(
    const int size,
    const std::complex<float>* A,
    const std::complex<float>* B)
{
    float max_abs_error = 0.0f;
    float max_ref_val = 0.0f;
    float max_val = 0.0f;
    for (int i=0; i<size; i++) {
        float abs_error = abs(A[i] - B[i]);
        if ( abs_error > max_abs_error ) {
            max_abs_error = abs_error;
        }
        if (abs(B[i]) > max_ref_val) {
            max_ref_val = abs(B[i]);
        }
        if (abs(A[i]) > max_val) {
            max_val = abs(A[i]);
        }
    }
    if (max_ref_val == 0.0f) {
        if (max_val == 0.0f)
            // both grid are zero
            return 0.0f;
        else
            // refrence grid is zero, but computed grid not
            return std::numeric_limits<float>::infinity();
    } else {
        return max_abs_error / max_ref_val;
    }
}
