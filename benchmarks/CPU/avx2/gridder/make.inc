# C++ compiler
CXX=icpc

# Default C++ compiler flags
CXXFLAGS=-std=c++11 -qopenmp -xcore-avx2 -mkl -O3
OPTREPORT=-qopt-report=3

# Additional compiler flags for debug mode
DEBUGFLAGS=-g -fno-omit-frame-pointer -ggdb
