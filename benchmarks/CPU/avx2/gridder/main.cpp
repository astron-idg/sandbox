#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#include "Globals.h"
#include "bench.h"

using namespace std;


int main(int argc, char **argv)
{
    // Sizes
    auto jobsize = 1024;

    cout << endl;
    cout << "Setting:" << endl;
    cout << "NUM_THREADS = " << omp_get_max_threads() << endl;
    cout << "NR_REPETITIONS = " << NR_REPETITIONS << endl;
    cout << "JOBSIZE = " << jobsize << endl;
    cout << "NR_TIMESTEPS = " << NR_TIMESTEPS << endl;
    cout << "NR_CHANNELS = " << NR_CHANNELS << endl;
    cout << "NR_POLARIZATIONS = " << NR_POLARIZATIONS << endl;
    cout << "SUBGRIDSIZE = " << SUBGRIDSIZE << endl;
    cout << "NR_STATIONS = " << NR_STATIONS << endl;
    cout << "NR_TIMESLOTS = " << NR_TIMESLOTS << endl;
    cout << "GRIDSIZE = " << GRIDSIZE << endl;
    cout << "W_OFFSET = " << W_OFFSET << endl;
    cout << "IMAGESIZE = " << IMAGESIZE << endl;
    cout << "INTEGRATION_TIME = " << INTEGRATION_TIME << endl;

    // Use run, check, and run_and_check below

    // Baseline performance
    // run( "Reference (parallel)" , jobsize,
    //      (FunctionPtr) kernel_gridder_reference);

    // run_and_check( "HaswellEP (old optimized, const NR_TIMESTEPS)", jobsize,
    //                (FunctionPtr) kernel_gridder_haswellep_constant);

    // run_and_check( "HaswellEP (old optimized, variable nr_timesteps)", jobsize,
    //                (FunctionPtr) kernel_gridder_haswellep);

    cout << endl;
    cout << "--------- Start optimizing ---------" << endl;

    run( "Reference (base)", jobsize,
         (FunctionPtr) kernel_gridder_01);

    run_and_check( "Preload visibilities", jobsize,
          (FunctionPtr) kernel_gridder_02);

    run_and_check( "Vectorize over channels", jobsize,
          (FunctionPtr) kernel_gridder_03);

    run_and_check( "Precompute phase", jobsize,
          (FunctionPtr) kernel_gridder_04);

    run_and_check( "Precompute phasor", jobsize,
          (FunctionPtr) kernel_gridder_05);

    run_and_check( "Introduce VML", jobsize,
          (FunctionPtr) kernel_gridder_06);

    run_and_check( "Make nr_timesteps non-const", jobsize,
          (FunctionPtr) kernel_gridder_07a);

    run_and_check( "Split inner loop", jobsize,
          (FunctionPtr) kernel_gridder_07b);

    // Best performance without hyperthreading and kmp affinity scatter
    run_and_check( "Replace inner loop by intrinsics", jobsize,
         (FunctionPtr) kernel_gridder_08);

    run_and_check( "Split intrinsics loop", jobsize,
         (FunctionPtr) kernel_gridder_09);

    return EXIT_SUCCESS;
}
