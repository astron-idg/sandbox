# C++ compiler
CXX=g++

# Default C++ compiler flags
CXXFLAGS=-std=c++11 -O3 -fopenmp -march=core-avx2 -ffast-math

# Additional compiler flags for debug mode
DEBUGFLAGS=-g -fno-omit-frame-pointer -ggdb
OPTREPORT=-fopt-info-vec-optimized

# Libraries to link
LFLAGS = -L$(MKLROOT)/lib/intel64
LIBS = -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -liomp5 -ldl -lpthread -lm
