#include "bench.h"

using namespace std;

void run( const string& name, int jobsize, FunctionPtr kernel_adder )
{
    auto size_uvw = 1ULL * jobsize * NR_TIMESTEPS * sizeof(UVW) / sizeof(float);
    auto size_wavenumbers = 1ULL * NR_CHANNELS;
    auto size_metadata = 1ULL * jobsize * sizeof(Metadata) / sizeof(int);
    auto size_subgrid = 1ULL * jobsize * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;
    auto size_grid = 1ULL * NR_POLARIZATIONS * GRIDSIZE * GRIDSIZE;

    // Data
    auto uvw = new float[NR_REPETITIONS * size_uvw];
    auto wavenumbers = new float[NR_REPETITIONS * size_wavenumbers];
    auto metadata = new int[NR_REPETITIONS * size_metadata];
    auto subgrid = new float_complex[NR_REPETITIONS * size_subgrid];
    auto grid = new float_complex[NR_REPETITIONS * size_grid];

    // Initialize data (NR_REPETITIONS sets of same input)
    auto uvw_ptr = uvw;
    auto wavenumbers_ptr = wavenumbers;
    auto metadata_ptr = metadata;
    auto subgrid_ptr = subgrid;
    auto grid_ptr = grid;
    for (int i = 0; i < NR_REPETITIONS; i++) {
        init_uvw(uvw_ptr, 2, 1, jobsize * NR_TIMESTEPS, INTEGRATION_TIME);
        init_metadata(metadata_ptr, uvw_ptr, wavenumbers_ptr, 2, 1,
                      NR_TIMESTEPS, jobsize,
                      NR_CHANNELS, GRIDSIZE, SUBGRIDSIZE, IMAGESIZE);
        init_subgrid(subgrid_ptr, size_subgrid);
        init_grid(grid_ptr, size_grid);
        uvw_ptr += size_uvw;
        wavenumbers_ptr += size_wavenumbers;
        metadata_ptr += size_metadata;
        subgrid_ptr += size_subgrid;
        grid_ptr += size_grid;
    }

    // clear cache
    size_t num_bytes = 100*1024*1024;
    auto clear = new char[num_bytes];
    memset_array(clear, num_bytes);

    // Run reference kernel
    metadata_ptr = metadata;
    subgrid_ptr = subgrid;
    grid_ptr = grid;

    auto total_runtime = -omp_get_wtime();
    auto min_runtime = numeric_limits<double>::infinity();

    for (int i = 0; i < NR_REPETITIONS; i++) {

        auto runtime = -omp_get_wtime();

        kernel_adder(
            jobsize,
            (MetadataType *) metadata_ptr,
            (SubGridType *) subgrid_ptr,
            (GridType *) grid_ptr);

        metadata_ptr += size_metadata;
        subgrid_ptr += size_subgrid;
        grid_ptr += size_grid;

        runtime += omp_get_wtime();
        min_runtime = fmin(min_runtime, runtime);
    }

    total_runtime += omp_get_wtime();
    auto avg_runtime = total_runtime/NR_REPETITIONS;
    auto flops = adder_flops(jobsize);
    auto bytes = adder_bytes(jobsize);
    auto gflops = flops * 1e-9 / min_runtime;
    auto ksubgrids =  jobsize * 1e-3 / min_runtime;
    auto gbytes = bytes * 1e-9 / min_runtime;
    printf("\n%s\n", name.c_str());
    printf("Avg. runtime:     %f\n", avg_runtime);
    printf("Min. runtime:     %f\n", min_runtime);
    printf("KSubgrids:        %f\n", ksubgrids);
    printf("Gflops (approx.): %f\n", gflops);
    printf("Bandwidth [GB/s]: %f\n", gbytes);

    // Free memory
    delete[] uvw;
    delete[] wavenumbers;
    delete[] metadata;
    delete[] subgrid;
    delete[] grid;
}




void check( const string& name, int jobsize, FunctionPtr kernel_adder )
{
    auto size_uvw = 1ULL * jobsize * NR_TIMESTEPS * sizeof(UVW) / sizeof(float);
    auto size_wavenumbers = 1ULL * NR_CHANNELS;
    auto size_metadata = 1ULL * jobsize * sizeof(Metadata) / sizeof(int);
    auto size_subgrid = 1ULL * jobsize * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;
    auto size_grid = 1ULL * NR_POLARIZATIONS * GRIDSIZE * GRIDSIZE;

    // Data
    auto uvw = new float[size_uvw];
    auto wavenumbers = new float[size_wavenumbers];
    auto metadata = new int[size_metadata];
    auto subgrid = new float_complex[size_subgrid];
    auto grid_ref = new float_complex[size_grid];
    auto grid = new float_complex[size_grid];

    // Initialize data (NR_REPETITIONS sets of same input)
    init_uvw(uvw, 2, 1, jobsize * NR_TIMESTEPS, INTEGRATION_TIME);
    init_wavenumbers(wavenumbers, NR_CHANNELS);
    init_metadata(metadata, uvw, wavenumbers, 2, 1,
                  NR_TIMESTEPS, jobsize,
                  NR_CHANNELS, GRIDSIZE, SUBGRIDSIZE, IMAGESIZE);
    init_subgrid(subgrid, size_subgrid);

    kernel_adder(
        jobsize,
        (MetadataType *) metadata,
        (SubGridType *) subgrid,
        (GridType *) grid);

    // Run reference kernel
    kernel_adder_reference(
        jobsize,
        (MetadataType *) metadata,
        (SubGridType *) subgrid,
        (GridType *) grid);

    if (name != "") cout << name << endl;
    float tol = 1000*std::numeric_limits<float>::epsilon();
    float grid_error = get_accucary(size_subgrid,
                                       (complex<float>*) grid,
                                       (complex<float>*) grid_ref);

    // Report results
    if (grid_error < tol) {
        cout << "PASSED! Error: ";
    } else {
        cout << "FAILED! Error: ";
    }
    cout << scientific << grid_error << endl;

    // Free memory
    delete[] uvw;
    delete[] wavenumbers;
    delete[] metadata;
    delete[] subgrid;
    delete[] grid_ref;
    delete[] grid;
}


void check( int jobsize, FunctionPtr kernel_adder )
{
    check("", jobsize, kernel_adder);
}


void run_and_check( const string& name, int jobsize, FunctionPtr kernel_adder )
{
    run( name, jobsize, kernel_adder );
    check( "", jobsize, kernel_adder );
}
