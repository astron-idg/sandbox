#pragma once

#include <iostream>
#include <iomanip>
#include <complex>
#include <cstdint>
#include <limits>
#include <omp.h>
#include <cstring>

#include "Globals.h"
#include "auxiliary.h"
#include "adder.h"

using FunctionPtr = void (*)(int, void*, void*, void*);

void run( const std::string& name, int jobsize, FunctionPtr kernel_adder );
void check( int jobsize, FunctionPtr kernel_adder );
void check( const std::string& name, int jobsize, FunctionPtr kernel_adder );
void run_and_check( const std::string& name, int jobsize, FunctionPtr kernel_adder );
