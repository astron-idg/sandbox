# C++ compiler
CXX=icpc

# Default C++ compiler flags
CXXFLAGS=-std=c++11 -openmp -xcore-avx2 -mkl -O3
OPTREPORT=-opt-report=3

# Additional compiler flags for debug mode
DEBUGFLAGS=-g -fno-omit-frame-pointer -ggdb
