#pragma once

#include "Globals.h"

// Reference implementation
void kernel_adder_reference(
    const int jobsize,
    const MetadataType __restrict__ *metadata,
    const SubGridType  __restrict__ *subgrid,
    GridType           __restrict__ *grid
    );


// Original optimized code for HaswellEP
void kernel_adder_haswellep(
    const int jobsize,
    const MetadataType __restrict__ *metadata,
    const SubGridType  __restrict__ *subgrid,
    GridType           __restrict__ *grid
    );


// Start of optimizing:
void kernel_adder_01(
    const int jobsize,
    const MetadataType __restrict__ *metadata,
    const SubGridType  __restrict__ *subgrid,
    GridType           __restrict__ *grid
    );

