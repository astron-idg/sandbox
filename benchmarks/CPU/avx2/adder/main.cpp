#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#include "Globals.h"
#include "bench.h"

using namespace std;


int main(int argc, char **argv)
{
    // Sizes
    auto jobsize = 1024;

    cout << endl;
    cout << "Setting:" << endl;
    cout << "NUM_THREADS = " << omp_get_max_threads() << endl;
    cout << "NR_REPETITIONS = " << NR_REPETITIONS << endl;
    cout << "JOBSIZE = " << jobsize << endl;
    cout << "NR_TIMESTEPS = " << NR_TIMESTEPS << endl;
    cout << "NR_CHANNELS = " << NR_CHANNELS << endl;
    cout << "NR_POLARIZATIONS = " << NR_POLARIZATIONS << endl;
    cout << "SUBGRIDSIZE = " << SUBGRIDSIZE << endl;
    cout << "NR_STATIONS = " << NR_STATIONS << endl;
    cout << "NR_TIMESLOTS = " << NR_TIMESLOTS << endl;
    cout << "GRIDSIZE = " << GRIDSIZE << endl;
    cout << "W_OFFSET = " << W_OFFSET << endl;
    cout << "IMAGESIZE = " << IMAGESIZE << endl;
    cout << "INTEGRATION_TIME = " << INTEGRATION_TIME << endl;

    // Use run, check, and run_and_check below

    // Baseline performance
    run( "Reference (parallel)", jobsize,
        (FunctionPtr) kernel_adder_reference);

    run_and_check( "HaswellEP (old optimized)", jobsize,
        (FunctionPtr) kernel_adder_haswellep);

    cout << endl;
    cout << "--------- Start optimizing ---------" << endl;

    //run( "Reference (base)", jobsize,
    //     (FunctionPtr) kernel_adder_01);

    return EXIT_SUCCESS;
}
