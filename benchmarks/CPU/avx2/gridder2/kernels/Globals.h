#ifndef IDG_GLOBALS_H_
#define IDG_GLOBALS_H_

const int NR_REPETITIONS   = 3;
const int NR_TIME          = 4096;
const int NR_CHANNELS      = 16;
const int NR_POLARIZATIONS = 4;
const int SUBGRIDSIZE      = 24;
const int NR_STATIONS      = 30;
const int NR_BASELINES     = (NR_STATIONS * (NR_STATIONS-1)) / 2;
const int NR_TIMESLOTS     = 32;
const int GRIDSIZE         = 1024;
const float W_OFFSET       = 0.0f;
const float IMAGESIZE      = 0.025f;
const int INTEGRATION_TIME = 1;


/* Structures */
namespace idg {
    typedef struct { float u, v, w; } UVW;

    typedef struct { int x, y; } Coordinate;

    typedef struct { int station1, station2; } Baseline;

    typedef struct {
        int baseline_offset;
        int time_offset;
        int nr_timesteps;
        int aterm_index;
        Baseline baseline;
        Coordinate coordinate; } Metadata;

    typedef struct {float real; float imag; } float2;
    typedef struct {double real; double imag; } double2;
}

/* Inline operations */
namespace idg {
    inline float2 operator*(const float2& x, const float2& y)
    {
        return {x.real*y.real - x.imag*y.imag,
                x.real*y.imag + x.imag*y.real};
    }

    inline float2 operator*(const float2& x, const float& a)
    {
        return {x.real*a, x.imag*a};
    }

    inline float2 operator*(const float& a, const float2& x)
    {
        return x*a;
    }

    inline float2 operator+(const float2& x, const float2& y)
    {
        return {x.real + y.real, x.imag + y.imag};
    }

    inline void operator+=(float2& x, const float2& y) {
        x.real += y.real;
        x.imag += y.imag;
    }

    inline float2 conj(const float2& x) {
        return {x.real, -x.imag};
    }
}

#endif
