#include <mkl_vml.h>

#include "gridder.h"


void kernel_gridder_vml(
    const int           nr_subgrids,
    const float         w_offset_in_lambda,
    const int           nr_channels,
    const idg::UVW		uvw[],
    const float         wavenumbers[],
    const idg::float2   visibilities[][NR_POLARIZATIONS],
    const float         spheroidal[SUBGRIDSIZE][SUBGRIDSIZE],
    const idg::float2   aterm[][NR_STATIONS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS],
    const idg::Metadata metadata[],
          idg::float2   subgrid[][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE]
    )
{
    // Find offset of first subgrid
    const idg::Metadata m       = metadata[0];
    const int baseline_offset_1 = m.baseline_offset;
    const int time_offset_1     = m.time_offset; // should be 0

    #pragma omp parallel shared(uvw, wavenumbers, visibilities, spheroidal, aterm, metadata)
    {
        #ifdef USE_LIKWID
        likwid_markerThreadInit();
        likwid_markerStartRegion("gridder_vml");
        #endif

        // Iterate all subgrids
        #pragma omp for
        for (int s = 0; s < nr_subgrids; s++) {
            // Load metadata
            const idg::Metadata m  = metadata[s];
            const int offset       = (m.baseline_offset - baseline_offset_1) +
                                     (m.time_offset - time_offset_1);
            const int nr_timesteps = m.nr_timesteps;
            const int aterm_index  = m.aterm_index;
            const int station1     = m.baseline.station1;
            const int station2     = m.baseline.station2;
            const int x_coordinate = m.coordinate.x;
            const int y_coordinate = m.coordinate.y;

            // Compute u and v offset in wavelenghts
            const float u_offset = (x_coordinate + SUBGRIDSIZE/2 - GRIDSIZE/2) * (2*M_PI / IMAGESIZE);
            const float v_offset = (y_coordinate + SUBGRIDSIZE/2 - GRIDSIZE/2) * (2*M_PI / IMAGESIZE);
            const float w_offset = 2*M_PI * w_offset_in_lambda; // TODO: check!

            // Preload visibilities
            float vis_real[nr_timesteps][NR_POLARIZATIONS][NR_CHANNELS] __attribute__((aligned(32)));
            float vis_imag[nr_timesteps][NR_POLARIZATIONS][NR_CHANNELS] __attribute__((aligned(32)));

            for (int time = 0; time < nr_timesteps; time++) {
                for (int chan = 0; chan < NR_CHANNELS; chan++) {
                    size_t index = (offset + time)*nr_channels + chan;
                    for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        vis_real[time][pol][chan] = visibilities[index][pol].real;
                        vis_imag[time][pol][chan] = visibilities[index][pol].imag;
                    }
                }
            }

            // Iterate all pixels in subgrid
            for (int y = 0; y < SUBGRIDSIZE; y++) {
                for (int x = 0; x < SUBGRIDSIZE; x++) {

                    // Compute phase
                    float phase[nr_timesteps][NR_CHANNELS] __attribute__((aligned(32)));

                    // Compute l,m,n
                    const float l = (x-(SUBGRIDSIZE/2)) * IMAGESIZE/SUBGRIDSIZE;
                    const float m = (y-(SUBGRIDSIZE/2)) * IMAGESIZE/SUBGRIDSIZE;
                    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
                    // accurately for small values of l and m
                    const float tmp = (l * l) + (m * m);
                    const float n = tmp / (1.0f + sqrtf(1.0f - tmp));

                    for (int time = 0; time < nr_timesteps; time++) {
                        // Load UVW coordinates
                        float u = uvw[offset + time].u;
                        float v = uvw[offset + time].v;
                        float w = uvw[offset + time].w;

                        // Compute phase index
                        float phase_index = u*l + v*m + w*n;

                        // Compute phase offset
                        float phase_offset = u_offset*l + v_offset*m + w_offset*n;

                        for (int chan = 0; chan < NR_CHANNELS; chan++) {
                            // Compute phase
                            float wavenumber = wavenumbers[chan];
                            phase[time][chan] = phase_offset - (phase_index * wavenumber);
                        }
                    } // end time

                    // Compute phasor
                    float phasor_real[nr_timesteps][NR_CHANNELS] __attribute__((aligned(32)));
                    float phasor_imag[nr_timesteps][NR_CHANNELS] __attribute__((aligned(32)));
                    vmsSinCos(
                        nr_timesteps * NR_CHANNELS,
                        (float *) phase,
                        (float *) phasor_imag,
                        (float *) phasor_real,
                        VML_LA);

                    // Multiply visibilities with phasor and reduce for all timesteps and channels
                    idg::float2 pixels[NR_POLARIZATIONS];

                    // Initialize pixel for every polarization
                    float pixels_xx_real = 0.0f;
                    float pixels_xy_real = 0.0f;
                    float pixels_yx_real = 0.0f;
                    float pixels_yy_real = 0.0f;
                    float pixels_xx_imag = 0.0f;
                    float pixels_xy_imag = 0.0f;
                    float pixels_yx_imag = 0.0f;
                    float pixels_yy_imag = 0.0f;

                    // Update pixel for every timestep
                    for (int time = 0; time < nr_timesteps; time++) {
                        // Update pixel for every channel
                        #pragma omp simd reduction(+:pixels_xx_real,pixels_xx_imag,  \
                                                     pixels_xy_real,pixels_xy_imag,  \
                                                     pixels_yx_real,pixels_yx_imag,  \
                                                     pixels_yy_real,pixels_yy_imag)
                        for (int chan = 0; chan < NR_CHANNELS; chan++) {
                            pixels_xx_real += vis_real[time][0][chan] * phasor_real[time][chan];
                            pixels_xx_imag += vis_real[time][0][chan] * phasor_imag[time][chan];
                            pixels_xx_real -= vis_imag[time][0][chan] * phasor_imag[time][chan];
                            pixels_xx_imag += vis_imag[time][0][chan] * phasor_real[time][chan];

                            pixels_xy_real += vis_real[time][1][chan] * phasor_real[time][chan];
                            pixels_xy_imag += vis_real[time][1][chan] * phasor_imag[time][chan];
                            pixels_xy_real -= vis_imag[time][1][chan] * phasor_imag[time][chan];
                            pixels_xy_imag += vis_imag[time][1][chan] * phasor_real[time][chan];

                            // #pragma distribute_point

                            pixels_yx_real += vis_real[time][2][chan] * phasor_real[time][chan];
                            pixels_yx_imag += vis_real[time][2][chan] * phasor_imag[time][chan];
                            pixels_yx_real -= vis_imag[time][2][chan] * phasor_imag[time][chan];
                            pixels_yx_imag += vis_imag[time][2][chan] * phasor_real[time][chan];

                            pixels_yy_real += vis_real[time][3][chan] * phasor_real[time][chan];
                            pixels_yy_imag += vis_real[time][3][chan] * phasor_imag[time][chan];
                            pixels_yy_real -= vis_imag[time][3][chan] * phasor_imag[time][chan];
                            pixels_yy_imag += vis_imag[time][3][chan] * phasor_real[time][chan];
                        }
                     }

                    // Combine real and imaginary parts
                    pixels[0] = {pixels_xx_real, pixels_xx_imag};
                    pixels[1] = {pixels_xy_real, pixels_xy_imag};
                    pixels[2] = {pixels_yx_real, pixels_yx_imag};
                    pixels[3] = {pixels_yy_real, pixels_yy_imag};

                    // Load a term for station1
                    idg::float2 aXX1 = aterm[aterm_index][station1][y][x][0];
                    idg::float2 aXY1 = aterm[aterm_index][station1][y][x][1];
                    idg::float2 aYX1 = aterm[aterm_index][station1][y][x][2];
                    idg::float2 aYY1 = aterm[aterm_index][station1][y][x][3];

                    // Load aterm for station2
                    idg::float2 aXX2 = conj(aterm[aterm_index][station2][y][x][0]);
                    idg::float2 aXY2 = conj(aterm[aterm_index][station2][y][x][1]);
                    idg::float2 aYX2 = conj(aterm[aterm_index][station2][y][x][2]);
                    idg::float2 aYY2 = conj(aterm[aterm_index][station2][y][x][3]);

                    // Apply aterm to subgrid: P*A1
                    // [ pixels[0], pixels[1];    [ aXX1, aXY1;
                    //   pixels[2], pixels[3] ] *   aYX1, aYY1 ]
                    idg::float2 pixelsXX = pixels[0];
                    idg::float2 pixelsXY = pixels[1];
                    idg::float2 pixelsYX = pixels[2];
                    idg::float2 pixelsYY = pixels[3];
                    pixels[0]  = (pixelsXX * aXX1);
                    pixels[0] += (pixelsXY * aYX1);
                    pixels[1]  = (pixelsXX * aXY1);
                    pixels[1] += (pixelsXY * aYY1);
                    pixels[2]  = (pixelsYX * aXX1);
                    pixels[2] += (pixelsYY * aYX1);
                    pixels[3]  = (pixelsYX * aXY1);
                    pixels[3] += (pixelsYY * aYY1);

                    // Apply aterm to subgrid: A2^H*P
                    // [ aXX2, aYX1;      [ pixels[0], pixels[1];
                    //   aXY1, aYY2 ]  *    pixels[2], pixels[3] ]
                    pixelsXX = pixels[0];
                    pixelsXY = pixels[1];
                    pixelsYX = pixels[2];
                    pixelsYY = pixels[3];
                    pixels[0]  = (pixelsXX * aXX2);
                    pixels[0] += (pixelsYX * aYX2);
                    pixels[1]  = (pixelsXY * aXX2);
                    pixels[1] += (pixelsYY * aYX2);
                    pixels[2]  = (pixelsXX * aXY2);
                    pixels[2] += (pixelsYX * aYY2);
                    pixels[3]  = (pixelsXY * aXY2);
                    pixels[3] += (pixelsYY * aYY2);

                    // Load spheroidal
                    float sph = spheroidal[y][x];

                    // Compute shifted position in subgrid
                    int x_dst = (x + (SUBGRIDSIZE/2)) % SUBGRIDSIZE;
                    int y_dst = (y + (SUBGRIDSIZE/2)) % SUBGRIDSIZE;

                    // Set subgrid value
                    for (int pol = 0; pol < NR_POLARIZATIONS; pol++) {
                        subgrid[s][pol][y_dst][x_dst] += pixels[pol] * sph;
                    }
                } // end x
            } // end y
        } // end s
        #ifdef USE_LIKWID
        likwid_markerStopRegion("gridder_vml");
        #endif
    } // end omp parallel
} // end kernel_gridder_
