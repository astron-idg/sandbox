#pragma once

#include <cmath>
#include <cstring>

#include <omp.h>
#ifdef USE_LIKWID
#include <likwid.h>
#endif

#include "Globals.h"

// Reference implementation
void kernel_gridder_ref(
    const int           nr_subgrids,
    const float         w_offset_in_lambda,
    const int           nr_channels,
    const idg::UVW		uvw[],
    const float         wavenumbers[],
    const idg::float2   visibilities[][NR_POLARIZATIONS],
    const float         spheroidal[SUBGRIDSIZE][SUBGRIDSIZE],
    const idg::float2   aterm[][NR_STATIONS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS],
    const idg::Metadata metadata[],
          idg::float2   subgrid[][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE]);

// Optimized implementations (for Intel compiler)
void kernel_gridder_vml(
    const int           nr_subgrids,
    const float         w_offset_in_lambda,
    const int           nr_channels,
    const idg::UVW		uvw[],
    const float         wavenumbers[],
    const idg::float2   visibilities[][NR_POLARIZATIONS],
    const float         spheroidal[SUBGRIDSIZE][SUBGRIDSIZE],
    const idg::float2   aterm[][NR_STATIONS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS],
    const idg::Metadata metadata[],
          idg::float2   subgrid[][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE]);

void kernel_gridder_svml(
    const int           nr_subgrids,
    const float         w_offset_in_lambda,
    const int           nr_channels,
    const idg::UVW		uvw[],
    const float         wavenumbers[],
    const idg::float2   visibilities[][NR_POLARIZATIONS],
    const float         spheroidal[SUBGRIDSIZE][SUBGRIDSIZE],
    const idg::float2   aterm[][NR_STATIONS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS],
    const idg::Metadata metadata[],
          idg::float2   subgrid[][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE]);

void kernel_gridder_hybrid(
    const int           nr_subgrids,
    const float         w_offset_in_lambda,
    const int           nr_channels,
    const idg::UVW		uvw[],
    const float         wavenumbers[],
    const idg::float2   visibilities[][NR_POLARIZATIONS],
    const float         spheroidal[SUBGRIDSIZE][SUBGRIDSIZE],
    const idg::float2   aterm[][NR_STATIONS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS],
    const idg::Metadata metadata[],
          idg::float2   subgrid[][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE]);
