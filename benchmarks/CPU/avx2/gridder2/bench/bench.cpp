#include "bench.h"

using namespace std;

void run(const string& name, idg::Parameters params, FunctionPtr kernel_gridder)
{
    float w_offset = 0;
    int kernel_size = (SUBGRIDSIZE / 4) + 1;

    // Allocate and initialize data structures
    auto size_visibilities = 1ULL * NR_BASELINES*NR_TIME*
                                    NR_CHANNELS*NR_POLARIZATIONS;
    auto size_uvw          = 1ULL * NR_BASELINES*NR_TIME*3;
    auto size_wavenumbers  = 1ULL * NR_CHANNELS;
    auto size_aterm        = 1ULL * NR_STATIONS*NR_TIMESLOTS*
                                    NR_POLARIZATIONS*SUBGRIDSIZE*SUBGRIDSIZE;
    auto size_spheroidal   = 1ULL * SUBGRIDSIZE*SUBGRIDSIZE;
    auto size_grid         = 1ULL * NR_POLARIZATIONS*GRIDSIZE*GRIDSIZE;
    auto size_baselines    = 1ULL * NR_BASELINES*2;
    auto visibilities  = new complex<float>[size_visibilities];
    auto uvw           = new float[size_uvw];
    auto wavenumbers   = new float[size_wavenumbers];
    auto aterm         = new complex<float>[size_aterm];
    auto aterm_offsets = new int[NR_TIMESLOTS+1];
    auto spheroidal    = new float[size_spheroidal];
    auto baselines     = new int[size_baselines];

    idg::init_example_visibilities(visibilities, NR_BASELINES, NR_TIME, NR_CHANNELS, NR_POLARIZATIONS);
    idg::init_example_uvw(uvw, NR_STATIONS, NR_BASELINES, NR_TIME, INTEGRATION_TIME);
    idg::init_example_wavenumbers(wavenumbers, NR_CHANNELS);
    idg::init_example_aterm(aterm, NR_TIMESLOTS, NR_STATIONS, SUBGRIDSIZE, NR_POLARIZATIONS);
    idg::init_example_aterm_offsets(aterm_offsets, NR_TIMESLOTS, NR_TIME);
    idg::init_example_spheroidal(spheroidal, SUBGRIDSIZE);
    idg::init_example_baselines(baselines, NR_STATIONS, NR_BASELINES);

    // Create plan
    auto plan         = idg::Plan(params, uvw, wavenumbers, baselines, aterm_offsets, kernel_size);
    auto nr_subgrids  = plan.get_nr_subgrids();
    auto nr_timesteps = plan.get_nr_timesteps();
    auto metadata     = plan.get_metadata_ptr();

    // Allocate subgrids
    auto size_subgrids = 1ULL * nr_subgrids * NR_POLARIZATIONS * SUBGRIDSIZE * SUBGRIDSIZE;
    auto subgrids = new complex<float>[size_subgrids];

    // Run kernel
    auto total_runtime = -omp_get_wtime();
    auto min_runtime = numeric_limits<double>::infinity();

    for (int i = 0; i < NR_REPETITIONS; i++) {
        auto runtime = -omp_get_wtime();

        kernel_gridder(
            nr_subgrids,
            w_offset,
            NR_CHANNELS,
            (idg::UVW *) uvw,
            wavenumbers,
            (idg::float2 *) visibilities,
            spheroidal,
            (idg::float2 *) aterm,
            (int *) metadata,
            (idg::float2 *) subgrids);

        runtime += omp_get_wtime();
        min_runtime = fmin(min_runtime, runtime);
    }

    // Report performance
    total_runtime += omp_get_wtime();
    auto avg_runtime = total_runtime/NR_REPETITIONS;
    auto flops = gridder_flops(nr_timesteps, nr_subgrids);
    auto fmas = flops / 2;
    auto sincos = gridder_sincos(nr_timesteps, nr_subgrids);
    auto ops = fmas + sincos;
    auto bytes = gridder_bytes(nr_timesteps, nr_subgrids);
    auto nr_visibilities = gridder_visibilities(nr_timesteps);
    auto gflops = flops * 1e-9 / min_runtime;
    auto gfmas = fmas * 1e-9 / min_runtime;
    auto gsincos = sincos * 1e-9 / min_runtime;
    auto gops = ops * 1e-9 / min_runtime;
    auto mvis =  nr_visibilities * 1e-6 / min_runtime;
    auto gbytes = bytes * 1e-9 / min_runtime;
    auto oi = (float) ops / bytes;
    printf("\n%s\n", name.c_str());
    printf("Avg. runtime:     %.2f\n", avg_runtime);
    printf("Min. runtime:     %.2f\n", min_runtime);
    printf("Gbytes:           %.2f\n", bytes * 1e-9);
    printf("Gflops:           %.2f\n", flops * 1e-9);
    printf("Gfmas:            %.2f\n", fmas * 1e-9);
    printf("Gsincos:          %.2f\n", sincos * 1e-9);
    printf("Gops:             %.2f\n", ops * 1e-9);
    printf("Mvisibilities:    %.2f\n", mvis);
    printf("Gflops/s:         %.2f\n", gflops);
    printf("Gfmas/s:          %.2f\n", gfmas);
    printf("Gops/s:           %.2f\n", gops);
    printf("Gsincos/s:        %.2f\n", gsincos);
    printf("Bandwidth [GB/s]: %.2f\n", gbytes);
    printf("OI (lower bound): %.2f\n", oi);

    // Free memory for data structures
    delete[] visibilities;
    delete[] uvw;
    delete[] wavenumbers;
    delete[] aterm;
    delete[] aterm_offsets;
    delete[] spheroidal;
    delete[] baselines;
    delete[] subgrids;
}
