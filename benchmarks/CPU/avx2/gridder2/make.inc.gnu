# C++ compiler
CXX=g++

# Default C++ compiler flags
CXXFLAGS=-std=c++11 -O3 -fopenmp -march=core-avx2 -ffast-math
# OPTREPORT=-fopt-info-vec-optimized

# Additional compiler flags for debug mode
# DEBUGFLAGS=-g -fno-omit-frame-pointer -ggdb

# Libraries to link
LFLAGS = -L$(MKLROOT)/lib/intel64
LIBS = -ldl -lpthread -lm -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_avx2 -lmkl_vml_avx2
