#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#include "Parameters.h"
#include "Globals.h"
#include "bench.h"

using namespace std;


int main(int argc, char **argv)
{
    // Initialize configuration
    idg::Parameters params;
    params.set_nr_time(NR_TIME);
    params.set_nr_channels(NR_CHANNELS);
    params.set_subgrid_size(SUBGRIDSIZE);
    params.set_nr_stations(NR_STATIONS);
    params.set_nr_timeslots(NR_TIMESLOTS);
    params.set_grid_size(GRIDSIZE);
    params.set_imagesize(IMAGESIZE);

    // Print configuration
    clog << params;
    clog << endl;

    cout << "--------- Running kernels ---------" << endl;

    #if USE_LIKWID
	likwid_markerInit();
    #endif

    //run( "Reference", params, (FunctionPtr) kernel_gridder_ref);

    run( "Optimized (VML)", params, (FunctionPtr) kernel_gridder_vml);
    run( "Optimized (SVML)", params, (FunctionPtr) kernel_gridder_svml);
    run( "Optimized (Hybrid)", params, (FunctionPtr) kernel_gridder_hybrid);

    #if USE_LIKWID
	likwid_markerClose();
    #endif

    return EXIT_SUCCESS;
}
