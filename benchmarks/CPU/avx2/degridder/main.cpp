#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

#include "Globals.h"
#include "bench.h"

using namespace std;


int main(int argc, char **argv)
{
    // Sizes
    auto jobsize = 1024;

    cout << endl;
    cout << "Setting:" << endl;
    cout << "NUM_THREADS = " << omp_get_max_threads() << endl;
    cout << "NR_REPETITIONS = " << NR_REPETITIONS << endl;
    cout << "JOBSIZE = " << jobsize << endl;
    cout << "NR_TIMESTEPS = " << NR_TIMESTEPS << endl;
    cout << "NR_CHANNELS = " << NR_CHANNELS << endl;
    cout << "NR_POLARIZATIONS = " << NR_POLARIZATIONS << endl;
    cout << "SUBGRIDSIZE = " << SUBGRIDSIZE << endl;
    cout << "NR_STATIONS = " << NR_STATIONS << endl;
    cout << "NR_TIMESLOTS = " << NR_TIMESLOTS << endl;
    cout << "GRIDSIZE = " << GRIDSIZE << endl;
    cout << "W_OFFSET = " << W_OFFSET << endl;
    cout << "IMAGESIZE = " << IMAGESIZE << endl;
    cout << "INTEGRATION_TIME = " << INTEGRATION_TIME << endl;

    // Use run, check, and run_and_check below

    // Baseline performance
    // run( "Reference (parallel)" , jobsize,
    //     (FunctionPtr) kernel_degridder_reference);

    // run( "HaswellEP (old optimized)", jobsize,
    //       (FunctionPtr) kernel_degridder_haswellep);

    cout << endl;
    cout << "--------- Start optimizing ---------" << endl;

    run( "Reference (base)", jobsize,
         (FunctionPtr) kernel_degridder_01);

    run( "Loop fission for pixels", jobsize,
         (FunctionPtr) kernel_degridder_02);

    run( "Vectorized reduction of visibilities", jobsize,
         (FunctionPtr) kernel_degridder_03);

    run( "Changed layout of pixels datastructure", jobsize,
         (FunctionPtr) kernel_degridder_04);

    run( "Split pixels in real and imaginary", jobsize,
         (FunctionPtr) kernel_degridder_05);

    run( "Use VML to compute phasor", jobsize,
         (FunctionPtr) kernel_degridder_06);

    run( "Precompute phase_index and phase_offset", jobsize,
         (FunctionPtr) kernel_degridder_07);

    run( "Inner loop using intrinsics", jobsize,
         (FunctionPtr) kernel_degridder_08);

    run( "Split intrinsics loop", jobsize,
         (FunctionPtr) kernel_degridder_09);

    run( "l,n,m compute out", jobsize,
         (FunctionPtr) kernel_degridder_10);

    return EXIT_SUCCESS;
}
