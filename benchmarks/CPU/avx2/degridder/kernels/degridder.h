#pragma once

#include "Globals.h"

// Reference implementation
void kernel_degridder_reference(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

// Original optimized code for HaswellEP
void kernel_degridder_haswellep(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

// Start of optimizing:
void kernel_degridder_01(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_02(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_03(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_04(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_05(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_06(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_07(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_08(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_09(
	const int jobsize, const float w_offset,
	const UVWType		   __restrict__ *uvw,
	const WavenumberType   __restrict__ *wavenumbers,
	      VisibilitiesType __restrict__ *visibilities,
	const SpheroidalType   __restrict__ *spheroidal,
	const ATermType		   __restrict__ *aterm,
	const MetadataType	   __restrict__ *metadata,
	const SubGridType      __restrict__ *subgrid);

void kernel_degridder_10(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
          VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    const SubGridType      __restrict__ *subgrid);

void kernel_degridder_11(
    const int jobsize, const float w_offset,
    const UVWType		   __restrict__ *uvw,
    const WavenumberType   __restrict__ *wavenumbers,
          VisibilitiesType __restrict__ *visibilities,
    const SpheroidalType   __restrict__ *spheroidal,
    const ATermType		   __restrict__ *aterm,
    const MetadataType	   __restrict__ *metadata,
    const SubGridType      __restrict__ *subgrid);
