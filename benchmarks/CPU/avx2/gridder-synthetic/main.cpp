#include <iostream>
#include <immintrin.h>
#include <omp.h>

#define NR_REPETITIONS  5
#define COUNT           1000000ULL
#define VECTOR_LENGTH   8
#define SIZE            24
#define TIME            128
#define CHANNELS        8
#define COMPRESSION     100

__m256 s1, s2, s3, s4, s5, s6, s7, s8;
__m256 v1, v2, v3, v4;
__m256 v5, v6, v7, v8;
__m256 p1, p2;

float v1_[COUNT/COMPRESSION][TIME][4][CHANNELS] __attribute__((aligned(32)));
float v2_[COUNT/COMPRESSION][TIME][4][CHANNELS] __attribute__((aligned(32)));


void kernel_v0() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT/VECTOR_LENGTH; i++) {
        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                for (int t = 0; t < TIME; t++) {
                    s1 = _mm256_fmadd_ps(s1, v1, p1);
                    s2 = _mm256_fmadd_ps(s2, v1, p2);
                    s3 = _mm256_fmadd_ps(s3, v2, p1);
                    s4 = _mm256_fmadd_ps(s4, v2, p2);
                    s5 = _mm256_fmadd_ps(s5, v3, p1);
                    s6 = _mm256_fmadd_ps(s6, v3, p2);
                    s7 = _mm256_fmadd_ps(s7, v4, p1);
                    s8 = _mm256_fmadd_ps(s8, v4, p2);

                    s1 = _mm256_fmsub_ps(s1, v5, p2);
                    s2 = _mm256_fmadd_ps(s2, v5, p1);
                    s3 = _mm256_fmsub_ps(s3, v6, p2);
                    s4 = _mm256_fmadd_ps(s4, v6, p1);
                    s5 = _mm256_fmsub_ps(s5, v7, p2);
                    s6 = _mm256_fmadd_ps(s6, v7, p1);
                    s7 = _mm256_fmsub_ps(s7, v8, p2);
                    s8 = _mm256_fmadd_ps(s8, v8, p1);
                }
            }
        }
    }
}

void kernel_v1() {
    #pragma omp parallel for schedule(static)
    for (unsigned long long i = 0; i < COUNT/VECTOR_LENGTH; i++) {

        const int index = i / COMPRESSION;

        float p1_[TIME][CHANNELS] __attribute__((aligned(32)));
        float p2_[TIME][CHANNELS] __attribute__((aligned(32)));

        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {

                for (int t = 0; t < TIME; t++) {
                    for (int c = 0; c < CHANNELS; c++) {
                        p1_[t][c] = y + x;
                        p2_[t][c] = y * x;
                    }
                }

                for (int t = 0; t < TIME; t++) {
                    __m256 p1 = _mm256_load_ps(&p1_[t][0]);
                    __m256 p2 = _mm256_load_ps(&p2_[t][0]);

                    __m256 v1 = _mm256_load_ps(&v1_[index][t][0][0]);
                    __m256 v2 = _mm256_load_ps(&v1_[index][t][1][0]);
                    __m256 v3 = _mm256_load_ps(&v1_[index][t][2][0]);
                    __m256 v4 = _mm256_load_ps(&v1_[index][t][3][0]);

                    s1 = _mm256_fmadd_ps(s1, v1, p1);
                    s2 = _mm256_fmadd_ps(s2, v1, p2);
                    s3 = _mm256_fmadd_ps(s3, v2, p1);
                    s4 = _mm256_fmadd_ps(s4, v2, p2);
                    s5 = _mm256_fmadd_ps(s5, v3, p1);
                    s6 = _mm256_fmadd_ps(s6, v3, p2);
                    s7 = _mm256_fmadd_ps(s7, v4, p1);
                    s8 = _mm256_fmadd_ps(s8, v4, p2);

                    __m256 v5 = _mm256_load_ps(&v2_[index][t][0][0]);
                    __m256 v6 = _mm256_load_ps(&v2_[index][t][1][0]);
                    __m256 v7 = _mm256_load_ps(&v2_[index][t][2][0]);
                    __m256 v8 = _mm256_load_ps(&v2_[index][t][3][0]);

                    s1 = _mm256_fmsub_ps(s1, v5, p2);
                    s2 = _mm256_fmadd_ps(s2, v5, p1);
                    s3 = _mm256_fmsub_ps(s3, v6, p2);
                    s4 = _mm256_fmadd_ps(s4, v6, p1);
                    s5 = _mm256_fmsub_ps(s5, v7, p2);
                    s6 = _mm256_fmadd_ps(s6, v7, p1);
                    s7 = _mm256_fmsub_ps(s7, v8, p2);
                    s8 = _mm256_fmadd_ps(s8, v8, p1);
                }
            }
        }
    }
}
unsigned long long ops() {
    return SIZE*SIZE*TIME*16*2*COUNT;
}

void report(unsigned long long ops, double runtime) {
    std::cout << "OPS = " << std::scientific << double(ops) << ", ";
    std::cout << "RUNTIME = " << std::scientific << runtime << ", ";
    std::cout << "GFLOPS = " << ops / runtime / 1e9 << std::endl;
}

int main() {
    //std::cout << "FMA REDUCE" << std::endl;
    for (int i = 0; i < NR_REPETITIONS; i++) {
        double runtime = -omp_get_wtime();
        kernel_v0();
        runtime += omp_get_wtime();
        report(ops(), runtime);
    }
    //std::cout << "FMA REDUCE + MEM LOAD" << std::endl;
    //for (int i = 0; i < NR_REPETITIONS; i++) {
    //    double runtime = -omp_get_wtime();
    //    kernel_v1();
    //    runtime += omp_get_wtime();
    //    report(ops(), runtime);
    //}
    return EXIT_SUCCESS;
}
