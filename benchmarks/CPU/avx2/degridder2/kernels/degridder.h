#pragma once

#include <cmath>
#include <cstring>

#include <omp.h>
#if USE_LIKWID
#include <likwid.h>
#endif

#include "Globals.h"

// Optimized implementation (for Intel compiler)
void kernel_degridder_vml(
    const int           nr_subgrids,
    const float         w_offset_in_lambda,
    const int           nr_channels,
    const idg::UVW		uvw[],
    const float         wavenumbers[],
          idg::float2   visibilities[][NR_POLARIZATIONS],
    const float         spheroidal[SUBGRIDSIZE][SUBGRIDSIZE],
    const idg::float2   aterm[][NR_STATIONS][SUBGRIDSIZE][SUBGRIDSIZE][NR_POLARIZATIONS],
    const idg::Metadata metadata[],
    const idg::float2   subgrid[][NR_POLARIZATIONS][SUBGRIDSIZE][SUBGRIDSIZE]);
