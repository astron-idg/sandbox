#pragma once

#include <complex>
#include <cstring>

#include "Globals.h"
#include "uvwsim.h"

uint64_t degridder_visibilities(uint64_t nr_timesteps);
uint64_t degridder_sincos(uint64_t nr_timesteps, uint64_t nr_subgrids);
uint64_t degridder_flops(uint64_t nr_timesteps, uint64_t nr_subgrids);
uint64_t degridder_bytes(uint64_t nr_timesteps, uint64_t nr_subgrids);
void memset_array(void *clear, size_t num_bytes);

void init_wavenumbers(float wavenumbers[], int size_wavenumbers);
void init_visibilities(idg::float2 visibilities[], int size_visibilities);
void init_spheroidal(float spheroidal[], int size_spheroidal);
void init_aterm(idg::float2 aterm[], int size_aterm);
void init_subgrid(idg::float2 subgrid[], int size_subgrid);

// from code init routines:
void init_uvw(void *ptr, int nr_stations, int nr_baselines,
              int nr_time, int integration_time);
void init_baselines(void *ptr, int nr_stations, int nr_baselines);
void* init_baselines(int nr_stations, int nr_baselines);
void init_metadata(void *ptr, void *_uvw, void *_wavenumbers,
                   int nr_stations, int nr_baselines, int nr_timesteps,
                   int nr_timeslots, int nr_channels, int gridsize,
                   int subgridsize, float imagesize);

float get_accucary(
    const int size,
    const std::complex<float>* A,
    const std::complex<float>* B);
