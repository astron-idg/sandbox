#ifndef IDG_PARAMETERS_H_
#define IDG_PARAMETERS_H_

#include <iostream>

namespace idg {

  class Parameters
  {
  public:

      /// Define the environment names searched for
      static const std::string ENV_NR_STATIONS;
      static const std::string ENV_NR_CHANNELS;
      static const std::string ENV_NR_TIME;
      static const std::string ENV_NR_TIMESLOTS;
      static const std::string ENV_IMAGESIZE;
      static const std::string ENV_GRIDSIZE;
      static const std::string ENV_SUBGRIDSIZE;
      static const std::string ENV_JOBSIZE;

      /// Constructor: default reads values from ENV or sets default
      Parameters()
      {
          set_from_env();
      }

      // default copy constructor/assignment okay

      // default destructur
      ~Parameters() = default;

      // get methods
      unsigned int get_nr_stations() const { return nr_stations; }
      unsigned int get_nr_baselines() const { return nr_baselines; }
      unsigned int get_nr_channels() const { return nr_channels; }
      unsigned int get_nr_time() const { return nr_time; }
      unsigned int get_nr_timeslots() const { return nr_timeslots; }
      float get_imagesize() const { return imagesize; }
      unsigned int get_grid_size() const { return grid_size; }
      unsigned int get_subgrid_size() const { return subgrid_size; }
      unsigned int get_job_size() const { return job_size; }
      unsigned int get_nr_polarizations() const { return nr_polarizations; }

      // set methods
      void set_nr_stations(unsigned int ns);
      void set_nr_channels(unsigned int nc);
      void set_nr_time(unsigned int nt);
      void set_nr_timeslots(unsigned int nt);
      void set_imagesize(float imagesize);
      void set_subgrid_size(unsigned int sgs);
      void set_grid_size(unsigned int gs);
      void set_job_size(unsigned int js);

      // auxiliary functions
      void print() const;
      void print(std::ostream& os) const;
      void set_from_env();

  private:
      unsigned int nr_stations;
      unsigned int nr_baselines;     // nr_stations*(nr_stations-1)/2
      unsigned int nr_channels;
      unsigned int nr_time;
      unsigned int nr_timeslots;     // for each time slot, one A-term
      static const unsigned int nr_polarizations = 4;
      float        imagesize;        // angular resolution in radians
      unsigned int grid_size;
      unsigned int subgrid_size;
      unsigned int job_size;
  };

  // helper functions
  std::ostream& operator<<(std::ostream& os, const Parameters& c);

} // namespace idg

#endif
