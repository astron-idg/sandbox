#include <iostream>
#include <iomanip>
#include <complex>
#include <cstdint>
#include <limits>
#include <cstring>

#include "Init.h"
#include "Plan.h"
#include "Globals.h"
#include "auxiliary.h"
#include "degridder.h"

using FunctionPtr = void (*)(
    int,            // nr_subgrids
    float,          // w_offset_in_lambda
    int,            // nr_channels
    idg::UVW*,      // uvw
    void*,          // wavenumbers
    idg::float2*,   // visibilities
    void*,          // spheroidal
    void*,          // aterm
    int*,           // metadata
    idg::float2*);  // subgrid

void run(const std::string& name, idg::Parameters params, FunctionPtr kernel_gridder);
