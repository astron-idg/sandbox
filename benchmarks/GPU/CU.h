#include <iostream>

#include <cuda.h>

struct dim3;

namespace cu {

    void init(unsigned flags = 0);

    template<typename T>
    class Error : public std::exception {
        public:
            Error(T result):
            _result(result) {}

            operator T() const {
                return _result;
            }

        private:
            T _result;
    };


    class Device {
        public:
            Device();
            Device(int ordinal);

            static int getCount();
            std::string get_name() const;
            int get_capability() const;

            template <CUdevice_attribute attribute>
            int get_attribute() const {
                int value;
                if (cuDeviceGetAttribute(&value, attribute, _device) != CUDA_SUCCESS) {
                    std::cerr << "CUDA Error: could not get attribute: " << attribute << std::endl;
                    exit(EXIT_FAILURE);
                }
                return value;
            }

            operator CUdevice();

        private:
            CUdevice _device;
    };


    class Context {
        public:
            Context();
            Context(Device& device, int flags = 0);
            ~Context();
            void setCurrent() const;
            void synchronize();
            void reset();

            operator CUcontext();

        private:
            CUcontext _context;
            CUdevice _device;
    };


    class DeviceMemory  {

        public:
            DeviceMemory(size_t size);
            ~DeviceMemory();

            size_t capacity();
            size_t size();

            template <typename T> operator T *() {
                if (_size) {
                    return static_cast<T *>(&_ptr);
                } else {
                    return static_cast<T *>(&_nullptr);
                }
            }

            template <typename T> operator T () {
                if (_size) {
                    return static_cast<T>(_ptr);
                } else {
                    return static_cast<T>(_nullptr);
                }
            }

        private:
            CUdeviceptr _ptr;
            size_t _capacity;
            size_t _size;
            static const CUdeviceptr _nullptr = 0;
    };


    class Module {
        public:
            Module(const char *file_name);
            ~Module();

            operator CUmodule();

        private:
            CUmodule _module;
    };


    class Function {
        public:
            Function(Module &module, const char *name);
            Function(CUfunction function);

            int get_attribute(CUfunction_attribute attribute);

            operator CUfunction();

        private:
            CUfunction _function;
    };


    class Event {
        public:
            Event(int flags = CU_EVENT_DEFAULT);
            ~Event();

            void synchronize();
            float elapsedTime(Event &second);

            operator CUevent();

        private:
            CUevent _event;
    };


    class Stream {
        public:
            Stream(int flags = CU_STREAM_DEFAULT);
            ~Stream();

            void memcpyHtoDAsync(CUdeviceptr devPtr, const void *hostPtr, size_t size);
            void memcpyDtoHAsync(void *hostPtr, CUdeviceptr devPtr, size_t size);
            void launchKernel(Function &function, dim3 grid, dim3 block, unsigned sharedMemBytes, const void **parameters);
            void query();
            void synchronize();
            void waitEvent(Event &event);
            void addCallback(CUstreamCallback callback, void *userData, int flags = 0);
            void record(Event &event);

            operator CUstream();

        private:
            CUstream _stream;
    };

} // end namespace cu
