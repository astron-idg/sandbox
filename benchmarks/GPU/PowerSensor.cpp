#include <iostream>

#include "PowerSensor.h"

namespace powersensor {

    PowerSensor* get_power_sensor()
    {
        // Read power sensor from environment
        char *power_sensor_char = getenv("POWER_SENSOR");
        if (power_sensor_char != NULL) {
            std::string power_sensor_str = std::string(power_sensor_char);

            // Try to initialize the specified PowerSensor
            #if defined(USE_POWERSENSOR)
            if (power_sensor_str.compare(name_nvml) == 0) {
                return powersensor::nvml::NVMLPowerSensor::create(0, NULL);
            } else if (power_sensor_str.find(name_arduino) != std::string::npos) {
                return powersensor::arduino::ArduinoPowerSensor::create(power_sensor_str.c_str(), NULL);
            }
            #endif
        }

        // Use the DummyPowerSensor as backup
        return powersensor::DummyPowerSensor::create();
    }

    #if not defined(USE_POWERSENSOR)
    class DummyPowerSensor_ : public DummyPowerSensor {
        public:
            virtual State read();
            virtual double seconds(const State &firstState, const State &secondState) override;
            virtual double Joules(const State &firstState, const State &secondState) override;
            virtual double Watt(const State &firstState, const State &secondState) override;
    };

    DummyPowerSensor* DummyPowerSensor::create()
    {
        return new DummyPowerSensor_();
    }

    State DummyPowerSensor_::read() {
        State state;
        state.timeAtRead = omp_get_wtime();
        return state;
    }

    double DummyPowerSensor_::seconds(const State &firstState, const State &secondState) {
        return secondState.timeAtRead - firstState.timeAtRead;
    }

    double DummyPowerSensor_::Joules(const State &firstState, const State &secondState) {
        return secondState.joulesAtRead - firstState.joulesAtRead;
    }

    double DummyPowerSensor_::Watt(const State &firstState, const State &secondState) {
        return Joules(firstState, secondState) /
               seconds(firstState, secondState);
    }
    #endif

} // end namespace powersensor
