#include <string>

#include <omp.h>

#if defined(USE_POWERSENSOR)
#include "powersensor.h"
#endif

namespace powersensor {

    static std::string name_nvml("nvml");
    static std::string name_arduino("tty");

    #if not defined(USE_POWERSENSOR)
    class State {
        public:
            double timeAtRead;
            double joulesAtRead;
    };

    class PowerSensor {
        public:
            virtual ~PowerSensor() {}

            virtual State read() = 0;

            virtual double seconds(const State &firstState, const State &secondState) = 0;
            virtual double Joules(const State &firstState, const State &secondState) = 0;
            virtual double Watt(const State &firstState, const State &secondState) = 0;
    };

    class DummyPowerSensor : public PowerSensor {
        public:
            static DummyPowerSensor* create();
    };
    #endif

    PowerSensor* get_power_sensor();

} // end namespace powersensor
