#include <cuComplex.h>

/*
    Parameters
*/
#if __CUDA_ARCH__ >= 500
// Maxwell
#define BATCH_SIZE 384
#elif __CUDA_ARCH__ >= 600
// Pascal
#define BATCH_SIZE 384
#elif __CUDA_ARCH__ >= 700
// Volta
#define BATCH_SIZE 128
#else
#error unknown architecture
#endif
#define MAX_NR_CHANNELS 8
#define NR_POLARIZATIONS 4


/*
    Macros
*/
#define ALIGN(N,A) (((N)+(A)-1)/(A)*(A))


/*
    Structures
*/
typedef struct { float u, v, w; } UVW;
typedef struct { int x, y, z; } Coordinate;
typedef struct { int station1, station2; } Baseline;
typedef struct { int baseline_offset; int time_offset; int nr_timesteps;
                 int aterm_index;
                 Baseline baseline; Coordinate coordinate; } Metadata;


/*
   Index methods
*/
inline __device__ long index_subgrid(
    int subgrid_size,
    int s,
    int pol,
    int y,
    int x)
{
    // subgrid: [nr_subgrids][NR_POLARIZATIONS][subgrid_size][subgrid_size]
   return s * NR_POLARIZATIONS * subgrid_size * subgrid_size +
          pol * subgrid_size * subgrid_size +
          y * subgrid_size +
          x;
}

inline __device__ int index_visibility(
    int nr_channels,
    int time,
    int chan,
    int pol)
{
    // visibilities: [nr_time][nr_channels][nr_polarizations]
    return time * nr_channels * NR_POLARIZATIONS +
           chan * NR_POLARIZATIONS +
           pol;
}

inline __device__ int index_aterm(
    int subgrid_size,
    int nr_stations,
    int aterm_index,
    int station,
    int y,
    int x)
{
    // aterm: [nr_aterms][subgrid_size][subgrid_size][NR_POLARIZATIONS]
    int aterm_nr = (aterm_index * nr_stations + station);
    return aterm_nr * subgrid_size * subgrid_size * NR_POLARIZATIONS +
           y * subgrid_size * NR_POLARIZATIONS +
           x * NR_POLARIZATIONS;
}


/*
    Helper methods
 */
inline __device__ void read_aterm(
    int subgrid_size,
    int nr_stations,
    int aterm_index,
    int station,
    int y,
    int x,
    const float2 *aterms_ptr,
    float2 *aXX,
    float2 *aXY,
    float2 *aYX,
    float2 *aYY)
{
    int station_idx = index_aterm(subgrid_size, nr_stations, aterm_index, station, y, x);
    float4 *aterm_ptr = (float4 *) &aterms_ptr[station_idx];
    float4 atermA = aterm_ptr[0];
    float4 atermB = aterm_ptr[1];
    *aXX = make_float2(atermA.x, atermA.y);
    *aXY = make_float2(atermA.z, atermA.w);
    *aYX = make_float2(atermB.x, atermB.y);
    *aYY = make_float2(atermB.z, atermB.w);
}


/*
    Math methods
*/
inline __device__ void operator+=(float2 &a, float2 b)
{
    a.x += b.x;
    a.y += b.y;
}

inline __device__ float2 operator*(float2 a, float b)
{
    return make_float2(a.x * b, a.y * b);
}

inline __device__ float2 operator*(float2 a, float2 b) {
    return make_float2(a.x * b.x - a.y * b.y,
                       a.x * b.y + a.y * b.x);
}

inline __device__ float2 conj(float2 a)
{
    return cuConjf(a);
}

inline float __device__ compute_l(
    int x,
    int subgrid_size,
    float image_size)
{
    return (x+0.5-(subgrid_size/2)) * image_size/subgrid_size;
}

inline float __device__ compute_m(
    int y,
    int subgrid_size,
    float image_size)
{
    return compute_l(y, subgrid_size, image_size);
}

inline float __device__ compute_n(
    float l,
    float m)
{
    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
    // accurately for small values of l and m
    const float tmp = (l * l) + (m * m);
    return tmp > 1.0 ? 1.0 : tmp / (1.0f + sqrtf(1.0f - tmp));
}

inline __device__ float raw_sin(float a)
{
    float r;
    asm ("sin.approx.ftz.f32 %0,%1;" : "=f"(r) : "f"(a));
    return r;
}

inline __device__ float raw_cos(float a)
{
    float r;
    asm ("cos.approx.ftz.f32 %0,%1;" : "=f"(r) : "f"(a));
    return r;
}

inline __device__ void apply_aterm(
    const float2 aXX1, const float2 aXY1,
    const float2 aYX1, const float2 aYY1,
    const float2 aXX2, const float2 aXY2,
    const float2 aYX2, const float2 aYY2,
    float2 pixels[NR_POLARIZATIONS])
{
    float2 pixelsXX = pixels[0];
    float2 pixelsXY = pixels[1];
    float2 pixelsYX = pixels[2];
    float2 pixelsYY = pixels[3];

    // Apply aterm to subgrid: P = A1 * P
    // [ pixels[0], pixels[1];  = [ aXX1, aXY1;  [ pixelsXX, pixelsXY;
    //   pixels[2], pixels[3] ]     aYX1, aYY1 ]   pixelsYX], pixelsYY ] *
    pixels[0]  = (pixelsXX * aXX1);
    pixels[0] += (pixelsYX * aXY1);
    pixels[1]  = (pixelsXY * aXX1);
    pixels[1] += (pixelsYY * aXY1);
    pixels[2]  = (pixelsXX * aYX1);
    pixels[2] += (pixelsYX * aYY1);
    pixels[3]  = (pixelsXY * aYX1);
    pixels[3] += (pixelsYY * aYY1);

    pixelsXX = pixels[0];
    pixelsXY = pixels[1];
    pixelsYX = pixels[2];
    pixelsYY = pixels[3];

    // Apply aterm to subgrid: P = P * A2^H
    //    [ pixels[0], pixels[1];  =   [ pixelsXX, pixelsXY;  *  [ conj(aXX2), conj(aYX2);
    //      pixels[2], pixels[3] ]       pixelsYX, pixelsYY ]      conj(aXY2), conj(aYY2) ]
    pixels[0]  = (pixelsXX * conj(aXX2));
    pixels[0] += (pixelsXY * conj(aXY2));
    pixels[1]  = (pixelsXX * conj(aYX2));
    pixels[1] += (pixelsXY * conj(aYY2));
    pixels[2]  = (pixelsYX * conj(aXX2));
    pixels[2] += (pixelsYY * conj(aXY2));
    pixels[3]  = (pixelsYX * conj(aYX2));
    pixels[3] += (pixelsYY * conj(aYY2));
}

inline __device__ void apply_aterm(
    const float2 aXX1, const float2 aXY1, const float2 aYX1, const float2 aYY1,
    const float2 aXX2, const float2 aXY2, const float2 aYX2, const float2 aYY2,
          float2 &uvXX,      float2 &uvXY,      float2 &uvYX,      float2 &uvYY)
{
    float2 uv[NR_POLARIZATIONS] = {uvXX, uvXY, uvYX, uvYY};

    apply_aterm(
        aXX1, aXY1, aYX1, aYY1,
        aXX2, aXY2, aYX2, aYY2,
        uv);

    uvXX = uv[0];
    uvXY = uv[1];
    uvYX = uv[2];
    uvYY = uv[3];
}

/*
    Shared memory
*/
__shared__ float2 visibilities_[NR_POLARIZATIONS][BATCH_SIZE];
__shared__ float4 uvw_[BATCH_SIZE];
__shared__ float wavenumbers_[MAX_NR_CHANNELS];


/*
    Kernel gridder
*/
template<int current_nr_channels>
__device__ void
    kernel_gridder_(
    const int                           grid_size,
    const int                           subgrid_size,
    const float                         image_size,
    const float                         w_step,
    const int                           nr_channels,
    const int                           channel_offset,
    const int                           nr_stations,
    const UVW*             __restrict__ uvw,
    const float*           __restrict__ wavenumbers,
    const float2*          __restrict__ visibilities,
    const float*           __restrict__ spheroidal,
    const float2*          __restrict__ aterm,
    const Metadata*        __restrict__ metadata,
          float2*          __restrict__ subgrid)
{
    int tidx = threadIdx.x;
    int tidy = threadIdx.y;
    int tid = tidx + tidy * blockDim.x;
    int nr_threads = blockDim.x * blockDim.y;
    int s = blockIdx.x;

	// Set subgrid to zero
	if (channel_offset == 0) {
		for (int i = tid; i < subgrid_size * subgrid_size; i += nr_threads) {
			int idx_xx = index_subgrid(subgrid_size, s, 0, 0, i);
			int idx_xy = index_subgrid(subgrid_size, s, 1, 0, i);
			int idx_yx = index_subgrid(subgrid_size, s, 2, 0, i);
			int idx_yy = index_subgrid(subgrid_size, s, 3, 0, i);
            subgrid[idx_xx] = make_float2(0, 0);
            subgrid[idx_xy] = make_float2(0, 0);
            subgrid[idx_yx] = make_float2(0, 0);
            subgrid[idx_yy] = make_float2(0, 0);
		}
	}

    __syncthreads();

    // Load metadata for first subgrid
    const Metadata &m_0 = metadata[0];

	// Load metadata for current subgrid
    const Metadata &m = metadata[s];
    const int time_offset_global = (m.baseline_offset - m_0.baseline_offset) + m.time_offset;
    const int nr_timesteps = m.nr_timesteps;
    const int aterm_index = m.aterm_index;
    const int station1 = m.baseline.station1;
    const int station2 = m.baseline.station2;
    const int x_coordinate = m.coordinate.x;
    const int y_coordinate = m.coordinate.y;

    // Compute u,v,w offset in wavelenghts
    const float u_offset = (x_coordinate + subgrid_size/2 - grid_size/2) / image_size * 2 * M_PI;
    const float v_offset = (y_coordinate + subgrid_size/2 - grid_size/2) / image_size * 2 * M_PI;
    const float w_offset = w_step * ((float) m.coordinate.z + 0.5) * 2 * M_PI;

	// Load wavenumbers
	for (int chan = tid; chan < current_nr_channels; chan += nr_threads) {
		wavenumbers_[chan] = wavenumbers[channel_offset + chan];
	}

    // Iterate all pixels in subgrid
    for (int i = tid; i < ALIGN(subgrid_size * subgrid_size, nr_threads); i += nr_threads) {
        // Private pixels
        float2 uvXX = make_float2(0, 0);
        float2 uvXY = make_float2(0, 0);
        float2 uvYX = make_float2(0, 0);
        float2 uvYY = make_float2(0, 0);

        // Compute l,m,n, phase_offset
        int y = i / subgrid_size;
        int x = i % subgrid_size;
        float l = compute_l(x, subgrid_size, image_size);
        float m = compute_m(y, subgrid_size, image_size);
        float n = compute_n(l, m);
        float phase_offset = u_offset*l + v_offset*m + w_offset*n;

        // Iterate timesteps
        int current_nr_timesteps = BATCH_SIZE / MAX_NR_CHANNELS;
        for (int time_offset_local = 0; time_offset_local < nr_timesteps; time_offset_local += current_nr_timesteps) {
            current_nr_timesteps = nr_timesteps - time_offset_local < current_nr_timesteps ?
                                   nr_timesteps - time_offset_local : current_nr_timesteps;

            __syncthreads();

            // Load UVW
            for (int time = tid; time < current_nr_timesteps; time += nr_threads) {
                UVW a = uvw[time_offset_global + time_offset_local + time];
                uvw_[time] = make_float4(a.u, a.v, a.w, 0);
            }

            // Load visibilities
            for (int i = tid; i < current_nr_timesteps*current_nr_channels; i += nr_threads) {
                int idx_time = time_offset_global + time_offset_local + (i / current_nr_channels);
                int idx_chan = channel_offset + (i % current_nr_channels);
                int idx_vis_xx = index_visibility(nr_channels, idx_time, idx_chan, 0);
                int idx_vis_xy = index_visibility(nr_channels, idx_time, idx_chan, 1);
                int idx_vis_yx = index_visibility(nr_channels, idx_time, idx_chan, 2);
                int idx_vis_yy = index_visibility(nr_channels, idx_time, idx_chan, 3);
                visibilities_[0][i] = visibilities[idx_vis_xx];
                visibilities_[1][i] = visibilities[idx_vis_xy];
                visibilities_[2][i] = visibilities[idx_vis_yx];
                visibilities_[3][i] = visibilities[idx_vis_yy];
            }

            __syncthreads();

            // Iterate current batch of timesteps
            for (int time = 0; time < current_nr_timesteps; time++) {
                // Load UVW coordinates
                float u = uvw_[time].x;
                float v = uvw_[time].y;
                float w = uvw_[time].z;

                // Compute phase index and phase offset
                float phase_index = u*l + v*m + w*n;

                #pragma unroll
                for (int chan = 0; chan < current_nr_channels; chan++) {
                    float wavenumber = wavenumbers_[chan];

                    // Load visibilities from shared memory
                    float2 visXX = visibilities_[0][time*current_nr_channels+chan];
                    float2 visXY = visibilities_[1][time*current_nr_channels+chan];
                    float2 visYX = visibilities_[2][time*current_nr_channels+chan];
                    float2 visYY = visibilities_[3][time*current_nr_channels+chan];

                    // Compute phasor
                    float phase = phase_offset - (phase_index * wavenumber);
                    float2 phasor = make_float2(raw_cos(phase), raw_sin(phase));

                    // Multiply visibility by phasor
                    uvXX.x += phasor.x * visXX.x;
                    uvXX.y += phasor.x * visXX.y;
                    uvXX.x -= phasor.y * visXX.y;
                    uvXX.y += phasor.y * visXX.x;

                    uvXY.x += phasor.x * visXY.x;
                    uvXY.y += phasor.x * visXY.y;
                    uvXY.x -= phasor.y * visXY.y;
                    uvXY.y += phasor.y * visXY.x;

                    uvYX.x += phasor.x * visYX.x;
                    uvYX.y += phasor.x * visYX.y;
                    uvYX.x -= phasor.y * visYX.y;
                    uvYX.y += phasor.y * visYX.x;

                    uvYY.x += phasor.x * visYY.x;
                    uvYY.y += phasor.x * visYY.y;
                    uvYY.x -= phasor.y * visYY.y;
                    uvYY.y += phasor.y * visYY.x;
                } // end for chan
            } // end for time
        } // end for time_offset_local

        if (i < subgrid_size * subgrid_size) {
            int y = i / subgrid_size;
            int x = i % subgrid_size;

            // Compute shifted position in subgrid
            int x_dst = (x + (subgrid_size/2)) % subgrid_size;
            int y_dst = (y + (subgrid_size/2)) % subgrid_size;

            // Load aterm for station1
            float2 aXX1, aXY1, aYX1, aYY1;
            read_aterm(subgrid_size, nr_stations, aterm_index, station1, y, x, aterm, &aXX1, &aXY1, &aYX1, &aYY1);

            // Load aterm for station2
            float2 aXX2, aXY2, aYX2, aYY2;
            read_aterm(subgrid_size, nr_stations, aterm_index, station2, y, x, aterm, &aXX2, &aXY2, &aYX2, &aYY2);

            // Apply the conjugate transpose of the A-term
            apply_aterm(
                conj(aXX1), conj(aYX1), conj(aXY1), conj(aYY1),
                conj(aXX2), conj(aYX2), conj(aXY2), conj(aYY2),
                uvXX, uvXY, uvYX, uvYY);

            // Load spheroidal
            float spheroidal_ = spheroidal[i];

            // Set subgrid value
            int idx_xx = index_subgrid(subgrid_size, s, 0, y_dst, x_dst);
            int idx_xy = index_subgrid(subgrid_size, s, 1, y_dst, x_dst);
            int idx_yx = index_subgrid(subgrid_size, s, 2, y_dst, x_dst);
            int idx_yy = index_subgrid(subgrid_size, s, 3, y_dst, x_dst);
            subgrid[idx_xx] += uvXX * spheroidal_;
            subgrid[idx_xy] += uvXY * spheroidal_;
            subgrid[idx_yx] += uvYX * spheroidal_;
            subgrid[idx_yy] += uvYY * spheroidal_;
        }
    } // end for i (pixels)
} // end kernel_gridder_

#define KERNEL_GRIDDER_TEMPLATE(current_nr_channels) \
    for (; (channel_offset + current_nr_channels) <= nr_channels; channel_offset += current_nr_channels) { \
        kernel_gridder_<current_nr_channels>( \
            grid_size, subgrid_size, image_size, w_step, nr_channels, channel_offset, nr_stations, \
            uvw, wavenumbers, visibilities, spheroidal, aterm, metadata, subgrid); \
    }

extern "C" {

__global__ void
    kernel_gridder_04(
    const int                           grid_size,
    const int                           subgrid_size,
    const float                         image_size,
    const float                         w_step,
    const int                           nr_channels,
    const int                           nr_stations,
    const UVW*             __restrict__ uvw,
    const float*           __restrict__ wavenumbers,
    const float2*          __restrict__ visibilities,
    const float*           __restrict__ spheroidal,
    const float2*          __restrict__ aterm,
    const Metadata*        __restrict__ metadata,
          float2*          __restrict__ subgrid)
{
	int channel_offset = 0;
	KERNEL_GRIDDER_TEMPLATE(8);
	KERNEL_GRIDDER_TEMPLATE(7);
	KERNEL_GRIDDER_TEMPLATE(6);
	KERNEL_GRIDDER_TEMPLATE(5);
	KERNEL_GRIDDER_TEMPLATE(4);
	KERNEL_GRIDDER_TEMPLATE(3);
	KERNEL_GRIDDER_TEMPLATE(2);
	KERNEL_GRIDDER_TEMPLATE(1);
}

} // end extern "C"
