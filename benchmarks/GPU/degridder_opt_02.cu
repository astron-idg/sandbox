#include <cuComplex.h>

/*
    Parameters
*/
#define BLOCK_SIZE 128
#if __CUDA_ARCH__ >= 500
// Maxwell
#define BATCH_SIZE 256
#elif __CUDA_ARCH__ >= 600
// Pascal
#define BATCH_SIZE 512
#elif __CUDA_ARCH__ >= 700
// Volta
#define BATCH_SIZE 256
#else
#error unknown architecture
#endif
#define MAX_NR_CHANNELS 8
#define NR_POLARIZATIONS 4

/*
    Macros
*/
#define ALIGN(N,A) (((N)+(A)-1)/(A)*(A))


/*
    Structures
*/
typedef struct { float u, v, w; } UVW;
typedef struct { int x, y, z; } Coordinate;
typedef struct { int station1, station2; } Baseline;
typedef struct { int baseline_offset; int time_offset; int nr_timesteps;
                 int aterm_index;
                 Baseline baseline; Coordinate coordinate; } Metadata;


/*
   Index methods
*/
inline __device__ long index_subgrid(
    int subgrid_size,
    int s,
    int pol,
    int y,
    int x)
{
    // subgrid: [nr_subgrids][NR_POLARIZATIONS][subgrid_size][subgrid_size]
   return s * NR_POLARIZATIONS * subgrid_size * subgrid_size +
          pol * subgrid_size * subgrid_size +
          y * subgrid_size +
          x;
}

inline __device__ int index_visibility(
    int nr_channels,
    int time,
    int chan,
    int pol)
{
    // visibilities: [nr_time][nr_channels][nr_polarizations]
    return time * nr_channels * NR_POLARIZATIONS +
           chan * NR_POLARIZATIONS +
           pol;
}

inline __device__ int index_aterm(
    int subgrid_size,
    int nr_stations,
    int aterm_index,
    int station,
    int y,
    int x)
{
    // aterm: [nr_aterms][subgrid_size][subgrid_size][NR_POLARIZATIONS]
    int aterm_nr = (aterm_index * nr_stations + station);
    return aterm_nr * subgrid_size * subgrid_size * NR_POLARIZATIONS +
           y * subgrid_size * NR_POLARIZATIONS +
           x * NR_POLARIZATIONS;
}


/*
    Helper methods
 */
inline __device__ void read_aterm(
    int subgrid_size,
    int nr_stations,
    int aterm_index,
    int station,
    int y,
    int x,
    const float2 *aterms_ptr,
    float2 *aXX,
    float2 *aXY,
    float2 *aYX,
    float2 *aYY)
{
    int station_idx = index_aterm(subgrid_size, nr_stations, aterm_index, station, y, x);
    float4 *aterm_ptr = (float4 *) &aterms_ptr[station_idx];
    float4 atermA = aterm_ptr[0];
    float4 atermB = aterm_ptr[1];
    *aXX = make_float2(atermA.x, atermA.y);
    *aXY = make_float2(atermA.z, atermA.w);
    *aYX = make_float2(atermB.x, atermB.y);
    *aYY = make_float2(atermB.z, atermB.w);
}


/*
    Math methods
*/
inline __device__ void operator+=(float2 &a, float2 b) {
    a.x += b.x;
    a.y += b.y;
}

inline __device__ float4 operator*(float4 a, float b) {
    return make_float4(a.x * b, a.y * b, a.z * b, a.w * b);
}

inline __device__ float2 operator*(float2 a, float b)
{
    return make_float2(a.x * b, a.y * b);
}

inline __device__ float2 operator*(float2 a, float2 b) {
    return make_float2(a.x * b.x - a.y * b.y,
                       a.x * b.y + a.y * b.x);
}

inline __device__ float2 conj(float2 a)
{
    return cuConjf(a);
}

inline float __device__ compute_l(
    int x,
    int subgrid_size,
    float image_size)
{
    return (x+0.5-(subgrid_size/2)) * image_size/subgrid_size;
}

inline float __device__ compute_m(
    int y,
    int subgrid_size,
    float image_size)
{
    return compute_l(y, subgrid_size, image_size);
}

inline float __device__ compute_n(
    float l,
    float m)
{
    // evaluate n = 1.0f - sqrt(1.0 - (l * l) - (m * m));
    // accurately for small values of l and m
    const float tmp = (l * l) + (m * m);
    return tmp > 1.0 ? 1.0 : tmp / (1.0f + sqrtf(1.0f - tmp));
}

inline __device__ float raw_sin(float a)
{
    float r;
    asm ("sin.approx.ftz.f32 %0,%1;" : "=f"(r) : "f"(a));
    return r;
}

inline __device__ float raw_cos(float a)
{
    float r;
    asm ("cos.approx.ftz.f32 %0,%1;" : "=f"(r) : "f"(a));
    return r;
}

inline __device__ void apply_aterm(
    const float2 aXX1, const float2 aXY1,
    const float2 aYX1, const float2 aYY1,
    const float2 aXX2, const float2 aXY2,
    const float2 aYX2, const float2 aYY2,
    float2 pixels[NR_POLARIZATIONS])
{
    float2 pixelsXX = pixels[0];
    float2 pixelsXY = pixels[1];
    float2 pixelsYX = pixels[2];
    float2 pixelsYY = pixels[3];

    // Apply aterm to subgrid: P = A1 * P
    // [ pixels[0], pixels[1];  = [ aXX1, aXY1;  [ pixelsXX, pixelsXY;
    //   pixels[2], pixels[3] ]     aYX1, aYY1 ]   pixelsYX], pixelsYY ] *
    pixels[0]  = (pixelsXX * aXX1);
    pixels[0] += (pixelsYX * aXY1);
    pixels[1]  = (pixelsXY * aXX1);
    pixels[1] += (pixelsYY * aXY1);
    pixels[2]  = (pixelsXX * aYX1);
    pixels[2] += (pixelsYX * aYY1);
    pixels[3]  = (pixelsXY * aYX1);
    pixels[3] += (pixelsYY * aYY1);

    pixelsXX = pixels[0];
    pixelsXY = pixels[1];
    pixelsYX = pixels[2];
    pixelsYY = pixels[3];

    // Apply aterm to subgrid: P = P * A2^H
    //    [ pixels[0], pixels[1];  =   [ pixelsXX, pixelsXY;  *  [ conj(aXX2), conj(aYX2);
    //      pixels[2], pixels[3] ]       pixelsYX, pixelsYY ]      conj(aXY2), conj(aYY2) ]
    pixels[0]  = (pixelsXX * conj(aXX2));
    pixels[0] += (pixelsXY * conj(aXY2));
    pixels[1]  = (pixelsXX * conj(aYX2));
    pixels[1] += (pixelsXY * conj(aYY2));
    pixels[2]  = (pixelsYX * conj(aXX2));
    pixels[2] += (pixelsYY * conj(aXY2));
    pixels[3]  = (pixelsYX * conj(aYX2));
    pixels[3] += (pixelsYY * conj(aYY2));
}

inline __device__ void apply_aterm(
    const float2 aXX1, const float2 aXY1, const float2 aYX1, const float2 aYY1,
    const float2 aXX2, const float2 aXY2, const float2 aYX2, const float2 aYY2,
          float2 &uvXX,      float2 &uvXY,      float2 &uvYX,      float2 &uvYY)
{
    float2 uv[NR_POLARIZATIONS] = {uvXX, uvXY, uvYX, uvYY};

    apply_aterm(
        aXX1, aXY1, aYX1, aYY1,
        aXX2, aXY2, aYX2, aYY2,
        uv);

    uvXX = uv[0];
    uvXY = uv[1];
    uvYX = uv[2];
    uvYY = uv[3];
}


/*
    Shared memory
*/
__shared__ float4 shared[3][BATCH_SIZE];


/*
    Kernels
*/
template<int current_nr_channels>
__device__ void kernel_degridder_(
    const int                         grid_size,
    const int                         subgrid_size,
    const float                       image_size,
    const float                       w_step,
    const int                         nr_channels,
    const int                         channel_offset,
    const int                         nr_stations,
    const UVW*           __restrict__ uvw,
    const float*         __restrict__ wavenumbers,
          float2*        __restrict__ visibilities,
    const float*         __restrict__ spheroidal,
    const float2*        __restrict__ aterm,
    const Metadata*      __restrict__ metadata,
          float2*        __restrict__ subgrid)
{
    int s          = blockIdx.x;
    int tidx       = threadIdx.x;
    int tidy       = threadIdx.y;
    int tid        = tidx + tidy * blockDim.x;
    int nr_threads = blockDim.x * blockDim.y;

    // Load metadata for first subgrid
    const Metadata &m_0 = metadata[0];

    // Load metadata for current subgrid
    const Metadata &m = metadata[s];
    const int time_offset_global = (m.baseline_offset - m_0.baseline_offset) + m.time_offset;
    const int nr_timesteps = m.nr_timesteps;
    const int aterm_index = m.aterm_index;
    const int station1 = m.baseline.station1;
    const int station2 = m.baseline.station2;
    const int x_coordinate = m.coordinate.x;
    const int y_coordinate = m.coordinate.y;

    // Compute u,v,w offset in wavelenghts
    const float u_offset = (x_coordinate + subgrid_size/2 - grid_size/2) / image_size * 2 * M_PI;
    const float v_offset = (y_coordinate + subgrid_size/2 - grid_size/2) / image_size * 2 * M_PI;
    const float w_offset = w_step * ((float) m.coordinate.z + 0.5) * 2 * M_PI;

    if (channel_offset == 0) {
        for (unsigned pixel = tid; pixel < subgrid_size * subgrid_size; pixel += nr_threads) {
            unsigned y = pixel / subgrid_size;
            unsigned x = pixel % subgrid_size;

            // Compute shifted position in subgrid
            int x_src = (x + (subgrid_size/2)) % subgrid_size;
            int y_src = (y + (subgrid_size/2)) % subgrid_size;

            // Load spheroidal
            float spheroidal_ = spheroidal[pixel];

            // Load pixels
            int idx_xx = index_subgrid(subgrid_size, s, 0, y_src, x_src);
            int idx_xy = index_subgrid(subgrid_size, s, 1, y_src, x_src);
            int idx_yx = index_subgrid(subgrid_size, s, 2, y_src, x_src);
            int idx_yy = index_subgrid(subgrid_size, s, 3, y_src, x_src);
            float2 pixelXX = subgrid[idx_xx] * spheroidal_;
            float2 pixelXY = subgrid[idx_xy] * spheroidal_;
            float2 pixelYX = subgrid[idx_yx] * spheroidal_;
            float2 pixelYY = subgrid[idx_yy] * spheroidal_;

            // Load aterm for station1
            float2 aXX1, aXY1, aYX1, aYY1;
            read_aterm(subgrid_size, nr_stations, aterm_index, station1, y, x, aterm, &aXX1, &aXY1, &aYX1, &aYY1);

            // Load aterm for station2
            float2 aXX2, aXY2, aYX2, aYY2;
            read_aterm(subgrid_size, nr_stations, aterm_index, station2, y, x, aterm, &aXX2, &aXY2, &aYX2, &aYY2);

            // Apply the conjugate transpose of the A-term
            apply_aterm(
                aXX1, aYX1, aXY1, aYY1,
                aXX2, aYX2, aXY2, aYY2,
                pixelXX, pixelXY, pixelYX, pixelYY);

            // Apply spheroidal and store pixels
            subgrid[idx_xx] = pixelXX;
            subgrid[idx_xy] = pixelXY;
            subgrid[idx_yx] = pixelYX;
            subgrid[idx_yy] = pixelYY;
        }
    }

    // Iterate visibilities
    for (int time = tid; time < ALIGN(nr_timesteps, nr_threads); time += nr_threads) {
        float2 visXX[MAX_NR_CHANNELS];
        float2 visXY[MAX_NR_CHANNELS];
        float2 visYX[MAX_NR_CHANNELS];
        float2 visYY[MAX_NR_CHANNELS];

        for (int chan = 0; chan < current_nr_channels; chan++) {
            visXX[chan] = make_float2(0, 0);
            visXY[chan] = make_float2(0, 0);
            visYX[chan] = make_float2(0, 0);
            visYY[chan] = make_float2(0, 0);
        }

        float u, v, w;

        if (time < nr_timesteps) {
            u = uvw[time_offset_global + time].u;
            v = uvw[time_offset_global + time].v;
            w = uvw[time_offset_global + time].w;
        }

        __syncthreads();

        // Iterate pixels
        const int nr_pixels = subgrid_size * subgrid_size;
        int current_nr_pixels = BATCH_SIZE;
        for (int pixel_offset = 0; pixel_offset < nr_pixels; pixel_offset += current_nr_pixels) {
            current_nr_pixels = nr_pixels - pixel_offset < min(nr_threads, BATCH_SIZE) ?
                                nr_pixels - pixel_offset : min(nr_threads, BATCH_SIZE);

            __syncthreads();

            // Prepare data
            for (int j = tid; j < current_nr_pixels; j += nr_threads) {
                int y = (pixel_offset + j) / subgrid_size;
                int x = (pixel_offset + j) % subgrid_size;

                // Compute shifted position in subgrid
                int x_src = (x + (subgrid_size/2)) % subgrid_size;
                int y_src = (y + (subgrid_size/2)) % subgrid_size;

                // Load pixels
                int idx_xx = index_subgrid(subgrid_size, s, 0, y_src, x_src);
                int idx_xy = index_subgrid(subgrid_size, s, 1, y_src, x_src);
                int idx_yx = index_subgrid(subgrid_size, s, 2, y_src, x_src);
                int idx_yy = index_subgrid(subgrid_size, s, 3, y_src, x_src);
                float2 pixelsXX = subgrid[idx_xx];
                float2 pixelsXY = subgrid[idx_xy];
                float2 pixelsYX = subgrid[idx_yx];
                float2 pixelsYY = subgrid[idx_yy];

                // Compute l,m,n and phase offset
                const float l = compute_l(x, subgrid_size, image_size);
                const float m = compute_m(y, subgrid_size, image_size);
                const float n = compute_n(l, m);
                float phase_offset = u_offset*l + v_offset*m + w_offset*n;

                // Store values in shared memory
                shared[0][j] = make_float4(pixelsXX.x, pixelsXX.y, pixelsXY.x, pixelsXY.y);
                shared[1][j] = make_float4(pixelsYX.x, pixelsYX.y, pixelsYY.x, pixelsYY.y);
                shared[2][j] = make_float4(l, m, n, phase_offset);
            } // end for j (pixels)

             __syncthreads();

            // Iterate current batch of pixels
            for (int k = 0; k < current_nr_pixels; k++) {
                // Load pixels from shared memory
                float2 apXX = make_float2(shared[0][k].x, shared[0][k].y);
                float2 apXY = make_float2(shared[0][k].z, shared[0][k].w);
                float2 apYX = make_float2(shared[1][k].x, shared[1][k].y);
                float2 apYY = make_float2(shared[1][k].z, shared[1][k].w);

                // Load l,m,n
                float l = shared[2][k].x;
                float m = shared[2][k].y;
                float n = shared[2][k].z;

                // Load phase offset
                float phase_offset = shared[2][k].w;

                // Compute phase index
                float phase_index = u * l + v * m + w * n;

                for (int chan = 0; chan < current_nr_channels; chan++) {
                    // Load wavenumber
                    float wavenumber = wavenumbers[channel_offset + chan];

                    // Compute phasor
                    float  phase  = (phase_index * wavenumber) - phase_offset;
                    float2 phasor = make_float2(raw_cos(phase), raw_sin(phase));

                    // Multiply pixels by phasor
                    visXX[chan].x += phasor.x * apXX.x;
                    visXX[chan].y += phasor.x * apXX.y;
                    visXX[chan].x -= phasor.y * apXX.y;
                    visXX[chan].y += phasor.y * apXX.x;

                    visXY[chan].x += phasor.x * apXY.x;
                    visXY[chan].y += phasor.x * apXY.y;
                    visXY[chan].x -= phasor.y * apXY.y;
                    visXY[chan].y += phasor.y * apXY.x;

                    visYX[chan].x += phasor.x * apYX.x;
                    visYX[chan].y += phasor.x * apYX.y;
                    visYX[chan].x -= phasor.y * apYX.y;
                    visYX[chan].y += phasor.y * apYX.x;

                    visYY[chan].x += phasor.x * apYY.x;
                    visYY[chan].y += phasor.x * apYY.y;
                    visYY[chan].x -= phasor.y * apYY.y;
                    visYY[chan].y += phasor.y * apYY.x;
                } // end for chan
            } // end for k (batch)
        } // end for j (pixels)

        for (int chan = 0; chan < current_nr_channels; chan++) {
            if (time < nr_timesteps) {
                // Store visibility
                const float scale = 1.0f / (nr_pixels);
                int idx_time = time_offset_global + time;
                int idx_chan = channel_offset + chan;
                int idx_vis = index_visibility(nr_channels, idx_time, idx_chan, 0);
                float4 visA = make_float4(visXX[chan].x, visXX[chan].y, visXY[chan].x, visXY[chan].y);
                float4 visB = make_float4(visYX[chan].x, visYX[chan].y, visYY[chan].x, visYY[chan].y);
                float4 *vis_ptr = (float4 *) &visibilities[idx_vis];
                vis_ptr[0] = visA * scale;
                vis_ptr[1] = visB * scale;
            }
        } // end for chan
    } // end for time
} // end kernel_degridder_

#define KERNEL_DEGRIDDER_TEMPLATE(current_nr_channels) \
    for (; (channel_offset + current_nr_channels) <= nr_channels; channel_offset += current_nr_channels) { \
        kernel_degridder_<current_nr_channels>( \
            grid_size, subgrid_size, image_size, w_step, nr_channels, channel_offset, nr_stations, \
            uvw, wavenumbers, visibilities, spheroidal, aterm, metadata, subgrid); \
    }

extern "C" {
__global__ void
__launch_bounds__(BLOCK_SIZE, 4)
    kernel_degridder(
    const int                         grid_size,
    const int                         subgrid_size,
    const float                       image_size,
    const float                       w_step,
    const int                         nr_channels,
    const int                         nr_stations,
    const UVW*           __restrict__ uvw,
    const float*         __restrict__ wavenumbers,
          float2*        __restrict__ visibilities,
    const float*         __restrict__ spheroidal,
    const float2*        __restrict__ aterm,
    const Metadata*      __restrict__ metadata,
          float2*        __restrict__ subgrid)
{
	int channel_offset = 0;
	KERNEL_DEGRIDDER_TEMPLATE(8);
	KERNEL_DEGRIDDER_TEMPLATE(4);
	KERNEL_DEGRIDDER_TEMPLATE(2);
	KERNEL_DEGRIDDER_TEMPLATE(1);
}
} // end extern "C"
