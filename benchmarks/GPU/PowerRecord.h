#include <cuda.h>

#include "PowerSensor.h"
#include "CU.h"

class PowerRecord {
    public:
        PowerRecord(powersensor::PowerSensor *sensor);

        void enqueue(cu::Stream &stream);
        static void getPower(CUstream, CUresult, void *userData);
        powersensor::PowerSensor *sensor;
        powersensor::State state;
        cu::Event event;
};
