#include <iomanip>
#include <complex>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cudaProfiler.h>

#include "PowerRecord.h"

// Number of times to run each kernel
#define NR_ITERATIONS 5

// Number of times to run each benchmark
#define NR_BENCHMARKS 10


// Power and runtime measurement
powersensor::PowerSensor *powerSensor;

void report(
    std::string name,
    double seconds,
    double joules,
    float gflops = 0,
    float gbytes = 0,
    unsigned long mvis = 0)
{
    int w1 = 20;
    int w2 = 7;
    std::cout << std::setw(w1) << std::string(name) << ": ";
    std::cout << std::setprecision(2) << std::fixed;
    std::cout << std::setw(w2) << seconds * 1e3 << " ms";
    if (gflops != 0) {
        std::cout << ", " << std::setw(w2) << gflops / seconds << " GFlops/s";
    }
    if (gbytes != 0) {
        std::cout << ", " << std::setw(w2) << gbytes / seconds << " GB/s";
    }
    if (gflops != 0 && gbytes != 0) {
        float arithmetic_intensity = gflops / gbytes;
        std::cout << ", " << std::setw(w2) << arithmetic_intensity << " Flop/byte";
    }

    double watt = joules / seconds;
    if (joules != 0) {
        std::cout << ", " << std::setw(w2) << watt << " Watt";
    }
    if (gflops !=0 && joules != 0) {
        double efficiency = gflops / joules;
        std::cout << ", " << std::setw(w2) << efficiency << " GFlop/W";
    }

    if (mvis != 0) {
        std::cout << ", " << std::setw(w2) << mvis / seconds << " MVis/s";
    }
    std::cout << std::endl;
}


void run_kernel(
    const char *name,
    cu::Stream &stream,
    cu::Function &function,
    dim3 gridDim,
    dim3 blockDim,
    const void **parameters,
    float gflops,
    float gbytes,
    unsigned long mvis = 0)
{
    // Initialized power records
    PowerRecord start(powerSensor);
    PowerRecord end(powerSensor);

    // Enqueue start of measurement
    start.enqueue(stream);

    /// Launch kernel
    for (int i = 0; i < NR_ITERATIONS; i++) {
        stream.launchKernel(function, gridDim, blockDim, 0, parameters);
    }

    // Enqueue end of measurement
    end.enqueue(stream);

    // Wait for execution to finish
    stream.synchronize();

    // Report
    double seconds = powerSensor->seconds(start.state, end.state);
    double joules  = powerSensor->Joules(start.state, end.state);
    seconds /= NR_ITERATIONS;
    joules  /= NR_ITERATIONS;
    report(name, seconds, joules, gflops, gbytes, mvis);
}


void run_peak_flops(
    cu::Device &device,
    cu::Stream &stream)
{
    // Parameters
    int multiProcessorCount = device.get_attribute<CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT>();
    int maxThreadsPerBlock = device.get_attribute<CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK>();

    // Amount of work performed
    double gflops = (1e-9 * multiProcessorCount * maxThreadsPerBlock) * (1ULL * 2 * 2 * 2048 * 1024 * 8);
    double gbytes = 0;

    // Load kernel
    cu::Module module("peak_flops.cubin");
    cu::Function function(module, "kernel_fma");

    // Kernel dimensions
    dim3 gridDim(multiProcessorCount);
    dim3 blockDim(maxThreadsPerBlock);

    // Setup parameters
    cu::DeviceMemory d_data(multiProcessorCount * maxThreadsPerBlock * sizeof(float));
    const void *parameters[] = { d_data };

    // Run kernel
    run_kernel("peak_flops", stream, function, gridDim, blockDim, parameters, gflops, gbytes);
}

/*
 * IDG types
 */
typedef struct { int x, y, z; } Coordinate;

typedef struct { unsigned int station1, station2; } Baseline;

typedef struct {
    int baseline_offset;
    int time_offset;
    int nr_timesteps;
    int aterm_index;
    Baseline baseline;
    Coordinate coordinate; } Metadata;

/*
    Operation and byte count
*/
uint64_t flops_gridder(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size,
    uint64_t nr_correlations)
{
    // Number of flops per visibility
    uint64_t flops_per_visibility = 0;
    flops_per_visibility += 5; // phase index
    flops_per_visibility += 5; // phase offset
    flops_per_visibility += nr_channels * 2; // phase
    flops_per_visibility += nr_channels * nr_correlations * 8; // update

    // Number of flops per subgrid
    uint64_t flops_per_subgrid = 0;
    flops_per_subgrid += 6; // shift

    // Total number of flops
    uint64_t flops_total = 0;
    flops_total += nr_timesteps * subgrid_size * subgrid_size * flops_per_visibility;
    flops_total += nr_subgrids  * subgrid_size * subgrid_size * flops_per_subgrid;
    return flops_total;
}

uint64_t flops_gridder_post(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size,
    uint64_t nr_correlations)
{
    // Number of flops per visibility
    uint64_t flops_per_visibility = 0;

    // Number of flops per subgrid
    uint64_t flops_per_subgrid = 0;
    flops_per_subgrid += nr_correlations * 30; // aterm
    flops_per_subgrid += nr_correlations * 2; // spheroidal

    // Total number of flops
    uint64_t flops_total = 0;
    flops_total += nr_timesteps * subgrid_size * subgrid_size * flops_per_visibility;
    flops_total += nr_subgrids  * subgrid_size * subgrid_size * flops_per_subgrid;
    return flops_total;
}

uint64_t bytes_gridder(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size,
    uint64_t nr_correlations)
{
    // Number of bytes per uvw coordinate
    uint64_t bytes_per_uvw = 0;
    bytes_per_uvw += 1ULL * 3 * sizeof(float); // read uvw

    // Number of bytes per visibility
    uint64_t bytes_per_vis = 0;
    bytes_per_vis += 1ULL * nr_channels * nr_correlations * 2 * sizeof(float); // read visibilities

    // Number of bytes per pixel
    uint64_t bytes_per_pix = 0;
    bytes_per_pix += 1ULL * nr_correlations * 2 * sizeof(float); // read pixel
    bytes_per_pix += 1ULL * nr_correlations * 2 * sizeof(float); // write pixel

    // Number of bytes per aterm
    uint64_t bytes_per_aterm = 0;
    //bytes_per_aterm += 1ULL * 2 * nr_correlations * 2 * sizeof(float); // read aterm

    // Number of bytes per spheroidal
    uint64_t bytes_per_spheroidal = 0;
    //bytes_per_spheroidal += 1ULL * sizeof(float); // read spheroidal

    // Total number of bytes
    uint64_t bytes_total = 0;
    bytes_total += 1ULL * nr_timesteps * bytes_per_uvw;
    bytes_total += 1ULL * nr_timesteps * bytes_per_vis;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_pix;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_aterm;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_spheroidal;
    return bytes_total;
}

uint64_t bytes_gridder_post(
    uint64_t nr_channels,
    uint64_t nr_timesteps,
    uint64_t nr_subgrids,
    uint64_t subgrid_size,
    uint64_t nr_correlations)
{
    // Number of bytes per uvw coordinate
    uint64_t bytes_per_uvw = 0;

    // Number of bytes per visibility
    uint64_t bytes_per_vis = 0;

    // Number of bytes per pixel
    uint64_t bytes_per_pix = 0;
    bytes_per_pix += 1ULL * nr_correlations * 2 * sizeof(float); // read pixel
    bytes_per_pix += 1ULL * nr_correlations * 2 * sizeof(float); // write pixel

    // Number of bytes per aterm
    uint64_t bytes_per_aterm = 0;
    bytes_per_aterm += 1ULL * 2 * nr_correlations * 2 * sizeof(float); // read aterm

    // Number of bytes per spheroidal
    uint64_t bytes_per_spheroidal = 0;
    bytes_per_spheroidal += 1ULL * sizeof(float); // read spheroidal

    // Total number of bytes
    uint64_t bytes_total = 0;
    bytes_total += 1ULL * nr_timesteps * bytes_per_uvw;
    bytes_total += 1ULL * nr_timesteps * bytes_per_vis;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_pix;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_aterm;
    bytes_total += 1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_spheroidal;
    return bytes_total;
}

void run_idg_gridder(
    cu::Device &device,
    cu::Stream &stream)
{
    // Parameters
    int multiProcessorCount = device.get_attribute<CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT>();
    int maxThreadsPerBlock = device.get_attribute<CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK>();

    // Load kernel
    cu::Module module_01("gridder_opt_01.cubin");
    cu::Function function_gridder_01(module_01, "kernel_gridder_01");
    cu::Function function_gridder_post_01(module_01, "kernel_gridder_post_01");
    cu::Module module_02("gridder_opt_02.cubin");
    cu::Function function_gridder_02(module_02, "kernel_gridder_02");
    cu::Module module_02a("gridder_opt_02a.cubin");
    cu::Function function_gridder_02a(module_02a, "kernel_gridder_02a");
    cu::Module module_03("gridder_opt_03.cubin");
    cu::Function function_gridder_03(module_03, "kernel_gridder_03");
    cu::Module module_04("gridder_opt_04.cubin");
    cu::Function function_gridder_04(module_04, "kernel_gridder_04");
    cu::Module module_05("gridder_opt_05.cubin");
    cu::Function function_gridder_05(module_05, "kernel_gridder_05");
    cu::Module module_06("gridder_opt_06.cubin");
    cu::Function function_gridder_06(module_06, "kernel_gridder_06");
    cu::Module module_07("gridder_opt_07.cubin");
    cu::Function function_gridder_07(module_07, "kernel_gridder_07");
    cu::Module module_08("gridder_opt_08.cubin");
    cu::Function function_gridder_08(module_08, "kernel_gridder_08");
    cu::Module module_09("gridder_opt_09.cubin");
    cu::Function function_gridder_09_1(module_09, "kernel_gridder_1");
    cu::Function function_gridder_09_2(module_09, "kernel_gridder_2");
    cu::Function function_gridder_09_3(module_09, "kernel_gridder_3");
    cu::Function function_gridder_09_4(module_09, "kernel_gridder_4");
    cu::Function function_gridder_09_5(module_09, "kernel_gridder_5");
    cu::Function function_gridder_09_6(module_09, "kernel_gridder_6");
    cu::Function function_gridder_09_7(module_09, "kernel_gridder_7");
    cu::Function function_gridder_09_8(module_09, "kernel_gridder_8");
    cu::Function function_gridder_09_n(module_09, "kernel_gridder");

    // Setup parameters
    const int grid_size            = 8192; // unused
    const unsigned nr_correlations = 4;
    const int subgrid_size         = 32;
    const float image_size         = 0.01;
    const float w_step             = 0;
    const int nr_channels          = 16;
    const int nr_stations          = 30;
    const int nr_timeslots         = 64;
    const int nr_timesteps         = 128; // per subgrid

    // Derived parameters
    auto nr_baselines = (nr_stations * (nr_stations - 1)) / 2;
    auto nr_subgrids  = nr_baselines * nr_timeslots;
    auto total_nr_timesteps = nr_subgrids * nr_timesteps;
    auto mvis = 1e-6 * total_nr_timesteps * nr_channels;

    // Size of data structures
    auto sizeof_uvw          = 1ULL * nr_subgrids * 3 * sizeof(float);
    auto sizeof_wavenumbers  = 1ULL * nr_channels * sizeof(float);
    auto sizeof_visibilities = 1ULL * nr_baselines * nr_timesteps * nr_channels * nr_correlations * sizeof(std::complex<float>);
    auto sizeof_spheroidal   = 1ULL * subgrid_size * subgrid_size * sizeof(float);
    auto sizeof_aterms       = 1ULL * nr_stations * nr_timeslots * nr_correlations * subgrid_size * subgrid_size * sizeof(std::complex<float>);
    auto sizeof_subgrids     = 1ULL * nr_subgrids * subgrid_size * subgrid_size * nr_correlations * sizeof(std::complex<float>);
    auto sizeof_metadata     = 1ULL * nr_subgrids * sizeof(Metadata);

    // gridder_opt_03: store uvw as float4 for better alignment
    sizeof_uvw = 1ULL * nr_subgrids * nr_correlations * 4 * sizeof(float);

    // Initialize metadata
    Metadata metadata[nr_subgrids];
    for (auto bl = 0; bl < nr_baselines; bl++) {
        for (auto ts = 0; ts < nr_timeslots; ts++ ) {
            auto idx = bl * nr_timeslots + ts;

            // Metadata settings
            int baseline_offset = bl;
            int time_offset = 0;
            int aterm_index = 0; // unused
            Baseline baseline = { 0, 0 }; // unused
            Coordinate coordinate = { 0, 0 }; // unused

            // Set metadata for current subgrid
            Metadata m = { baseline_offset, time_offset, nr_timesteps, aterm_index, baseline, coordinate };
            metadata[bl * nr_timeslots + ts] = m;
        }
    }

    // Allocate device memory
    cu::DeviceMemory d_uvw(sizeof_uvw);
    cu::DeviceMemory d_wavenumbers(sizeof_wavenumbers);
    cu::DeviceMemory d_visibilities(sizeof_visibilities);
    cu::DeviceMemory d_spheroidal(sizeof_spheroidal);
    cu::DeviceMemory d_aterms(sizeof_aterms);
    cu::DeviceMemory d_metadata(sizeof_metadata);
    cu::DeviceMemory d_subgrids(sizeof_subgrids);

    // Copy metadata to device
    stream.memcpyHtoDAsync(d_metadata, metadata, sizeof_metadata);

    // Setup parameters
    const void *parameters_gridder[] = {
        &grid_size, &subgrid_size, &image_size, &w_step, &nr_channels, &nr_stations,
        d_uvw, d_wavenumbers, d_visibilities,
        d_spheroidal, d_aterms, d_metadata, d_subgrids };

    const void *parameters_gridder_post[] = {
        &subgrid_size, &nr_stations,
        d_spheroidal, d_aterms, d_metadata, d_subgrids };

    // Kernel dimensions
    dim3 gridDim(nr_subgrids);
    dim3 blockDim(128); // this has to match value in kernel

    // Run kernel gridder_01
    auto gflops = 1e-9 * flops_gridder(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    auto gbytes = 1e-9 * bytes_gridder(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    run_kernel("idg_gridder_01", stream, function_gridder_01, gridDim, blockDim, parameters_gridder, gflops, gbytes, mvis);

    // Run kernel gridder_post_01
    auto gflops_post = 1e-9 * flops_gridder_post(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    auto gbytes_post = 1e-9 * bytes_gridder_post(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    run_kernel("idg_gridder_post_01", stream, function_gridder_post_01, gridDim, blockDim, parameters_gridder_post, gflops_post, gbytes_post);

    // Run kernel gridder_02
    auto gflops_full = gflops + gflops_post;
    auto gbytes_full = gbytes + gbytes_post;
    run_kernel("idg_gridder_02", stream, function_gridder_02, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);
    run_kernel("idg_gridder_02a", stream, function_gridder_02a, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run kernel gridder_03
    run_kernel("idg_gridder_03", stream, function_gridder_03, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run kernel gridder_04
    run_kernel("idg_gridder_04", stream, function_gridder_04, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run kernel gridder_05
    run_kernel("idg_gridder_05", stream, function_gridder_05, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run kernel gridder_06
    run_kernel("idg_gridder_06", stream, function_gridder_06, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run kernel gridder_07
    run_kernel("idg_gridder_07", stream, function_gridder_07, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run kernel gridder_08
    run_kernel("idg_gridder_08", stream, function_gridder_08, gridDim, blockDim, parameters_gridder, gflops_full, gbytes_full, mvis);

    // Run gridder 03 for different number of channels
    for (int current_nr_channels = 1; current_nr_channels <= 16; current_nr_channels++) {
        printf("nr_channels=%2d ", current_nr_channels);
        parameters_gridder[4] = &current_nr_channels;
        auto gflops = 0;
        gflops += 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
        gflops += 1e-9 * flops_gridder_post(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
        auto mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
        run_kernel("idg_gridder_03", stream, function_gridder_03, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);
    }

    // Run kernel gridder_09
    int current_nr_channels;

    // 1 channel
    current_nr_channels = 1;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_1, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 2 channels
    current_nr_channels = 2;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_2, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 3 channels
    current_nr_channels = 3;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_3, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 4 channels
    current_nr_channels = 4;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_4, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 5 channels
    current_nr_channels = 5;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_5, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 6 channels
    current_nr_channels = 6;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_6, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 7 channels
    current_nr_channels = 7;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_7, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // 8 channels
    current_nr_channels = 8;
    parameters_gridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_gridder_09", stream, function_gridder_09_8, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);

    // more channels
    for (current_nr_channels = 9; current_nr_channels <= 16; current_nr_channels++) {
        printf("nr_channels=%2d ", current_nr_channels);
        parameters_gridder[4] = &current_nr_channels;
        gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
        mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
        run_kernel("idg_gridder_09", stream, function_gridder_09_n, gridDim, blockDim, parameters_gridder, gflops, 0, mvis);
    }

}


void run_idg_degridder(
    cu::Device &device,
    cu::Stream &stream)
{
    // Parameters
    int multiProcessorCount = device.get_attribute<CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT>();
    int maxThreadsPerBlock = device.get_attribute<CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK>();

    // Load kernels
    cu::Module module_01("degridder_opt_01.cubin");
    cu::Function function_degridder(module_01, "kernel_degridder");
    cu::Function function_degridder_pre(module_01, "kernel_degridder_pre");
    cu::Module module_02("degridder_opt_02.cubin");
    cu::Function function_degridder_full(module_02, "kernel_degridder");
    cu::Module module_03("degridder_opt_03.cubin");
    cu::Function function_degridder_03_1(module_03, "kernel_degridder_1");
    cu::Function function_degridder_03_2(module_03, "kernel_degridder_2");
    cu::Function function_degridder_03_3(module_03, "kernel_degridder_3");
    cu::Function function_degridder_03_4(module_03, "kernel_degridder_4");
    cu::Function function_degridder_03_5(module_03, "kernel_degridder_5");
    cu::Function function_degridder_03_6(module_03, "kernel_degridder_6");
    cu::Function function_degridder_03_7(module_03, "kernel_degridder_7");
    cu::Function function_degridder_03_8(module_03, "kernel_degridder_8");
    cu::Function function_degridder_03_n(module_03, "kernel_degridder");

    // Setup parameters
    const int grid_size            = 8192; // unused
    const unsigned nr_correlations = 4;
    const int subgrid_size         = 32;
    const float image_size         = 0.01;
    const float w_step             = 0;
    const int nr_channels          = 16;
    const int nr_stations          = 30;
    const int nr_timeslots         = 64;
    const int nr_timesteps         = 128; // per subgrid

    // Derived parameters
    auto nr_baselines = (nr_stations * (nr_stations - 1)) / 2;
    auto nr_subgrids  = nr_baselines * nr_timeslots;
    auto total_nr_timesteps = nr_subgrids * nr_timesteps;
    auto mvis = 1e-6 * total_nr_timesteps * nr_channels;

    // Size of data structures
    auto sizeof_uvw          = 1ULL * nr_subgrids * nr_correlations * 3 * sizeof(float);
    auto sizeof_wavenumbers  = 1ULL * nr_channels * sizeof(float);
    auto sizeof_visibilities = 1ULL * nr_baselines * nr_timesteps * nr_channels * nr_correlations * sizeof(std::complex<float>);
    auto sizeof_spheroidal   = 1ULL * subgrid_size * subgrid_size * sizeof(float);
    auto sizeof_aterms       = 1ULL * nr_stations * nr_timeslots * nr_correlations * subgrid_size * subgrid_size * sizeof(std::complex<float>);
    auto sizeof_subgrids     = 1ULL * nr_subgrids * subgrid_size * subgrid_size * nr_correlations * sizeof(std::complex<float>);
    auto sizeof_metadata     = 1ULL * nr_subgrids * sizeof(Metadata);

    // Initialize metadata
    Metadata metadata[nr_subgrids];
    for (auto bl = 0; bl < nr_baselines; bl++) {
        for (auto ts = 0; ts < nr_timeslots; ts++ ) {
            auto idx = bl * nr_timeslots + ts;

            // Metadata settings
            int baseline_offset = bl;
            int time_offset = 0;
            int aterm_index = 0; // unused
            Baseline baseline = { 0, 0 }; // unused
            Coordinate coordinate = { 0, 0 }; // unused

            // Set metadata for current subgrid
            Metadata m = { baseline_offset, time_offset, nr_timesteps, aterm_index, baseline, coordinate };
            metadata[bl * nr_timeslots + ts] = m;
        }
    }

    // Allocate device memory
    cu::DeviceMemory d_uvw(sizeof_uvw);
    cu::DeviceMemory d_wavenumbers(sizeof_wavenumbers);
    cu::DeviceMemory d_visibilities(sizeof_visibilities);
    cu::DeviceMemory d_spheroidal(sizeof_spheroidal);
    cu::DeviceMemory d_aterms(sizeof_aterms);
    cu::DeviceMemory d_metadata(sizeof_metadata);
    cu::DeviceMemory d_subgrids(sizeof_subgrids);

    // Copy metadata to device
    stream.memcpyHtoDAsync(d_metadata, metadata, sizeof_metadata);

    // Setup parameters
    const void *parameters_degridder[] = {
        &grid_size, &subgrid_size, &image_size, &w_step, &nr_channels, &nr_stations,
        d_uvw, d_wavenumbers, d_visibilities,
        d_spheroidal, d_aterms, d_metadata, d_subgrids };

    const void *parameters_degridder_pre[] = {
        &subgrid_size, &nr_stations,
        d_spheroidal, d_aterms, d_metadata, d_subgrids };

    // Kernel dimensions
    dim3 gridDim(nr_subgrids);
    dim3 blockDim(128); // this has to match value in kernel

    // Run kernel degridder_pre
    auto gflops_pre = 1e-9 * flops_gridder_post(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    auto gbytes_pre = 1e-9 * bytes_gridder_post(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    run_kernel("idg_degridder_pre", stream, function_degridder_pre, gridDim, blockDim, parameters_degridder_pre, gflops_pre, gbytes_pre, 0);

    // Run kernel degridder
    auto gflops = 1e-9 * flops_gridder(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    auto gbytes = 1e-9 * bytes_gridder(nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    run_kernel("idg_degridder", stream, function_degridder, gridDim, blockDim, parameters_degridder, gflops, gbytes, mvis);

    // Run kernel degridder_full
    auto gflops_full = gflops + gflops_pre;
    auto gbytes_full = gbytes + gbytes_pre;
    run_kernel("idg_degridder_full", stream, function_degridder_full, gridDim, blockDim, parameters_degridder, gflops_full, gbytes_full, mvis);

    // Run kernel degridder_03
    int current_nr_channels;

    // 1 channel
    current_nr_channels = 1;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_1, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 2 channels
    current_nr_channels = 2;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_2, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 3 channels
    current_nr_channels = 3;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_3, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 4 channels
    current_nr_channels = 4;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_4, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 5 channels
    current_nr_channels = 5;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_5, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 6 channels
    current_nr_channels = 6;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_6, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 7 channels
    current_nr_channels = 7;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_7, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // 8 channels
    current_nr_channels = 8;
    parameters_degridder[4] = &current_nr_channels;
    gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
    mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
    printf("nr_channels=%2d ", current_nr_channels);
    run_kernel("idg_degridder_03", stream, function_degridder_03_8, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);

    // more channels
    for (current_nr_channels = 9; current_nr_channels <= 16; current_nr_channels++) {
        printf("nr_channels=%2d ", current_nr_channels);
        parameters_degridder[4] = &current_nr_channels;
        gflops = 1e-9 * flops_gridder(current_nr_channels, total_nr_timesteps, nr_subgrids, subgrid_size, nr_correlations);
        mvis   = 1e-6 * total_nr_timesteps * current_nr_channels;
        run_kernel("idg_degridder_03", stream, function_degridder_03_n, gridDim, blockDim, parameters_degridder, gflops, 0, mvis);
    }
}


int main() {
    // Read device number from envirionment
    char *cstr_deviceNumber = getenv("CUDA_DEVICE");
    unsigned deviceNumber = cstr_deviceNumber ? atoi (cstr_deviceNumber) : 0;

    // Setup CUDA
    cu::init();
    cu::Device device(deviceNumber);
    cu::Context context(device);
    context.setCurrent();
    cu::Stream stream;

    // Print CUDA device information
    std::cout << "Device " << deviceNumber << ": " << device.get_name() << std::endl;
    std::cout << "Compute capability: " << device.get_capability() << std::endl;

    // Initialized powerSensor
    powerSensor = powersensor::get_power_sensor();

    // Run benchmark
    cuProfilerStart();
    for (int i = 0; i < NR_BENCHMARKS; i++) {
        run_peak_flops(device, stream);
        run_idg_gridder(device, stream);
        run_idg_degridder(device, stream);
    }
    cuProfilerStop();

    // Remove powerSensor
    delete powerSensor;

    return EXIT_SUCCESS;
}
