#include "CU.h"

#include <sstream>
#include <cstring>
#include <stdexcept>
#include <cassert>

#include <vector_types.h>

#define assertCudaCall(val) __assertCudaCall(val, #val, __FILE__, __LINE__)
#define checkCudaCall(val)  __checkCudaCall(val, #val, __FILE__, __LINE__)

namespace cu {

    /*
        Error checking
    */
    inline void __assertCudaCall(
        CUresult result,
        char const *const func,
        const char *const file,
        int const line)
    {
        if (result != CUDA_SUCCESS) {
            const char *msg;
            cuGetErrorString(result, &msg);
            std::cerr << "CUDA Error at " << file;
            std::cerr << ":" << line;
            std::cerr << " in function " << func;
            std::cerr << ": " << msg;
            std::cerr << std::endl;
            throw Error<CUresult>(result);
        }
    }

    inline void __checkCudaCall(
        CUresult result,
        char const *const func,
        const char *const file,
        int const line)
    {
        try {
            __assertCudaCall(result, func, file, line);
        } catch (Error<CUresult>& error) {
            // pass
        }
    }


    /*
        Init
    */
    void init(unsigned flags) {
        assertCudaCall(cuInit(flags));
    }


    /*
        Class Device
    */
    int Device::getCount() {
        int nrDevices;
        assertCudaCall(cuDeviceGetCount(&nrDevices));
        return nrDevices;
    }

    Device::Device(int ordinal) {
        assertCudaCall(cuDeviceGet(&_device, ordinal));
    }

    std::string Device::get_name() const {
        char name[64];
        assertCudaCall(cuDeviceGetName(name, sizeof(name), _device));
        return std::string(name);
    }

    int Device::get_capability() const {
        int capability = 10 * get_attribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR>() +
                              get_attribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR>();
        return capability;
    }

    Device::operator CUdevice() {
        return _device;
    }


    /*
        Class Context
    */
    Context::Context() {
        _context = NULL;
    }

    Context::Context(Device& device, int flags) {
        _device = device;
        assertCudaCall(cuCtxCreate(&_context, flags, device));
    }

    Context::~Context() {
        assertCudaCall(cuCtxDestroy(_context));
    }

    void Context::setCurrent() const {
        assertCudaCall(cuCtxSetCurrent(_context));
    }

    void Context::synchronize() {
        assertCudaCall(cuCtxSynchronize());
    }

    void Context::reset() {
        assertCudaCall(cuDevicePrimaryCtxReset(_device));
    }

    Context::operator CUcontext() {
        return _context;
    }


    /*
        DeviceMemory
    */

    const CUdeviceptr DeviceMemory::_nullptr;

    DeviceMemory::DeviceMemory(size_t size) {
        _capacity = size;
        _size = size;
        if (size)
        {
            assertCudaCall(cuMemAlloc(&_ptr, size));
        }
    }

    DeviceMemory::~DeviceMemory() {
        if (_capacity) assertCudaCall(cuMemFree(_ptr));
    }

    size_t DeviceMemory::capacity() {
        return _capacity;
    }

    size_t DeviceMemory::size() {
        return _size;
    }


    /*
       Module
    */
    Module::Module(const char *file_name) {
        assertCudaCall(cuModuleLoad(&_module, file_name));
    }

    Module::~Module() {
        assertCudaCall(cuModuleUnload(_module));
    }

    Module::operator CUmodule() {
        return _module;
    }


    /*
        Function
    */
    Function::Function(Module &module, const char *name) {
        assertCudaCall(cuModuleGetFunction(&_function, module, name));
    }

    Function::Function(CUfunction function) {
        _function = function;
    }

    int Function::get_attribute(CUfunction_attribute attribute) {
        int value;
        assertCudaCall(cuFuncGetAttribute(&value, attribute, _function));
        return value;
    }

    Function::operator CUfunction() {
        return _function;
    }


    /*
        Event
    */
    Event::Event(int flags) {
        assertCudaCall(cuEventCreate(&_event, flags));
    }

    Event::~Event() {
        assertCudaCall(cuEventDestroy(_event));
    }

    void Event::synchronize() {
        assertCudaCall(cuEventSynchronize(_event));
    }

    float Event::elapsedTime(Event &second) {
        float ms;
        assertCudaCall(cuEventElapsedTime(&ms, second, _event));
        return ms;
    }

    Event::operator CUevent() {
        return _event;
    }


    /*
        Stream
    */
    Stream::Stream(int flags) {
        assertCudaCall(cuStreamCreate(&_stream, flags));
    }

    Stream::~Stream() {
        assertCudaCall(cuStreamDestroy(_stream));
    }

    void Stream::memcpyHtoDAsync(CUdeviceptr devPtr, const void *hostPtr, size_t size) {
        assertCudaCall(cuMemcpyHtoDAsync(devPtr, hostPtr, size, _stream));
    }

    void Stream::memcpyDtoHAsync(void *hostPtr, CUdeviceptr devPtr, size_t size) {
        assertCudaCall(cuMemcpyDtoHAsync(hostPtr, devPtr, size, _stream));
    }

    void Stream::launchKernel(Function &function, dim3 grid, dim3 block, unsigned sharedMemBytes, const void **parameters) {
        assertCudaCall(cuLaunchKernel(
            function, grid.x, grid.y, grid.z, block.x, block.y, block.z,
            sharedMemBytes, _stream, const_cast<void **>(parameters), 0));
    }

    void Stream::synchronize() {
        assertCudaCall(cuStreamSynchronize(_stream));
    }

    void Stream::waitEvent(Event &event) {
        assertCudaCall(cuStreamWaitEvent(_stream, event, 0));
    }

    void Stream::addCallback(CUstreamCallback callback, void *userData, int flags) {
        assertCudaCall(cuStreamAddCallback(_stream, callback, userData, flags));
    }

    void Stream::record(Event &event) {
        assertCudaCall(cuEventRecord(event, _stream));
    }

    Stream::operator CUstream() {
        return _stream;
    }

} // end namespace cu
