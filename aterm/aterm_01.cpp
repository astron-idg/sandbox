#include "aterm.h"

template <typename T> inline void matmul(
    const T *a,
    const T *b,
          T *c)
{
    c[0]  = a[0] * b[0];
    c[1]  = a[0] * b[1];
    c[2]  = a[2] * b[0];
    c[3]  = a[2] * b[1];
    c[0] += a[1] * b[2];
    c[1] += a[1] * b[3];
    c[2] += a[3] * b[2];
    c[3] += a[3] * b[3];
}

template <typename T> inline void conjugate(
    const T *a,
          T *b)
{
    float s[8] = {1, -1, 1, -1, 1, -1, 1, -1};
    float *a_ptr = (float *) a;
    float *b_ptr = (float *) b;

    for (unsigned i = 0; i < 8; i++) {
        b_ptr[i] = s[i] * a_ptr[i];
    }
}

template <typename T> inline void transpose(
    const T *a,
          T *b)
{
    b[0] = a[0];
    b[1] = a[2];
    b[2] = a[1];
    b[3] = a[3];
}

template <typename T> inline void hermitian(
    const T *a,
          T *b)
{
    T temp[4];
    conjugate(a, temp);
    transpose(temp, b);
}

template <typename T> inline void apply_aterm_gridder(
          T *pixels,
    const T *aterm1,
    const T *aterm2)
{
    // Aterm 1 hermitian
    T aterm1_h[4];
    hermitian(aterm1, aterm1_h);

    // Apply aterm: P = A1^H * P
    T temp[4];
    matmul(aterm1_h, pixels, temp);

    // Apply aterm: P = P * A2
    matmul(temp, aterm2, pixels);
}

template <typename T> inline void apply_aterm_degridder(
          T *pixels,
    const T *aterm1,
    const T *aterm2)
{
    // Apply aterm: P = A1 * P
    T temp[4];
    matmul(aterm1, pixels, temp);

    // Aterm 2 hermitian
    T aterm2_h[4];
    hermitian(aterm2, aterm2_h);

    // Apply aterm: P = P * A2^H
    matmul(temp, aterm2_h, pixels);
}

void kernel_gridder_01(
    idg::float2 *pixels,
    idg::float2 *aterms)
{
    idg::float2 *aterm1_ptr = (idg::float2 *) &aterms[0];
    idg::float2 *aterm2_ptr = (idg::float2 *) &aterms[4];
    apply_aterm_gridder(pixels, aterm1_ptr, aterm2_ptr);
}

void kernel_degridder_01(
    idg::float2 *pixels,
    idg::float2 *aterms)
{
    idg::float2 *aterm1_ptr = (idg::float2 *) &aterms[0];
    idg::float2 *aterm2_ptr = (idg::float2 *) &aterms[4];
    apply_aterm_degridder(pixels, aterm1_ptr, aterm2_ptr);
}
