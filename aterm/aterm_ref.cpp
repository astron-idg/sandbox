#include "aterm.h"

template <typename T> inline void apply_aterm(
    const T aXX1, const T aXY1,
    const T aYX1, const T aYY1,
    const T aXX2, const T aXY2,
    const T aYX2, const T aYY2,
    T pixels[NR_POLARIZATIONS])
{
    T pixelsXX = pixels[0];
    T pixelsXY = pixels[1];
    T pixelsYX = pixels[2];
    T pixelsYY = pixels[3];

    // Apply aterm to subgrid: P = A1 * P
    // [ pixels[0], pixels[1];  = [ aXX1, aXY1;  [ pixelsXX, pixelsXY;
    //   pixels[2], pixels[3] ]     aYX1, aYY1 ]   pixelsYX], pixelsYY ] *
    pixels[0]  = (pixelsXX * aXX1);
    pixels[0] += (pixelsYX * aXY1);
    pixels[1]  = (pixelsXY * aXX1);
    pixels[1] += (pixelsYY * aXY1);
    pixels[2]  = (pixelsXX * aYX1);
    pixels[2] += (pixelsYX * aYY1);
    pixels[3]  = (pixelsXY * aYX1);
    pixels[3] += (pixelsYY * aYY1);

    pixelsXX = pixels[0];
    pixelsXY = pixels[1];
    pixelsYX = pixels[2];
    pixelsYY = pixels[3];

    // Apply aterm to subgrid: P = P * A2^H
    //    [ pixels[0], pixels[1];  =   [ pixelsXX, pixelsXY;  *  [ conj(aXX2), conj(aYX2);
    //      pixels[2], pixels[3] ]       pixelsYX, pixelsYY ]      conj(aXY2), conj(aYY2) ]
    pixels[0]  = (pixelsXX * conj(aXX2));
    pixels[0] += (pixelsXY * conj(aXY2));
    pixels[1]  = (pixelsXX * conj(aYX2));
    pixels[1] += (pixelsXY * conj(aYY2));
    pixels[2]  = (pixelsYX * conj(aXX2));
    pixels[2] += (pixelsYY * conj(aXY2));
    pixels[3]  = (pixelsYX * conj(aYX2));
    pixels[3] += (pixelsYY * conj(aYY2));
}

void kernel_gridder_ref(
    idg::float2 *pixels,
    idg::float2 *aterms)
{
    // Load a term for station1
    idg::float2 aXX1 = aterms[0];
    idg::float2 aXY1 = aterms[1];
    idg::float2 aYX1 = aterms[2];
    idg::float2 aYY1 = aterms[3];

    // Load aterm for station2
    idg::float2 aXX2 = aterms[4];
    idg::float2 aXY2 = aterms[5];
    idg::float2 aYX2 = aterms[6];
    idg::float2 aYY2 = aterms[7];

    // Apply aterm
    apply_aterm(
        conj(aXX1), conj(aYX1), conj(aXY1), conj(aYY1),
        conj(aXX2), conj(aYX2), conj(aXY2), conj(aYY2),
        pixels);
}

void kernel_degridder_ref(
    idg::float2 *pixels,
    idg::float2 *aterms)
{
    // Load a term for station1
    idg::float2 aXX1 = aterms[0];
    idg::float2 aXY1 = aterms[1];
    idg::float2 aYX1 = aterms[2];
    idg::float2 aYY1 = aterms[3];

    // Load aterm for station2
    idg::float2 aXX2 = aterms[4];
    idg::float2 aXY2 = aterms[5];
    idg::float2 aYX2 = aterms[6];
    idg::float2 aYY2 = aterms[7];

    // Apply aterm
    apply_aterm(
        aXX1, aXY1, aYX1, aYY1,
        aXX2, aXY2, aYX2, aYY2,
        pixels);
}
