#define NR_POLARIZATIONS 4

#include <complex>

namespace idg {
    typedef struct {float real; float imag; } float2;

    inline float2 operator*(const float2& x, const float2& y)
    {
        return {x.real*y.real - x.imag*y.imag,
                x.real*y.imag + x.imag*y.real};
    }

    inline float2 operator*(const float2& x, const float& a)
    {
        return {x.real*a, x.imag*a};
    }

    inline float2 operator*(const float& a, const float2& x)
    {
        return x*a;
    }

    inline float2 operator+(const float2& x, const float2& y)
    {
        return {x.real + y.real, x.imag + y.imag};
    }

    inline float2 operator-(const float2& x, const float2& y)
    {
        return {x.real - y.real, x.imag - y.imag};
    }

    inline void operator+=(float2& x, const float2& y) {
        x.real += y.real;
        x.imag += y.imag;
    }

    inline float2 conj(const float2& x) {
        return {x.real, -x.imag};
    }
}

void kernel_gridder_ref(idg::float2 *pixels, idg::float2 *aterms);
void kernel_degridder_ref(idg::float2 *pixels, idg::float2 *aterms);

void kernel_gridder_01(idg::float2 *pixels, idg::float2 *aterms);
void kernel_degridder_01(idg::float2 *pixels, idg::float2 *aterms);
