#include <iostream>
#include <ostream>
#include <cstring>

#include "aterm.h"

using namespace std;

namespace idg {
    std::ostream& operator<<(std::ostream& os, const float2& x);
    
    ostream& operator<<(ostream& os, const float2& x)
    {
        os << "(" << x.real << "," << x.imag << ")";
        return os;
    }
}

void init_pixels(
    idg::float2 *pixels)
{
    pixels[0] = {2.0, 0.1};
    pixels[1] = {3.0, 0.2};
    pixels[2] = {4.0, 0.3};
    pixels[3] = {5.0, 0.4};
}

void init_aterms(
    idg::float2 *aterms)
{
    memset(aterms, 0, 2*NR_POLARIZATIONS*sizeof(idg::float2));
    aterms[0] = {2.0, 0.1};
    aterms[1] = {3.0, 0.2};
    aterms[2] = {4.0, 0.3};
    aterms[3] = {5.0, 0.4};
    aterms[4] = {6.0, 0.5};
    aterms[5] = {7.0, 0.6};
    aterms[6] = {8.0, 0.7};
    aterms[7] = {9.0, 0.8};
}

void print_pixels(
    idg::float2 *pixels_ref,
    idg::float2 *pixels_opt)
{
    for (int i = 0; i < NR_POLARIZATIONS; i++) {
        idg::float2 pixel_ref = pixels_ref[i];
        idg::float2 pixel_opt = pixels_opt[i];
        cout << pixel_ref;
        cout << " - ";
        cout << pixel_opt;
        cout << " = ";
        cout << pixel_ref - pixel_opt;
        cout << endl;
    }
}

int main(int argc, char **argv) {
    // Initialize aterms
    idg::float2 aterms[8];
    init_aterms(aterms);

    // Initialize pixels
    idg::float2 pixels_ref[NR_POLARIZATIONS];    
    idg::float2 pixels_opt[NR_POLARIZATIONS];    
    init_pixels(pixels_ref);
    init_pixels(pixels_opt);

    // Run kernels
    kernel_gridder_ref(pixels_ref, aterms);
    kernel_gridder_01(pixels_opt, aterms);
    kernel_degridder_ref(pixels_ref, aterms);
    kernel_degridder_01(pixels_opt, aterms);

    // Print pixels
    print_pixels(pixels_ref, pixels_opt);
}
